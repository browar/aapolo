﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class PopUpService : IPopUpService
    {
        private readonly IBaseRepository<tpopup> _repository;

        public PopUpService(IBaseRepository<tpopup> repository)
        {
            _repository = repository;
        }

        public async Task<int> CambiarActividad(int id)
        {
            var popUpAModificar = GetById(id);
            var bool_activo = popUpAModificar.bool_activo;
            await DesactivarTodos();
            popUpAModificar.bool_activo = !bool_activo;
            _repository.Update(popUpAModificar);
            await _repository.SaveChangesAsync();
            return 1;
        }

        private async Task DesactivarTodos()
        {
            var popUps = (await _repository.GetAll()).ToList();

            foreach (var item in popUps)
            {
                item.bool_activo = false;
                _repository.Update(item);
            }

            await _repository.SaveChangesAsync();
        }

        public async Task<List<tpopup>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public tpopup GetById(int id)
        {
            return _repository.GetById(id);
        }

        public async Task<tpopup> GetPopUpActivo()
        {
            return (await _repository.GetByCondition(x=> x.bool_activo));
        }

        public async Task<int> Save(tpopup popUp)
        {
            popUp.bool_activo = false;
            _repository.Add(popUp);
            await _repository.SaveChangesAsync();
            return 1;
        }

        public async Task<int> Update(tpopup popUp)
        {
            var popUpAModificar = GetById(popUp.cod_popup);
            popUpAModificar = PopulatePopUp(popUpAModificar, popUp);
            _repository.Update(popUpAModificar);
            await _repository.SaveChangesAsync();
            return 1;
        }

        private tpopup PopulatePopUp(tpopup popUpAModificar, tpopup popup)
        {
            popUpAModificar.fec_vigencia = popup.fec_vigencia;
            popUpAModificar.txt_desc = popup.txt_desc;
            popUpAModificar.txt_imagen = popup.txt_imagen;
            popUpAModificar.txt_video = popup.txt_video;
            return popUpAModificar;

        }
    }
}
