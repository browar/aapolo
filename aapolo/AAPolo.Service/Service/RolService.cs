﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class RolService : IRolService
    {
        private readonly IBaseRepository<trol> _repository;

        public RolService(IBaseRepository<trol> repository)
        {
            _repository = repository;
        }

        public async Task<List<trol>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public trol GetById(int id)
        {
            return _repository.GetById(id);
        }
    }
}
