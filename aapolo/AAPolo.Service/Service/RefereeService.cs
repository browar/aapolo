﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;

namespace AAPolo.Service.Service
{
    public class RefereeService : IRefereeService
    {
        private readonly IBaseRepository<treferee> _repository;

        public RefereeService(IBaseRepository<treferee> repository)
        {
            _repository = repository;
        }

        public async Task<List<treferee>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public treferee GetById(int cod_referee)
        {
            return _repository.GetById(cod_referee);
        }
    }
}
