﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TipoNotificacionService : ITipoNotificacionService
    {
        private readonly IBaseRepository<ttipo_notificacion> _repository;

        public TipoNotificacionService(IBaseRepository<ttipo_notificacion> repository)
        {
            _repository = repository;
        }

        public async Task<List<ttipo_notificacion>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public ttipo_notificacion GetById(int id)
        {
            return _repository.GetById(id);
        }
    }
}
