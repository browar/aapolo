﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IPopUpService
    {
        Task<List<tpopup>> GetAll();
        Task<int> Save(tpopup popUp);
        Task<int> Update(tpopup popUp);
        Task<int> CambiarActividad(int id);
        tpopup GetById(int id);
        Task<tpopup> GetPopUpActivo();
    }
}
