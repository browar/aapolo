﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IRefereeService
    {
        Task<List<treferee>> GetAll();
        treferee GetById(int cod_referee);
    }
}
