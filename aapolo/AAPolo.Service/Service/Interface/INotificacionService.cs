﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface INotificacionService
    {
        Task<List<tnotificacion>> GetAll();
        Task<List<tnotificacion>> GetNotificacionesRecibidas();
        tnotificacion GetById(int id);
        Task<int> Save(tnotificacion tnotificacion);
        Task<int> Update(tnotificacion tnotificacion);
        Task<int> Delete(tnotificacion tnotificacion);
    }
}
