﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITipoNotificacionService
    {
        Task<List<ttipo_notificacion>> GetAll();
        ttipo_notificacion GetById(int id);
    }
}
