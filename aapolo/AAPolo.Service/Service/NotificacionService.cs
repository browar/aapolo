﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class NotificacionService : INotificacionService
    {
        private readonly IBaseRepository<tnotificacion> _repository;
        private readonly ITipoNotificacionService tipoNotificacionService;
        private readonly ITipoJugadorService tipoJugadorService;
        private readonly ICategoriaHandicapService categoriaHandicapService;
        private readonly IRolService rolService;
        private readonly IBaseRepository<tnotificacion_rol> notificacionRolRepository;
        private readonly IBaseRepository<tnotificacion_tipo_jugador> notificacionTipoJugadorRepository;

        public NotificacionService(IBaseRepository<tnotificacion> repository, ITipoNotificacionService _tipoNotificacionService,
            IRolService _rolService, ITipoJugadorService _tipoJugadorService, IBaseRepository<tnotificacion_rol> _notificacionRolRepository
            , IBaseRepository<tnotificacion_tipo_jugador> _notificacionTipoJugadorRepository, ICategoriaHandicapService _categoriaHandicapService)
        {
            tipoNotificacionService = _tipoNotificacionService;
            rolService = _rolService;
            tipoJugadorService = _tipoJugadorService;
            notificacionRolRepository = _notificacionRolRepository;
            categoriaHandicapService = _categoriaHandicapService;
            notificacionTipoJugadorRepository = _notificacionTipoJugadorRepository;
            _repository = repository;
        }

        public async Task<List<tnotificacion>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public tnotificacion GetById(int id)
        {
            return _repository.GetById(id);
        }

        public async Task<List<tnotificacion>> GetNotificacionesRecibidas()
        {
            var notificaciones = (await _repository.GetMany(x => x.fec_enviado <= DateTime.Now)).ToList();
            return notificaciones;
        }

        public async Task<int> Save(tnotificacion tnotificacion)
        {
            var notificacionAGuardar = new tnotificacion();
            var nuevaTnotificacion = PopulateNotificacion(notificacionAGuardar, tnotificacion);
            _repository.Add(nuevaTnotificacion);
            await _repository.SaveChangesAsync();

            nuevaTnotificacion = PopulateTypesNotificacion(nuevaTnotificacion, tnotificacion);

            _repository.Update(nuevaTnotificacion);
            await _repository.SaveChangesAsync();

            return 1;
        }

        public async Task DeleteTypesNotification(tnotificacion notificacion)
        {
            var roles = notificacion.tnotificacion_rol.Count;
            var rolesABorrar = notificacion.tnotificacion_rol.ToList() ;
            for (var i = 0; i < roles; i++)
            {
                notificacionRolRepository.Delete(rolesABorrar[i]);
            }

            var tipos = notificacion.tnotificacion_tipo_jugador.Count;
            var tiposABorrar = notificacion.tnotificacion_tipo_jugador.ToList();
            for (var i = 0; i < tipos; i++)
            {
                notificacionTipoJugadorRepository.Delete(tiposABorrar[i]);
            }

            await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(tnotificacion tnotificacion)
        {
            try
            {
                var notificacionAGuardar = _repository.GetById(tnotificacion.cod_notificacion);
                var nuevaTnotificacion = PopulateNotificacion(notificacionAGuardar, tnotificacion);
                _repository.Update(nuevaTnotificacion);
                await _repository.SaveChangesAsync();

                await DeleteTypesNotification(notificacionAGuardar);
                nuevaTnotificacion = PopulateTypesNotificacion(nuevaTnotificacion, tnotificacion);

                _repository.Update(nuevaTnotificacion);
                await _repository.SaveChangesAsync();

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<int> Delete(tnotificacion tnotificacion)
        {
            throw new NotImplementedException();
        }

        private tnotificacion PopulateNotificacion(tnotificacion notificacionAModificar, tnotificacion notificacion)
        {
            notificacionAModificar.txt_alta = notificacion.txt_alta;
            notificacionAModificar.txt_asunto = notificacion.txt_asunto;
            notificacionAModificar.txt_desc = notificacion.txt_desc;
            notificacionAModificar.bool_destacado = notificacion.bool_destacado;
            notificacionAModificar.cod_enviado_por = notificacion.cod_enviado_por;
            notificacionAModificar.fec_enviado = notificacion.fec_enviado;
            notificacionAModificar.nro_handicap_maximo = notificacion.nro_handicap_maximo;
            notificacionAModificar.nro_handicap_minimo = notificacion.nro_handicap_minimo;
            notificacionAModificar.cod_tipo_notificacion = notificacion.cod_tipo_notificacion;
            notificacionAModificar.ttipo_notificacion = tipoNotificacionService.GetById(notificacion.cod_tipo_notificacion);

            return notificacionAModificar;
        }

        private tnotificacion PopulateTypesNotificacion(tnotificacion notificacionAModificar, tnotificacion notificacion)
        {
            var listaTipoJugador = new List<tnotificacion_tipo_jugador>();
            foreach (var notificacionJugador in notificacion.tnotificacion_tipo_jugador)
            {
                var notificacionTipo = new tnotificacion_tipo_jugador();
                notificacionTipo.cod_categoria_handicap = notificacionJugador.cod_categoria_handicap;
                notificacionTipo.cod_notificacion = notificacionAModificar.cod_notificacion;
                notificacionTipo.cod_tipo_jugador = notificacionJugador.cod_tipo_jugador;
                notificacionTipo.ttipo_jugador = tipoJugadorService.GetById(notificacionJugador.cod_tipo_jugador.Value);
                notificacionTipo.tcategoria_handicap = categoriaHandicapService.GetById(notificacionJugador.cod_categoria_handicap.Value);
                notificacionTipo.tnotificacion = notificacionAModificar;
                listaTipoJugador.Add(notificacionTipo);
            }
            var listaRol = new List<tnotificacion_rol>();
            notificacionAModificar.tnotificacion_tipo_jugador = listaTipoJugador;

            foreach (var notificacionRol in notificacion.tnotificacion_rol)
            {
                var rol = new tnotificacion_rol();
                rol.cod_notificacion = notificacionAModificar.cod_notificacion;
                rol.tnotificacion = notificacionAModificar;
                rol.cod_rol = notificacionRol.cod_rol;
                rol.trol = rolService.GetById(notificacionRol.cod_rol.Value);
                listaRol.Add(notificacionRol);
            }
            notificacionAModificar.tnotificacion_rol = listaRol;

            return notificacionAModificar;
        }
    }
}
