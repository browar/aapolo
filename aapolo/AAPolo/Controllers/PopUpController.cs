﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class PopUpController : ApiController
    {
        private readonly IPopUpService popUpService;
        IMapper Mapper;

        public PopUpController(IPopUpService _popUpService)
        {
            popUpService = _popUpService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        // GET: api/PopUp
        public async Task<IHttpActionResult> Get()
        {
            var popUpList = Mapper.Map<List<tpopupDTO>>(await popUpService.GetAll());
            return Ok(popUpList);
        }

        // GET: api/PopUp/5
        public IHttpActionResult Get(int id)
        {
            var popUp = Mapper.Map<tpopupDTO>(popUpService.GetById(id));
            return Ok(popUp);
        }

        // GET: api/PopUp/5
        public async Task<IHttpActionResult> GetPopUpActivo()
        {
            var popUp = Mapper.Map<tpopupDTO>(await popUpService.GetPopUpActivo());
            return Ok(popUp);
        }

        public async Task<IHttpActionResult> Post([FromBody]string popUpDTO)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var popUp = JsonConvert.DeserializeObject<tpopupDTO>(popUpDTO, dateTimeConverter);
            var popupAGuardar = Mapper.Map<tpopup>(popUp);
            if ((await popUpService.Save(popupAGuardar)) == 1)
            {
                return Ok("Se ha creado correctamente el popup");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo agregarse el popup"));
            }
        }

        public async Task<IHttpActionResult> Put(int id, [FromBody]string popUpDTO)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var popup = JsonConvert.DeserializeObject<tpopupDTO>(popUpDTO, dateTimeConverter);
            var popUpAModificar = Mapper.Map<tpopup>(popup);
            if ((await popUpService.Update(popUpAModificar)) == 1)
            {
                return Ok("Se ha modificado correctamente el popup");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo modificarse el popup"));
            }

        }
        // DELETE: api/PopUp/5
        public async Task<IHttpActionResult> Delete(int id)
        {
            if ((await popUpService.CambiarActividad(id)) == 1)
            {
                return Ok("Se ha modificado correctamente el popup");
            }
            else
            {
                return InternalServerError(new Exception("No se pudo cambiar la actividad"));
            }

        }
    }
}
