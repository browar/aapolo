﻿using AAPolo.Entity.DTO;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AAPolo.Controllers
{
    [EnableCors(origins: "http://localhost:56596", headers: "*", methods: "*", SupportsCredentials = true)]
    public class JugadoresSelectController : ApiController
    {
        private readonly IUsuarioService usuarioService;
        private readonly IClubService clubService;
        IMapper Mapper;

        public JugadoresSelectController(IUsuarioService _usuarioService, IClubService _clubService)
        {
            usuarioService = _usuarioService;
            clubService = _clubService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        public async Task<IHttpActionResult> Get()
        {
            var result = new
            {
                users = Mapper.Map<List<usuarioSelectDTO>>(await usuarioService.GetAll())
            };
            return Ok(result);
        }
    }
}
