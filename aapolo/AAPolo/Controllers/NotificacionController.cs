﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class NotificacionController : ApiController
    {
        private readonly INotificacionService notificacionService;
        IMapper Mapper;

        public NotificacionController(INotificacionService _notificacionService)
        {
            notificacionService = _notificacionService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        // GET: api/Notificacion
        public async Task<IHttpActionResult> Get()
        {
            var notificaciones = Mapper.Map<List<tnotificacionDTO>>(await notificacionService.GetAll());

            return Ok(notificaciones);
        }

        public async Task<IHttpActionResult> GetNotificacionesRecibidas()
        {
            var notificaciones = Mapper.Map<List<tnotificacionDTO>>(await notificacionService.GetNotificacionesRecibidas());

            return Ok(notificaciones);
        }

        // GET: api/Notificacion/5
        public IHttpActionResult Get(int id)
        {
            var notificacion = Mapper.Map<tnotificacionDTO>(notificacionService.GetById(id));
            return Ok(notificacion);
        }

        // POST: api/Notificacion
        public async Task<IHttpActionResult> Post([FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var listaNotificaciones = JsonConvert.DeserializeObject<tnotificacion>(data, dateTimeConverter);
            var notificacion = Mapper.Map<tnotificacion>(listaNotificaciones);
            await notificacionService.Save(notificacion);

            return Ok("Notificaciones dadas de alta");
        }

        // PUT: api/Notificacion/5
        public async Task<IHttpActionResult> Put(int id, [FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var listaNotificaciones = JsonConvert.DeserializeObject<tnotificacion>(data, dateTimeConverter);
            var notificacion = Mapper.Map<tnotificacion>(listaNotificaciones);
            await notificacionService.Update(notificacion);

            return Ok("Notificaciones modificadas");
        }

        // DELETE: api/Notificacion/5
        public void Delete(int id)
        {
        }
    }
}
