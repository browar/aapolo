﻿using AAPolo.Entity.DTO;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AAPolo.Controllers
{
    public class UsuarioClubController : ApiController
    {
        private readonly IUsuarioClubService usuarioClubService;
        private readonly IUsuarioService usuarioService;
        IMapper Mapper;

        public UsuarioClubController(IUsuarioClubService _usuarioClubService, IUsuarioService _usuarioService)
        {
            usuarioClubService = _usuarioClubService;
            usuarioService = _usuarioService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        public async Task<IHttpActionResult> Get(int? clubId)
        {
            var usersDTO = Mapper.Map<List<JugadoresDTO>>(await usuarioService.GetByClub(clubId.Value));
            return Ok(usersDTO);
        }

        [HttpGet]
        public async Task<IHttpActionResult> AceptarRechazar(int? idUsuario, bool opcion)
        {
            await usuarioClubService.AceptarRechazar(idUsuario.Value, opcion);
            return Ok("Ok");
        }


        public IHttpActionResult Get()
        {
            throw new NotImplementedException();
        }

        public IHttpActionResult Post([FromBody]string data)
        {
            throw new NotImplementedException();
        }

        public IHttpActionResult Put(int id, [FromBody]string data)
        {

            throw new NotImplementedException();

        }
        public IHttpActionResult Delete(int cod_user)
        {
            throw new NotImplementedException();
        }
    }
}
