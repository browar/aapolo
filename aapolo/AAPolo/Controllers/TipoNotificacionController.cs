﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class TipoNotificacionController : ApiController
    {
        private readonly ITipoJugadorService tipoJugadorService;
        private readonly ICategoriaHandicapService categoriaHandicapService;
        private readonly IRolService rolService;
        IMapper Mapper;

        public TipoNotificacionController(ITipoJugadorService _tipoJugadorService, ICategoriaHandicapService _categoriaHandicapService,
            IRolService _rolService)
        {
            tipoJugadorService = _tipoJugadorService;
            categoriaHandicapService = _categoriaHandicapService;
            rolService = _rolService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }
        // GET: api/TipoNotificacion
        public async Task<IHttpActionResult> Get()
        {
            var result = new
            {
                tiposJugadores = Mapper.Map<List<ttipo_jugadorDTO>>(await tipoJugadorService.GetAll()),
                categorias = Mapper.Map<List<tcategoria_handicapDTO>>(await categoriaHandicapService.GetAll()),
                roles = Mapper.Map<List<trolDTO>>(await rolService.GetAll())
            };


            return Ok(result);
        }

        // GET: api/TipoNotificacion/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TipoNotificacion
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/TipoNotificacion/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TipoNotificacion/5
        public void Delete(int id)
        {
        }
    }
}
