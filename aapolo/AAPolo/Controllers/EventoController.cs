﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class EventoController : ApiController
    {
        private readonly IEventoService eventoService;
        private readonly ITipoEventoService tipoEventoService;
        private readonly SendMail sendMail;
        IMapper Mapper;

        public EventoController(IEventoService _eventoService, ITipoEventoService _tipoEventoService)
        {
            eventoService = _eventoService;
            tipoEventoService = _tipoEventoService;
            sendMail = new SendMail();
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        public async Task<IHttpActionResult> Get()
        {
            var evento = Mapper.Map<List<teventoDTO>>(await eventoService.GetAll());
            return Ok(evento);
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            var evento = new teventoDTO();
            if (id > 0)
            {
                evento = Mapper.Map<teventoDTO>(eventoService.GetById(id));
            }
            var result = new
            {
                evento = evento,
                tipos = Mapper.Map<List<ttipo_eventoDTO>>(await tipoEventoService.GetAll())
            };
            return Ok(result);
        }

        public async Task<IHttpActionResult> Post([FromBody]string eventoDTO)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var evento = JsonConvert.DeserializeObject<teventoDTO>(eventoDTO, dateTimeConverter);
            var eventoAGuardar = Mapper.Map<tevento>(evento);
            if ((await eventoService.Save(eventoAGuardar)) == 1)
            {
                sendMail.SendMailNotificacion("El usuario: " + evento.usuario_alta + " ha creado el evento " +
                        evento.txt_desc, "Se ha creado un evento");

                return Ok("Se ha creado correctamente el evento");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo agregarse el evento"));
            }
        }

        public async Task<IHttpActionResult> Put(int id, [FromBody]string eventoDTO)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var evento = JsonConvert.DeserializeObject<teventoDTO>(eventoDTO, dateTimeConverter);
            var eventoAModificar = Mapper.Map<tevento>(evento);
            if ((await eventoService.Update(eventoAModificar)) == 1)
            {
                return Ok("Se ha modificado correctamente el evento");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo modificarse el evento"));
            }

        }
        public async Task<IHttpActionResult> Delete(int id)
        {
            if ((await eventoService.Delete(id)) == 1)
            {
                return Ok("Se ha dado de baja correctamente el evento");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo darse de baja el club"));
            }
        }

    }
}
