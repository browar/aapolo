﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace AAPolo.Utils
{
    public class SendNotification
    {
        public void SendPushNotificationPolo(string platform, string message, string title, string userMobileId)
        {
            try
            {
                string applicationID = "AAAAFkHqy4E:APA91bGqkUs2aOKTmYKtq-gEjwBad4N-QtuNDGDQo9H--7-gkxlOKDTj5HUOMiEAQSA-h2vwsVBr8fPv6zc6_k7Qq3_oijzo8AvsLf7gF7n--qsn2xOGVUd8VH_4z3jk-KNOk3MkzYWv";//"AAAAFkHqy4E:APA91bGqkUs2aOKTmYKtq-gEjwBad4N-QtuNDGDQo9H--7-gkxlOKDTj5HUOMiEAQSA-h2vwsVBr8fPv6zc6_k7Qq3_oijzo8AvsLf7gF7n--qsn2xOGVUd8VH_4z3jk-KNOk3MkzYWv";
                //string senderId = "30............8";
                string deviceId = userMobileId;//"egZo-YthS8w:APA91bGm1-UONlkn_LQInnYx9A5fySf3BFv5t2gmgekKfkUII5kODJtXJ_93eI_OYGIhXizVGDDyFDvRqNBD7eI9nTAUiqFPENQh-NRXypdvDmB5hJlTivJo3Xtj9dg1sKr6FJRAZ7_6";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                object data;
                if (platform.ToUpper() == "ANDROID")
                {
                    data = new
                    {
                        to = deviceId,
                        data = new
                        {
                            body = message,// "Testeo Push",
                            title = title,// "Notificacion de ejemplo enviada desde la pc",
                            style = "inbox",
                            message = message,
                            sound = "default",
                            badge = 1,
                        }
                    };
                }

                else
                {
                    data = new
                    {
                        to = deviceId,
                        notification = new
                        {
                            body = message,// "Testeo Push",
                            title = title,// "Notificacion de ejemplo enviada desde la pc",
                            style = "inbox",
                            message = message,
                            sound = "default",
                            badge = 1,
                        }
                    };
                }
                
                var json = JsonConvert.SerializeObject(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                //tRequest.Headers.Add(string.Format("Content-Type: application/json"));
                //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }
    }
}