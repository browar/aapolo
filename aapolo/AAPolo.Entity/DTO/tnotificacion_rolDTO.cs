﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tnotificacion_rolDTO
    {
        public int cod_notificacion_rol { get; set; }
        public int cod_notificacion { get; set; }
        public int? cod_rol { get; set; }
    }
}
