﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tnotificacion_tipo_jugadorDTO
    {
        public int cod_notificacion_tipo_jugador { get; set; }
        public int cod_notificacion { get; set; }
        public int? cod_tipo_jugador { get; set; }
        public int? cod_categoria_handicap { get; set; }
    }
}
