﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class trefereeDTO
    {
        public int cod_referee { get; set; }
        public int cod_tipo_referee { get; set; }
        public int cod_usuario { get; set; }        
        public virtual usuarioSelectDTO users { get; set; }
    }
}
