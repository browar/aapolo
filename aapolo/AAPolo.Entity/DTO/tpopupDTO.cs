﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tpopupDTO
    {
        public int cod_popup { get; set; }
        public string txt_desc { get; set; }
        public DateTime? fec_vigencia { get; set; }
        public string txt_imagen { get; set; }
        public string txt_video { get; set; }
        public bool bool_activo { get; set; }
    }
}
