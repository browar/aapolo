﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tnotificacion_usuarioDTO
    {
        public int cod_notificacion_usuario { get; set; }
        public int cod_notificacion { get; set; }
        public int? cod_usuario { get; set; }
        public bool? bool_leido { get; set; }        
    }
}
