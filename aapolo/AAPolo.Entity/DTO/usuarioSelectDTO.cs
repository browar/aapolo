﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class usuarioSelectDTO
    {
        public int cod_usuario { get; set; }
        public string nro_doc { get; set; }
        public string txt_nombre { get; set; }
        public string txt_apellido { get; set; }
        public DateTime? fec_nacimiento { get; set; }
        public bool? bool_acepta_terminos { get; set; }
        public string txt_imagen { get; set; }
        public List<tcontacto_usuarioDTO> tcontacto_usuario { get; set; }
    }
}
