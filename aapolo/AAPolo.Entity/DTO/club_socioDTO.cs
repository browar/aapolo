﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class club_socioDTO
    {
        public int cod_jugador_club { get; set; }
        public int? cod_club { get; set; }
        public int? cod_usuario { get; set; }
        public bool bool_principal { get; set; }
        public usersDTO users { get; set; }
    }
}
