﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class NoticiaHomeDTO
    {
        public int cod_noticia { get; set; }
        public int? cod_tipo_noticia { get; set; }
        public string txt_titulo { get; set; }
        public string txt_copete { get; set; }
        public string txt_cuerpo { get; set; }
        public DateTime? fec_alta { get; set; }
        public int? bool_activo { get; set; }
        public int? bool_destacada { get; set; }
        public string url_imagen { get; set; }
        public string url_video { get; set; }
        public string url_slideshow { get; set; }
    }
}
