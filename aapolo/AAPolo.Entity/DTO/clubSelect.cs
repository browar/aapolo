﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class clubSelect
    {
        public int cod_club { get; set; }
        public int? cod_categoria_club { get; set; }
        public string txt_desc { get; set; }
        public string cod_club_desc { get; set; }
        public int? nro_canchas { get; set; }
        public int? cod_estado { get; set; }
        public int? cod_admin_club { get; set; }
        public DateTime? fec_ult_pago { get; set; }
    }
}
