namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tnotificacion_usuario
    {
        [Key]
        public int cod_notificacion_usuario { get; set; }

        public int cod_notificacion { get; set; }

        public int? cod_usuario { get; set; }

        public bool? bool_leido { get; set; }

        public virtual tnotificacion tnotificacion { get; set; }

        public virtual users users { get; set; }
    }
}
