namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tsede")]
    public partial class tsede
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tsede()
        {
            tcancha = new HashSet<tcancha>();
        }

        [Key]
        public int cod_sede { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        public int cod_domicilio { get; set; }

        public int cod_club { get; set; }

        public int? cod_tipo_sede { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tcancha> tcancha { get; set; }

        public virtual tclub tclub { get; set; }

        public virtual tdomicilio tdomicilio { get; set; }

        public virtual ttipo_sede ttipo_sede { get; set; }
    }
}
