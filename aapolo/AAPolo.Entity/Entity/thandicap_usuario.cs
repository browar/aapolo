namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class thandicap_usuario
    {
        [Key]
        public int cod_handicap_usuario { get; set; }

        public int cod_handicap { get; set; }

        public int cod_usuario { get; set; }

        public bool? bool_principal { get; set; }

        public int? nro_handicap { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fec_desde { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fec_hasta { get; set; }

        public virtual thandicap thandicap { get; set; }

        public virtual users users { get; set; }
    }
}
