namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tnotificacion_tipo_jugador
    {
        [Key]
        public int cod_notificacion_tipo_jugador { get; set; }

        public int cod_notificacion { get; set; }

        public int? cod_tipo_jugador { get; set; }

        public int? cod_categoria_handicap { get; set; }

        public virtual tcategoria_handicap tcategoria_handicap { get; set; }

        public virtual tnotificacion tnotificacion { get; set; }

        public virtual ttipo_jugador ttipo_jugador { get; set; }
    }
}
