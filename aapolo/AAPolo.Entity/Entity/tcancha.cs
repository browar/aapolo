namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tcancha")]
    public partial class tcancha
    {
        [Key]
        public int cod_cancha { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        public int cod_sede { get; set; }

        public virtual tsede tsede { get; set; }
    }
}
