namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class thandicap_usuario_historico
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int cod_handicap_usuario_historico { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cod_handicap_usuario { get; set; }

        public int cod_handicap { get; set; }

        public int cod_usuario { get; set; }

        public bool? bool_principal { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fec_ult_modificacion { get; set; }

        public int? nro_handicap { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fec_desde { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fec_hasta { get; set; }

        public virtual thandicap thandicap { get; set; }

        public virtual users users { get; set; }
    }
}
