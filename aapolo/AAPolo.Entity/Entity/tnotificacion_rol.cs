namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tnotificacion_rol
    {
        [Key]
        public int cod_notificacion_rol { get; set; }

        public int cod_notificacion { get; set; }

        public int? cod_rol { get; set; }

        public virtual tnotificacion tnotificacion { get; set; }

        public virtual trol trol { get; set; }
    }
}
