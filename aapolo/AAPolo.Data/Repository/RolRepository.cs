﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class RolRepository : BaseRepository<trol, IAAPoloContext>, IRolRepository
    {
        public RolRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
