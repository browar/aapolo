﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class RefereeRepository : BaseRepository<treferee, IAAPoloContext>, IRefereeRepository
    {
        public RefereeRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
