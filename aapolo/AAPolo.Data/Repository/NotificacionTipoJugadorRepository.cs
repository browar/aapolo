﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class NotificacionTipoJugadorRepository : BaseRepository<tnotificacion_tipo_jugador, IAAPoloContext>, INotificacionTipoJugadorRepository
    {
        public NotificacionTipoJugadorRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
