﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class NotificacionRolRepository : BaseRepository<tnotificacion_rol, IAAPoloContext>, INotificacionRolRepository
    {
        public NotificacionRolRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
