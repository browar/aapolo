﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class PaisService : IPaisService
    {
        private readonly IBaseRepository<tpais> _repository;

        public PaisService(IBaseRepository<tpais> repository)
        {
            _repository = repository;
        }

        public async Task<List<tpais>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(tpais pais)
        {
            _repository.Add(pais);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(tpais pais)
        {
            var paisAModificar = _repository.GetById(pais.cod_pais);
            paisAModificar.txt_desc = pais.txt_desc;
            _repository.Update(paisAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_pais)
        {
            var paisABorrar = _repository.GetById(cod_pais);
            _repository.Delete(paisABorrar);
            return await _repository.SaveChangesAsync();
        }

        public tpais GetById(int cod_pais)
        {
            return _repository.GetById(cod_pais);
        }
    }
}
