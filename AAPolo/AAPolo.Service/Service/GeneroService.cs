﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class GeneroService : IGeneroService
    {
        private readonly IBaseRepository<tgenero> _repository;

        public GeneroService(IBaseRepository<tgenero> repository)
        {
            _repository = repository;
        }

        public async Task<int> Delete(int cod_genero)
        {
            var generoABorrar = _repository.GetById(cod_genero);
            _repository.Delete(generoABorrar);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<tgenero>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public tgenero GetById(int cod_genero)
        {
            return _repository.GetById(cod_genero);
        }

        public async Task<int> Save(tgenero genero)
        {
            _repository.Add(genero);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(tgenero genero)
        {
            var generoAModificar = _repository.GetById(genero.cod_genero);
            generoAModificar.txt_desc = genero.txt_desc;
            _repository.Update(generoAModificar);
            return (await _repository.SaveChangesAsync());
        }
    }
}
