﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TipoContactoService : ITipoContactoService
    {
        private readonly IBaseRepository<ttipo_contacto> _repository;

        public TipoContactoService(IBaseRepository<ttipo_contacto> repository)
        {
            _repository = repository;
        }

        public async Task<List<ttipo_contacto>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(ttipo_contacto contacto)
        {
            _repository.Add(contacto);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(ttipo_contacto contacto)
        {
            var contactoAModificar = _repository.GetById(contacto.cod_tipo_contacto);
            contactoAModificar.txt_desc = contacto.txt_desc;
            _repository.Update(contactoAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_contacto)
        {
            var contactoABorrar = _repository.GetById(cod_contacto);
            _repository.Delete(contactoABorrar);
            return await _repository.SaveChangesAsync();
        }

        public ttipo_contacto GetById(int cod_contacto)
        {
            return _repository.GetById(cod_contacto);
        }
    }
}
