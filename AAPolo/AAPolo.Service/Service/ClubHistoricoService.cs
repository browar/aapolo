﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class ClubHistoricoService : IClubHistoricoService
    {
        private readonly IBaseRepository<tclub_historico> _repository;
        private readonly ICategoriaClubService categoriaClubService;
        private readonly IContactoService contactoService;
        private readonly IDomicilioService domicilioService;

        public ClubHistoricoService(IBaseRepository<tclub_historico> repository, ICategoriaClubService _categoriaClubService, 
            IContactoService _contactoService, IDomicilioService _domicilioService)
        {
            _repository = repository;
            categoriaClubService = _categoriaClubService;
            contactoService = _contactoService;
            domicilioService = _domicilioService;
        }

        public async Task<List<tclub_historico>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public void Save(tclub_historico clubHistorico)
        {
            clubHistorico.tdomicilio = domicilioService.GetById(Convert.ToInt32(clubHistorico.cod_domicilio));
            clubHistorico.tcategoria_club = categoriaClubService.GetById(Convert.ToInt32(clubHistorico.cod_categoria_club));
            _repository.Add(clubHistorico);
        }

        public tclub_historico GetById(int codHistorico)
        {
            return _repository.GetById(codHistorico);
        }
    }
}
