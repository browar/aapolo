﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class MunicipioService : IMunicipioService
    {
        private readonly IBaseRepository<tmunicipio> _repository;
        private readonly IProvinciaService provinciaService;

        public MunicipioService(IBaseRepository<tmunicipio> repository, IProvinciaService _provinciaService)
        {
            _repository = repository;
            provinciaService = _provinciaService;
        }

        public async Task<List<tmunicipio>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(tmunicipio municipio)
        {
            municipio.tprovincia = provinciaService.GetById(municipio.cod_provincia);
            _repository.Add(municipio);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(tmunicipio municipio)
        {
            var municipioAModificar = _repository.GetById(municipio.cod_municipio);
            municipioAModificar.txt_desc = municipio.txt_desc;
            municipioAModificar.cod_provincia = municipio.cod_provincia;
            municipioAModificar.tprovincia = provinciaService.GetById(municipio.cod_provincia);
            _repository.Update(municipioAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_municipio)
        {
            var municipioABorrar = _repository.GetById(cod_municipio);
            _repository.Delete(municipioABorrar);
            return await _repository.SaveChangesAsync();
        }

        public tmunicipio GetById(int cod_municipio)
        {
            return _repository.GetById(cod_municipio);
        }

        public async Task<List<tmunicipio>> GetByCodProvincia(int cod_porvincia)
        {
            return (await _repository.GetMany(x => x.cod_provincia == cod_porvincia)).ToList();
        }
    }
}
