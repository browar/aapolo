﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TipoDomicilioService : ITipoDomicilioService
    {
        private readonly IBaseRepository<ttipo_domicilio> _repository;

        public TipoDomicilioService(IBaseRepository<ttipo_domicilio> repository)
        {
            _repository = repository;
        }

        public async Task<List<ttipo_domicilio>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(ttipo_domicilio tipoDomicilio)
        {
            _repository.Add(tipoDomicilio);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(ttipo_domicilio tipoDomicilio)
        {
            var tipoDomicilioAModificar = _repository.GetById(tipoDomicilio.cod_tipo_domicilio);
            tipoDomicilioAModificar.txt_desc = tipoDomicilio.txt_desc;
            _repository.Update(tipoDomicilioAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_tipoDomicilio)
        {
            var tipoDomicilioABorrar = _repository.GetById(cod_tipoDomicilio);
            _repository.Delete(tipoDomicilioABorrar);
            return await _repository.SaveChangesAsync();
        }

        public ttipo_domicilio GetById(int cod_tipoDomicilio)
        {
            return _repository.GetById(cod_tipoDomicilio);
        }
    }
}
