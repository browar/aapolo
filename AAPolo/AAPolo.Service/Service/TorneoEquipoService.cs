﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TorneoEquipoService : ITorneoEquipoService
    {
        private readonly IBaseRepository<tequipo_user> _repository;
        private readonly IUsuarioService userService;
        private readonly IBaseRepository<tequipo> equipoService;
        private readonly IEstadoService estadoService;

        public TorneoEquipoService(IBaseRepository<tequipo_user> repository, IUsuarioService _userService,
            IBaseRepository<tequipo> _equipoService, IEstadoService _estadoService)
        {
            _repository = repository;
            userService = _userService;
            equipoService = _equipoService;
            estadoService = _estadoService;
        }

        public async Task<int> Delete(int cod_equipo_user)
        {
            var equipoDelete = _repository.GetById(cod_equipo_user);
            _repository.Delete(equipoDelete);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<tequipo_user>> GetByUser(int? cod_usuario)
        {
            if (cod_usuario.HasValue)
            {
                return (await _repository.GetMany(x => x.cod_usuario == cod_usuario)).ToList();
            }

            return null;
        }

        public async Task<List<tequipo_user>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public tequipo_user GetById(int cod_equipo_user)
        {
            return _repository.GetById(cod_equipo_user);
        }

        public async Task<int> Save(List<tequipo_user> equipousers)
        {
            foreach (var equipo in equipousers)
            {

                //torneoEquipo.tequipo = equipoService.GetById(torneoEquipo.cod_equipo);
                //torneoEquipo.testado = estadoService.GetById(torneoEquipo.cod_estado);

                _repository.Add(equipo);
            }
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(tequipo_user torneoEquipo)
        {
            var torneoAModificar = _repository.GetById(torneoEquipo.cod_equipo_jugadores);
            torneoAModificar.bool_capitan = torneoEquipo.bool_capitan;
            torneoAModificar.cod_equipo = torneoEquipo.cod_equipo;
            torneoAModificar.tequipo = equipoService.GetById(torneoEquipo.cod_equipo);
            torneoAModificar.cod_estado = torneoEquipo.cod_estado;
            torneoAModificar.testado = estadoService.GetById(torneoEquipo.cod_estado);
            torneoAModificar.bool_suplente = torneoEquipo.bool_suplente;
            torneoAModificar.cod_estado = torneoEquipo.cod_estado;
            torneoAModificar.testado = estadoService.GetById(torneoEquipo.cod_estado);
            _repository.Update(torneoAModificar);
            return (await _repository.SaveChangesAsync());
        }
    }
}
