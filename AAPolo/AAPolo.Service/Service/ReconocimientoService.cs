﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class ReconocimientoService : IReconocimientoService
    {
        private readonly IBaseRepository<treconocimiento> _repository;
        private readonly ITorneoService torneoService;

        public ReconocimientoService(IBaseRepository<treconocimiento> repository, ITorneoService _torneoService)
        {
            _repository = repository;
            torneoService = _torneoService;
        }

        public async Task<int> Delete(int cod_reconocimiento)
        {
            var reconocimiento = _repository.GetById(cod_reconocimiento);
            _repository.Delete(reconocimiento);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<treconocimiento>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public treconocimiento GetById(int cod_reconocimiento)
        {
            return _repository.GetById(cod_reconocimiento);
        }

        public async Task<int> Save(treconocimiento treconocimiento)
        {
            treconocimiento.ttorneo = torneoService.GetById(Convert.ToInt32(treconocimiento.cod_torneo));
            _repository.Add(treconocimiento);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(treconocimiento treconocimiento)
        {
            var reconocimientoAModificar = GetById(treconocimiento.cod_reconocimiento);
            reconocimientoAModificar.cod_tipo_reconocimiento = treconocimiento.cod_tipo_reconocimiento;
            reconocimientoAModificar.cod_torneo = treconocimiento.cod_torneo;
            reconocimientoAModificar.ttorneo = torneoService.GetById(Convert.ToInt32(treconocimiento.cod_torneo));
            return (await _repository.SaveChangesAsync());
        }
    }
}
