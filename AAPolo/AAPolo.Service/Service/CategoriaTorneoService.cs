﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class CategoriaTorneoService: ICategoriaTorneoService
    {
        private readonly IBaseRepository<tcategoria_torneo> _repository;

        public CategoriaTorneoService(IBaseRepository<tcategoria_torneo> repository)
        {
            _repository = repository;
        }

        public async Task<List<tcategoria_torneo>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(tcategoria_torneo tipotorneo)
        {
            _repository.Add(tipotorneo);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(tcategoria_torneo categoriaTorneo)
        {
            var categoriaTorneoAModificar = _repository.GetById(categoriaTorneo.cod_categoria_torneo);
            categoriaTorneoAModificar.txt_desc = categoriaTorneo.txt_desc;

            _repository.Update(categoriaTorneoAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_categoriaTorneo)
        {
            var categoriaTorneoABorrar = _repository.GetById(cod_categoriaTorneo);
            _repository.Delete(categoriaTorneoABorrar);
            return await _repository.SaveChangesAsync();
        }

        public tcategoria_torneo GetById(int cod_categoriaTorneo)
        {
            return _repository.GetById(cod_categoriaTorneo);
        }
    }
}
