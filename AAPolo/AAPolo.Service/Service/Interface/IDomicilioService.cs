﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IDomicilioService
    {
        Task<List<tdomicilio>> GetAll();
        Task<int> Save(tdomicilio domicilio);
        Task<int> Update(tdomicilio domicilio);
        Task<int> Delete(int cod_domicilio);
        tdomicilio GetById(int cod_domicilio);
        Task<tdomicilio> GetByCondition(tdomicilio domicilio);
        tdomicilio PopularDomicilio(tdomicilio domicilioAModificar, tdomicilio domicilio); 
    }
}
