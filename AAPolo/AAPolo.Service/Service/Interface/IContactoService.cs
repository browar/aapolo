﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IContactoService
    {
        Task<List<tcontacto>> GetAll();
        Task<int> Save(tcontacto contacto);
        Task<int> Update(tcontacto contacto);
        Task<int> Delete(int cod_contacto);
        tcontacto GetById(int cod_contacto);
        Task<tcontacto> GetByCondition(tcontacto contacto);
    }
}
