﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IHandicapService
    {
        Task<List<thandicap>> GetAll();
        Task<int> Save(thandicap handicap);
        Task<int> Update(thandicap handicap);
        Task<int> Delete(int cod_handicap);
        thandicap GetById(int cod_handicap);
    }
}
