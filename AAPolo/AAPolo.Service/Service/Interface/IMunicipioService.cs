﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IMunicipioService
    {
        Task<List<tmunicipio>> GetAll();
        Task<int> Save(tmunicipio municipio);
        Task<int> Update(tmunicipio municipio);
        Task<int> Delete(int cod_municipio);
        tmunicipio GetById(int cod_municipio);
        Task<List<tmunicipio>> GetByCodProvincia(int cod_porvincia);
    }
}
