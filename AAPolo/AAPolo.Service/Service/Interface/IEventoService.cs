﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IEventoService
    {
        Task<List<tevento>> GetAll();
        Task<int> Save(tevento evento);
        Task<int> Update(tevento evento);
        Task<int> Delete(int cod_evento);
        tevento GetById(int cod_evento);
    }
}
