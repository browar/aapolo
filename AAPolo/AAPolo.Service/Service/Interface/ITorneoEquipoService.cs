﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITorneoEquipoService
    {
        Task<List<tequipo_user>> GetByUser(int? cod_usuario);
        Task<int> Save(List<tequipo_user> equipoUsers);
        Task<int> Update(tequipo_user torneoEquipo);
        Task<int> Delete(int cod_equipo_user);
        tequipo_user GetById(int cod_equipo_user);
    }
}
