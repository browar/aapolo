﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IRespuestaService
    {
        Task<List<trespuesta>> GetEnviadosPorUsuario(int id);
        Task<List<trespuesta>> GetRecibidosPorUsuario(int id);
        Task Save(trespuesta respuesta);
        Task<List<trespuesta>> GetRespuestasALaAAP();
        trespuesta GetRespuestaById(int id);
    }
}
