﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IEquipoService
    {
        Task<List<tequipo>> GetAll();
        Task<List<tequipo>> GetByUser(int? cod_equipo);
        Task<int> Save(tequipo equipo);
        Task<int> Update(tequipo equipo);
        Task<int> Delete(int cod_equipo);
        tequipo GetById(int cod_equipo);
        Task<List<tequipo>> GetByUser(int cod_usuario);
        Task PagarEquipo(int cod_usuario, int cod_torneo, int nro_resultado_pago);
        Task<bool> GetByTorneoYCodUsuario(int cod_torneo, int cod_usuario);
        Task PagarEquipoByIdEquipo(int cod_equipo);
    }
}
