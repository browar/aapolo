﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IHistorialPagoService
    {
        Task<List<thistorial_pagos>> GetAll();
        Task<int> Save(thistorial_pagos pagos);
        Task<int> Update(thistorial_pagos pagos);
        Task<int> Delete(int cod_pagos);
        thistorial_pagos GetById(int cod_pagos);
        Task<List<thistorial_pagos>> GetByCodUsuario(int cod_usuario);
        Task<List<thistorial_pagos>> GetByCodClub(int cod_club);
    }
}
