﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITipoNoticiaService
    {
        Task<List<ttipo_noticia>> GetAll();
        Task<int> Save(ttipo_noticia tipoNoticia);
        Task<int> Update(ttipo_noticia tipoNoticia);
        Task<int> Delete(int cod_tipoNoticia);
        ttipo_noticia GetById(int cod_tipoNoticia);
    }
}
