﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IPartidoService
    {
        Task<List<tpartido>> GetByTorneo(int cod_torneo);
        Task DeleteByTorneo(int cod_torneo);
        tpartido GetById(int cod_partido);
    }
}
