﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ICategoriaClubService
    {
        Task<List<tcategoria_club>> GetAll();
        Task<int> Save(tcategoria_club categoriaClub);
        Task<int> Update(tcategoria_club categoriaClub);
        Task<int> Delete(int cod_categoria_club);
        tcategoria_club GetById(int cod_categoria_club);
    }
}
