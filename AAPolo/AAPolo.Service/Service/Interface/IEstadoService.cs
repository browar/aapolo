﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IEstadoService
    {
        Task<List<testado>> GetAll();
        Task<int> Save(testado estado);
        Task<int> Update(testado estado);
        Task<int> Delete(int cod_estado);
        testado GetById(int cod_estado);
    }
}
