﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IHandicapUsuarioService
    {
        Task<List<thandicap_usuario>> GetMany(int jugadorId);
        Task<int> BorrarTodosPorJugadorId(int cod_usuario);
        thandicap_usuario PopulateHandicapUsuario(thandicap_usuario handicapUsuario);
        thandicap_usuario GetById(int idHandicap);
        Task<List<thandicap_usuario_historico>> GetHistoricos(int cod_handicap_usuario, bool bool_principal);
    }
}
