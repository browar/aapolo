﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITorneoHistoricoService
    {
        Task<List<ttorneo_historico>> GetAll();
        void Save(ttorneo_historico clubHistorico);
        ttorneo_historico GetById(int cod_torneo_historico);
    }
}
