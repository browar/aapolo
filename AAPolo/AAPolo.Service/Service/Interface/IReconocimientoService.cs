﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IReconocimientoService
    {
        Task<List<treconocimiento>> GetAll();
        Task<int> Save(treconocimiento reconocimiento);
        Task<int> Update(treconocimiento reconocimiento);
        Task<int> Delete(int cod_reconocimiento);
        treconocimiento GetById(int cod_reconocimiento);
    }
}
