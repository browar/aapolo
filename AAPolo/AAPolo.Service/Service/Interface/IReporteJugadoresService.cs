﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
namespace AAPolo.Service.Service.Interface
{
    public interface IReporteJugadoresService
    {
        Task<List<reporteCuentaJugadores>> GetAll();
        Task<List<reporteCuentaJugadores>> GetAllCuentas();
        Task<List<reporteJugadorDTO>> GetAllPlayers();
    }
}
