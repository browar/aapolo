﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITipoDomicilioService
    {
        Task<List<ttipo_domicilio>> GetAll();
        Task<int> Save(ttipo_domicilio tipoDomicilio);
        Task<int> Update(ttipo_domicilio tipoDomicilio);
        Task<int> Delete(int cod_tipoDomicilio);
        ttipo_domicilio GetById(int cod_tipoDomicilio);
    }
}
