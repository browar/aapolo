﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITipoJugadorService
    {
        Task<List<ttipo_jugador>> GetAll();
        Task<int> Save(ttipo_jugador tipoJugador);
        Task<int> Update(ttipo_jugador tipoJugador);
        Task<int> Delete(int cod_tipoJugador);
        ttipo_jugador GetById(int cod_tipoJugador);
    }
}
