﻿using AAPolo.Entity.Entity;

namespace AAPolo.Service.Service.Interface
{
    public interface ITipoPagoService
    {
        ttipo_pago GetById(int cod_tipo_pago);
    }
}
