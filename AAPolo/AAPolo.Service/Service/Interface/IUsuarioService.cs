﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IUsuarioService
    {
        Task<List<users>> GetAll();
        Task<int> Save(users usuario);
        Task<int> Update(users usuario);
        Task<int> Delete(int cod_user);
        users GetById(int cod_user);
        Task<users> PopularUsuario(users usuarioAModificar, users usuario);
        users_historico PopularUsuarioHistorico(users_historico usuarioHistorico, users usuario, string user);
        List<userListDTO> GetImportantUsers(int page);
        Task CambiarHandicap(List<users> usuarios);
        Task<users> GetByDocumentAndPassword(users user);
        Task<users> GetByDocument(string nro_doc);
        Task<int> ActualizarContrasena(users usuario);
        Task<List<users>> GetMany(int page, string nombre, int cod_tipo_jugador, int cod_club, int nro_handicap);
        List<userListDTO> GetManyBySP(int page, string nombre, int cod_tipo_jugador, int cod_club, int nro_handicap, int pageSize);
        Task<List<users>> GetVeedores();
        Task<List<users>> GetBySelect();
        Task<List<users>> GetByClub(int idClub);
        Task<List<users>> searchUsers(int page, string search);
        Task<int> AceptarTerminos(int cod_usuario, users usuario);
        Task<List<users>> searchUsersSinHandicap(int page, string search);
        void ActualizaArticulo(int cod_usuario);
        Task BlanquearContrasena(int nro_doc);
        Task AsociarClubes(users usuarioExistente);
        Task AsociarContactos(users usuarioExistente);
        Task UpdatePlatformAndRegister(users usuario);
        Task<int> CountUsers();
        Task<List<thistorial_pagos>> GetHistorialPagos(int cod_user);
        List<userListDTO> GetAllBySP(int page);
        Task<List<users>> searchUsersPase(int page, string search);
        Task<thandicap_usuario_historico> GetHistoricoSinPago(int id);
    }
}
