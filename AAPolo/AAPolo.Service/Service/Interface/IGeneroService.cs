﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IGeneroService
    {
        Task<List<tgenero>> GetAll();
        Task<int> Save(tgenero genero);
        Task<int> Update(tgenero genero);
        Task<int> Delete(int cod_genero);
        tgenero GetById(int cod_genero);
    }
}
