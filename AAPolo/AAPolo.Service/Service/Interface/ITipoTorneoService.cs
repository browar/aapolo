﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITipoTorneoService
    {
        Task<List<ttipo_torneo>> GetAll();
        Task<int> Save(ttipo_torneo tipoTorneo);
        Task<int> Update(ttipo_torneo tipoTorneo);
        Task<int> Delete(int cod_tipoTorneo);
        ttipo_torneo GetById(int cod_tipoTorneo);
    }
}
