﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface INoticiaService
    {
        Task<List<tnoticia>> GetAll();
        //List<tnoticia> GetRelatedById(string cod_noticia);
        
        Task<int> Save(tnoticia noticia);
        Task<int> Update(tnoticia noticia);
        Task<int> Delete(int cod_noticia);
        tnoticia GetById(int cod_noticia);
        Task<int> UpdateEstado(int id);
        Task<int> UpdateEstadoDestacada(int id);
        List<NoticiaHomeDTO> GetNoticiasDestacadas();
        List<NoticiaHomeDTO> GetNoticiasActivas();
        List<noticiaAdminDTO> GetAllBySp();

    }
}
