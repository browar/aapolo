﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAPolo.Entity.Entity;
namespace AAPolo.Service.Service.Interface
{
    public interface IReporteTorneoService
    {
        Task<List<reporteTorneo>> GetByID(int id);
        Task<List<reporteCuentaTorneo>> GetAll();


    }
}
