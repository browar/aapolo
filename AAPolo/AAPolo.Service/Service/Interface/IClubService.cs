﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IClubService
    {
        Task<List<tclub>> GetAll();
        Task<int> Save(tclub club);
        Task<int> Update(tclub club);
        Task<int> Delete(int cod_club);
        tclub GetById(int cod_club);
        Task<tclub> PopularClub(tclub clubAModificar, tclub club);
        tclub_historico PopularClubHistorico(tclub_historico clubHistorico, tclub club, string user);
        Task<int> UpdateAdminClub(int adminClubId, int clubId);
        List<clubSelect> GetClubesPagos();
    }
}
