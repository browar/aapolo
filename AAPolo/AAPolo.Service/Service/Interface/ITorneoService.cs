﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITorneoService
    {
        Task<List<ttorneo>> GetAll();
        Task<int> Save(ttorneo torneo);
        Task<int> Update(ttorneo torneo);
        Task<int> Delete(int cod_user);
        ttorneo GetById(int cod_user);
        ttorneo_historico PopularTorneoHistorico(ttorneo_historico torneoAModificar, ttorneo torneo, string user);
    }
}
