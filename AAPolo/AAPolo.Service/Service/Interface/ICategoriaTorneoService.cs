﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ICategoriaTorneoService
    {
        Task<List<tcategoria_torneo>> GetAll();
        Task<int> Save(tcategoria_torneo categoriaTorneo);
        Task<int> Update(tcategoria_torneo categoriaTorneo);
        Task<int> Delete(int cod_categoriaTorneo);
        tcategoria_torneo GetById(int cod_categoriaTorneo);
    }
}
