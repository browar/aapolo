﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITipoEventoService
    {
        Task<List<ttipo_evento>> GetAll();
        Task<int> Save(ttipo_evento tipoEvento);
        Task<int> Update(ttipo_evento tipoEvento);
        Task<int> Delete(int cod_tipo_evento);
        ttipo_evento GetById(int cod_tipo_evento);
    }
}
