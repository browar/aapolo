﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IUsuarioHistoricoService
    {
        Task<List<users_historico>> GetAll();
        Task Save(users_historico usuarioHistorico);
        users_historico GetById(int cod_usuario_historico);
    }
}
