﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IProvinciaService
    {
        Task<List<tprovincia>> GetAll();
        Task<int> Save(tprovincia provincia);
        Task<int> Update(tprovincia provincia);
        Task<int> Delete(int cod_provincia);
        tprovincia GetById(int cod_provincia);
    }
}
