﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IInstanciaService
    {
        Task<List<tinstancia>> GetAll();
        tinstancia GetById(int cod_instancia);
    }
}
