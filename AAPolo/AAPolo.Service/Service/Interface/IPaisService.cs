﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IPaisService
    {
        Task<List<tpais>> GetAll();
        Task<int> Save(tpais pais);
        Task<int> Update(tpais pais);
        Task<int> Delete(int cod_pais);
        tpais GetById(int cod_pais);
    }
}
