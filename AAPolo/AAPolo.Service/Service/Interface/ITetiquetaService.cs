﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace AAPolo.Service.Service.Interface
{
    public interface ITetiquetaService
    {
        Task<List<tetiqueta>> GetAll();
        Task<int> Save(tetiqueta etiqueta);
        Task<int> Update(tetiqueta etiqueta);
        Task<int> Delete(int cod_noticia);
        tnoticia GetById(int cod_noticia);
    }
}
