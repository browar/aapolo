﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITipoContactoService
    {
        Task<List<ttipo_contacto>> GetAll();
        Task<int> Save(ttipo_contacto contacto);
        Task<int> Update(ttipo_contacto contacto);
        Task<int> Delete(int cod_tipo_contacto);
        ttipo_contacto GetById(int cod_tipo_contacto);
    }
}
