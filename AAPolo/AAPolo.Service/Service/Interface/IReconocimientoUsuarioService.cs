﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IReconocimientoUsuarioService
    {
        Task<List<treconocimiento_usuario>> GetAll();
        Task<List<treconocimiento_usuario>> GetByUser(int? cod_usuario);
        Task<int> Save(treconocimiento_usuario reconocimiento_usuario);
        Task<int> Update(treconocimiento_usuario reconocimiento_usuario);
        Task<int> Delete(int cod_reconocimiento_usuario);
        treconocimiento_usuario GetById(int cod_reconocimiento_usuario);
    }
}
