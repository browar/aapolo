﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IMiembroService
    {
        Task<List<tmiembro>> GetAll();
        Task<int> Save(tmiembro miembro);
        Task<int> Update(tmiembro miembro);
        Task Delete(int cod_miembro);
        tmiembro GetById(int cod_miembro);
        Task<tmiembro> getByDocumentoYPassword(string nro_doc, string password);
        Task<tmiembro> getByDocument(string nro_doc);
        Task<int> ActualizarContrasena(string dni, string password);
        Task<tmiembro> GetByNroTarjeta(string tarjeta);
        Task<tmiembro> GetByCodUsuario(int cod_usuario);
    }
}
