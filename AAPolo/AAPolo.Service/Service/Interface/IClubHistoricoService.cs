﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IClubHistoricoService
    {
        Task<List<tclub_historico>> GetAll();
        void Save(tclub_historico clubHistorico);
        tclub_historico GetById(int cod_club_historico);
    }
}
