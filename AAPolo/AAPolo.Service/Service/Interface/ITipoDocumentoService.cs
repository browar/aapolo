﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ITipoDocumentoService
    {
        Task<List<ttipo_doc>> GetAll();
        Task<int> Save(ttipo_doc tipoDocumento);
        Task<int> Update(ttipo_doc tipoDocumento);
        Task<int> Delete(int cod_tipo_documento);
        ttipo_doc GetById(int cod_tipo_documento);
    }
}
