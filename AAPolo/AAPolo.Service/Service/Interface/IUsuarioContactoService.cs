﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IUsuarioContactoService
    {
        Task<List<tcontacto_usuario>> GetAll();
        Task<int> Save(List<tcontacto_usuario> categoriasUsuarios);
        Task<int> Update(tcontacto_usuario contactoUsuario);
        Task<int> Delete(int cod_contacto_usuario);
        tcontacto_usuario GetById(int cod_contacto_usuario);
        Task<List<tcontacto_usuario>> ObtenerContactosUsuario(List<tcontacto_usuario> contactos);
        Task<int> BorrarPorUsuario(int cod_usuario);
        Task<int> SaveOne(tcontacto_usuario contactoUsuario);
    }
}
