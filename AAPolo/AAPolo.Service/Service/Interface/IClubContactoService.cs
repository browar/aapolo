﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IClubContactoService
    {
        Task<List<tcontacto_club>> GetAll();
        Task<int> Save(List<tcontacto_club> contactoClub);
        Task<int> Update(tcontacto_club contactoClub);
        Task<int> Delete(int cod_contactoClub);
        tcontacto_club GetById(int cod_contactoClub);
        Task<List<tcontacto_club>> ObtenerContactosClub(List<tcontacto_club> contactos);
        Task<int> BorrarPorClub(int cod_club);
    }
}
