﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IUsuarioAdminService
    {
        Task<users_admin> GetByUserAndPassword(users_admin user);
        users_admin GetById(int id);
        Task<users_admin> GetByUsername(string username);
    }
}
