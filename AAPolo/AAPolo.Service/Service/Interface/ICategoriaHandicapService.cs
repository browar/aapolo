﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface ICategoriaHandicapService
    {
        Task<List<tcategoria_handicap>> GetAll();
        Task<int> Save(tcategoria_handicap categoriaHandicap);
        Task<int> Update(tcategoria_handicap categoriaHandicap);
        Task<int> Delete(int cod_categoriaHandicap);
        tcategoria_handicap GetById(int cod_categoriaHandicap);
    }
}
