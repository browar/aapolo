﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service.Interface
{
    public interface IUsuarioClubService
    {
        Task<List<tuser_club>> GetMany(int jugadorId);
        Task<bool> BorrarTodosPorJugadorId(int jugadorId);
        Task<List<tuser_club>> GetManyByClubId(int clubId);
        Task AceptarRechazar(int idUsuario, bool opcion);
    }
}
