﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class RespuestaService : IRespuestaService
    {
        private readonly IBaseRepository<trespuesta> repository;
        private readonly IUsuarioService userService;
        private readonly INotificacionService notificacionService;

        public RespuestaService(IBaseRepository<trespuesta> _repository, IUsuarioService _userService, INotificacionService _notificacionService)
        {
            repository = _repository;
            userService = _userService;
            notificacionService = _notificacionService;
        }

        public trespuesta GetRespuestaById(int id)
        {
            return repository.GetById(id);
        }

        public async Task<List<trespuesta>> GetEnviadosPorUsuario(int id)
        {
            var respuestas = (await repository.GetMany(x => x.cod_usuario_envia == id)).ToList();
            return respuestas;
        }

        public async Task<List<trespuesta>> GetRecibidosPorUsuario(int id)
        {
            var respuestas = (await repository.GetMany(x => x.cod_usuario_recibe == id)).ToList();
            return respuestas;
        }

        public async Task<List<trespuesta>> GetRespuestasALaAAP()
        {
            var respuestas = (await repository.GetMany(x => x.cod_usuario_recibe == null)).ToList();
            return respuestas;
        }

        public async Task Save(trespuesta respuesta)
        {
            respuesta = PopulateRespuesta(respuesta);
            repository.Add(respuesta);
            await repository.SaveChangesAsync();
        }

        private trespuesta PopulateRespuesta(trespuesta respuesta)
        {
            if (respuesta.cod_notificacion.HasValue)
                respuesta.tnotificacion = notificacionService.GetById(respuesta.cod_notificacion.Value);

            if (respuesta.cod_usuario_envia.HasValue)
                respuesta.users = userService.GetById(respuesta.cod_usuario_envia.Value);

            if (respuesta.cod_usuario_recibe.HasValue)
                respuesta.users1 = userService.GetById(respuesta.cod_usuario_recibe.Value);

            respuesta.fec_enviado = DateTime.Now;

            return respuesta;
        }
    }
}
