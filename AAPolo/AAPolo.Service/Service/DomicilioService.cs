﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class DomicilioService : IDomicilioService
    {
        private readonly IBaseRepository<tdomicilio> _repository;
        private readonly IMunicipioService municipioService;
        private readonly IPaisService paisService;
        private readonly IProvinciaService provinciaService;
        private readonly ITipoDomicilioService tipoDomicilioService;

        public DomicilioService(IBaseRepository<tdomicilio> repository, IMunicipioService _municipioService,
            IPaisService _paisService, IProvinciaService _provinciaService, ITipoDomicilioService _tipoDomicilioService)
        {
            _repository = repository;
            municipioService = _municipioService;
            paisService = _paisService;
            provinciaService = _provinciaService;
            tipoDomicilioService = _tipoDomicilioService;
        }

        public async Task<tdomicilio> GetByCondition(tdomicilio domicilio)
        {
            var direccion = (await _repository.GetMany(x => x.txt_domicilio == domicilio.txt_domicilio
                  && x.nro_domicilio == domicilio.nro_domicilio)).ToList();
            if (direccion == null)
            {
                direccion = (await _repository.GetMany(x => x.txt_domicilio == domicilio.txt_domicilio)).ToList();
            }

            return direccion.FirstOrDefault();
        }

        public async Task<List<tdomicilio>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(tdomicilio domicilio)
        {
            domicilio.fec_alta = DateTime.Now;
            PopularDomicilio(domicilio, domicilio);
            _repository.Add(domicilio);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(tdomicilio domicilio)
        {
            var domicilioAModificar = (await _repository.GetByCondition(x => x.cod_domicilio == domicilio.cod_domicilio));
            if (domicilioAModificar == null)
                domicilioAModificar = (await GetByCondition(domicilio));

            domicilioAModificar = PopularDomicilio(domicilioAModificar, domicilio);

            _repository.Update(domicilioAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_domicilio)
        {
            var domicilioABorrar = _repository.GetById(cod_domicilio);
            _repository.Delete(domicilioABorrar);
            return await _repository.SaveChangesAsync();
        }

        public tdomicilio GetById(int cod_domicilio)
        {
            return _repository.GetById(cod_domicilio);
        }

        public tdomicilio PopularDomicilio(tdomicilio domicilioAModificar, tdomicilio domicilio)
        {
            domicilioAModificar.fec_ult_modificacion = DateTime.Now;

            domicilioAModificar.txt_domicilio = domicilio.txt_domicilio;
            domicilioAModificar.nro_domicilio = domicilio.nro_domicilio;
            domicilioAModificar.tpais = paisService.GetById(Convert.ToInt32(domicilio.cod_pais));
            domicilioAModificar.cod_pais = domicilioAModificar.tpais.cod_pais;

            if (domicilio.cod_provincia.HasValue)
                domicilioAModificar.tprovincia = provinciaService.GetById(Convert.ToInt32(domicilio.cod_provincia.Value));

            if (domicilio.tprovincia != null)
                domicilioAModificar.cod_provincia = domicilio.cod_provincia;

            if (domicilio.cod_municipio.HasValue)
                domicilioAModificar.tmunicipio = municipioService.GetById(Convert.ToInt32(domicilio.cod_municipio.Value));

            if (domicilio.tmunicipio != null)
                domicilioAModificar.cod_municipio = domicilio.tmunicipio.cod_municipio;
            else
            {
                domicilioAModificar.tmunicipio = municipioService.GetById(1040);
                domicilioAModificar.cod_municipio = 1040;
            }
            domicilioAModificar.ttipo_domicilio = tipoDomicilioService.GetById(Convert.ToInt32(domicilio.cod_tipo_domicilio));
            domicilioAModificar.cod_tipo_domicilio = domicilioAModificar.ttipo_domicilio.cod_tipo_domicilio;
            domicilioAModificar.txt_departamento = domicilio.txt_departamento;
            domicilioAModificar.nro_cod_postal = domicilio.nro_cod_postal;
            domicilioAModificar.nro_piso = domicilio.nro_piso;

            return domicilioAModificar;
        }
    }
}
