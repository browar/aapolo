﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TipoJugadorService : ITipoJugadorService
    {
        private readonly IBaseRepository<ttipo_jugador> _repository;

        public TipoJugadorService(IBaseRepository<ttipo_jugador> repository)
        {
            _repository = repository;
        }

        public async Task<int> Delete(int cod_tipoJugador)
        {
            var tipoJugadorABorrar = _repository.GetById(cod_tipoJugador);
            _repository.Delete(tipoJugadorABorrar);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<ttipo_jugador>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public ttipo_jugador GetById(int cod_tipoJugador)
        {
            return _repository.GetById(cod_tipoJugador);
        }

        public async Task<int> Save(ttipo_jugador tipoJugador)
        {
            _repository.Add(tipoJugador);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(ttipo_jugador tipoJugador)
        {
            var tipoJugadorAModificar = _repository.GetById(tipoJugador.cod_tipo_jugador);
            tipoJugadorAModificar.txt_desc = tipoJugador.txt_desc;
            _repository.Update(tipoJugadorAModificar);
            return (await _repository.SaveChangesAsync());
        }
    }
}
