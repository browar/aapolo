﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class ReconocimientoUsuarioService : IReconocimientoUsuarioService
    {
        private readonly IBaseRepository<treconocimiento_usuario> _repository;
        private readonly IUsuarioService userService;
        private readonly IReconocimientoService reconocimientoService;

        public ReconocimientoUsuarioService(IBaseRepository<treconocimiento_usuario> repository, IUsuarioService _userService, IReconocimientoService _reconocimientoService)
        {
            _repository = repository;
            userService = _userService;
            reconocimientoService = _reconocimientoService;
        }

        public async Task<int> Delete(int cod_reconocimiento_usuario)
        {
            var reconocimientoUsuario = _repository.GetById(cod_reconocimiento_usuario);
            _repository.Delete(reconocimientoUsuario);
            await _repository.SaveChangesAsync();
            return 1;
        }

        public async Task<List<treconocimiento_usuario>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public treconocimiento_usuario GetById(int cod_reconocimiento_usuario)
        {
            return _repository.GetById(cod_reconocimiento_usuario);
        }

        public async Task<List<treconocimiento_usuario>> GetByUser(int? cod_usuario)
        {
            return (await _repository.GetMany(x => x.cod_usuario == cod_usuario)).ToList();
        }

        public async Task<int> Save(treconocimiento_usuario reconocimientoUsuario)
        {
            reconocimientoUsuario.users = userService.GetById(reconocimientoUsuario.cod_usuario);
            reconocimientoUsuario.treconocimiento = reconocimientoService.GetById(reconocimientoUsuario.cod_reconocimiento);
            _repository.Add(reconocimientoUsuario);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(treconocimiento_usuario reconocimientoUsuario)
        {
            var reconocimientoAModificar = GetById(reconocimientoUsuario.cod_reconocimiento_usuario);
            reconocimientoAModificar.fec_reconocimiento = reconocimientoUsuario.fec_reconocimiento;
            reconocimientoAModificar.users = userService.GetById(reconocimientoUsuario.cod_usuario);
            reconocimientoAModificar.treconocimiento = reconocimientoService.GetById(reconocimientoUsuario.cod_reconocimiento);
            return (await _repository.SaveChangesAsync());
        }
    }
}
