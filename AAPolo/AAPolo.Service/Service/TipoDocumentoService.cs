﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TipoDocumentoService : ITipoDocumentoService
    {
        private readonly IBaseRepository<ttipo_doc> _repository;

        public TipoDocumentoService(IBaseRepository<ttipo_doc> repository)
        {
            _repository = repository;
        }

        public async Task<List<ttipo_doc>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(ttipo_doc TipoDocumento)
        {
            _repository.Add(TipoDocumento);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(ttipo_doc TipoDocumento)
        {
            var TipoDocumentoAModificar = _repository.GetById(TipoDocumento.cod_tipo_doc);
            TipoDocumentoAModificar.txt_desc = TipoDocumento.txt_desc;
            _repository.Update(TipoDocumentoAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_TipoDocumento)
        {
            var TipoDocumentoABorrar = _repository.GetById(cod_TipoDocumento);
            _repository.Delete(TipoDocumentoABorrar);
            return await _repository.SaveChangesAsync();
        }

        public ttipo_doc GetById(int cod_TipoDocumento)
        {
            return _repository.GetById(cod_TipoDocumento);
        }
    }
}
