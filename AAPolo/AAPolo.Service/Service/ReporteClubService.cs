﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class ReporteClubService : IReporteClubService
    {
        //private readonly IBaseRepository<reporteCuentaClub> _repository;
        private readonly IAAPoloContext _iAAPoloContext;
        public ReporteClubService( IAAPoloContext iAAPoloContext)//IBaseRepository<reporteCuentaClub> repository,
        {
            //_repository = repository;
            _iAAPoloContext = iAAPoloContext;
        }
        public async Task<List<reporteCuentaClub>> GetAll()
        {
            var miObj = _iAAPoloContext.ExecuteStoredProcedureBrowar<reporteCuentaClub>("sp_get_reporte_cuenta_clubes");
            return (miObj).ToList();
        }
    }
}

