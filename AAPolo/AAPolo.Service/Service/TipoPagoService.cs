﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TipoPagoService : ITipoPagoService
    {
        private readonly IBaseRepository<ttipo_pago> repository;

        public TipoPagoService(IBaseRepository<ttipo_pago> _repository)
        {
            repository = _repository;
        }

        public ttipo_pago GetById(int cod_tipo_pago)
        {
            return repository.GetById(cod_tipo_pago);
        }
    }
}
