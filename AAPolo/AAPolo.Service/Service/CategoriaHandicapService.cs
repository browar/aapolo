﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class CategoriaHandicapService : ICategoriaHandicapService
    {
        private readonly IBaseRepository<tcategoria_handicap> _repository;

        public CategoriaHandicapService(IBaseRepository<tcategoria_handicap> repository)
        {
            _repository = repository;
        }

        public async Task<int> Delete(int cod_categoriaHandicap)
        {
            var categoriaHandicapABorrar = _repository.GetById(cod_categoriaHandicap);
            _repository.Delete(categoriaHandicapABorrar);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<tcategoria_handicap>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public tcategoria_handicap GetById(int cod_categoriaHandicap)
        {
            return _repository.GetById(cod_categoriaHandicap);
        }

        public async Task<int> Save(tcategoria_handicap categoriaHandicap)
        {
            _repository.Add(categoriaHandicap);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(tcategoria_handicap categoriaHandicap)
        {
            var categoriaHandicapAModificar = _repository.GetById(categoriaHandicap.cod_categoria_handicap);
            categoriaHandicapAModificar.txt_desc = categoriaHandicap.txt_desc;
            _repository.Update(categoriaHandicapAModificar);
            return (await _repository.SaveChangesAsync());
        }
    }
}
