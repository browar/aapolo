﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class Tnoticia_archivoService : ITnoticia_archivoService
    {
        private readonly IBaseRepository<tnoticia_archivo> _repository;


        public Tnoticia_archivoService(IBaseRepository<tnoticia_archivo> repository)
        {
            _repository = repository;
        }

        public async Task<List<tnoticia_archivo>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(tnoticia_archivo noticiaArchivo)
        {
            _repository.Add(noticiaArchivo);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(tnoticia_archivo noticiaArchivo)
        {

            var archivoAModificar = _repository.GetById(noticiaArchivo.cod_noticia_archivo);
            archivoAModificar.txt_desc = noticiaArchivo.txt_desc;
            archivoAModificar.url_archivo = noticiaArchivo.url_archivo;
            archivoAModificar.fec_alta = DateTime.Today;
            _repository.Update(archivoAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_noticia)
        {
            var noticiaABorrar = _repository.GetById(cod_noticia);
            _repository.Delete(noticiaABorrar);
            return await _repository.SaveChangesAsync();
        }

        public tnoticia_archivo GetById(int cod_etiqueta)
        {
            return _repository.GetById(cod_etiqueta);
        }

        Task<List<ttipo_noticia>> ITnoticia_archivoService.GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<int> Save(ttipo_noticia tipoNoticia)
        {
            throw new NotImplementedException();
        }

        public Task<int> Update(ttipo_noticia tipoNoticia)
        {
            throw new NotImplementedException();
        }

        ttipo_noticia ITnoticia_archivoService.GetById(int cod_tipoNoticia)
        {
            throw new NotImplementedException();
        }
    }

}

