﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class UsuarioAdminService : IUsuarioAdminService
    {
        private readonly IBaseRepository<users_admin> repository;
        private readonly Encrypt encrypt;

        public UsuarioAdminService(IBaseRepository<users_admin> _repository)
        {
            repository = _repository;
            encrypt = new Encrypt();
        }

        public users_admin GetById(int id)
        {
            return repository.GetById(id);
        }

        public async Task<users_admin> GetByUserAndPassword(users_admin user)
        {
            string passwordHashed = encrypt.ToSHA256(user.txt_password);

            return (await repository.GetByCondition(x=> x.txt_password == passwordHashed && x.bool_activo == true && x.txt_usuario == user.txt_usuario));

        }

        public async Task<users_admin> GetByUsername(string username)
        {
           return (await repository.GetByCondition(x => x.txt_usuario == username && x.bool_activo == true));
        }
    }
}
