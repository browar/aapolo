﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class UsuarioHistoricoService : IUsuarioHistoricoService
    {
        private readonly IBaseRepository<users_historico> _repository;

        public UsuarioHistoricoService(IBaseRepository<users_historico> repository)
        {
            _repository = repository;
        }

        public async Task<List<users_historico>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task Save(users_historico usuarioHistorico)
        {
            _repository.Add(usuarioHistorico);
            await _repository.SaveChangesAsync();
        }

        public users_historico GetById(int codHistorico)
        {
            return _repository.GetById(codHistorico);
        }
    }
}
