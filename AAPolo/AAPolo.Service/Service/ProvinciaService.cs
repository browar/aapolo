﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class ProvinciaService : IProvinciaService
    {
        private readonly IBaseRepository<tprovincia> _repository;
        private readonly IPaisService paisService;

        public ProvinciaService(IBaseRepository<tprovincia> repository, IPaisService _paisService)
        {
            _repository = repository;
            paisService = _paisService;
        }

        public async Task<List<tprovincia>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(tprovincia provincia)
        {
            provincia.tpais = paisService.GetById(provincia.cod_pais);
            _repository.Add(provincia);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(tprovincia provincia)
        {
            var provinciaAModificar = _repository.GetById(provincia.cod_provincia);
            provinciaAModificar.txt_desc = provincia.txt_desc;
            provinciaAModificar.cod_pais = provincia.cod_pais;
            provinciaAModificar.tpais = paisService.GetById(provincia.cod_pais);
            _repository.Update(provinciaAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_provincia)
        {
            var provinciaABorrar = _repository.GetById(cod_provincia);
            _repository.Delete(provinciaABorrar);
            return await _repository.SaveChangesAsync();
        }

        public tprovincia GetById(int cod_provincia)
        {
            return _repository.GetById(cod_provincia);
        }
    }
}
