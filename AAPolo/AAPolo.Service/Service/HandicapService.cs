﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class HandicapService : IHandicapService
    {
        private readonly IBaseRepository<thandicap> _repository;

        public HandicapService(IBaseRepository<thandicap> repository)
        {
            _repository = repository;
        }

        public async Task<int> Delete(int cod_handicap)
        {
            var handicapABorrar = _repository.GetById(cod_handicap);
            _repository.Delete(handicapABorrar);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<thandicap>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public thandicap GetById(int cod_handicap)
        {
            return _repository.GetById(cod_handicap);
        }

        public async Task<int> Save(thandicap handicap)
        {
            _repository.Add(handicap);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(thandicap handicap)
        {
            var handicapAModificar = _repository.GetById(handicap.cod_handicap);
            handicapAModificar.txt_desc = handicap.txt_desc;
            _repository.Update(handicapAModificar);
            return (await _repository.SaveChangesAsync());
        }
    }
}
