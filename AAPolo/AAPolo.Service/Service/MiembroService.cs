﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class MiembroService : IMiembroService
    {
        private readonly IBaseRepository<tmiembro> _repository;
        private readonly ITipoDocumentoService tipoDocumentoService;
        private readonly IPaisService paisService;
        private readonly ITipoContactoService tipoContactoService;
        private readonly IDomicilioService domicilioService;
        private readonly IGeneroService generoService;
        private readonly IClubService clubService;
        private readonly IUsuarioService usuarioService;
        private readonly Encrypt encrypt;

        public MiembroService(IBaseRepository<tmiembro> repository, ITipoDocumentoService _tipoDocumentoService, IPaisService _paisService,
            IDomicilioService _domicilioService, IGeneroService _generoService, IUsuarioService _usuarioService, ITipoContactoService _tipoContactoService, IClubService _clubService)
        {
            tipoDocumentoService = _tipoDocumentoService;
            paisService = _paisService;
            domicilioService = _domicilioService;
            generoService = _generoService;
            usuarioService = _usuarioService;
            tipoContactoService = _tipoContactoService;
            clubService = _clubService;
            _repository = repository;
            encrypt = new Encrypt();
        }

        public async Task Delete(int cod_miembro)
        {
            throw new NotImplementedException();
        }

        public async Task<List<tmiembro>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<tmiembro> GetByCodUsuario(int cod_usuario)
        {
            return (await _repository.GetByCondition(x=> x.cod_usuario == cod_usuario));
        }

        public tmiembro GetById(int cod_miembro)
        {
            return _repository.GetById(cod_miembro);
        }

        public async Task<tmiembro> getByDocumentoYPassword(string nro_doc, string password)
        {
            var hash = encrypt.ToSHA256(password);
            return await _repository.GetByCondition(x => x.nro_doc == nro_doc && x.txt_password == hash);
        }

        public async Task<tmiembro> getByDocument(string nro_doc)
        {
            return await _repository.GetByCondition(x => x.nro_doc == nro_doc);
        }

        public async Task<int> Save(tmiembro miembro)
        {
            var existe = await GetByDocument(miembro.nro_doc);
            if (existe == null)
            {
                miembro = await PopulateMiembroToSave(miembro);
                _repository.Add(miembro);
                await _repository.SaveChangesAsync();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public async Task<tmiembro> GetByNroTarjeta(string tarjeta)
        {
            return await _repository.GetByCondition(x => x.txt_tarjeta == tarjeta);
        }


        private async Task<tmiembro> GetByDocument(string nro_doc)
        {
            return (await _repository.GetByCondition(x => x.nro_doc == nro_doc.Trim()));
        }

        public async Task<int> Update(tmiembro miembro)
        {
            var miembroBuscado = _repository.GetById(miembro.cod_miembro);
            var miembroAModificar = await PopulateMiembroToUpdate(miembroBuscado, miembro);
            _repository.Update(miembroAModificar);
            await _repository.SaveChangesAsync();
            return 1;
        }

        public async Task<int> ActualizarContrasena(string dni, string password)
        {
            var miembroBuscado = await GetByDocument(dni);
            if (miembroBuscado == null)
                return 0;
            miembroBuscado.txt_password = encrypt.ToSHA256(password);
            _repository.Update(miembroBuscado);
            await _repository.SaveChangesAsync();
            return 1;
        }

        private async Task<tmiembro> PopulateMiembroToSave(tmiembro miembro)
        {
            await domicilioService.Save(miembro.tdomicilio);
            miembro.tdomicilio = await domicilioService.GetByCondition(miembro.tdomicilio);
            if (miembro.tdomicilio != null)
                miembro.cod_domicilio = miembro.tdomicilio.cod_domicilio;
            miembro.tgenero = generoService.GetById(miembro.cod_genero);
            miembro.ttipo_doc = tipoDocumentoService.GetById(miembro.cod_tipo_doc.Value);
            miembro.tgenero = generoService.GetById(miembro.cod_genero);
            miembro.tpais = paisService.GetById(miembro.cod_nacionalidad.Value);
            if (miembro.cod_club.HasValue)
                miembro.tclub = clubService.GetById(miembro.cod_club.Value);
            miembro.bool_activo = true;
            var existeUser = await usuarioService.GetByDocument(miembro.nro_doc);
            if (existeUser != null)
            {
                miembro.cod_usuario = existeUser.cod_usuario;
                miembro.users = existeUser;
                var contactos = new List<tcontacto_usuario>();
                var telefono = new tcontacto_usuario();
                telefono.tcontacto = new tcontacto();
                telefono.tcontacto.ttipo_contacto = new ttipo_contacto();
                telefono.tcontacto.cod_tipo_contacto = 1;
                telefono.tcontacto.ttipo_contacto = tipoContactoService.GetById(1);
                telefono.tcontacto.txt_desc = miembro.txt_celular;
                var mail = new tcontacto_usuario();
                telefono.cod_usuario = miembro.cod_usuario.Value;
                mail.cod_usuario = miembro.cod_usuario.Value;
                mail.tcontacto = new tcontacto();
                mail.tcontacto.ttipo_contacto = new ttipo_contacto();
                mail.tcontacto.cod_tipo_contacto = 3;
                mail.tcontacto.ttipo_contacto = tipoContactoService.GetById(3);
                mail.tcontacto.txt_desc = miembro.txt_mail;

                existeUser.cod_domicilio = miembro.cod_domicilio;

                existeUser.tdomicilio = miembro.tdomicilio;
                miembro.txt_password = existeUser.txt_password;
                existeUser.txt_tarjeta = miembro.txt_tarjeta;
                await usuarioService.Update(existeUser);
                var principal = existeUser.tuser_club.Where(x => x.bool_principal == true).FirstOrDefault();
                if (principal != null)
                {
                    if (principal.cod_club != miembro.cod_club)
                    {
                        principal.cod_usuario = existeUser.cod_usuario;
                        principal.cod_club = miembro.cod_club;
                        principal.tclub = clubService.GetById(miembro.cod_club.Value);
                    }
                }
                else
                {
                    var clubes = new List<tuser_club>();
                    var club = new tuser_club();
                    club.bool_principal = true;
                    club.users = existeUser;
                    club.cod_usuario = existeUser.cod_usuario;
                    club.cod_club = miembro.cod_club;
                    club.tclub = clubService.GetById(miembro.cod_club.Value);
                    clubes.Add(club);
                    existeUser.tuser_club = clubes;
                }
                contactos.Add(telefono);
                contactos.Add(mail);
                existeUser.tcontacto_usuario = contactos;
                await usuarioService.AsociarContactos(existeUser);
                await usuarioService.AsociarClubes(existeUser);
            }
            else
            {
                miembro.txt_password = encrypt.ToSHA256(miembro.nro_doc.Trim());
            }

            miembro.usuario_alta = "Registro";
            miembro.fec_alta = DateTime.Now;
            return miembro;
        }

        private async Task<tmiembro> PopulateMiembroToUpdate(tmiembro miembroAModificar, tmiembro miembro)
        {
            await domicilioService.Save(miembro.tdomicilio);
            miembroAModificar.tdomicilio = await domicilioService.GetByCondition(miembro.tdomicilio);
            miembroAModificar.cod_domicilio = miembro.tdomicilio.cod_domicilio;
            miembroAModificar.tgenero = generoService.GetById(miembro.cod_genero);
            miembroAModificar.ttipo_doc = tipoDocumentoService.GetById(miembro.cod_tipo_doc.Value);
            miembroAModificar.tgenero = generoService.GetById(miembro.cod_genero);
            miembroAModificar.tpais = paisService.GetById(miembro.cod_nacionalidad.Value);
            miembroAModificar.fec_ult_modificacion = DateTime.Now;
            var existeUser = await usuarioService.GetByDocument(miembro.nro_doc);
            if (existeUser != null)
            {
                miembroAModificar.cod_usuario = existeUser.cod_usuario;
                miembroAModificar.users = existeUser;
                var contactos = new List<tcontacto_usuario>();
                var telefono = miembroAModificar.users.tcontacto_usuario.Where(x => x.tcontacto.cod_tipo_contacto == 1).FirstOrDefault();
                telefono.tcontacto.txt_desc = miembro.txt_celular;
                var mail = miembroAModificar.users.tcontacto_usuario.Where(x => x.tcontacto.cod_tipo_contacto == 3).FirstOrDefault();
                mail.tcontacto.txt_desc = miembro.txt_mail;
                contactos.Add(telefono);
                contactos.Add(mail);
                existeUser.tcontacto_usuario = contactos;
                existeUser.cod_domicilio = miembro.cod_domicilio;
                existeUser.tdomicilio = miembro.tdomicilio;
                await usuarioService.Update(existeUser);
            }

            return miembro;
        }
    }
}
