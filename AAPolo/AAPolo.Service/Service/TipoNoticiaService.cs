﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TipoNoticiaService : ITipoNoticiaService
    {
        private readonly IBaseRepository<ttipo_noticia> _repository;

        public TipoNoticiaService(IBaseRepository<ttipo_noticia> repository)
        {
            _repository = repository;
        }

        public async Task<int> Delete(int cod_tipoNoticia)
        {
            var tipoNoticiaABorrar = _repository.GetById(cod_tipoNoticia);
            _repository.Delete(tipoNoticiaABorrar);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<ttipo_noticia>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public ttipo_noticia GetById(int cod_tipoNoticia)
        {
            return _repository.GetById(cod_tipoNoticia);
        }

        public async Task<int> Save(ttipo_noticia tipoNoticia)
        {
            _repository.Add(tipoNoticia);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(ttipo_noticia tipoNoticia)
        {
            var tipoNoticiaAModificar = _repository.GetById(tipoNoticia.cod_tipo_noticia);
            tipoNoticiaAModificar.txt_desc = tipoNoticia.txt_desc;
            _repository.Update(tipoNoticiaAModificar);
            return (await _repository.SaveChangesAsync());
        }
    }
}
