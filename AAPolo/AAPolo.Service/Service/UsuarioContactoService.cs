﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class UsuarioContactoService : IUsuarioContactoService
    {
        private readonly IBaseRepository<tcontacto_usuario> _repository;
        private readonly IContactoService contactoService;
        private readonly IBaseRepository<users> userRepository;
        private readonly ITipoContactoService tipoContactoService;

        public UsuarioContactoService(IBaseRepository<tcontacto_usuario> repository, IContactoService _contactoService, IBaseRepository<users> _userRepository,
            ITipoContactoService _tipoContactoService)
        {
            _repository = repository;
            contactoService = _contactoService;
            userRepository = _userRepository;
            tipoContactoService = _tipoContactoService;
        }

        public async Task<int> Delete(int cod_contacto_usuario)
        {
            var usuarioContactoABorrar = _repository.GetById(cod_contacto_usuario);
            _repository.Delete(usuarioContactoABorrar);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> BorrarPorUsuario(int cod_usuario)
        {
            var contactos = await _repository.GetMany(x=> x.cod_usuario == cod_usuario);
            foreach (var contacto in contactos)
            {
                _repository.Delete(contacto);
            }
            return (await _repository.SaveChangesAsync());
        }


        public async Task<List<tcontacto_usuario>> GetAll()
        {
            return (await (_repository.GetAll())).ToList();
        }

        public tcontacto_usuario GetById(int cod_contacto_usuario)
        {
            return _repository.GetById(cod_contacto_usuario);
        }

        public async Task<int> SaveOne(tcontacto_usuario contactoUsuario)
        {
            var contacto = new tcontacto();
            contacto = contactoUsuario.tcontacto;
            await contactoService.Save(contacto);
            contactoUsuario.users = userRepository.GetById(contactoUsuario.cod_usuario);
            contactoUsuario.cod_contacto = contacto.cod_contacto;
            contactoUsuario.tcontacto = contacto;
            _repository.Add(contactoUsuario);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Save(List<tcontacto_usuario> contactosUsuario)
        {
            foreach (var contactoUsuario in contactosUsuario)
            {
                contactoUsuario.tcontacto = contactoService.GetById(contactoUsuario.cod_contacto);
                contactoUsuario.users = userRepository.GetById(contactoUsuario.cod_usuario);
                _repository.Add(contactoUsuario);
            }
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(tcontacto_usuario contacto)
        {
            var contactoClubAModificar = _repository.GetById(contacto.cod_contacto_usuario);
            _repository.Update(contacto);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<tcontacto_usuario>> ObtenerContactosUsuario(List<tcontacto_usuario> contactos)
        {
            var listContactos = new List<tcontacto_usuario>();
            foreach (var contacto in contactos)
            {
                var contactoId = GetById(contacto.cod_contacto_usuario);
                if (contactoId == null)
                {
                    var nuevoContacto = (await contactoService.GetByCondition(contacto.tcontacto));
                    if (nuevoContacto != null)
                    {
                        contacto.tcontacto = nuevoContacto;
                        listContactos.Add(contacto);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(contacto.tcontacto.txt_desc))
                        {
                            contactoId = contacto;

                            contactoId.tcontacto.ttipo_contacto = tipoContactoService.GetById(contactoId.tcontacto.cod_tipo_contacto.Value);
                            listContactos.Add(contactoId);
                        }
                    }
                }
                else
                {
                    contacto.tcontacto.ttipo_contacto = tipoContactoService.GetById(contacto.tcontacto.cod_tipo_contacto.Value);
                    listContactos.Add(contacto);
                }
            }

            return listContactos;

        }
    }
}
