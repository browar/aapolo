﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class EventoService : IEventoService
    {
        private readonly IBaseRepository<tevento> _repository;
        private readonly IBaseRepository<ttorneo> torneoRepository;
        private readonly ITipoEventoService tipoEventoService;

        public EventoService(IBaseRepository<tevento> repository, IBaseRepository<ttorneo> _torneoRepository, ITipoEventoService _tipoEventoService)
        {
            _repository = repository;
            torneoRepository = _torneoRepository;
            tipoEventoService = _tipoEventoService;
        }

        public async Task<List<tevento>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(tevento evento)
        {
            evento.fec_alta = DateTime.Now;
            if(evento.cod_torneo.HasValue)
                evento.ttorneo = torneoRepository.GetById(evento.cod_torneo.Value);
            evento.ttipo_evento = tipoEventoService.GetById(evento.cod_tipo_evento);
            evento.bool_activo = true;
            _repository.Add(evento);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(tevento evento)
        {
            var eventoAModificar = _repository.GetById(evento.cod_evento);
            eventoAModificar.fec_comienzo = evento.fec_comienzo;
            eventoAModificar.fec_comienzo = eventoAModificar.fec_comienzo.AddHours(-3);
            eventoAModificar.fec_ultima_modificacion = DateTime.Now;
            if (evento.cod_torneo.HasValue)
                evento.ttorneo = torneoRepository.GetById(evento.cod_torneo.Value);

            eventoAModificar.ttipo_evento = tipoEventoService.GetById(evento.cod_tipo_evento);
            eventoAModificar.cod_tipo_evento = evento.cod_tipo_evento;
            eventoAModificar.usuario_ultima_modificacion = evento.usuario_ultima_modificacion;
            eventoAModificar.txt_imagen = evento.txt_imagen;
            eventoAModificar.txt_desc_larga = evento.txt_desc_larga;
            eventoAModificar.txt_desc = evento.txt_desc;
            
            _repository.Update(eventoAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_evento)
        {
            var eventoABorrar = _repository.GetById(cod_evento);
            eventoABorrar.bool_activo = !eventoABorrar.bool_activo;

            _repository.Update(eventoABorrar);
            await _repository.SaveChangesAsync();
            return 1;
        }

        public tevento GetById(int cod_evento)
        {
            return _repository.GetById(cod_evento);
        }
    }
}
