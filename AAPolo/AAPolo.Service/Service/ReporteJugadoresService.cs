﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class ReporteJugadoresService : IReporteJugadoresService
    {
        //private readonly IBaseRepository<reporteCuentaJugadores> _repository;
        private readonly IAAPoloContext _iAAPoloContext;
        public ReporteJugadoresService(IAAPoloContext iAAPoloContext)
        {
            //_repository = repository;
            _iAAPoloContext = iAAPoloContext;
        }
        public async Task<List<reporteCuentaJugadores>> GetAll()
        {
            var miObj = _iAAPoloContext.ExecuteStoredProcedureBrowar<reporteCuentaJugadores>("sp_get_reporte_cuenta_jugadores");
            return (miObj).ToList();
        }

        public async Task<List<reporteCuentaJugadores>> GetAllCuentas()
        {
            var miObj = _iAAPoloContext.ExecuteStoredProcedureBrowar<reporteCuentaJugadores>("sp_get_reporte_all_cuenta_jugadores");
            return (miObj).ToList();
        }

        public async Task<List<reporteJugadorDTO>> GetAllPlayers()
        {
            var miObj = _iAAPoloContext.ExecuteStoredProcedureBrowar<reporteJugadorDTO>("sp_get_reporte_jugadores");
            return (miObj).ToList();
        }
    }
}
