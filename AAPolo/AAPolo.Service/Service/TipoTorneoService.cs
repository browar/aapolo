﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TipoTorneoService : ITipoTorneoService
    {
        private readonly IBaseRepository<ttipo_torneo> _repository;

        public TipoTorneoService(IBaseRepository<ttipo_torneo> repository)
        {
            _repository = repository;
        }

        public async Task<List<ttipo_torneo>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(ttipo_torneo tipoTorneo)
        {
            _repository.Add(tipoTorneo);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(ttipo_torneo tipoTorneo)
        {
            var tipotorneoAModificar = _repository.GetById(tipoTorneo.cod_tipo_torneo);
            tipotorneoAModificar.txt_desc = tipoTorneo.txt_desc;
            _repository.Update(tipotorneoAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_tipoTorneo)
        {
            var tipotorneoABorrar = _repository.GetById(cod_tipoTorneo);
            _repository.Delete(tipotorneoABorrar);
            return await _repository.SaveChangesAsync();
        }

        public ttipo_torneo GetById(int cod_tipoTorneo)
        {
            return _repository.GetById(cod_tipoTorneo); 
        }
    }
}
