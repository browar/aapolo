﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class NoticiaService : INoticiaService
    {
        private readonly IBaseRepository<tnoticia> _repository;
        private readonly IBaseRepository<tnoticia_etiqueta> _etiquetaRepository;
        private readonly IBaseRepository<tnoticia_archivo> _tnoticia_archivoRepository;
        private readonly IAAPoloContext _iAAPoloContext;

        public NoticiaService(IBaseRepository<tnoticia> repository, IBaseRepository<tnoticia_etiqueta> etiquetaRepository, IBaseRepository<tnoticia_archivo> tnoticia_archivoRepository, IAAPoloContext iAAPoloContext)
        {
            _repository = repository;
            _etiquetaRepository = etiquetaRepository;
            _tnoticia_archivoRepository = tnoticia_archivoRepository;
            _iAAPoloContext = iAAPoloContext;
        }

        public async Task<List<tnoticia>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public List<noticiaAdminDTO> GetAllBySp()
        {
            var miObj = _iAAPoloContext.ExecuteStoredProcedureBrowar<noticiaAdminDTO>("sp_get_noticias_all");
            return (miObj).ToList();
        }
        //public List<tnoticia> GetRelatedById(string cod_noticia)
        //{
        //
        //    var miObj = _iAAPoloContext.ExecuteStoredProcedure<tnoticia>("sp_get_noticia_test", cod_noticia);
        //    List<tnoticia> noticiasRelacionadas = new List<tnoticia>();
        //    tnoticia miNoti;
        //    foreach (var miNoticia in miObj)
        //    {
        //        miNoti = new tnoticia();
        //        miNoti = GetById(miNoticia.cod_noticia);
        //        noticiasRelacionadas.Add(miNoti);
        //    }
        //    return noticiasRelacionadas;
        //}

        public async Task<int> Save(tnoticia noticia)
        {
            //noticia.fec_alta = DateTime.Now;
            noticia.fec_ult_modificacion = DateTime.Now;
            noticia.bool_activo = 1;
            noticia.bool_destacada = 0;
            _repository.Add(noticia);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(tnoticia noticia)
        {
            var noticiaAModificar = _repository.GetById(noticia.cod_noticia);
            noticiaAModificar.txt_titulo = noticia.txt_titulo;
            noticiaAModificar.txt_copete = noticia.txt_copete;
            noticiaAModificar.txt_cuerpo = noticia.txt_cuerpo;
            noticiaAModificar.fec_ult_modificacion = DateTime.Now;
            noticiaAModificar.fec_alta = noticia.fec_alta;
            noticiaAModificar.bool_activo = noticia.bool_activo;
            noticiaAModificar.txt_adjunto_footer = noticia.txt_adjunto_footer;
            var longEtiquetas = noticiaAModificar.tnoticia_etiqueta.Count;
            var misEtiquetas = noticiaAModificar.tnoticia_etiqueta.ToList();
            for (int i= 0; i < longEtiquetas;i++)
            {
                _etiquetaRepository.Delete(misEtiquetas[i]);
            }
            noticiaAModificar.tnoticia_etiqueta = noticia.tnoticia_etiqueta;
            
            var longArchivos = noticiaAModificar.tnoticia_archivo.Count;
            var misArchivos = noticiaAModificar.tnoticia_archivo.ToList();
            for (int i = 0; i < longArchivos; i++)
            {
                _tnoticia_archivoRepository.Delete(misArchivos[i]);
            }
            
            noticiaAModificar.tnoticia_archivo = noticia.tnoticia_archivo;

            _repository.Update(noticiaAModificar);
            return await _repository.SaveChangesAsync();
        }
        public async Task<int> UpdateEstado(int id)
        {
            var noticiaAModificar = _repository.GetById(id);
            noticiaAModificar.bool_activo = noticiaAModificar.bool_activo == 1 ? 0 : 1;
            _repository.Update(noticiaAModificar);
            return await _repository.SaveChangesAsync();
            //return await Update(noticiaAModificar);

        }
        public async Task<int> UpdateEstadoDestacada(int id)
        {
            var noticiaAModificar = _repository.GetById(id);
            noticiaAModificar.bool_destacada = noticiaAModificar.bool_destacada == 1 ? 0 : 1;
            _repository.Update(noticiaAModificar);
            return await _repository.SaveChangesAsync();
            //return await Update(noticiaAModificar);

        }
        

        public async Task<int> Delete(int cod_noticia)
        {
            var noticiaABorrar = _repository.GetById(cod_noticia);
            _repository.Delete(noticiaABorrar);
            return await _repository.SaveChangesAsync();
        }

        public tnoticia GetById(int cod_noticia)
        {
            return _repository.GetById(cod_noticia);
        }

        public List<NoticiaHomeDTO> GetNoticiasDestacadas()
        {
            var miObj = _iAAPoloContext.ExecuteStoredProcedureBrowar<NoticiaHomeDTO>("sp_get_noticias_destacadas");
            return (miObj).ToList();
        }

        public List<NoticiaHomeDTO> GetNoticiasActivas()
        {
            var miObj = _iAAPoloContext.ExecuteStoredProcedureBrowar<NoticiaHomeDTO>("sp_get_noticias_activas");
            return (miObj).ToList();
        }

    }
}
