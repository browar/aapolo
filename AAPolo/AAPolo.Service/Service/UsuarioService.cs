﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IBaseRepository<users> repository;
        private readonly IDomicilioService domicilioService;
        private readonly ITipoDocumentoService tipoDocumentoService;
        private readonly IContactoService contactoService;
        private readonly IClubService clubService;
        private readonly IUsuarioClubService usuarioClubService;
        private readonly IUsuarioContactoService contactoUsuarioService;
        private readonly ITipoJugadorService tipoJugadorService;
        private readonly IUsuarioHistoricoService usuarioHistoricoService;
        private readonly IGeneroService generoService;
        private readonly IHandicapUsuarioService handicapUsuarioService;
        private readonly IBaseRepository<thandicap_usuario_historico> handicapHistoricoService;
        private readonly IBaseRepository<tpais> paisService;
        private readonly IBaseRepository<thistorial_pagos> historialPagosService;
        private IBaseRepository<thandicap> handicapService;
        private IAAPoloContext apoloContext;
        private readonly Encrypt encrypt;


        public UsuarioService(IBaseRepository<users> _repository, IDomicilioService _domicilioService,
            ITipoDocumentoService _tipoDocumentoService, IContactoService _contactoService, IClubService _clubService,
            IUsuarioClubService _usuarioClubService, IUsuarioContactoService _contactoUsuarioService, ITipoJugadorService _tipoJugadorService,
            IUsuarioHistoricoService _usuarioHistoricoService, IGeneroService _generoService, IHandicapUsuarioService _handicapUsuarioService,
            IBaseRepository<thandicap_usuario_historico> _handicapHistoricoService, IBaseRepository<tpais> _paisService, IBaseRepository<thandicap> _handicapService,
            IBaseRepository<thistorial_pagos> _historialPagosService, IAAPoloContext _apoloContext)
        {
            repository = _repository;
            domicilioService = _domicilioService;
            tipoDocumentoService = _tipoDocumentoService;
            contactoService = _contactoService;
            clubService = _clubService;
            usuarioClubService = _usuarioClubService;
            contactoUsuarioService = _contactoUsuarioService;
            tipoJugadorService = _tipoJugadorService;
            usuarioHistoricoService = _usuarioHistoricoService;
            generoService = _generoService;
            handicapUsuarioService = _handicapUsuarioService;
            handicapHistoricoService = _handicapHistoricoService;
            paisService = _paisService;
            handicapService = _handicapService;
            historialPagosService = _historialPagosService;
            apoloContext = _apoloContext;
            encrypt = new Encrypt();
        }

        public async Task<List<users>> GetAll()
        {
            return (await repository.GetMany(x => x.cod_tipo_doc != 6)).ToList();
        }

        public List<userListDTO> GetAllBySP(int page)
        {
            var misParametros = new List<System.Data.SqlClient.SqlParameter>();
            System.Data.SqlClient.SqlParameter parameter = new System.Data.SqlClient.SqlParameter();
            parameter.ParameterName = "@numberPage";
            parameter.SqlDbType = System.Data.SqlDbType.Int;
            parameter.Direction = System.Data.ParameterDirection.Input;
            parameter.Value = page;
            misParametros.Add(parameter);
            var miObj = apoloContext.ExecuteStoredProcedureBrowar<userListDTO>("sp_get_jugadores_all", misParametros.ToArray());
            return (miObj).ToList();
        }


        public async Task<List<users>> searchUsers(int page, string search)
        {
            return (await repository.GetMany(x => ((x.txt_nombre + " " + x.txt_apellido).Contains(search) || x.nro_doc == search)
            && x.cod_tipo_jugador != 6 && x.thandicap_usuario.Any())).ToList();
        }

        public async Task<List<users>> searchUsersSinHandicap(int page, string search)
        {
            var usersList = (await repository.GetMany(x => ((x.txt_nombre + " " + x.txt_apellido).Contains(search) || x.nro_doc == search)
            && x.cod_tipo_jugador != 6 && x.thandicap_usuario.Any() && x.fec_nacimiento != null)).ToList();

            var usersSinHandicapPago = usersList.Where(x => !x.thistorial_pagos.Any(m => m.fec_vencimiento >= DateTime.Now && m.cod_tipo_pago == 1 && m.nro_resultado_pago == 1)
            || x.thistorial_pagos.Count == 0).ToList();

            return usersSinHandicapPago;
        }

        public async Task<List<users>> searchUsersPase(int page, string search)
        {
            var usersList = (await repository.GetMany(x => ((x.txt_nombre + " " + x.txt_apellido).Contains(search) || x.nro_doc == search)
            && x.cod_tipo_jugador != 6 && x.thandicap_usuario.Any())).ToList();

            var usersSinHandicapPago = usersList.Where(x => x.thistorial_pagos.Any(m => m.fec_vencimiento >= DateTime.Now && m.cod_tipo_pago == 1 && m.nro_resultado_pago == 1)).ToList();

            return usersSinHandicapPago;
        }

        public async Task<List<users>> GetBySelect()
        {
            return (await repository.GetMany(x => x.bool_activo.Value && x.cod_tipo_doc != 6 && x.thandicap_usuario.Any())).ToList();
        }

        public List<userListDTO> GetImportantUsers(int page)
        {

            var misParametros = new List<System.Data.SqlClient.SqlParameter>();
            System.Data.SqlClient.SqlParameter parameterPage = new System.Data.SqlClient.SqlParameter();
            parameterPage.ParameterName = "@numberPage";
            parameterPage.SqlDbType = System.Data.SqlDbType.Int;
            parameterPage.Direction = System.Data.ParameterDirection.Input;
            parameterPage.Value = page;
            misParametros.Add(parameterPage);

            System.Data.SqlClient.SqlParameter parameterPageSize = new System.Data.SqlClient.SqlParameter();
            parameterPageSize.ParameterName = "@pageSize";
            parameterPageSize.SqlDbType = System.Data.SqlDbType.Int;
            parameterPageSize.Direction = System.Data.ParameterDirection.Input;
            parameterPageSize.Value = 12;
            misParametros.Add(parameterPageSize);


            var miObj = apoloContext.ExecuteStoredProcedureBrowar<userListDTO>("sp_get_jugadores_destacados", misParametros.ToArray());

            return (miObj).ToList();
        }

        public async Task<int> CountUsers()
        {
            return (await repository.GetMany(x => x.bool_activo == true && x.thandicap_usuario.Any(r => r.bool_principal == true && r.nro_handicap > 7))).Count();
        }

        public List<userListDTO> GetManyBySP(int page, string nombre, int cod_tipo_jugador, int cod_club, int nro_handicap, int pageSize)
        {
            var misParametros = new List<System.Data.SqlClient.SqlParameter>();
            System.Data.SqlClient.SqlParameter parameterPage = new System.Data.SqlClient.SqlParameter();
            parameterPage.ParameterName = "@numberPage";
            parameterPage.SqlDbType = System.Data.SqlDbType.Int;
            parameterPage.Direction = System.Data.ParameterDirection.Input;
            parameterPage.Value = page;
            misParametros.Add(parameterPage);

            System.Data.SqlClient.SqlParameter parameterPageSize = new System.Data.SqlClient.SqlParameter();
            parameterPageSize.ParameterName = "@pageSize";
            parameterPageSize.SqlDbType = System.Data.SqlDbType.Int;
            parameterPageSize.Direction = System.Data.ParameterDirection.Input;
            parameterPageSize.Value = pageSize;
            misParametros.Add(parameterPageSize);

            if (!string.IsNullOrEmpty(nombre))
            {
                System.Data.SqlClient.SqlParameter parameterNombre = new System.Data.SqlClient.SqlParameter();
                parameterNombre.ParameterName = "@nombre";
                parameterNombre.SqlDbType = System.Data.SqlDbType.VarChar;
                parameterNombre.Direction = System.Data.ParameterDirection.Input;
                parameterNombre.Value = nombre;
                misParametros.Add(parameterNombre);
            }

            System.Data.SqlClient.SqlParameter parameterTipoJugador = new System.Data.SqlClient.SqlParameter();
            parameterTipoJugador.ParameterName = "@codigoJugador";
            parameterTipoJugador.SqlDbType = System.Data.SqlDbType.Int;
            parameterTipoJugador.Direction = System.Data.ParameterDirection.Input;
            parameterTipoJugador.Value = cod_tipo_jugador;
            misParametros.Add(parameterTipoJugador);

            System.Data.SqlClient.SqlParameter parameterClub = new System.Data.SqlClient.SqlParameter();
            parameterClub.ParameterName = "@codClub";
            parameterClub.SqlDbType = System.Data.SqlDbType.Int;
            parameterClub.Direction = System.Data.ParameterDirection.Input;
            parameterClub.Value = cod_club;
            misParametros.Add(parameterClub);

            System.Data.SqlClient.SqlParameter parameterHandicap = new System.Data.SqlClient.SqlParameter();
            parameterHandicap.ParameterName = "@nro_handicap";
            parameterHandicap.SqlDbType = System.Data.SqlDbType.Int;
            parameterHandicap.Direction = System.Data.ParameterDirection.Input;
            parameterHandicap.Value = nro_handicap;
            misParametros.Add(parameterHandicap);

            var miObj = apoloContext.ExecuteStoredProcedureBrowar<userListDTO>("sp_get_jugadores_search", misParametros.ToArray());

            return (miObj).ToList();
        }


        public async Task<List<users>> GetMany(int page, string nombre, int cod_tipo_jugador, int cod_club, int nro_handicap)
        {
            var usersSearch = (await repository.GetMany(x =>
            ((x.txt_nombre + " " + x.txt_apellido).Contains(nombre) || string.IsNullOrEmpty(nombre))
            && (x.cod_tipo_jugador == cod_tipo_jugador || cod_tipo_jugador == 0) && (x.tuser_club.Any(p => p.cod_club == cod_club) || cod_club == 0) &&
            (x.thandicap_usuario.Any(r => r.nro_handicap == nro_handicap) || nro_handicap == -1))).ToList();

            foreach (var item in usersSearch)
            {
                if (item.thandicap_usuario.Count > 0)
                    item.nro_hand = item.thandicap_usuario.FirstOrDefault().nro_handicap;
            }


            return usersSearch;
        }

        public async Task<int> Save(users usuario)
        {
            usuario.fec_alta = DateTime.Now;
            var domicilio = await domicilioService.GetByCondition(usuario.tdomicilio);
            if (domicilio == null)
            {
                await domicilioService.Save(usuario.tdomicilio);
                domicilio = await domicilioService.GetByCondition(usuario.tdomicilio);
            }
            else
                await domicilioService.Update(usuario.tdomicilio);

            usuario = await PopularUsuarioGuardar(usuario);
            usuario.tdomicilio = domicilio;
            usuario.txt_password = encrypt.ToSHA256(usuario.nro_doc.Trim());
            repository.Add(usuario);
            await repository.SaveChangesAsync();
            var historico = PopularUsuarioHistorico(new users_historico(), usuario, "test");
            historico.tdomicilio = domicilio;
            await usuarioHistoricoService.Save(historico);

            var listaUser = new List<users>();
            listaUser.Add(usuario);
            await CambiarHandicap(listaUser);
            apoloContext.Database.ExecuteSqlCommand(@"exec [dbo].[sp_upd_jugadorID] @cod_usuario=" + usuario.cod_usuario);
            return 1;
        }

        public void ActualizaArticulo(int cod_usuario)
        {
            apoloContext.Database.ExecuteSqlCommand(@"exec [dbo].[sp_upd_jugadorID] @cod_usuario=" + cod_usuario);
            repository.SaveChanges();
        }

        public async Task<int> Update(users usuario)
        {
            var usuarioAModificar = repository.GetById(usuario.cod_usuario);
            var domicilio = await domicilioService.GetByCondition(usuario.tdomicilio);
            if (domicilio == null)
            {
                await domicilioService.Save(usuario.tdomicilio);
                domicilio = await domicilioService.GetByCondition(usuario.tdomicilio);
            }
            else
                await domicilioService.Update(usuario.tdomicilio);

            usuarioAModificar.cod_domicilio = domicilio.cod_domicilio;
            usuarioAModificar.tdomicilio = domicilio;

            usuarioAModificar = await PopularUsuario(usuarioAModificar, usuario);

            repository.Update(usuarioAModificar);
            await repository.SaveChangesAsync();

            var historico = PopularUsuarioHistorico(new users_historico(), usuarioAModificar, "test");
            historico.tdomicilio = domicilio;
            await usuarioHistoricoService.Save(historico);

            return 1;
        }

        public async Task<int> Delete(int cod_user)
        {
            var usuarioABorrar = repository.GetById(cod_user);
            usuarioABorrar.bool_activo = !usuarioABorrar.bool_activo;
            repository.Update(usuarioABorrar);
            await repository.SaveChangesAsync();
            return 1;
        }

        public async Task<users> GetByDocumentAndPassword(users user)
        {
            string passwordHashed = encrypt.ToSHA256(user.txt_password);

            return (await repository.GetByCondition(x => x.txt_password == passwordHashed && x.bool_activo == true && x.nro_doc == user.nro_doc));
        }

        public async Task<users> GetByDocument(string nro_doc)
        {
            return (await repository.GetByCondition(x => x.nro_doc == nro_doc));
        }


        public async Task<int> ActualizarContrasena(users usuario)
        {
            var usuarioAModificar = await repository.GetByCondition(x => x.nro_doc == usuario.nro_doc);
            if (usuarioAModificar == null)
                return 0;

            usuarioAModificar.txt_password = encrypt.ToSHA256(usuario.txt_password.Trim());

            repository.Update(usuarioAModificar);
            await repository.SaveChangesAsync();

            return usuarioAModificar.cod_usuario;
        }

        public async Task<int> AceptarTerminos(int cod_usuario, users usuario)
        {
            var usuarioAModificar = repository.GetById(cod_usuario);

            var contactos = await contactoUsuarioService.ObtenerContactosUsuario(usuarioAModificar.tcontacto_usuario.ToList());
            await contactoUsuarioService.BorrarPorUsuario(usuarioAModificar.cod_usuario);
            usuarioAModificar.tcontacto_usuario = usuario.tcontacto_usuario;

            usuarioAModificar.bool_acepta_terminos = true;
            repository.Update(usuarioAModificar);
            await repository.SaveChangesAsync();
            return 1;
        }


        public users GetById(int cod_user)
        {
            return repository.GetById(cod_user);
        }

        public async Task<List<thistorial_pagos>> GetHistorialPagos(int cod_user)
        {
            return (await historialPagosService.GetMany(x => (x.cod_usuario == cod_user || x.cod_usuario_alta == cod_user) && x.nro_resultado_pago == 1)).ToList();
        }

        public users_historico PopularUsuarioHistorico(users_historico usuarioHistorico, users usuario, string user)
        {
            usuarioHistorico.fec_alta_historico = DateTime.Now;
            usuarioHistorico.usuario_alta_historico = user;
            usuarioHistorico.ttipo_doc = tipoDocumentoService.GetById(Convert.ToInt32(usuario.cod_tipo_doc));
            usuarioHistorico.ttipo_jugador = tipoJugadorService.GetById(usuario.cod_tipo_jugador);
            usuarioHistorico.bool_activo = usuario.bool_activo;
            usuarioHistorico.cant_intentos_acceso = usuario.cant_intentos_acceso;
            usuarioHistorico.bool_activo = usuario.bool_activo;
            usuarioHistorico.txt_apellido = usuario.txt_apellido;
            usuarioHistorico.txt_nombre = usuario.txt_nombre;
            usuarioHistorico.tgenero = generoService.GetById(usuario.cod_genero);
            usuarioHistorico.cod_genero = usuario.cod_genero;
            usuarioHistorico.txt_password = usuario.txt_password.Trim();
            usuarioHistorico.url_foto = usuario.url_foto;
            usuarioHistorico.nro_doc = usuario.nro_doc;
            usuarioHistorico.usuario_alta = usuario.usuario_alta;
            usuarioHistorico.fec_alta = usuario.fec_alta;
            usuarioHistorico.cod_usuario = usuario.cod_usuario;
            usuarioHistorico.txt_facebook = usuario.txt_facebook;
            usuarioHistorico.txt_twitter = usuario.txt_twitter;
            usuarioHistorico.txt_instagram = usuario.txt_instagram;
            usuarioHistorico.tdomicilio = usuario.tdomicilio;

            return usuarioHistorico;
        }

        public async Task<users> PopularUsuario(users usuarioAModificar, users usuario)
        {
            var listaClubes = new List<tuser_club>();
            usuarioAModificar.fec_ult_modificacion = DateTime.Now;
            usuarioAModificar.ttipo_doc = tipoDocumentoService.GetById(Convert.ToInt32(usuario.cod_tipo_doc));
            usuarioAModificar.cod_tipo_doc = usuarioAModificar.ttipo_doc.cod_tipo_doc;
            usuarioAModificar.cant_intentos_acceso = usuario.cant_intentos_acceso;
            usuarioAModificar.cod_estado = usuario.cod_estado;
            usuarioAModificar.bool_activo = usuario.bool_activo;
            usuarioAModificar.txt_apellido = usuario.txt_apellido;
            usuarioAModificar.txt_nombre = usuario.txt_nombre;
            usuarioAModificar.txt_password = usuario.txt_password.Trim();
            usuarioAModificar.url_foto = usuario.url_foto;
            usuarioAModificar.nro_doc = usuario.nro_doc;
            usuarioAModificar.usuario_alta = usuario.usuario_alta;
            usuarioAModificar.txt_facebook = usuario.txt_facebook;
            usuarioAModificar.fec_nacimiento = usuario.fec_nacimiento;
            usuarioAModificar.tgenero = generoService.GetById(usuario.cod_genero);
            usuarioAModificar.txt_instagram = usuario.txt_instagram;
            usuarioAModificar.txt_twitter = usuario.txt_twitter;
            usuarioAModificar.cod_tipo_jugador = usuario.cod_tipo_jugador;
            usuarioAModificar.ttipo_jugador = tipoJugadorService.GetById(usuario.cod_tipo_jugador);
            usuarioAModificar.txt_imagen = usuario.txt_imagen;
            usuarioAModificar.bool_activo = usuario.bool_activo;
            usuarioAModificar.bool_veedor = usuario.bool_veedor;
            usuarioAModificar.txt_tarjeta = usuario.txt_tarjeta;
            if (usuario.cod_nacionalidad.HasValue)
            {
                usuarioAModificar.tpais = paisService.GetById(usuario.cod_nacionalidad.Value);
                usuarioAModificar.cod_nacionalidad = usuario.cod_nacionalidad;
            }
            else
            {
                usuarioAModificar.tpais = paisService.GetById(33);
                usuarioAModificar.cod_nacionalidad = 33;

            }

            foreach (var item in usuario.tcontacto_usuario)
            {
                if (!string.IsNullOrEmpty(item.tcontacto.txt_desc))
                {
                    if (item.tcontacto.cod_tipo_contacto == 1)
                    {
                        var contacto = usuarioAModificar.tcontacto_usuario.FirstOrDefault(x => x.tcontacto.cod_tipo_contacto == 1);
                        if (contacto != null)
                        {
                            contacto.tcontacto.txt_desc = item.tcontacto.txt_desc;
                            await contactoService.Update(contacto.tcontacto);
                        }
                        else
                        {
                            item.cod_usuario = usuarioAModificar.cod_usuario;
                            await contactoUsuarioService.SaveOne(item);
                        }
                    }
                    else
                    {
                        var contacto = usuarioAModificar.tcontacto_usuario.FirstOrDefault(x => x.tcontacto.cod_tipo_contacto == 3);
                        if (contacto != null)
                        {
                            contacto.tcontacto.txt_desc = item.tcontacto.txt_desc;
                            await contactoService.Update(contacto.tcontacto);
                        }
                        else
                        {
                            item.cod_usuario = usuarioAModificar.cod_usuario;
                            await contactoUsuarioService.SaveOne(contacto);
                        }
                    }
                }
            }

            var clubes = usuario.tuser_club.ToList();

            foreach (var club in clubes)
            {
                if (club.cod_club.HasValue)
                {
                    club.tclub = clubService.GetById(club.cod_club.Value);
                    club.users = usuarioAModificar;
                    club.fec_alta = DateTime.Now;
                    club.fec_vigencia = DateTime.Now.AddYears(1);
                    club.cod_usuario = usuarioAModificar.cod_usuario;
                    listaClubes.Add(club);
                }
            }

            usuarioAModificar.tuser_club = listaClubes;

            await usuarioClubService.BorrarTodosPorJugadorId(usuarioAModificar.cod_usuario);
            await handicapUsuarioService.BorrarTodosPorJugadorId(usuario.cod_usuario);

            List<thandicap_usuario> usuarioHandicap = new List<thandicap_usuario>();

            foreach (var handicap in usuario.thandicap_usuario)
            {
                var nuevoHandicap = handicapUsuarioService.PopulateHandicapUsuario(handicap);

                usuarioHandicap.Add(nuevoHandicap);
            }

            usuarioAModificar.thandicap_usuario = usuarioHandicap;

            var listaUser = new List<users>();
            listaUser.Add(usuarioAModificar);
            await CambiarHandicap(listaUser);

            return usuarioAModificar;
        }

        public async Task<users> PopularUsuarioGuardar(users usuario)
        {
            var listaClubes = new List<tuser_club>();
            usuario.ttipo_doc = tipoDocumentoService.GetById(Convert.ToInt32(usuario.cod_tipo_doc));
            usuario.ttipo_jugador = tipoJugadorService.GetById(usuario.cod_tipo_jugador);
            usuario.bool_activo = true;
            usuario.tgenero = generoService.GetById(usuario.cod_genero);
            var contactos = usuario.tcontacto_usuario;
            usuario.tcontacto_usuario = await contactoUsuarioService.ObtenerContactosUsuario(usuario.tcontacto_usuario.ToList());
            usuario.tpais = paisService.GetById(usuario.cod_nacionalidad.Value);
            foreach (var club in usuario.tuser_club)
            {
                club.tclub = clubService.GetById(club.cod_club.Value);
                club.fec_alta = DateTime.Now;
                listaClubes.Add(club);
            }

            var handicaps = new List<thandicap_usuario>();

            foreach (var item in usuario.thandicap_usuario)
            {
                var nuevoHandicap = handicapUsuarioService.PopulateHandicapUsuario(item);

                handicaps.Add(nuevoHandicap);
            }

            usuario.thandicap_usuario = handicaps;

            usuario.tuser_club = listaClubes;
            usuario.tpais = paisService.GetById(usuario.cod_nacionalidad.Value);
            return usuario;
        }

        public async Task<thandicap_usuario_historico> GetHistoricoSinPago(int id)
        {
            var user = repository.GetById(id);
            var historialPago = (await historialPagosService.GetMany(x => (x.cod_usuario == id || x.cod_usuario_alta == id) && x.cod_tipo_pago == 1)).LastOrDefault();
            var handicapPrincipal = user.thandicap_usuario.Where(p => p.bool_principal.Value == true).ToList()[0];
            if (historialPago.nro_resultado_pago != 1)
            {
                var historicos = (await handicapHistoricoService.GetMany(x => x.cod_usuario == id)).OrderBy(x=> x.cod_handicap_usuario_historico).ToList();

                return historicos.Where(x=> x.cod_handicap != handicapPrincipal.cod_handicap).LastOrDefault();
            }
            else {
                return null;
            }
        }

        public async Task CambiarHandicap(List<users> usuarios)
        {
            foreach (var usuario in usuarios)
            {
                var usuarioAModificar = repository.GetById(usuario.cod_usuario);

                List<thandicap_usuario> usuarioHandicap = new List<thandicap_usuario>();

                foreach (var handicap in usuario.thandicap_usuario)
                {
                    var nuevoHand = handicapUsuarioService.GetById(handicap.cod_handicap_usuario);
                    var nuevoHandicap = new thandicap_usuario();
                    if (nuevoHand != null)
                    {
                        nuevoHandicap = handicapUsuarioService.PopulateHandicapUsuario(nuevoHand);
                    }
                    else
                    {
                        nuevoHandicap = handicapUsuarioService.PopulateHandicapUsuario(handicap);
                    }
                    nuevoHandicap.fec_desde = DateTime.Now;
                    if (!nuevoHandicap.thandicap.txt_vigencia.Contains("Anual"))
                    {
                        if (DateTime.Now.Month < 12)
                        {
                            nuevoHandicap.fec_hasta = new DateTime(DateTime.Now.Year, (DateTime.Now.Month + 1), DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month + 1));
                        }
                        else
                        {
                            nuevoHandicap.fec_hasta = new DateTime(DateTime.Now.Year, (DateTime.Now.Month), DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                        }
                    }
                    else
                    {
                        nuevoHandicap.fec_hasta = new DateTime(DateTime.Now.Year, 12, 31);
                    }
                    nuevoHandicap.cod_usuario = usuario.cod_usuario;
                    nuevoHandicap.bool_principal = handicap.bool_principal;
                    nuevoHandicap.users = usuarioAModificar;
                    usuarioHandicap.Add(nuevoHandicap);
                    var tieneSecundario = usuarioAModificar.thandicap_usuario.Where(c => !c.bool_principal.Value).FirstOrDefault();
                    if (tieneSecundario != null)
                    {
                        var handicapSecundario = new thandicap_usuario();

                        handicapSecundario.bool_principal = false;
                        handicapSecundario.cod_handicap = tieneSecundario.cod_handicap;
                        handicapSecundario.fec_desde = tieneSecundario.fec_desde;
                        handicapSecundario.fec_hasta = tieneSecundario.fec_hasta;
                        handicapSecundario.nro_handicap = tieneSecundario.nro_handicap;
                        handicapSecundario.thandicap = tieneSecundario.thandicap;
                        handicapSecundario.users = tieneSecundario.users;
                        usuarioHandicap.Add(handicapSecundario);
                    }
                }
                await handicapUsuarioService.BorrarTodosPorJugadorId(usuarioAModificar.cod_usuario);
                usuarioAModificar.thandicap_usuario = usuarioHandicap;
                repository.Update(usuarioAModificar);

                await repository.SaveChangesAsync();

                foreach (var item in usuarioAModificar.thandicap_usuario)
                {
                    var historico = PopularHandicapHistorico(item);
                    handicapHistoricoService.Add(historico);
                }
            }

            // TO DO: FALTA AGREGAR EL HISTORICO

            await repository.SaveChangesAsync();
        }

        public async Task AsociarClubes(users usuarioExistente)
        {
            var listaClubes = new List<tuser_club>();
            foreach (var club in usuarioExistente.tuser_club)
            {
                club.tclub = clubService.GetById(club.cod_club.Value);
                listaClubes.Add(club);
            }

            await repository.SaveChangesAsync();
        }

        public async Task AsociarContactos(users usuarioExistente)
        {
            var usuarioAModificar = repository.GetById(usuarioExistente.cod_usuario);
            usuarioAModificar.tcontacto_usuario = usuarioExistente.tcontacto_usuario;

            await repository.SaveChangesAsync();
        }

        public thandicap_usuario_historico PopularHandicapHistorico(thandicap_usuario handicapUsuario)
        {
            var historico = new thandicap_usuario_historico();
            historico.bool_principal = handicapUsuario.bool_principal;
            historico.cod_handicap = handicapUsuario.cod_handicap;
            historico.cod_handicap_usuario = handicapUsuario.cod_handicap_usuario;
            historico.fec_desde = handicapUsuario.fec_desde;
            historico.fec_hasta = handicapUsuario.fec_hasta;
            historico.fec_ult_modificacion = DateTime.Now;
            historico.nro_handicap = handicapUsuario.nro_handicap;
            historico.thandicap = handicapService.GetById(handicapUsuario.cod_handicap);
            historico.cod_usuario = handicapUsuario.cod_usuario;
            historico.users = repository.GetById(handicapUsuario.cod_usuario);

            return historico;
        }

        public async Task UpdatePlatformAndRegister(users usuario)
        {
            repository.Update(usuario);
            await repository.SaveChangesAsync();
        }

        public async Task<List<users>> GetVeedores()
        {
            return (await repository.GetMany(x => x.bool_veedor.Value)).ToList();
        }

        public async Task<List<users>> GetByClub(int idClub)
        {
            return (await repository.GetMany(x => x.tuser_club.Any(p => p.cod_club == idClub))).ToList();
        }

        public async Task BlanquearContrasena(int cod_usuario)
        {
            var usuarioAModificar = repository.GetById(cod_usuario);
            usuarioAModificar.txt_password = encrypt.ToSHA256(usuarioAModificar.nro_doc.Trim());

            repository.Update(usuarioAModificar);
            await repository.SaveChangesAsync();
        }
    }
}
