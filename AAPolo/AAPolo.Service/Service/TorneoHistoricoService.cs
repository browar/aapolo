﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TorneoHistoricoService : ITorneoHistoricoService
    {
        private readonly IBaseRepository<ttorneo_historico> _repository;
        private readonly ITipoTorneoService tipoTorneoService;
        private readonly ICategoriaTorneoService categoriaTorneoService;

        public TorneoHistoricoService(IBaseRepository<ttorneo_historico> repository, ITipoTorneoService _tipoTorneoService,
            ICategoriaTorneoService _categoriaTorneoService)
        {
            _repository = repository;
            tipoTorneoService = _tipoTorneoService;
            categoriaTorneoService = _categoriaTorneoService;
        }

        public TorneoHistoricoService(IBaseRepository<ttorneo_historico> repository)
        {
            _repository = repository;
        }

        public async Task<List<ttorneo_historico>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public void Save(ttorneo_historico torneoHistorico)
        {
            torneoHistorico.tcategoria_torneo = categoriaTorneoService.GetById(torneoHistorico.cod_categoria_torneo);
            torneoHistorico.cod_categoria_torneo = torneoHistorico.tcategoria_torneo.cod_categoria_torneo;
            _repository.Add(torneoHistorico);
        }

        public ttorneo_historico GetById(int codHistorico)
        {
            return _repository.GetById(codHistorico);
        }
    }
}
