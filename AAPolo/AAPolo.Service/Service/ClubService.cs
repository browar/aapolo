﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class ClubService : IClubService
    {
        private readonly IBaseRepository<tclub> _repository;
        private readonly IDomicilioService domicilioService;
        private readonly ICategoriaClubService categoriaClubService;
        private readonly IContactoService contactoService;
        private readonly IClubHistoricoService clubHistoricoService;
        private readonly IClubContactoService clubContactoService;
        private readonly IBaseRepository<thistorial_pagos> historialPagoService;
        private readonly IAAPoloContext _iAAPoloContext;

        public ClubService(IBaseRepository<tclub> repository, IDomicilioService _domicilioService,
            ICategoriaClubService _categoriaClubService, IContactoService _contactoService, IClubHistoricoService _clubHistoricoService,
            IClubContactoService _clubContactoService, IBaseRepository<thistorial_pagos> _historialPagoService, IAAPoloContext iAAPoloContext)
        {
            _repository = repository;
            domicilioService = _domicilioService;
            categoriaClubService = _categoriaClubService;
            contactoService = _contactoService;
            clubHistoricoService = _clubHistoricoService;
            clubContactoService = _clubContactoService;
            historialPagoService = _historialPagoService;
            _iAAPoloContext = iAAPoloContext;
        }

        public async Task<List<tclub>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public List<clubSelect> GetClubesPagos()
        {
            var miObj = _iAAPoloContext.ExecuteStoredProcedureBrowar<clubSelect>("sp_get_clubes_pagos");
            return (miObj).ToList();
        }

        public async Task<List<thistorial_pagos>> GetByCodClub(int cod_club)
        {
            return (await historialPagoService.GetMany(x => x.cod_club_alta == cod_club)).ToList();
        }

        public async Task<int> Save(tclub club)
        {
            club.fec_alta = DateTime.Now;
            var domicilio = await domicilioService.GetByCondition(club.tdomicilio);
            if (domicilio == null)
            {
                await domicilioService.Save(club.tdomicilio);
                domicilio = await domicilioService.GetByCondition(club.tdomicilio);
            }
            else
            {
                await domicilioService.Update(club.tdomicilio);
            }
            club.tcategoria_club = categoriaClubService.GetById(club.cod_categoria_club.Value);
            club.tdomicilio = domicilio;
            _repository.Add(club);
            await _repository.SaveChangesAsync();

            var clubHistorico = new tclub_historico();
            clubHistorico = PopularClubHistorico(clubHistorico, club, club.usuario_alta);
            clubHistoricoService.Save(clubHistorico);

            await _repository.SaveChangesAsync();
            return 1;
        }

        public async Task<int> UpdateAdminClub(int adminClubId, int clubId)
        {
            if (adminClubId > 0)
            {
                var clubs = (await _repository.GetMany(x => x.cod_admin_club == adminClubId));
                foreach (var club in clubs)
                {
                    club.cod_admin_club = null;

                    _repository.Update(club);
                }

                var item= _repository.GetById(clubId);
                item.cod_admin_club = adminClubId;

                _repository.Update(item);
            }
            else
            {
                var club = _repository.GetById(clubId);
                club.cod_admin_club = null;

                _repository.Update(club);
            }

            await _repository.SaveChangesAsync();
            return 1;
        }

        public async Task<int> Update(tclub club)
        {
            var clubAModificar = _repository.GetById(club.cod_club);
            var domicilio = await domicilioService.GetByCondition(club.tdomicilio);
            if (await domicilioService.GetByCondition(club.tdomicilio) == null)
            {
                await domicilioService.Save(club.tdomicilio);
            }
            else
                await domicilioService.Update(club.tdomicilio);

            domicilio = await domicilioService.GetByCondition(club.tdomicilio);
            clubAModificar = await PopularClub(clubAModificar, club);
            club.tdomicilio = domicilio;
            var clubHistorico = new tclub_historico();
            clubHistorico = PopularClubHistorico(clubHistorico, clubAModificar, club.usuario_alta);
            clubHistoricoService.Save(clubHistorico);

            _repository.Update(clubAModificar);
            await _repository.SaveChangesAsync();
            return 1;
        }

        public async Task<int> Delete(int cod_club)
        {
            var clubABorrar = _repository.GetById(cod_club);
            clubABorrar.bool_activo = !clubABorrar.bool_activo;
            _repository.Update(clubABorrar);
            await _repository.SaveChangesAsync();
            return 1;
        }

        public tclub GetById(int cod_club)
        {
            return _repository.GetById(cod_club);
        }

        public tclub_historico PopularClubHistorico(tclub_historico clubHistorico, tclub club, string user)
        {
            // TODO : verificar si estan los campos correctos luego de los cambios en la base

            clubHistorico.tcategoria_club = categoriaClubService.GetById(Convert.ToInt32(club.cod_categoria_club));
            clubHistorico.cod_categoria_club = clubHistorico.tcategoria_club.cod_categoria_club;
            clubHistorico.tdomicilio = club.tdomicilio;
            clubHistorico.nro_canchas = club.nro_canchas;
            clubHistorico.cod_club_desc = club.cod_club_desc;
            clubHistorico.txt_desc = club.txt_desc;
            clubHistorico.txt_presidente = club.txt_presidente;
            clubHistorico.txt_vicepresidente = club.txt_vicepresidente;
            clubHistorico.txt_vocal1 = club.txt_vocal1;
            clubHistorico.txt_vocal2 = club.txt_vocal2;
            clubHistorico.txt_vocal3 = club.txt_vocal3;
            clubHistorico.txt_tesorero = club.txt_tesorero;
            clubHistorico.usuario_alta = club.usuario_alta;
            clubHistorico.cod_club = club.cod_club;
            clubHistorico.usuario_alta_historico = user;
            clubHistorico.tdomicilio = club.tdomicilio;
            return clubHistorico;
        }

        public async Task<tclub> PopularClub(tclub clubAModificar, tclub club)
        {
            clubAModificar.tcategoria_club = categoriaClubService.GetById(Convert.ToInt32(club.cod_categoria_club));
            clubAModificar.cod_categoria_club = clubAModificar.tcategoria_club.cod_categoria_club;
            clubAModificar.nro_canchas = club.nro_canchas;
            clubAModificar.cod_club_desc = club.cod_club_desc;
            clubAModificar.txt_desc = club.txt_desc;
            clubAModificar.txt_presidente = club.txt_presidente;
            clubAModificar.txt_vicepresidente = club.txt_vicepresidente;
            clubAModificar.txt_vocal1 = club.txt_vocal1;
            clubAModificar.txt_vocal2 = club.txt_vocal2;
            clubAModificar.txt_vocal3 = club.txt_vocal3;
            clubAModificar.usuario_alta = club.usuario_alta;
            clubAModificar.txt_tesorero = club.txt_tesorero;
            clubAModificar.fec_ult_modificacion = DateTime.Now;
            clubAModificar.nro_cant_socios = club.nro_cant_socios;
            clubAModificar.cod_admin_club = club.cod_admin_club;
            clubAModificar.fec_fundacion = club.fec_fundacion;
            clubAModificar.bool_activo = true;
            clubAModificar.txt_imagen = club.txt_imagen;
            var contactos = await clubContactoService.ObtenerContactosClub(club.tcontacto_club.ToList());
            await clubContactoService.BorrarPorClub(clubAModificar.cod_club);
            clubAModificar.tcontacto_club = club.tcontacto_club;

            return clubAModificar;
        }
    }
}
