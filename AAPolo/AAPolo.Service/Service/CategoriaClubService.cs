﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class CategoriaClubService : ICategoriaClubService
    {
        private readonly IBaseRepository<tcategoria_club> repository;

        public CategoriaClubService(IBaseRepository<tcategoria_club> _repository)
        {
            repository = _repository;
        }

        public async Task<List<tcategoria_club>> GetAll()
        {
            return (await repository.GetAll()).ToList();
        }

        public async Task<int> Save(tcategoria_club categoriaClub)
        {
            repository.Add(categoriaClub);
            return await repository.SaveChangesAsync();
        }

        public async Task<int> Update(tcategoria_club categoriaClub)
        {
            var categoriaClubAModificar = repository.GetById(categoriaClub.cod_categoria_club);
            categoriaClubAModificar.txt_desc = categoriaClub.txt_desc;
            categoriaClubAModificar.txt_desc_corta = categoriaClub.txt_desc_corta;
            repository.Update(categoriaClubAModificar);
            return await repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_user)
        {
            var categoriaClubABorrar = repository.GetById(cod_user);
            repository.Delete(categoriaClubABorrar);
            return await repository.SaveChangesAsync();
        }

        public tcategoria_club GetById(int cod_user)
        {
            return repository.GetById(cod_user);
        }
    }
}
