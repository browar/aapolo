﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class ClubContactoService : IClubContactoService
    {
        private readonly IBaseRepository<tcontacto_club> _repository;
        private readonly IContactoService contactoService;
        private readonly IBaseRepository<tclub> clubRepository;

        public ClubContactoService(IBaseRepository<tcontacto_club> repository, IContactoService _contactoService, IBaseRepository<tclub> _clubRepository)
        {
            _repository = repository;
            contactoService = _contactoService;
            clubRepository = _clubRepository;
        }

        public async Task<int> Delete(int cod_contactoClub)
        {
            var equipoABorrar = _repository.GetById(cod_contactoClub);
            _repository.Delete(equipoABorrar);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<tcontacto_club>> GetAll()
        {
            return (await (_repository.GetAll())).ToList();
        }

        public tcontacto_club GetById(int cod_contacto_club)
        {
            return _repository.GetById(cod_contacto_club);
        }

        public async Task<int> Save(List<tcontacto_club> contactosClub)
        {
            foreach (var contactoClub in contactosClub)
            {
                contactoClub.tcontacto = contactoService.GetById(contactoClub.cod_contacto);
                contactoClub.tclub = clubRepository.GetById(contactoClub.cod_club);
                _repository.Add(contactoClub);
            }
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(tcontacto_club contacto)
        {
            var contactoClubAModificar = _repository.GetById(contacto.cod_contacto_club);
            _repository.Update(contacto);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<tcontacto_club>> ObtenerContactosClub(List<tcontacto_club> contactos) {
            var listContactos = new List<tcontacto_club>();
            foreach (var contacto in contactos)
            {
                var contactoId = GetById(contacto.cod_contacto_club);
                if (contactoId == null)
                {
                    var nuevoContacto = (await contactoService.GetByCondition(contacto.tcontacto));
                    if (nuevoContacto != null)
                    {
                        contacto.tcontacto = nuevoContacto;
                        listContactos.Add(contacto);
                    }
                    else
                    {
                        contactoId = contacto;
                        listContactos.Add(contactoId);
                    }
                }
                else
                {
                    listContactos.Add(contacto);
                }
            }

            return listContactos;
        }

        public async Task<int> BorrarPorClub(int cod_club)
        {
            var contactos = await _repository.GetMany(x => x.cod_club == cod_club);
            foreach (var contacto in contactos)
            {
                _repository.Delete(contacto);
            }
            return (await _repository.SaveChangesAsync());
        }
    }
}
