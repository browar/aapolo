﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TetiquetaService : ITetiquetaService
    {
        private readonly IBaseRepository<tetiqueta> _repository;


        public TetiquetaService(IBaseRepository<tetiqueta> repository)
        {
            _repository = repository;
        }

        public async Task<List<tetiqueta>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(tetiqueta etiqueta)
        {
            _repository.Add(etiqueta);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Update(tetiqueta etiqueta)
        {

            var etiquetaAModificar = _repository.GetById(etiqueta.cod_etiqueta);
            etiquetaAModificar.txt_desc = etiqueta.txt_desc;
            _repository.Update(etiquetaAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_noticia)
        {
            var noticiaABorrar = _repository.GetById(cod_noticia);
            _repository.Delete(noticiaABorrar);
            return await _repository.SaveChangesAsync();
        }

        public tetiqueta GetById(int cod_etiqueta)
        {
            return _repository.GetById(cod_etiqueta);
        }

        tnoticia ITetiquetaService.GetById(int cod_noticia)
        {
            throw new NotImplementedException();
        }
    }

}

