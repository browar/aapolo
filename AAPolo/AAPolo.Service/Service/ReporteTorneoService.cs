﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class ReporteTorneoService : IReporteTorneoService
    {
        //private readonly IBaseRepository<reporteCuentaJugadores> _repository;
        private readonly IAAPoloContext _iAAPoloContext;
        public ReporteTorneoService(IAAPoloContext iAAPoloContext)
        {
            //_repository = repository;
            _iAAPoloContext = iAAPoloContext;
        }
        //public async Task<List<reporteTorneo>> GetByID(int id )
        //{
        //    var miResultado = new List<reporteTorneo>();
        //    var misParametros = new List<System.Data.SqlClient.SqlParameter>();
        //    System.Data.SqlClient.SqlParameter parameter = new System.Data.SqlClient.SqlParameter();
        //    parameter.ParameterName = "@cod_torneo";
        //    parameter.SqlDbType = System.Data.SqlDbType.Int;
        //    parameter.Direction = System.Data.ParameterDirection.Input;
        //    parameter.Value = id;
        //    misParametros.Add(parameter);
        //    var miResultadoTemp = _iAAPoloContext.ExecuteStoredProcedureBrowar<temporalTorneo>("sp_get_reporte_inscripciones_torneos", misParametros.ToArray());
        //    List<temporalTorneo> miObj = miResultadoTemp.ToList();
        //    if (miObj.Count() > 0)
        //    {
        //        var misEquipos = miObj.Select(x=>x.cod_equipo).Distinct();
        //        var datosTorneo = new ttorneo { cod_torneo = miObj.First().cod_torneo, txt_desc = miObj.First().Torneo };
        //        reporteTorneo miRepo;
        //        EquipoUserReporte miJugador;
        //        foreach (var i in misEquipos)
        //        {
        //            miRepo = new reporteTorneo();
        //            var miListaDeJugadores = new List<EquipoUserReporte>();
        //            miRepo.Torneo = datosTorneo;
        //            miRepo.Equipo = new tequipo { cod_equipo = i, txt_desc = miObj.Where(x=>x.cod_equipo == i).First().Equipo };
        //            if(miObj.Where(x => x.cod_equipo == i).First().cod_club != null) { 
        //                miRepo.Club = new tclub { cod_club = (int)miObj.Where(x => x.cod_equipo == i).First().cod_club, txt_desc = miObj.Where(x => x.cod_equipo == i).First().Club };
        //            }
        //            else
        //            {
        //                miRepo.Club = new tclub { txt_desc = miObj.Where(x => x.cod_equipo == i).First().Club };
        //            }

        //            foreach(var miEquipo in miObj.Where(x=>x.cod_equipo == i))
        //            {
        //                miJugador = new EquipoUserReporte();
        //                miJugador.cod_usuario = miEquipo.cod_usuario;
        //                miJugador.txt_nombre = miEquipo.NombreJugador;
        //                miJugador.txt_apellido = miEquipo.ApellidoJugador;
        //                miJugador.bool_suplente= miEquipo.bool_suplente;
        //                miJugador.bool_Capitan  = miEquipo.bool_Capitan;
        //                miListaDeJugadores.Add(miJugador);
        //            }
        //            miRepo.Usuarios = miListaDeJugadores;
        //            miRepo.sn_pago = miObj.Where(x => x.cod_equipo == i).First().sn_pago;
        //            miRepo.importe = miObj.Where(x => x.cod_equipo == i).First().importe;
        //            miRepo.fec_ult_pago = miObj.Where(x => x.cod_equipo == i).First().fec_ult_pago;
        //            miRepo.cod_historial_pago = miObj.Where(x => x.cod_equipo == i).First().cod_historial_pago;
        //            miRepo.txt_url_comprobante = miObj.Where(x => x.cod_equipo == i).First().txt_url_comprobante;
        //            miResultado.Add(miRepo);
        //        }
        //    }

        //    return miResultado; // (miObj).ToList();
        //}

        public async Task<List<reporteCuentaTorneo>> GetAll()
        {
            
            var miResultadoTemp =  _iAAPoloContext.ExecuteStoredProcedureBrowar<reporteCuentaTorneo>("sp_get_reporte_equipos_inscriptos");
            return miResultadoTemp.ToList();
        }
        public async Task<List<reporteTorneo>> GetByID(int id)
        {
            var miResultado = new List<reporteTorneo>();
            var misParametros = new List<System.Data.SqlClient.SqlParameter>();
            System.Data.SqlClient.SqlParameter parameter = new System.Data.SqlClient.SqlParameter();
            parameter.ParameterName = "@cod_torneo";
            parameter.SqlDbType = System.Data.SqlDbType.Int;
            parameter.Direction = System.Data.ParameterDirection.Input;
            parameter.Value = id;
            misParametros.Add(parameter);
            var miResultadoTemp = _iAAPoloContext.ExecuteStoredProcedureBrowar<temporalTorneo>("sp_get_reporte_inscripciones_torneos_equipos", misParametros.ToArray());
            List<temporalTorneo> miObj = miResultadoTemp.ToList();
            if (miObj.Count() > 0)
            {
                var misEquipos = miObj.Select(x => x.cod_equipo).Distinct();
                var datosTorneo = new ttorneo { cod_torneo = miObj.First().cod_torneo, txt_desc = miObj.First().Torneo };
                reporteTorneo miRepo;
                EquipoUserReporte miJugador;
                foreach (var i in misEquipos)
                {
                    miRepo = new reporteTorneo();
                    var miListaDeJugadores = new List<EquipoUserReporte>();
                    miRepo.Torneo = datosTorneo;
                    miRepo.Equipo = new tequipo { cod_equipo = i, txt_desc = miObj.Where(x => x.cod_equipo == i).First().Equipo };
                    if (miObj.Where(x => x.cod_equipo == i).First().cod_club != null)
                    {
                        miRepo.Club = new tclub { cod_club = (int)miObj.Where(x => x.cod_equipo == i).First().cod_club, txt_desc = miObj.Where(x => x.cod_equipo == i).First().Club };
                    }
                    else
                    {
                        miRepo.Club = new tclub { txt_desc = miObj.Where(x => x.cod_equipo == i).First().Club };
                    }

                    foreach (var miEquipo in miObj.Where(x => x.cod_equipo == i))
                    {
                        miJugador = new EquipoUserReporte();
                        miJugador.cod_usuario = miEquipo.cod_usuario;
                        miJugador.txt_nombre = miEquipo.NombreJugador;
                        miJugador.txt_apellido = miEquipo.ApellidoJugador;
                        miJugador.bool_suplente = miEquipo.bool_suplente;
                        miJugador.bool_Capitan = miEquipo.bool_Capitan;
                        miJugador.sexo = miEquipo.Sexo;
                        miJugador.email = miEquipo.Email;
                        miJugador.telefono = miEquipo.Telefono;
                        miJugador.handicap_principal = miEquipo.NroHandicapPP;
                        miJugador.handicap_secundario = miEquipo.NroHandicapSec;
                        miListaDeJugadores.Add(miJugador);
                    }
                    miRepo.Usuarios = miListaDeJugadores;
                    miRepo.sn_pago = miObj.Where(x => x.cod_equipo == i).First().sn_pago;
                    miRepo.importe = miObj.Where(x => x.cod_equipo == i).First().importe;
                    miRepo.fec_ult_pago = miObj.Where(x => x.cod_equipo == i).First().fec_ult_pago;
                    miRepo.cod_historial_pago = miObj.Where(x => x.cod_equipo == i).First().cod_historial_pago;
                    miRepo.txt_url_comprobante = miObj.Where(x => x.cod_equipo == i).First().txt_url_comprobante;
                    miResultado.Add(miRepo);
                }
            }

            return miResultado; // (miObj).ToList();
        }
        private class temporalTorneo
        {
            public int cod_torneo { get; set; }
            public string Torneo          { get; set; }
            public int cod_equipo      { get; set; }
            public string Equipo          { get; set; }
            public int? cod_club        { get; set; }
            public string Club            { get; set; }
            public int cod_usuario     { get; set; }
            public string NombreJugador   { get; set; }
            public string ApellidoJugador { get; set; }
            public int? bool_Capitan    { get; set; }
            public int? bool_suplente { get; set; }

            public int? cod_historial_pago { get; set; }
            public string txt_url_comprobante { get; set; }
            public int? sn_pago { get; set; }
            public decimal? importe { get; set; }
            public DateTime? fec_ult_pago { get; set; }
            public string Sexo { get; set; }
            public string Email       { get; set; }
            public string Telefono    { get; set; }
            public int? NroHandicapPP { get; set; }
            public int? NroHandicapSec{ get; set; }

        }


    }
}
