﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class EstadoService : IEstadoService
    {
        private readonly IBaseRepository<testado> _repository;

        public EstadoService(IBaseRepository<testado> repository)
        {
            _repository = repository;
        }

        public async Task<int> Delete(int cod_estado)
        {
            var estadoABorrar = _repository.GetById(cod_estado);
            _repository.Delete(estadoABorrar);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<testado>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public testado GetById(int cod_estado)
        {
            return _repository.GetById(cod_estado);
        }

        public async Task<int> Save(testado estado)
        {
            _repository.Add(estado);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(testado estado)
        {
            var estadoAModificar = _repository.GetById(estado.cod_estado);
            estadoAModificar.txt_desc = estado.txt_desc;
            _repository.Update(estadoAModificar);
            return (await _repository.SaveChangesAsync());
        }
    }
}
