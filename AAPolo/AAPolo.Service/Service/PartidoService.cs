﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class PartidoService : IPartidoService
    {
        private readonly IBaseRepository<tpartido> _repository;

        public PartidoService(IBaseRepository<tpartido> repository)
        {
            _repository = repository;
        }

        public async Task DeleteByTorneo(int cod_torneo)
        {
            var partidos = (await _repository.GetMany(x => x.cod_torneo == cod_torneo)).ToList();
            if (partidos.Count > 0)
            {
                var count = partidos.Count();
                for (int i = count; i > 0; i--)
                {
                    _repository.Delete(partidos[i-1]);
                }

                await _repository.SaveChangesAsync();
            }
        }

        public tpartido GetById(int cod_partido)
        {
            return _repository.GetById(cod_partido);
        }

        public async Task<List<tpartido>> GetByTorneo(int cod_torneo)
        {
            return (await _repository.GetMany(x => x.cod_torneo == cod_torneo)).ToList();
        }
    }
}
