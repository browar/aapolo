﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TipoEventoService : ITipoEventoService
    {
        private readonly IBaseRepository<ttipo_evento> _repository;

        public TipoEventoService(IBaseRepository<ttipo_evento> repository)
        {
            _repository = repository;
        }

        public async Task<int> Delete(int cod_tipo_evento)
        {
            var tipoEventoABorrar = _repository.GetById(cod_tipo_evento);
            _repository.Delete(tipoEventoABorrar);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<ttipo_evento>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public ttipo_evento GetById(int cod_tipo_evento)
        {
            return _repository.GetById(cod_tipo_evento);
        }

        public async Task<int> Save(ttipo_evento tipoEvento)
        {
            _repository.Add(tipoEvento);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(ttipo_evento tipoEvento)
        {
            var tipoEventoAModificar = _repository.GetById(tipoEvento.cod_tipo_evento);
            tipoEventoAModificar.txt_desc = tipoEvento.txt_desc;
            _repository.Update(tipoEventoAModificar);
            return (await _repository.SaveChangesAsync());
        }
    }
}
