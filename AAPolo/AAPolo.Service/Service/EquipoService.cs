﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class EquipoService : IEquipoService
    {
        private readonly IBaseRepository<tequipo> _repository;
        private readonly ITorneoEquipoService torneoEquipoService;
        private readonly IBaseRepository<ttorneo> torneoService;
        private readonly IEstadoService estadoService;
        private readonly IBaseRepository<users> usuarioService;
        private readonly IClubService clubService;

        public EquipoService(IBaseRepository<tequipo> repository, ITorneoEquipoService _torneoEquipoService, IBaseRepository<ttorneo> _torneoService,
            IEstadoService _estadoService, IBaseRepository<users> _usuarioService, IClubService _clubService)
        {
            _repository = repository;
            torneoEquipoService = _torneoEquipoService;
            torneoService = _torneoService;
            estadoService = _estadoService;
            usuarioService = _usuarioService;
            clubService = _clubService;
        }

        public async Task<int> Delete(int cod_equipo)
        {
            var equipoABorrar = _repository.GetById(cod_equipo);
            _repository.Delete(equipoABorrar);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<tequipo>> GetAll()
        {
            return (await (_repository.GetAll())).ToList();
        }

        public tequipo GetById(int cod_equipo)
        {
            return _repository.GetById(cod_equipo);
        }

        public async Task<List<tequipo>> GetByUser(int? cod_equipo)
        {
            return (await (_repository.GetMany(x => x.cod_equipo == cod_equipo))).ToList();
        }

        public async Task<int> Save(tequipo equipo)
        {
            var noExiste = await GetByDescripcionAndTorneo(equipo);
            if (noExiste)
            {
                equipo = PopularEquipo(equipo);
                _repository.Add(equipo);
                await _repository.SaveChangesAsync();
            }
            return 1;
        }

        public async Task<bool> GetByDescripcionAndTorneo(tequipo equipo)
        {
            var exist = await _repository.GetByCondition(x => x.txt_desc == equipo.txt_desc && x.cod_torneo == equipo.cod_torneo);

            if (exist == null)
                return true;
            else
                return false;
        }

        public tequipo PopularEquipo(tequipo equipo)
        {
            equipo.ttorneo = torneoService.GetById(equipo.cod_torneo);
            equipo.fec_alta = DateTime.Now;
            if (equipo.cod_club.HasValue)
                equipo.tclub = clubService.GetById(equipo.cod_club.Value);
            var tequipoUsers = new List<tequipo_user>();

            foreach (var equipoUser in equipo.tequipo_user)
            {
                var newEquipo = equipoUser;
                newEquipo.testado = estadoService.GetById(equipoUser.cod_estado);
                newEquipo.tequipo = equipo;
                newEquipo.users = usuarioService.GetById(equipoUser.cod_usuario);
                newEquipo.cod_torneo = equipo.cod_torneo;
            }

            return equipo;
        }

        public async Task<int> Update(tequipo equipo)
        {
            var equipoAModificar = _repository.GetById(equipo.cod_equipo);
            equipoAModificar.fec_ultima_modificacion = DateTime.Now;
            equipoAModificar.txt_ultima_modificación = equipo.txt_ultima_modificación;
            equipoAModificar.tclub = clubService.GetById(equipo.cod_club.Value);
            equipoAModificar.cod_club = equipo.cod_club.Value;
            _repository.Update(equipo);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<List<tequipo>> GetByUser(int cod_usuario)
        {
            var equipo = (await _repository.GetMany(x => x.tequipo_user.Any(p => p.cod_usuario == cod_usuario))).ToList();
            return equipo;
        }

        public async Task PagarEquipo(int cod_usuario, int cod_torneo, int nro_resultado_pago)
        {
            var equipo = (await _repository.GetMany(x => x.cod_torneo == cod_torneo && x.tequipo_user.Any(p => p.cod_usuario == cod_usuario))).LastOrDefault();

            foreach (var item in equipo.tequipo_user)
            {
                item.cod_estado = 1;
                await torneoEquipoService.Update(item);
            }

        }

        public async Task PagarEquipoByIdEquipo(int cod_equipo)
        {
            var equipo = (await _repository.GetMany(x => x.cod_equipo == cod_equipo)).LastOrDefault();

            foreach (var item in equipo.tequipo_user)
            {
                item.cod_estado = 1;
                await torneoEquipoService.Update(item);
            }

        }


        public async Task<bool> GetByTorneoYCodUsuario(int cod_torneo, int cod_usuario)
        {
            var user = (await _repository.GetByCondition(x => x.cod_torneo == cod_torneo && x.tequipo_user.Any(p => p.cod_usuario == cod_usuario)));
            if (user != null)
                return true;
            else
                return false;
        }
    }
}
