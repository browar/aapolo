﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class UsuarioClubService : IUsuarioClubService
    {
        private readonly IBaseRepository<tuser_club> repository;
        private readonly IJugadorRepository jugadorRepository;

        public UsuarioClubService(IBaseRepository<tuser_club> _repository, IJugadorRepository _jugadorRepository)
        {
            repository = _repository;
            jugadorRepository = _jugadorRepository;
        }

        public async Task<bool> BorrarTodosPorJugadorId(int jugadorId)
        {
            // TODO : Cambiar esto para que no afecte el index cuando se elimina los jugadoresClub.
            var jugadorClubes = await GetMany(jugadorId);
            foreach (var jugadorClub in jugadorClubes)
            {
                repository.Delete(jugadorClub);
            }

            await repository.SaveChangesAsync();
            return true;

        }

        public async Task<List<tuser_club>> GetMany(int jugadorId)
        {
            return (await repository.GetMany(x => x.cod_usuario == jugadorId)).ToList();
        }

        public async Task<List<tuser_club>> GetManyByClubId(int clubId)
        {
            return (await repository.GetMany(x => x.cod_club == clubId)).ToList();
        }

        public async Task AceptarRechazar(int idUsuario, bool opcion)
        {
            var usuarioAModificar = repository.GetById(idUsuario);
            usuarioAModificar.bool_rechazado = opcion;
            repository.Update(usuarioAModificar);
            await repository.SaveChangesAsync();
        }

    }
}
