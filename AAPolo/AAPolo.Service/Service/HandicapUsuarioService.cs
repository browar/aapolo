﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class HandicapUsuarioService : IHandicapUsuarioService
    {
        private readonly IBaseRepository<thandicap_usuario> _repository;
        private readonly ICategoriaHandicapService categoriaHandicapService;
        private readonly IHandicapService handicapService;
        private readonly IBaseRepository<thandicap_usuario_historico> historicoRepository;


        public HandicapUsuarioService(IBaseRepository<thandicap_usuario> repository, ICategoriaHandicapService _categoriaHandicapService,
            IHandicapService _handicapService, IBaseRepository<thandicap_usuario_historico> _historicoRepository)
        {
            categoriaHandicapService = _categoriaHandicapService;
            handicapService = _handicapService;
            _repository = repository;
            historicoRepository = _historicoRepository;
        }

        public async Task<List<thandicap_usuario>> GetMany(int cod_usuario)
        {
            return (await _repository.GetMany(x => x.cod_usuario == cod_usuario)).ToList();
        }

        public async Task<int> BorrarTodosPorJugadorId(int cod_usuario)
        {
            var handicaps = await GetMany(cod_usuario);
            foreach (var handicap in handicaps)
            {
                _repository.Delete(handicap);
            }

            await _repository.SaveChangesAsync();

            return 1;
        }

        public thandicap_usuario PopulateHandicapUsuario(thandicap_usuario handicapUsuario)
        {
            handicapUsuario.thandicap = handicapService.GetById(handicapUsuario.cod_handicap);
            handicapUsuario.thandicap.tcategoria_handicap = categoriaHandicapService.GetById(handicapUsuario.thandicap.cod_categoria_handicap.Value);
            return handicapUsuario;
        }

        public async Task<List<thandicap_usuario_historico>> GetHistoricos(int cod_usuario, bool bool_principal)
        {
            return (await historicoRepository.GetMany(x => x.cod_usuario == cod_usuario && x.bool_principal == bool_principal)).ToList();
        }

        public thandicap_usuario GetById(int idHandicap)
        {
            return _repository.GetById(idHandicap);
        }
    }
}
