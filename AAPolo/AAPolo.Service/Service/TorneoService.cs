﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class TorneoService : ITorneoService
    {
        private readonly IBaseRepository<ttorneo> _repository;
        private readonly ITorneoHistoricoService torneoHistoricoService;
        private readonly ITipoTorneoService tipoTorneoService;
        private readonly ICategoriaTorneoService categoriaTorneoService;
        private readonly ICategoriaHandicapService categoriaHandicapService;
        private readonly IEventoService eventoService;
        private readonly IPartidoService partidoService;
        private readonly IInstanciaService instanciaService;
        private readonly IEquipoService equipoService;
        private readonly IRefereeService refereeService;
        private readonly IBaseRepository<users> usuarioRepository;

        public TorneoService(IBaseRepository<ttorneo> repository, ITorneoHistoricoService _torneoHistoricoService, ITipoTorneoService _tipoTorneoService,
            ICategoriaTorneoService _categoriaTorneoService, ICategoriaHandicapService _categoriaHandicapService, IEventoService _eventoService, IPartidoService _partidoService,
            IInstanciaService _instanciaService, IEquipoService _equipoService, IBaseRepository<users> _usuarioRepository, IRefereeService _refereeService)
        {
            _repository = repository;
            torneoHistoricoService = _torneoHistoricoService;
            tipoTorneoService = _tipoTorneoService;
            categoriaTorneoService = _categoriaTorneoService;
            categoriaHandicapService = _categoriaHandicapService;
            eventoService = _eventoService;
            partidoService = _partidoService;
            instanciaService = _instanciaService;
            equipoService = _equipoService;
            usuarioRepository = _usuarioRepository;
            refereeService = _refereeService;
        }

        public async Task<List<ttorneo>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(ttorneo torneo)
        {
            torneo.fec_alta = DateTime.Now;
            torneo.tcategoria_torneo = categoriaTorneoService.GetById(torneo.cod_categoria_torneo);
            torneo.cod_categoria_torneo = torneo.tcategoria_torneo.cod_categoria_torneo;
            torneo.bool_activo = true;
            torneo.tcategoria_handicap = categoriaHandicapService.GetById(torneo.cod_categoria_handicap.Value);
            _repository.Add(torneo);

            await _repository.SaveChangesAsync();
            var torneoHistorico = new ttorneo_historico();
            torneoHistorico.cod_torneo = torneo.cod_torneo;
            torneoHistorico = PopularTorneoHistorico(torneoHistorico, torneo, torneo.usuario_alta);
            torneoHistoricoService.Save(torneoHistorico);
            await _repository.SaveChangesAsync();

            var evento = new tevento();
            evento.cod_torneo = torneo.cod_torneo;
            evento.txt_desc = torneo.txt_desc;
            evento.cod_tipo_evento = 1;
            evento.fec_comienzo = Convert.ToDateTime(torneo.fec_comienzo);
            evento.usuario_alta = "Torneo";
            await eventoService.Save(evento);
            return 1;
        }

        public async Task<int> Update(ttorneo torneo)
        {
            var torneoAModificar = _repository.GetById(torneo.cod_torneo);
            torneoAModificar.fec_ult_modificacion = DateTime.Now;
            await partidoService.DeleteByTorneo(torneoAModificar.cod_torneo);
            torneoAModificar = PopularTorneo(torneoAModificar, torneo);
            _repository.Update(torneoAModificar);

            var torneoHistorico = new ttorneo_historico();
            torneoHistorico = PopularTorneoHistorico(torneoHistorico, torneoAModificar, torneo.usuario_alta);
            torneoHistoricoService.Save(torneoHistorico);

            await _repository.SaveChangesAsync();
            return 1;
        }

        public async Task<int> Delete(int cod_torneo)
        {
            var torneoABorrar = _repository.GetById(cod_torneo);
            torneoABorrar.bool_activo = !torneoABorrar.bool_activo;
            if (!torneoABorrar.bool_activo.Value)
                torneoABorrar.bool_inscripcion = false;
            else
                torneoABorrar.bool_inscripcion = true;

            _repository.Update(torneoABorrar);
            await _repository.SaveChangesAsync();
            return 1;
        }

        public ttorneo GetById(int cod_torneo)
        {
            return _repository.GetById(cod_torneo);
        }

        public ttorneo_historico PopularTorneoHistorico(ttorneo_historico torneoHistorico, ttorneo torneo, string user)
        {
            torneoHistorico.fec_comienzo = torneo.fec_comienzo;
            torneoHistorico.fec_alta = torneo.fec_alta;
            torneoHistorico.fec_alta_historico = DateTime.Now;
            torneoHistorico.usuario_alta = torneo.usuario_alta;
            torneoHistorico.usuario_alta_historico = user;
            torneoHistorico.nro_handicap_equipo_minimo = torneo.nro_handicap_equipo_minimo;
            torneoHistorico.nro_handicap_equipo_maximo = torneo.nro_handicap_equipo_maximo;
            torneoHistorico.nro_handicap_jugador_minimo = torneo.nro_handicap_jugador_minimo;
            torneoHistorico.nro_handicap_jugador_maximo = torneo.nro_handicap_jugador_maximo;
            torneoHistorico.txt_desc = torneo.txt_desc;
            torneoHistorico.cod_categoria_torneo = torneo.cod_categoria_torneo;

            return torneoHistorico;
        }

        public ttorneo PopularTorneo(ttorneo torneoAModificar, ttorneo torneo)
        {
            torneoAModificar.fec_comienzo = torneo.fec_comienzo;
            torneoAModificar.fec_ult_modificacion = DateTime.Now;
            torneoAModificar.nro_handicap_equipo_maximo = torneo.nro_handicap_equipo_maximo;
            torneoAModificar.nro_handicap_equipo_minimo = torneo.nro_handicap_equipo_minimo;
            torneoAModificar.nro_handicap_jugador_maximo = torneo.nro_handicap_jugador_maximo;
            torneoAModificar.nro_handicap_jugador_minimo = torneo.nro_handicap_jugador_minimo;
            torneoAModificar.nro_handicap_especifico = torneo.nro_handicap_especifico;
            torneoAModificar.txt_desc = torneo.txt_desc;
            torneoAModificar.tcategoria_torneo = categoriaTorneoService.GetById(torneo.cod_categoria_torneo);
            torneoAModificar.cod_categoria_torneo = torneo.cod_categoria_torneo;
            torneoAModificar.tcategoria_handicap = categoriaHandicapService.GetById(torneo.cod_categoria_handicap.Value);
            torneoAModificar.cod_categoria_handicap = torneo.cod_categoria_handicap;
            torneoAModificar.nro_precio_inscripcion = torneo.nro_precio_inscripcion;
            torneoAModificar.txt_imagen = torneo.txt_imagen;
            torneoAModificar.txt_desc_larga = torneo.txt_desc_larga;
            torneoAModificar.bool_inscripcion = torneo.bool_inscripcion;
            torneoAModificar.bool_inscripcion_individual = torneo.bool_inscripcion_individual;
            torneoAModificar.bool_menores = torneo.bool_menores;

            var partidos = new List<tpartido>();
            foreach (var partido in torneo.tpartido)
            {
                var partidoCargado = new tpartido();
                partidoCargado.bool_activo = partido.bool_activo;
                partido.cod_equipo_local = partido.cod_equipo_local;
                partido.tinstancia = instanciaService.GetById(partido.cod_instancia.Value);
                partido.tequipo = equipoService.GetById(partido.cod_equipo_local);
                partido.tequipo1 = equipoService.GetById(partido.cod_equipo_visitante);
                partido.fec_partido = partido.fec_partido.AddHours(-3);
                if (partido.cod_referee.HasValue)
                    partido.treferee = refereeService.GetById(partido.cod_referee.Value);
                if (partido.cod_veedor.HasValue)
                    partido.users = usuarioRepository.GetById(partido.cod_veedor.Value);
                partidos.Add(partido);
            }

            torneoAModificar.tpartido = partidos;

            return torneoAModificar;
        }
    }
}
