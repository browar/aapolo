﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class InstanciaService : IInstanciaService
    {
        private readonly IBaseRepository<tinstancia> _repository;

        public InstanciaService(IBaseRepository<tinstancia> repository)
        {
            _repository = repository;
        }

        public async Task<List<tinstancia>> GetAll()
        {
            return (await (_repository.GetAll())).ToList();
        }

        public tinstancia GetById(int cod_instancia)
        {
            return _repository.GetById(cod_instancia);
        }
    }
}
