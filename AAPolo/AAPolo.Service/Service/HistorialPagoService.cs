﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class HistorialPagoService : IHistorialPagoService
    {
        private readonly IBaseRepository<thistorial_pagos> _repository;
        private readonly ITipoPagoService tipoPagoService;
        private readonly IEquipoService equipoService;
        private readonly IUsuarioService usuarioService;

        public HistorialPagoService(IBaseRepository<thistorial_pagos> repository, IUsuarioService _usuarioService, ITipoPagoService _tipoPagoService, IEquipoService _equipoService)
        {
            _repository = repository;
            usuarioService = _usuarioService;
            tipoPagoService = _tipoPagoService;
            equipoService = _equipoService;
        }

        public async Task<List<thistorial_pagos>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(thistorial_pagos pago)
        {
            pago = PopulatePago(pago);
            _repository.Add(pago);
            await _repository.SaveChangesAsync();

            return 1;
        }

        public async Task<int> Update(thistorial_pagos pago)
        {
            var digits = Math.Floor(Math.Log10(pago.cod_usuario_alta.Value) + 1);
            var textComprobante = (Convert.ToInt64(pago.txt_url_comprobante) - 400);
            var comprobante = textComprobante.ToString().Insert(Convert.ToInt32(digits), "-");
            var pagos = await _repository.GetMany(x => x.cod_usuario_alta == pago.cod_usuario_alta && x.nro_resultado_pago != 1);
            if (pagos.Count() == 0)
            {
                pagos = await _repository.GetMany(x => x.cod_usuario_alta == pago.cod_usuario_alta && x.nro_resultado_pago != 1);
            }

            if (pago.cod_historial_pagos != 0)
            {
                comprobante = pago.txt_url_comprobante;
                pagos = await _repository.GetMany(x => x.cod_historial_pagos == pago.cod_historial_pagos);
            }

            if (pagos != null)
            {
                foreach (var pagoAModificar in pagos)
                {
                    pagoAModificar.ttipo_pago = tipoPagoService.GetById(pagoAModificar.cod_tipo_pago);
                    if (pago.nro_resultado_pago.HasValue)
                        pagoAModificar.nro_resultado_pago = pago.nro_resultado_pago.Value;
                    pagoAModificar.txt_url_comprobante = comprobante;
                    pagoAModificar.cod_mercado_pago = pago.cod_mercado_pago;
                    _repository.Update(pagoAModificar);
                    if (pago.cod_torneo.HasValue)
                    {
                        if (pago.cod_torneo != 0)
                            await equipoService.PagarEquipo(pagoAModificar.cod_usuario, pagoAModificar.cod_torneo.Value, pagoAModificar.nro_resultado_pago.Value);
                    }
                }
                await _repository.SaveChangesAsync();
            }
            return 1;
        }

        public async Task<int> Delete(int cod_pago)
        {
            var pagoABorrar = _repository.GetById(cod_pago);
            _repository.Delete(pagoABorrar);
            return await _repository.SaveChangesAsync();
        }

        public thistorial_pagos GetById(int cod_pago)
        {
            return _repository.GetById(cod_pago);
        }

        public thistorial_pagos PopulatePago(thistorial_pagos pago)
        {
            //pago.users = usuarioService.GetById(pago.cod_usuario);
            pago.ttipo_pago = tipoPagoService.GetById(pago.cod_tipo_pago);
            pago.fec_alta = DateTime.Now;
            return pago;
        }


        public thistorial_pagos PopulatePagoModificar(thistorial_pagos populatePagoAModificar, thistorial_pagos pago)
        {
            //populatePagoAModificar.users = usuarioService.GetById(pago.cod_usuario);
            populatePagoAModificar.ttipo_pago = tipoPagoService.GetById(pago.cod_tipo_pago);
            populatePagoAModificar.nro_resultado_pago = pago.nro_resultado_pago;
            populatePagoAModificar.txt_url_comprobante = pago.txt_url_comprobante;
            pago.fec_alta = DateTime.Now;
            populatePagoAModificar.fec_vencimiento = pago.fec_vencimiento;
            populatePagoAModificar.cod_club_alta = pago.cod_club_alta;
            return pago;
        }

        public async Task<List<thistorial_pagos>> GetByCodUsuario(int cod_usuario)
        {
            return (await _repository.GetMany(x => x.nro_resultado_pago.Value == 1 && (x.cod_usuario_alta == cod_usuario || x.cod_usuario == cod_usuario))).ToList();
        }

        public async Task<List<thistorial_pagos>> GetByCodClub(int cod_club)
        {
            return (await _repository.GetMany(x => x.cod_club_alta == cod_club)).ToList();
        }

    }
}
