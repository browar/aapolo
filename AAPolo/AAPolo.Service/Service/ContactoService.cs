﻿using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Service.Service
{
    public class ContactoService : IContactoService
    {
        private readonly IBaseRepository<tcontacto> _repository;
        private readonly ITipoContactoService tipoContactoService;

        public ContactoService(IBaseRepository<tcontacto> repository, ITipoContactoService _tipoContactoService)
        {
            _repository = repository;
            tipoContactoService = _tipoContactoService;
        }

        public async Task<List<tcontacto>> GetAll()
        {
            return (await _repository.GetAll()).ToList();
        }

        public async Task<int> Save(tcontacto contacto)
        {
            contacto.ttipo_contacto = tipoContactoService.GetById(Convert.ToInt32(contacto.cod_tipo_contacto));
            _repository.Add(contacto);
            return (await _repository.SaveChangesAsync());
        }

        public async Task<int> Update(tcontacto contacto)
        {
            var contactoAModificar = _repository.GetById(contacto.cod_contacto);
            contactoAModificar.txt_desc = contacto.txt_desc;
            contactoAModificar.cod_tipo_contacto = contacto.cod_tipo_contacto;
            contactoAModificar.ttipo_contacto = tipoContactoService.GetById(Convert.ToInt32(contacto.cod_tipo_contacto));

            _repository.Update(contactoAModificar);
            return await _repository.SaveChangesAsync();
        }

        public async Task<int> Delete(int cod_contacto)
        {
            var contactoABorrar = _repository.GetById(cod_contacto);
            _repository.Delete(contactoABorrar);
            return await _repository.SaveChangesAsync();
        }

        public tcontacto GetById(int cod_contacto)
        {
            return _repository.GetById(cod_contacto);
        }

        public async Task<tcontacto> GetByCondition(tcontacto contacto)
        {
            return (await _repository.GetByCondition(x => x.cod_tipo_contacto == contacto.cod_tipo_contacto && x.txt_desc == contacto.txt_desc));
        }
    }
}
