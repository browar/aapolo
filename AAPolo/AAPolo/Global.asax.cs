﻿using AAPolo.Data.Context;
using AAPolo.Data.Context.Interface;
using AAPolo.Utils;
using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AAPolo
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            RegisterDependencies();
            log4net.Config.XmlConfigurator.Configure();
            ConfigureAutoMapper.ConfigureMapping();
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            log4net.ILog log = log4net.LogManager.GetLogger(typeof(object));
            using (log4net.NDC.Push(this.User.Identity.Name))
            {
                log.Fatal("Unhandled Exception", Server.GetLastError());
                var ambiente = ConfigurationManager.AppSettings["Ambiente"].ToString();
                if (ambiente.Equals("Prod") || ambiente.Equals("QA"))
                {
                    var sendMail = new SendMail();
                    sendMail.SendMailError(Server.GetLastError().StackTrace.ToString(), ambiente);
                }
            }
        }

        public static void RegisterDependencies()
        {
            #region Create the builder
            var builder = new ContainerBuilder();
            #endregion

            #region Setup a common pattern

            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => x.FullName.Contains("AAPolo.Data") || x.FullName.Contains("AAPolo.Service"));


            foreach (var assembly in assemblies)
            {
                builder.RegisterAssemblyTypes(assembly)
                    .AsImplementedInterfaces();
            }
            #endregion

            #region Register all controllers for the assembly
            builder.RegisterApiControllers(typeof(WebApiApplication).Assembly).InstancePerRequest();
            builder.RegisterType<AAPoloContext>().As<IAAPoloContext>().InstancePerLifetimeScope();
            #endregion

            #region Set the MVC Web Api dependency resolver to use Autofac
            var container = builder.Build();
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            #endregion
        }
    }
}
