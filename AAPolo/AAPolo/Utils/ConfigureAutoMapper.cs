﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AutoMapper;

namespace AAPolo.Utils
{
    public class ConfigureAutoMapper
    {
        public static MapperConfiguration MapperConfiguration;

        public static void ConfigureMapping()
        {
            MapperConfiguration = new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<tadjunto, tadjuntoDTO>().ReverseMap();
                    cfg.CreateMap<tcancha, tcanchaDTO>().ReverseMap();
                    cfg.CreateMap<tcategoria_club, tcategoria_clubDTO>().ReverseMap();
                    cfg.CreateMap<tcategoria_handicap, tcategoria_handicapDTO>().ReverseMap();
                    cfg.CreateMap<tcategoria_torneo, tcategoria_torneoDTO>().ReverseMap();
                    cfg.CreateMap<tclub, tclubDTO>().ReverseMap();
                    cfg.CreateMap<tclub, tclub_historicoDTO>().ReverseMap();
                    cfg.CreateMap<trespuesta, tclub_historicoDTO>().ReverseMap();
                    cfg.CreateMap<tcontacto, tcontactoDTO>().ReverseMap();
                    cfg.CreateMap<tdomicilio, tdomicilioDTO>().ReverseMap();
                    cfg.CreateMap<tequipo_userDTO, tequipo_user>().ReverseMap();
                    cfg.CreateMap<tequipo, tequipoDTO>().ReverseMap();
                    cfg.CreateMap<testado, testadoDTO>().ReverseMap();
                    cfg.CreateMap<tevento, teventoDTO>().ReverseMap();
                    cfg.CreateMap<tgenero, tgeneroDTO>().ReverseMap();
                    cfg.CreateMap<thandicap, thandicapDTO>().ReverseMap();
                    cfg.CreateMap<thandicap_usuario, thandicap_usuarioDTO>().ReverseMap();
                    cfg.CreateMap<thandicap_usuario_historico, thandicap_usuario_historicoDTO>().ReverseMap();
                    cfg.CreateMap<thistorial_pagos, thistorial_pagosDTO>().ReverseMap();
                    cfg.CreateMap<tinfraccion, tinfraccionDTO>().ReverseMap();
                    cfg.CreateMap<tinfraccion_partido, tinfraccion_partidoDTO>().ReverseMap();
                    cfg.CreateMap<tmotivo_baja, tmotivo_bajaDTO>().ReverseMap();
                    cfg.CreateMap<tmunicipio, tmunicipioDTO>().ReverseMap();
                    cfg.CreateMap<tnoticia, tnoticiaDTO>().ReverseMap();
                    cfg.CreateMap<tnoticia, NoticiaDestacadaDTO>().ReverseMap();
                    cfg.CreateMap<tnoticia_etiqueta, tnoticia_etiquetaDTO>().ReverseMap();
                    cfg.CreateMap<tnoticia_archivo, tnoticia_archivoDTO>().ReverseMap();
                    cfg.CreateMap<tnotificacion, tnotificacionDTO>().ReverseMap();
                    cfg.CreateMap<tnotificacion_adjunto, tnotificacion_adjuntoDTO>().ReverseMap();
                    cfg.CreateMap<tpais, tpaisDTO>().ReverseMap();
                    cfg.CreateMap<tpartido, tpartidoDTO>().ReverseMap();
                    cfg.CreateMap<tprovincia, tprovinciaDTO>().ReverseMap();
                    cfg.CreateMap<tranking, trankingDTO>().ReverseMap();
                    cfg.CreateMap<treconocimiento, treconocimientoDTO>().ReverseMap();
                    cfg.CreateMap<treconocimiento_usuario, treconocimiento_usuarioDTO>().ReverseMap();
                    cfg.CreateMap<tsede, tsedeDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_contacto, ttipo_contactoDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_doc, ttipo_docDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_domicilio, ttipo_domicilioDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_evento, ttipo_eventoDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_infraccion, ttipo_infraccionDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_jugador, ttipo_jugadorDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_noticia, ttipo_noticiaDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_notificacion, ttipo_notificacionDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_pago, ttipo_pagoDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_reconocimiento, ttipo_reconocimientoDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_sede, ttipo_sedeDTO>().ReverseMap();
                    cfg.CreateMap<ttipo_torneo, ttipo_torneoDTO>().ReverseMap();
                    cfg.CreateMap<ttorneo, ttorneoDTO>().ReverseMap();
                    cfg.CreateMap<ttorneo, torneoSelectDTO>().ReverseMap();
                    cfg.CreateMap<ttorneo, torneoHome>().ReverseMap();
                    cfg.CreateMap<ttorneo_historico, ttorneo_historicoDTO>().ReverseMap();
                    cfg.CreateMap<ttorneo, ttorneo_historicoDTO>().ReverseMap();
                    cfg.CreateMap<ttransicion, ttransicionDTO>().ReverseMap();
                    cfg.CreateMap<tuser_club, tuser_clubDTO>().ReverseMap();
                    cfg.CreateMap<users, usersDTO>().ReverseMap();
                    cfg.CreateMap<users_historico, users_historicoDTO>().ReverseMap();
                    cfg.CreateMap<users, users_historicoDTO>().ReverseMap();
                    cfg.CreateMap<tcontacto_club, tcontacto_clubDTO>().ReverseMap();
                    cfg.CreateMap<tcontacto_usuario, tcontacto_usuarioDTO>().ReverseMap();
                    cfg.CreateMap<users, club_socioDTO>().ReverseMap();
                    cfg.CreateMap<users, JugadoresDTO>().ReverseMap();
                    //cfg.CreateMap<reporteCuentaClub, reporteCuentaClubDTO>().ReverseMap();
                    cfg.CreateMap<users, JugadoresDTO>().ReverseMap();
                    cfg.CreateMap<users, usuarioSelectDTO>().ReverseMap();
                    cfg.CreateMap<tinstancia, tinstanciaDTO>().ReverseMap();
                    cfg.CreateMap<treferee, trefereeDTO>().ReverseMap();
                    cfg.CreateMap<tclub, clubSelect>().ReverseMap();
                    cfg.CreateMap<tmiembro, tmiembroDTO>().ReverseMap();
                    cfg.CreateMap<tpopup, tpopupDTO>().ReverseMap();
                    cfg.CreateMap<trol, trolDTO>().ReverseMap();
                    cfg.CreateMap<tnotificacion_usuario, tnotificacion_usuarioDTO>().ReverseMap();
                    cfg.CreateMap<tnotificacion_rol, tnotificacion_rolDTO>().ReverseMap();
                    cfg.CreateMap<tnotificacion_tipo_jugador, tnotificacion_tipo_jugadorDTO>().ReverseMap();
                    cfg.CreateMap<trespuesta, trespuestaDTO>().ReverseMap();
                    cfg.CreateMap<trespuesta_adjunto, trespuesta_adjuntoDTO>().ReverseMap();
                });
        }
    }
}