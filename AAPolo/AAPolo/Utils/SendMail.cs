﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;

namespace AAPolo.Utils
{
    public class SendMail
    {
        public void SendMailError(string mensaje, string ambiente)
        {
            MailMessage mail = new MailMessage("erroresAaPolo@amedia.com.ar", "sebastian.martin@amedia.com.ar");
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            var smtp = ConfigurationManager.AppSettings["smtp"].ToString();
            client.Host = smtp;
            mail.Subject = "¡Ocurrió un error en " + ambiente + "!";
            mail.Body = mensaje;
            mail.IsBodyHtml = true;
            client.Send(mail);
        }

        public void SendMailNotificacion(string mensaje, string titulo)
        {
            using (MailMessage mailMessage =
            new MailMessage(new MailAddress(@"no-reply@amedia.com.ar"),
            new MailAddress(@"no-reply@amedia.com.ar", "AAPolo")))
            {
                mailMessage.Body = "body";
                mailMessage.Subject = "subject";
                try
                {
                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.Credentials =
                        new System.Net.NetworkCredential(@"no-reply@amedia.com.ar", "sotelo21");
                    SmtpServer.Port = 587;
                    SmtpServer.Host = "smtp.gmail.com";
                    SmtpServer.EnableSsl = true;
                    var mail = new MailMessage();
                    var to = "sebastian.martin@amedia.com.ar,maru.apostolu@amedia.com.ar";
                    String[] addr = to.Split(','); // toemail is a string which contains many email address separated by comma
                    mail.From = new MailAddress(@"no-reply@amedia.com.ar", "AAPolo");
                    Byte i;
                    for (i = 0; i < addr.Length; i++)
                        mail.To.Add(addr[i]);
                    mail.Subject = titulo;
                    string html = "";
                    html = File.ReadAllText("C:\\Inetpub\\wwwroot\\amediaTest\\ApiPolo\\Views\\Mail\\mail-comunicados.html");
                    html = html.Replace("tituloComunicacion", titulo);
                    html = html.Replace("mensajeEnvio", mensaje);
                    html = html.Replace("fechaEnvio", DateTime.Now.ToString("dd MMMM yyyy"));
                    html = html.Replace("enviadoPor", "notificaciones@aapolo.com.ar");
                    mail.Body = html;// "cuerpo browar";
                    mail.IsBodyHtml = true;
                    mail.DeliveryNotificationOptions =
                        DeliveryNotificationOptions.OnFailure;
                    //   mail.ReplyTo = new MailAddress(toemail);
                    mail.ReplyToList.Add(@"no-reply@amedia.com.ar");
                    SmtpServer.Send(mail);
                 
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void SendMailPassword(string mailToSend, string mensaje, string titulo)
        {
            using (MailMessage mailMessage =
            new MailMessage(new MailAddress(@"no-reply@amedia.com.ar"),
            new MailAddress(@"no-reply@amedia.com.ar", "AAPolo")))
            {
                mailMessage.Body = "body";
                mailMessage.Subject = "subject";
                try
                {
                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.Credentials =
                        new System.Net.NetworkCredential(@"no-reply@amedia.com.ar", "sotelo21");
                    SmtpServer.Port = 587;
                    SmtpServer.Host = "smtp.gmail.com";
                    SmtpServer.EnableSsl = true;
                    var mail = new MailMessage();
                    var to = mailToSend;
                    String[] addr = to.Split(','); // toemail is a string which contains many email address separated by comma
                    mail.From = new MailAddress(@"no-reply@amedia.com.ar", "AAPolo");
                    Byte i;
                    for (i = 0; i < addr.Length; i++)
                        mail.To.Add(addr[i]);
                    mail.Subject = titulo;
                    string html = "";
                    html = File.ReadAllText("C:\\Inetpub\\wwwroot\\amediaTest\\ApiPolo\\Views\\Mail\\mail-comunicados.html");

                    //html = File.ReadAllText("C:\\Workspace\\AAPolo\\AApolo\\Views\\Mail\\mail-comunicados.html");
                    html = html.Replace("tituloComunicacion", titulo);
                    html = html.Replace("mensajeEnvio", mensaje);
                    html = html.Replace("fechaEnvio", DateTime.Now.ToString("dd MMMM yyyy"));
                    html = html.Replace("enviadoPor", "notificaciones@aapolo.com.ar");
                    mail.Body = html;// "cuerpo browar";
                    mail.IsBodyHtml = true;
                    mail.DeliveryNotificationOptions =
                        DeliveryNotificationOptions.OnFailure;
                    //   mail.ReplyTo = new MailAddress(toemail);
                    mail.ReplyToList.Add(@"no-reply@amedia.com.ar");
                    SmtpServer.Send(mail);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void SendMailBeneficios(string mailToSend, string mensaje, string titulo)
        {
            using (MailMessage mailMessage =
            new MailMessage(new MailAddress(@"no-reply@amedia.com.ar"),
            new MailAddress(@"no-reply@amedia.com.ar", "AAPolo")))
            {
                mailMessage.Body = "body";
                mailMessage.Subject = "subject";
                try
                {
                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.Credentials =
                        new System.Net.NetworkCredential(@"no-reply@amedia.com.ar", "sotelo21");
                    SmtpServer.Port = 587;
                    SmtpServer.Host = "smtp.gmail.com";
                    SmtpServer.EnableSsl = true;
                    var mail = new MailMessage();
                    var to = mailToSend;
                    String[] addr = to.Split(','); // toemail is a string which contains many email address separated by comma
                    mail.From = new MailAddress(@"no-reply@amedia.com.ar", "AAPolo");
                    Byte i;
                    for (i = 0; i < addr.Length; i++)
                        mail.To.Add(addr[i]);
                    mail.Subject = titulo;
                    string html = "";
                    html = File.ReadAllText("C:\\Inetpub\\wwwroot\\amediaTest\\ApiPolo\\Views\\Mail\\mail-beneficios.html");

                    //html = File.ReadAllText("C:\\Workspace\\AAPolo\\AApolo\\Views\\Mail\\mail-comunicados.html");
                    html = html.Replace("tituloComunicacion", titulo);
                    html = html.Replace("mensajeEnvio", mensaje);
                    html = html.Replace("fechaEnvio", DateTime.Now.ToString("dd MMMM yyyy"));
                    html = html.Replace("enviadoPor", "notificaciones@aapolo.com.ar");
                    mail.Body = html;// "cuerpo browar";
                    mail.IsBodyHtml = true;
                    mail.DeliveryNotificationOptions =
                        DeliveryNotificationOptions.OnFailure;
                    //   mail.ReplyTo = new MailAddress(toemail);
                    mail.ReplyToList.Add(@"no-reply@amedia.com.ar");
                    SmtpServer.Send(mail);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}