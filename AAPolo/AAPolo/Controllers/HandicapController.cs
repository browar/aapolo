﻿using AAPolo.Entity.DTO;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AAPolo.Controllers
{
    [EnableCors(origins: "http://localhost:56596", headers: "*", methods: "*", SupportsCredentials = true)]
    public class HandicapController : ApiController
    {
        private readonly IHandicapService handicapService;
        IMapper Mapper;

        public HandicapController(IHandicapService _handicapService)
        {
            handicapService = _handicapService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        // GET: api/PagoHandicap
        public async Task<IHttpActionResult> Get()
        {
            var result = Mapper.Map<List<thandicapDTO>>(await handicapService.GetAll());
            return Ok(result);
        }
    }
}
