﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class PromocionHandicapController : ApiController
    {
        private readonly IUsuarioService usuarioService;
        IMapper Mapper;

        public PromocionHandicapController(IUsuarioService _usuarioService)
        {
            usuarioService = _usuarioService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        // PUT: api/PromocionHandicap/5
        public async Task<IHttpActionResult> Put(int id, [FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var user = JsonConvert.DeserializeObject<List<usersDTO>>(data, dateTimeConverter);
            var usuariosAModificar = Mapper.Map<List<users>>(user);
            await usuarioService.CambiarHandicap(usuariosAModificar);

            return Ok("Se realizo correctamente las modificaciones");
        }
    }
}
