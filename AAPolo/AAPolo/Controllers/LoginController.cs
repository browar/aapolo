﻿using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class LoginController : ApiController
    {
        private readonly IUsuarioAdminService usuarioAdminService;
        private readonly IUsuarioService usuarioService;
        private readonly IMiembroService miembroService;
        private readonly SendMail sendMail;

        public LoginController(IUsuarioAdminService _usuarioAdminService, IUsuarioService _usuarioService, IMiembroService _miembroService)
        {
            usuarioAdminService = _usuarioAdminService;
            usuarioService = _usuarioService;
            miembroService = _miembroService;
            sendMail = new SendMail();
        }

        public async Task<IHttpActionResult> Get(string name)
        {
            var user = (await usuarioAdminService.GetByUsername(name));
            return Ok(user);
        }

        [HttpGet]
        public async Task<IHttpActionResult> ReestablecerPassword(string dni)
        {
            var user = (await usuarioService.GetByDocument(dni));
            if (user != null)
            {
                var contactoMail = user.tcontacto_usuario.Where(x => x.tcontacto.cod_tipo_contacto == 3).FirstOrDefault();
                string reestablecerPassword = "http://aapolo.com/Home/Index#/token=" + dni;
                sendMail.SendMailPassword(contactoMail.tcontacto.txt_desc, "Para reestablecer la contraseña ingrese en el siguiente link: " + reestablecerPassword + "", "Reestablecer contraseña");
            }
            else
            {
                var miembro = await miembroService.getByDocument(dni);
                string reestablecerPassword = "http://aapolo.com/Home/Index#/token=" + dni;
                sendMail.SendMailPassword(miembro.txt_mail, "Para reestablecer la contraseña ingrese en el siguiente link: " + reestablecerPassword + "", "Reestablecer contraseña");

            }
            return Ok("");
        }

        public async Task<IHttpActionResult> Post([FromBody]string usuario)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var usuarioAdmin = JsonConvert.DeserializeObject<users_admin>(usuario, dateTimeConverter);
            var userFound = (await usuarioAdminService.GetByUserAndPassword(usuarioAdmin));
            if (userFound != null)
            {
                return Ok(userFound);
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo loguearse"));
            }
        }
    }
}
