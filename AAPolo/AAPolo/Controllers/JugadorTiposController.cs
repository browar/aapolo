﻿using AAPolo.Entity.DTO;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class JugadorTiposController : ApiController
    {
        private readonly IUsuarioService jugadorService;
        private readonly IHandicapService handicapService;
        private readonly ICategoriaHandicapService categoriaHandicapService;
        private readonly IClubService clubService;
        private readonly IProvinciaService provinciaService;
        private readonly IPaisService paisService;
        private readonly IMunicipioService municipioService;
        private readonly ITipoDocumentoService tipoDocumentoService;
        private readonly ITipoJugadorService tipoJugadorService;
        private readonly IGeneroService generoService;
        IMapper Mapper;

        public JugadorTiposController(IUsuarioService _jugadorService, IHandicapService _handicapService, ICategoriaHandicapService _categoriaHandicapService,
            IClubService _clubService, IPaisService _paisService, ITipoJugadorService _tipoJugadorService, IProvinciaService _provinciaService,
            IMunicipioService _municipioService, ITipoDocumentoService _tipoDocumentoService, IGeneroService _generoService)
        {
            jugadorService = _jugadorService;
            handicapService = _handicapService;
            categoriaHandicapService = _categoriaHandicapService;
            clubService = _clubService;
            paisService = _paisService;
            tipoJugadorService = _tipoJugadorService;
            provinciaService = _provinciaService;
            municipioService = _municipioService;
            tipoDocumentoService = _tipoDocumentoService;
            generoService = _generoService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }


        // GET: api/Tipos
        public async Task<IHttpActionResult> Get()
        {
            var result = new
            {
                handicaps = Mapper.Map<List<thandicapDTO>>(await handicapService.GetAll()),
                categoriaHandicaps = Mapper.Map<List<tcategoria_handicapDTO>>(await categoriaHandicapService.GetAll()),
                clubes = Mapper.Map<List<clubSelect>>((clubService.GetClubesPagos())),
                paises = Mapper.Map<List<tpaisDTO>>(await paisService.GetAll()),
                tiposJugador = Mapper.Map<List<ttipo_jugadorDTO>>(await tipoJugadorService.GetAll()),
                provincias = Mapper.Map<List<tprovinciaDTO>>(await provinciaService.GetAll()),
                tiposDocumentos = Mapper.Map<List<ttipo_docDTO>>(await tipoDocumentoService.GetAll()),
                generos = Mapper.Map<List<tgeneroDTO>>(await generoService.GetAll())
            };

            return Ok(result);
        }

        // GET: api/Tipos/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Tipos
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Tipos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Tipos/5
        public void Delete(int id)
        {
        }
    }
}
