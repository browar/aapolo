﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class MiembroController : ApiController
    {
        private readonly IMiembroService miembroService;
        private readonly IProvinciaService provinciaService;
        private readonly IPaisService paisService;
        private readonly IMunicipioService municipioService;
        private readonly ITipoDocumentoService tipoDocumentoService;
        private readonly IGeneroService generoService;
        private readonly SendMail sendMail;
        IMapper Mapper;

        public MiembroController(IMiembroService _miembroService, IProvinciaService _provinciaService, IPaisService _paisService, IMunicipioService _municipioService,
            ITipoDocumentoService _tipoDocumentoService, IGeneroService _generoService)
        {
            miembroService = _miembroService;
            provinciaService = _provinciaService;
            paisService = _paisService;
            municipioService = _municipioService;
            tipoDocumentoService = _tipoDocumentoService;
            generoService = _generoService;
            sendMail = new SendMail();
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }


        // GET: api/Miembro
        public async Task<IHttpActionResult> Get()
        {
            var result = new
            {
                paises = Mapper.Map<List<tpaisDTO>>(await paisService.GetAll()),
                provincias = Mapper.Map<List<tprovinciaDTO>>(await provinciaService.GetAll()),
                generos = Mapper.Map<List<tgeneroDTO>>(await generoService.GetAll()),
                tiposDocumento = Mapper.Map<List<ttipo_docDTO>>(await tipoDocumentoService.GetAll())
            };

            return Ok(result);
        }

        public async Task<IHttpActionResult> Get(string tarjeta)
        {
            var userFound = await miembroService.GetByNroTarjeta(tarjeta);
            if (userFound != null)
            {
                return Ok(Mapper.Map<tmiembroDTO>(userFound));
            }
            else
            {
                return Ok("No hay Tarjeta Asociada");
            }
        }

        public async Task<IHttpActionResult> Get(string nro_doc, string password)
        {
            var userFound = await miembroService.getByDocumentoYPassword(nro_doc, password);
            if (userFound != null)
            {
                return Ok(Mapper.Map<tmiembroDTO>(userFound));
            }
            else
            {
                return InternalServerError(new Exception("Error"));
            }
        }

        // GET: api/Miembro/5
        public IHttpActionResult Get(int idMiembro)
        {
            var userFound = miembroService.GetById(idMiembro);
            if (userFound != null)
            {
                return Ok(Mapper.Map<tmiembroDTO>(userFound));
            }
            else
            {
                return InternalServerError(new Exception("Error"));
            }
        }

        // GET: api/Miembro/5
        [HttpGet]
        public async Task<IHttpActionResult> GetByCodUsuario(int cod_usuario)
        {
            var userFound = await miembroService.GetByCodUsuario(cod_usuario);
            if (userFound != null)
            {
                return Ok(Mapper.Map<tmiembroDTO>(userFound));
            }
            else
            {
                return InternalServerError(new Exception("Error"));
            }
        }

        // POST: api/Miembro
        public async Task<IHttpActionResult> Post([FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var miembroDTO = JsonConvert.DeserializeObject<tmiembroDTO>(data, dateTimeConverter);

            var miembroAGuardar = Mapper.Map<tmiembro>(miembroDTO);

            if ((await miembroService.Save(miembroAGuardar) == 1))
            {
                sendMail.SendMailBeneficios(miembroDTO.txt_mail, "Bienvenido "+ miembroDTO.txt_nombre + " " + miembroDTO.txt_apellido + ". <br> <br>" +
                    "Ya sos parte de la Polo Community, un Programa exclusivo que tiene como objetivo unir y conectar a la comunidad del polo. <br> <br>" +
                    "Próximamente te estaremos comunicando los beneficios exclusivos que tenemos para vos.<br> <br>" +
                    "Por consultas o sugerencias, no dudes en enviar un correo a beneficios@aapolo.com o bien llamar o enviar un mensaje vía Whatsapp al teléfono de contacto.<br> <br>"
                    , "¡Bienvenido!");

                return Ok("Se ha creado correctamente el miembro");
            }
            else
            {
                return InternalServerError(new Exception("El número de documento que ingreso ya existe, por favor, verifique su información."));
            }
        }

        // PUT: api/Miembro/5
        public async Task<IHttpActionResult> Put(int id, [FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var miembroDTO = JsonConvert.DeserializeObject<tmiembroDTO>(data, dateTimeConverter);

            var miembroAGuardar = Mapper.Map<tmiembro>(miembroDTO);

            if ((await miembroService.Update(miembroAGuardar) == 1))
            {
                return Ok("Se ha modificado correctamente el miembro");
            }
            else
            {
                return InternalServerError(new Exception("El número de documento que ingreso ya existe, por favor, verifique su información."));
            }
        }

        // DELETE: api/Miembro/5
        public void Delete(int id)
        {
        }
    }
}
