﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class LoginJugadorController : ApiController
    {
        private readonly IUsuarioService usuarioService;
        private readonly IMiembroService miembroService;
        IMapper Mapper;

        public LoginJugadorController(IUsuarioService _usuarioAdminService, IMiembroService _miembroService)
        {
            usuarioService = _usuarioAdminService;
            miembroService = _miembroService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        public async Task<IHttpActionResult> Get(string nro_doc)
        {
            var user = (await usuarioService.GetByDocument(nro_doc));
            return Ok(Mapper.Map<usuarioSelectDTO>(user));
        }

        public async Task<IHttpActionResult> Post([FromBody]string usuario)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var user = JsonConvert.DeserializeObject<users>(usuario, dateTimeConverter);
            var userFound = (await usuarioService.GetByDocumentAndPassword(user));
            if (userFound != null)
            {
                if (!String.IsNullOrEmpty(user.platform_mobile) && !String.IsNullOrEmpty(user.id_register))
                {
                    userFound.platform_mobile = user.platform_mobile;
                    userFound.id_register = user.id_register;
                    await usuarioService.UpdatePlatformAndRegister(userFound);
                }

                return Ok(Mapper.Map<usersDTO>(userFound));
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo loguearse"));
            }
        }

        public async Task<IHttpActionResult> CambioPassword([FromBody]string usuario)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var user = JsonConvert.DeserializeObject<users>(usuario, dateTimeConverter);
            var userFound = (await usuarioService.ActualizarContrasena(user));

            if (userFound == 0)
            {
                userFound = (await miembroService.ActualizarContrasena(user.nro_doc, user.txt_password));
            }
            if (userFound != 0)
            {
                return Ok(userFound);
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo loguearse"));
            }
        }

        [HttpPut]
        public async Task<IHttpActionResult> ActualizarTerminos(int id, [FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var user = JsonConvert.DeserializeObject<usersDTO>(data, dateTimeConverter);
            var usuarioAGuardar = Mapper.Map<users>(user);
            var userFound = (await usuarioService.AceptarTerminos(id, usuarioAGuardar));
            if (userFound == 1)
            {
                return Ok("Se ha actualizado correctamente la contraseña");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo loguearse"));
            }
        }
    }
}
