﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AAPolo.Controllers
{
    [EnableCors(origins: "http://localhost:56596", headers: "*", methods: "*", SupportsCredentials = true)]
    public class ClubController : ApiController
    {
        private readonly IClubService clubService;
        private readonly ICategoriaClubService categoriaClubService;
        IMapper Mapper;

        public ClubController(IClubService _clubService, ICategoriaClubService _categoriaClubService)
        {
            clubService = _clubService;
            categoriaClubService = _categoriaClubService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        public async Task<IHttpActionResult> Get()
        {
            var clubes = (await clubService.GetAll());

            var clubesDTO = Mapper.Map<List<clubSelect>>(clubes);
            return Ok(clubesDTO);
        }

        public async Task<IHttpActionResult> Get(string page)
        {
            var clubes = (await clubService.GetAll());
            var skipped = clubes.Skip(Convert.ToInt32(page) * 20).Take(20);
            var clubsMapped = Mapper.Map<List<tclubDTO>>(skipped);
            var result = new
            {
                pages = clubes.Count(),
                clubesDTO = clubsMapped,
                categoriaClubes = Mapper.Map<List<tcategoria_clubDTO>>(await categoriaClubService.GetAll())
            };
            return Ok(result);
        }


        [HttpGet]
        public IHttpActionResult ClubesPagos()
        {
            var clubes = clubService.GetClubesPagos();
            return Ok(clubes);
        }


        public async Task<IHttpActionResult> Get(int page, string nombre, int cod_categoria_club)
        {
            var clubesSearch = (await clubService.GetAll());

            if (!string.IsNullOrEmpty(nombre))
            {
                clubesSearch = (from p in clubesSearch
                               where (p.txt_desc.ToUpper()).Contains(nombre.ToUpper())
                               select p).ToList();
            }

            if (cod_categoria_club != 0)
            {
                clubesSearch = clubesSearch.Where(x => x.cod_categoria_club == cod_categoria_club).ToList();
            }

            var countTotal = clubesSearch.Count();

            var skipped = clubesSearch.Skip((page - 1) * 20).Take(20);
            var usersMapped = Mapper.Map<List<tclubDTO>>(skipped);
            var result = new
            {
                pages = countTotal,
                clubesDTO = usersMapped
            };

            return Ok(result);
        }

        public IHttpActionResult Get(int id)
        {
            var clubesDTO = Mapper.Map<tclubDTO>(clubService.GetById(id));
            return Ok(clubesDTO);
        }

        public async Task<IHttpActionResult> Post([FromBody]string clubDTO)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var club = JsonConvert.DeserializeObject<tclubDTO>(clubDTO, dateTimeConverter);
            var clubAGuardar = Mapper.Map<tclub>(club);
            if ((await clubService.Save(clubAGuardar)) == 1)
            {
                return Ok("Se ha creado correctamente el club");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo agregarse el club"));
            }
        }

        public async Task<IHttpActionResult> Put(int id, [FromBody]string clubDTO)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var club = JsonConvert.DeserializeObject<tclubDTO>(clubDTO, dateTimeConverter);
            var clubAModificar = Mapper.Map<tclub>(club);
            if ((await clubService.Update(clubAModificar)) == 1)
            {
                return Ok("Se ha modificado correctamente el club");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo modificarse el club"));
            }

        }
        public async Task<IHttpActionResult> Delete(int id)
        {
            if ((await clubService.Delete(id)) == 1)
            {
                return Ok("Se ha dado de baja correctamente el club");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo darse de baja el club"));
            }
        }
    }
}
