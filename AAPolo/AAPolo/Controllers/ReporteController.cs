﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using ClosedXML.Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AAPolo.Controllers
{
    [EnableCors(origins: "http://localhost:56596", headers: "*", methods: "*", SupportsCredentials = true)]
    public class ReporteController : ApiController
    {
        private readonly IReporteClubService reporteClubService;
        private readonly IReporteJugadoresService reporteJugadoresService;
        private readonly IReporteTorneoService reporteTorneoService;
        private readonly ITorneoService torneoService;
        IMapper Mapper;

        public ReporteController(IReporteClubService _reporteClubService, IReporteJugadoresService _reporteJugadoresService, IReporteTorneoService _reporteTorneoService, ITorneoService _torneoService)
        {
            reporteClubService = _reporteClubService;
            reporteJugadoresService = _reporteJugadoresService;
            reporteTorneoService = _reporteTorneoService;
            torneoService = _torneoService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }
        public async Task<IHttpActionResult> GetCuentaCorrienteClubes()
        {
            var clubes = (await reporteClubService.GetAll());
            var result = new reportesClubesResult
            {
                lstClubes = clubes,
                cant_abonaron = clubes.Count(x => x.importe > 0),
                cant_no_abonaron = clubes.Count(x => x.importe == 0),
                cant_clubes = clubes.Count()
            };
            return Ok(result);
        }

        public async Task<IHttpActionResult> GetCuentaCorrienteJugadores()
        {
            var jugadores = (await reporteJugadoresService.GetAll());
            var result = new reportesJugadoresResult
            {
                lstJugadores = jugadores,
                cant_abonaron = jugadores.Count(x => x.importe > 0),
                cant_no_abonaron = jugadores.Count(x => x.importe == 0),
                cant_jugadores = jugadores.Count()
            };
            return Ok(result);
        }
        public async Task<IHttpActionResult> GetCuentaTorneos()
        {
            var equipos = (await reporteTorneoService.GetAll());
            var result = new reportesEquiposResult
            {
                lstEquipos = equipos,
                cant_abonaron = equipos.Count(x => x.importe > 0),
                cant_no_abonaron = equipos.Count(x => x.importe == 0),
                cant_equipos = equipos.Count(),
                cant_torneos= equipos.First().CantTorneos
            };
            return Ok(result);
        }

        public async Task<IHttpActionResult> GetReporteInscripcionesTorneos(int id)
        {
            var equipos = (await reporteTorneoService.GetByID(id));
            ttorneo miTorneo;
            if (equipos.Count > 0) {
                miTorneo = new ttorneo { cod_torneo = equipos.First().Torneo.cod_torneo, txt_desc = equipos.First().Torneo.txt_desc };
            } else {
                var tmp = torneoService.GetById(id);
                miTorneo = new ttorneo { cod_torneo = tmp.cod_torneo, txt_desc = tmp.txt_desc };
            }
            //var obj = ;
            var result = new reportesInscripcionTorneo
            {
                lstEquipos = equipos,
                Torneo = miTorneo,
                cant_abonaron = equipos.Count(x => x.importe > 0),
                cant_no_abonaron = equipos.Count(x => x.importe == 0),
                cant_equipos = equipos.Count(),
                cant_clubes = equipos.Count(x=>x.Club.cod_club != 0)

            };
            return Ok(result);
        }

        public async Task<HttpResponseMessage> GetReporteCuentasCorrientes()
        {
//            var miRuta = System.Web.Hosting.HostingEnvironment.MapPath(@"/a");
            var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientes.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var clubes = (await reporteClubService.GetAll()).ToList();
            int fila1 = 4;
            int fila2 = 8;

            hoja.Cells("C" + (fila1).ToString()).Value = clubes.Count();
            hoja.Cells("D" + (fila1).ToString()).Value = clubes.Count(x => x.importe > 0);
            hoja.Cells("E" + (fila1).ToString()).Value = clubes.Count(x => x.importe == 0);
            foreach (var miClub in clubes)
            {
                hoja.Cells("C" + (fila2).ToString()).Value = miClub.Club;
                hoja.Cells("D" + (fila2).ToString()).Value = miClub.NombreAdmin + " " + miClub.ApellidoAdmin;
                hoja.Cells("E" + (fila2).ToString()).Value = miClub.CategoriaClub;
                hoja.Cells("F" + (fila2).ToString()).Value = miClub.fec_ult_pago.ToString();
                hoja.Cells("G" + (fila2).ToString()).Value = miClub.importe == null ? "" : ((int)miClub.importe).ToString("0.00");
                hoja.Cells("H" + (fila2).ToString()).Value = miClub.sn_pago == 1 ? "Al día" : "Vencido";
                fila2++;
            }


            // test
            var stream = new MemoryStream();
            
            // processing the stream.
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(stream);
                memoryStream.Close();
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };
            result.Content.Headers.ContentDisposition =
            new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReporteCuentaCorrienteClubes.xlsx"
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;

            //Response.End();
        }
        public async Task<HttpResponseMessage> GetReporteCuentasCorrientesClubes()
        {
            //            var miRuta = System.Web.Hosting.HostingEnvironment.MapPath(@"/a");
            var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientes.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var clubes = (await reporteClubService.GetAll()).ToList();
            int fila1 = 4;
            int fila2 = 8;

            hoja.Cells("C" + (fila1).ToString()).Value = clubes.Count();
            hoja.Cells("D" + (fila1).ToString()).Value = clubes.Count(x => x.importe > 0);
            hoja.Cells("E" + (fila1).ToString()).Value = clubes.Count(x => x.importe == 0);
            foreach (var miClub in clubes)
            {
                hoja.Cells("C" + (fila2).ToString()).Value = miClub.Club;
                hoja.Cells("D" + (fila2).ToString()).Value = miClub.NombreAdmin + " " + miClub.ApellidoAdmin;
                hoja.Cells("E" + (fila2).ToString()).Value = miClub.CategoriaClub;
                hoja.Cells("F" + (fila2).ToString()).Value = miClub.fec_ult_pago.ToString();
                hoja.Cells("G" + (fila2).ToString()).Value = miClub.importe == null ? "" : ((int)miClub.importe).ToString("0.00");
                hoja.Cells("H" + (fila2).ToString()).Value = miClub.sn_pago == 1 ? "Al día" : "Vencido";
                fila2++;
            }


            // test
            var stream = new MemoryStream();

            // processing the stream.
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(stream);
                memoryStream.Close();
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };
            result.Content.Headers.ContentDisposition =
            new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReporteCuentaCorrienteClubes.xlsx"
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;

            //Response.End();
        }

        public async Task<HttpResponseMessage> GetReporteCuentasCorrientesJugadores()
        {
            //var miRuta = System.Web.Hosting.HostingEnvironment.MapPath(@"/a");
            var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientesJugadores.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var Jugadores = (await reporteJugadoresService.GetAll()).ToList();
            int fila1 = 4;
            int fila2 = 8;

            hoja.Cells("C" + (fila1).ToString()).Value = Jugadores.Count();
            hoja.Cells("D" + (fila1).ToString()).Value = Jugadores.Count(x => x.importe > 0);
            hoja.Cells("E" + (fila1).ToString()).Value = Jugadores.Count(x => x.importe == 0);
            foreach (var miJugador in Jugadores)
            {
                hoja.Cells("C" + (fila2).ToString()).Value = miJugador.Nombre + " " + miJugador.Apellido;
                hoja.Cells("D" + (fila2).ToString()).Value = miJugador.Documento;
                hoja.Cells("E" + (fila2).ToString()).Value = miJugador.TipoJugador.ToString();
                hoja.Cells("F" + (fila2).ToString()).Value = miJugador.Pais;
                hoja.Cells("G" + (fila2).ToString()).Value = miJugador.CategoriaHandicap.ToString();
                hoja.Cells("H" + (fila2).ToString()).Value = miJugador.Handicap.ToString();
                hoja.Cells("I" + (fila2).ToString()).Value = miJugador.NroHandicap;
                hoja.Cells("J" + (fila2).ToString()).Value = miJugador.sn_pago == 1 ? "Al día" : "Vencido";
                hoja.Cells("K" + (fila2).ToString()).Value = miJugador.fec_ult_pago.ToString(); 
                hoja.Cells("L" + (fila2).ToString()).Value = miJugador.importe == null ? "" : ((int)miJugador.importe).ToString("0.00");
                hoja.Cells("M" + (fila2).ToString()).Value = miJugador.txt_url_comprobante;
                hoja.Cells("N" + (fila2).ToString()).Value = miJugador.JugadorID;
                hoja.Cells("O" + (fila2).ToString()).Value = miJugador.FechaAlta;
                hoja.Cells("P" + (fila2).ToString()).Value = miJugador.TipoPago;
                fila2++;
            }


            // test
            var stream = new MemoryStream();

            // processing the stream.
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(stream);
                memoryStream.Close();
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };
            result.Content.Headers.ContentDisposition =
            new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReporteCuentaCorrienteClubes.xlsx"
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;

            //Response.End();
        }

        public async Task<HttpResponseMessage> GetReporteJugadores()
        {
            //var miRuta = System.Web.Hosting.HostingEnvironment.MapPath(@"/a");
            var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteJugadores.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var Jugadores = (await reporteJugadoresService.GetAllPlayers()).ToList();
            int fila = 2;
            foreach (var miJugador in Jugadores)
            {
                hoja.Cells("A" + (fila).ToString()).Value = miJugador.JugadorID;
                hoja.Cells("B" + (fila).ToString()).Value = miJugador.Nombre;
                hoja.Cells("C" + (fila).ToString()).Value = miJugador.Apellido;
                hoja.Cells("D" + (fila).ToString()).Value = miJugador.TipoDoc;
                hoja.Cells("E" + (fila).ToString()).Value = miJugador.Documento;
                hoja.Cells("F" + (fila).ToString()).Value = miJugador.TipoJugador;
                hoja.Cells("G" + (fila).ToString()).Value = miJugador.FecNac;
                hoja.Cells("H" + (fila).ToString()).Value = miJugador.Nacionalidad;
                hoja.Cells("I" + (fila).ToString()).Value = miJugador.Genero;
                hoja.Cells("J" + (fila).ToString()).Value = miJugador.Activo;
                hoja.Cells("K" + (fila).ToString()).Value = miJugador.Mail;
                hoja.Cells("L" + (fila).ToString()).Value = miJugador.Telefono;
                hoja.Cells("M" + (fila).ToString()).Value = miJugador.Club;
                hoja.Cells("N" + (fila).ToString()).Value = miJugador.TipoHandicapPrincipal;
                hoja.Cells("O" + (fila).ToString()).Value = miJugador.CategoriaHandicapPrincipal;
                hoja.Cells("P" + (fila).ToString()).Value = miJugador.HandicapPrincipal;
                hoja.Cells("Q" + (fila).ToString()).Value = miJugador.TipoHandicapSecundario;
                hoja.Cells("R" + (fila).ToString()).Value = miJugador.CategoriaHandicapSecundario;
                hoja.Cells("S" + (fila).ToString()).Value = miJugador.HandicapSecundario;
                hoja.Cells("T" + (fila).ToString()).Value = miJugador.Domicilio;
                hoja.Cells("U" + (fila).ToString()).Value = miJugador.TipoDomicilio;
                hoja.Cells("V" + (fila).ToString()).Value = miJugador.Piso;
                hoja.Cells("W" + (fila).ToString()).Value = miJugador.Dpto;
                hoja.Cells("X" + (fila).ToString()).Value = miJugador.CodPostal;
                hoja.Cells("Z" + (fila).ToString()).Value = miJugador.Veedor;
                hoja.Cells("AA" + (fila).ToString()).Value = miJugador.AdminClub;
                fila++;
            }


            // test
            var stream = new MemoryStream();

            // processing the stream.
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(stream);
                memoryStream.Close();
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };
            result.Content.Headers.ContentDisposition =
            new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReporteJugadores.xlsx"
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;

            //Response.End();
        }

        public async Task<HttpResponseMessage> GetReporteAllCuentasCorrientesJugadores()
        {
            try
            {
                //var miRuta = System.Web.Hosting.HostingEnvironment.MapPath(@"/a");
                var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientesJugadores.xlsx"));
                IXLWorksheet hoja = libro.Worksheet(1);
                var Jugadores = (await reporteJugadoresService.GetAllCuentas()).ToList();
                int fila1 = 4;
                int fila2 = 8;

                hoja.Cells("C" + (fila1).ToString()).Value = Jugadores.Count();
                hoja.Cells("D" + (fila1).ToString()).Value = Jugadores.Count(x => x.importe > 0);
                hoja.Cells("E" + (fila1).ToString()).Value = Jugadores.Count(x => x.importe == 0);
                foreach (var miJugador in Jugadores)
                {
                    hoja.Cells("C" + (fila2).ToString()).Value = miJugador.Nombre + " " + miJugador.Apellido;
                    hoja.Cells("D" + (fila2).ToString()).Value = miJugador.Documento;
                    hoja.Cells("E" + (fila2).ToString()).Value = miJugador.TipoJugador.ToString();
                    hoja.Cells("F" + (fila2).ToString()).Value = miJugador.Pais;
                    hoja.Cells("G" + (fila2).ToString()).Value = miJugador.CategoriaHandicap.ToString();
                    hoja.Cells("H" + (fila2).ToString()).Value = miJugador.Handicap.ToString();
                    hoja.Cells("I" + (fila2).ToString()).Value = miJugador.NroHandicap;
                    hoja.Cells("J" + (fila2).ToString()).Value = miJugador.sn_pago == 1 ? "Pago" : "No pago";
                    hoja.Cells("K" + (fila2).ToString()).Value = miJugador.fec_ult_pago.ToString();
                    hoja.Cells("L" + (fila2).ToString()).Value = miJugador.importe == null ? "" : ((int)miJugador.importe).ToString("0.00");
                    hoja.Cells("M" + (fila2).ToString()).Value = miJugador.txt_url_comprobante;
                    hoja.Cells("N" + (fila2).ToString()).Value = miJugador.JugadorID;
                    hoja.Cells("O" + (fila2).ToString()).Value = miJugador.FechaAlta;
                    hoja.Cells("P" + (fila2).ToString()).Value = miJugador.TipoPago;
                    fila2++;
                }


                // test
                var stream = new MemoryStream();

                // processing the stream.
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    libro.SaveAs(memoryStream);
                    memoryStream.WriteTo(stream);
                    memoryStream.Close();
                }
                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(stream.ToArray())
                };
                result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = "ReporteCuentaCorrienteClubes.xlsx"
                };
                result.Content.Headers.ContentType =
                    new MediaTypeHeaderValue("application/octet-stream");

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //Response.End();
        }

        private static string incCol(string col)
        {
            if (col == "") return "A";
            string fPart = col.Substring(0, col.Length - 1);
            char lChar = col[col.Length - 1];
            if (lChar == 'Z') return incCol(fPart) + "A";
            return fPart + ++lChar;
        }
        public async Task<HttpResponseMessage> GetReporteInscripcionesEquiposExcel()
        {
            var equipos = (await reporteTorneoService.GetAll());

            var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteCuentasTorneos.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            int fila1 = 5;

            foreach (var miEquipo in equipos)
            {
                hoja.Cells("C" + (fila1).ToString()).Value = miEquipo.Torneo;
                hoja.Cells("D" + (fila1).ToString()).Value = miEquipo.Equipo;
                hoja.Cells("E" + (fila1).ToString()).Value = miEquipo.Club;
                hoja.Cells("F" + (fila1).ToString()).Value = miEquipo.NombreJugador;
                hoja.Cells("G" + (fila1).ToString()).Value = miEquipo.ApellidoJugador;
                hoja.Cells("H" + (fila1).ToString()).Value = miEquipo.sn_pago == 1 ? "Pagó" : "Pendiente";
                hoja.Cells("I" + (fila1).ToString()).Value = miEquipo.fec_ult_pago.ToString();
                hoja.Cells("J" + (fila1).ToString()).Value = miEquipo.importe;
                hoja.Cells("K" + (fila1).ToString()).Value = miEquipo.txt_url_comprobante;
                hoja.Cells("L" + (fila1).ToString()).Value = miEquipo.JugadorID;
                fila1++;
            }


            // test
            var stream = new MemoryStream();

            // processing the stream.
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(stream);
                memoryStream.Close();
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };
            result.Content.Headers.ContentDisposition =
            new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReporteCuentaTorneos.xlsx"
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }
        public async Task<HttpResponseMessage> GetReporteInscripcionesTorneosExcel(int id)
        {
            //TemplateReporteCuentasTorneos
            var equipos = (await reporteTorneoService.GetByID(id));
            //var miRuta = System.Web.Hosting.HostingEnvironment.MapPath(@"/a");
            //var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientesJugadores.xlsx"));

            var libro = new XLWorkbook();
            var nombreTorneo = equipos.First().Torneo.txt_desc;

            var AzulClubes = XLColor.FromArgb(0, 176, 240);
            var AmarilloEquipos= XLColor.FromArgb(255, 255, 0);
            var NaranjaTitulo = XLColor.FromArgb(255, 192, 0);
            var hoja = libro.Worksheets.Add("Equipos");
            //IXLWorksheet hoja = libro.Worksheet("Equipos");

            hoja.Cells("A1").Value = nombreTorneo;

            //IXLWorksheet hoja = libro.Worksheet(1);
            //var Jugadores = (await reporteJugadoresService.GetAll()).ToList();
            int fila1 = 4;
            //int fila2 = 8;

            var miLetra = "A";
            var miLetra2 = "B";
            var esMasculino = false;
            var RangoTemp = "";
            var maxFila = 4;
            var cantLoops = 1;
            IXLRange rngTemp;
            if (equipos.First().Usuarios.Count(x=>x.sexo== "Masculino") > 0)
            {
                esMasculino = true;
            }
            foreach (var miEquipo in equipos)
            {
                hoja.Cells(miLetra + (fila1).ToString()).Value = miEquipo.Club.txt_desc;

                RangoTemp = miLetra + (fila1).ToString() + ":" + miLetra2 + (fila1).ToString();
                rngTemp = hoja.Range(RangoTemp);
                rngTemp.Merge();
                rngTemp.FirstCell().Style
                    .Font.SetBold()
//                    .Font.SetFontName("Calibri")
                    .Fill.SetBackgroundColor(AzulClubes)
                    .Font.SetFontColor(XLColor.Black)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                    .Alignment.WrapText = true;
                ;
                fila1++;
                hoja.Cells(miLetra + (fila1).ToString()).Value = miEquipo.Equipo.txt_desc;
                RangoTemp = miLetra + (fila1).ToString() + ":" + miLetra2 + (fila1).ToString();
                rngTemp = hoja.Range(RangoTemp);
                rngTemp.Merge();
                rngTemp.FirstCell().Style
                    .Font.SetBold()
//                    .Font.SetFontName("Calibri")
                    .Fill.SetBackgroundColor(AmarilloEquipos)
                    .Font.SetFontColor(XLColor.Black)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                    .Alignment.WrapText = true;
                ;
                fila1++;
                foreach(var miJugador in miEquipo.Usuarios)
                {
                    hoja.Cells(miLetra + (fila1).ToString()).Value  = miJugador.txt_nombre + " " + miJugador.txt_apellido + (miJugador.bool_Capitan == 1 ? "(C)" : miJugador.bool_suplente == 1 ? "(S)" : "");
                    hoja.Cells(miLetra2 + (fila1).ToString()).Value = (miJugador.sexo == "Masculino" ? miJugador.handicap_principal : miJugador.handicap_secundario);
                    fila1++;
                }
                
                hoja.Cells(miLetra + (fila1).ToString()).Value = "Handicap Total";
                hoja.Cells(miLetra2 + (fila1).ToString()).Value = esMasculino ? miEquipo.Usuarios.Where(x=>x.bool_suplente == 0).Sum(x=>x.handicap_principal) : miEquipo.Usuarios.Where(x => x.bool_suplente == 0).Sum(x => x.handicap_secundario);
                fila1++;
                var capitan = miEquipo.Usuarios.Where(x => x.bool_Capitan == 1).FirstOrDefault();
                if(capitan != null)
                    hoja.Cells(miLetra + (fila1).ToString()).Value = capitan.telefono + " " + miEquipo.Usuarios.Where(x => x.bool_Capitan == 1).First().email;

                RangoTemp = miLetra + (fila1).ToString() + ":"+ miLetra2 + (fila1).ToString();
                rngTemp = hoja.Range(RangoTemp);
                rngTemp.Merge();
                hoja.Column(miLetra).Width = 30;
                hoja.Column(miLetra2).Width = 2.5;
                
                
                RangoTemp = miLetra + (4).ToString() + ":" + miLetra2 + (fila1).ToString();
                rngTemp = hoja.Range(RangoTemp);
                rngTemp.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTemp.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                if(miLetra == "J")
                {
                    if(cantLoops == 1)
                    {
                        maxFila = 0;
                    }
                    maxFila = fila1 + 2;
                    miLetra = "A";
                    miLetra2 = "B";
                    fila1 = maxFila;
                    cantLoops = cantLoops + 1;
                }
                else {
                    fila1 = maxFila;
                    miLetra = incCol(miLetra);
                    miLetra = incCol(miLetra);
                    miLetra = incCol(miLetra);
                    miLetra2 = incCol(miLetra2);
                    miLetra2 = incCol(miLetra2);
                    miLetra2 = incCol(miLetra2);
                }
            }
            for(int i=1; i <25; i++){
                hoja.Row(i).Height = 15;
            }
            RangoTemp = "A1:K1";
            rngTemp = hoja.Range(RangoTemp);
            rngTemp.Merge();
            rngTemp.FirstCell().Style
                .Font.SetBold()
                .Fill.SetBackgroundColor(NaranjaTitulo)
                .Font.SetFontColor(XLColor.Black)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Alignment.WrapText = true;
            ;



            // test
            var stream = new MemoryStream();

            // processing the stream.
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(stream);
                memoryStream.Close();
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };
            result.Content.Headers.ContentDisposition =
            new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReporteInscripcionTorneos.xlsx"
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;

            //Response.End();
        }


        public async Task<HttpResponseMessage> GetReporteInscripcionesTorneoJugadoresExcel(int id)
        {
            //TemplateReporteCuentasJugadorTorneo
            var equipos = (await reporteTorneoService.GetByID(id));
            var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientesTorneoJugadores.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var nombreTorneo = equipos.First().Torneo.txt_desc;

            hoja.Cells("A1").Value = nombreTorneo;
            
            int fila1 = 4;

            foreach (var miEquipo in equipos)
            {

                foreach (var miJugador in miEquipo.Usuarios)
                {
                    hoja.Cells("C" + (fila1).ToString()).Value = miJugador.txt_nombre + " " + miJugador.txt_apellido;
                    hoja.Cells("D" + (fila1).ToString()).Value = miJugador.handicap_principal;
                    hoja.Cells("E" + (fila1).ToString()).Value = miJugador.email;
                    hoja.Cells("F" + (fila1).ToString()).Value = miJugador.telefono;
                    if (miEquipo.sn_pago == 1)
                        hoja.Cells("G" + (fila1).ToString()).Value = "Pagó";
                    else
                        hoja.Cells("G" + (fila1).ToString()).Value = "No Pagó";
                }

                fila1++;
            }


            // test
            var stream = new MemoryStream();

            // processing the stream.
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(stream);
                memoryStream.Close();
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };
            result.Content.Headers.ContentDisposition =
            new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReporteInscripcionTorneosIndividual.xlsx"
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;

            //Response.End();
        }


        public class reportesClubesResult
        {
            public int cant_clubes;
            public int cant_abonaron;
            public int cant_no_abonaron;
            public List<reporteCuentaClub> lstClubes;
        }
        public class reportesJugadoresResult
        {
            public int cant_jugadores;
            public int cant_abonaron;
            public int cant_no_abonaron;
            public List<reporteCuentaJugadores> lstJugadores;
        }
        public class reportesInscripcionTorneo
        {
            public int cant_clubes;
            public int cant_equipos;
            public int cant_abonaron;
            public int cant_no_abonaron;
            public ttorneo Torneo;
            public List<reporteTorneo> lstEquipos;
        }
        public class reportesEquiposResult
        {
            public int cant_equipos;
            public int cant_abonaron;
            public int cant_no_abonaron;
            public int cant_torneos;
            public List<reporteCuentaTorneo> lstEquipos;
        }
    }
}
