﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AAPolo.Controllers
{
    public class PagoController : ApiController
    {
        private readonly IHistorialPagoService historialPagoService;
        private readonly SendMail sendMail;
        private readonly IUsuarioService usuarioService;
        IMapper Mapper;

        public PagoController(IHistorialPagoService _historialPagoService, IUsuarioService _usuarioService)
        {
            historialPagoService = _historialPagoService;
            sendMail = new SendMail();
            usuarioService = _usuarioService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        // GET: api/PagoHandicap/5
        public async Task<IHttpActionResult> Get(int id)
        {
            var historialPagos = Mapper.Map<List<thistorial_pagosDTO>>(await historialPagoService.GetByCodUsuario(id));
            return Ok(historialPagos);
        }

        public async Task<IHttpActionResult> Get(string cod_club)
        {
            var historialPagos = Mapper.Map<List<thistorial_pagosDTO>>(await historialPagoService.GetByCodClub(Convert.ToInt32(cod_club)));
            return Ok(historialPagos);
        }


        public async Task<IHttpActionResult> Get(int page, string search)
        {
            var users = (await usuarioService.searchUsersSinHandicap(page, search));
            var skipped = users.Take(30);
            var usersMapped = Mapper.Map<List<usuarioSelectDTO>>(users);
            var result = new
            {
                pages = users.Count(),
                usersDTO = usersMapped
            };
            return Ok(result);
        }

        // POST: api/PagoHandicap
        public async Task<IHttpActionResult> Post([FromBody]string data)
        {

            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var pago = JsonConvert.DeserializeObject<thistorial_pagosDTO>(data, dateTimeConverter);
            var pagoAGuardar = Mapper.Map<thistorial_pagos>(pago);
            if (pago.cod_historial_pagos != 0)
            {
                await historialPagoService.Update(pagoAGuardar);
                return Ok("Se ha modificado correctamente el pago");
            }
            if ((await historialPagoService.Save(pagoAGuardar)) == 1)
            {
                if (pagoAGuardar.nro_resultado_pago == 1)
                    sendMail.SendMailNotificacion("Se registro un pago " + pago.txt_desc + ", de " + pago.nro_importe, "Se ha registrado un pago");

                return Ok("Se ha creado correctamente el pago");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo pagarse"));
            }
        }

        // PUT: api/PagoHandicap/5
        public async Task<IHttpActionResult> Put(int id, [FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var pago = JsonConvert.DeserializeObject<thistorial_pagosDTO>(data, dateTimeConverter);
            var pagoAGuardar = Mapper.Map<thistorial_pagos>(pago);
            if ((await historialPagoService.Update(pagoAGuardar)) == 1)
            {
                if (pagoAGuardar.nro_resultado_pago == 1)
                    sendMail.SendMailNotificacion("Se registro un pago " + pago.txt_desc + ", de " + pago.nro_importe, "Se ha registrado un pago");
                return Ok("Se ha creado correctamente el pago");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo pagarse"));
            }
        }

        // DELETE: api/PagoHandicap/5
        public void Delete(int id)
        {
        }
    }
}
