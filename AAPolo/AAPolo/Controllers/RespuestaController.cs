﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class RespuestaController : ApiController
    {
        private readonly IRespuestaService respuestaService;
        IMapper Mapper;

        public RespuestaController(IRespuestaService _respuestaService)
        {
            respuestaService = _respuestaService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }


        // GET: api/Respuesta
        [HttpGet]
        public async Task<IHttpActionResult> Get(int idRespuesta)
        {
            var result = new
            {
                enviados = Mapper.Map<List<trespuestaDTO>>(await respuestaService.GetEnviadosPorUsuario(idRespuesta)),
                recibidos = Mapper.Map<List<trespuestaDTO>>(await respuestaService.GetRecibidosPorUsuario(idRespuesta))
            };

            return Ok(result);
        }

        // GET: api/Respuesta
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            var result = Mapper.Map<trespuestaDTO>(respuestaService.GetRespuestaById(id));
            return Ok(result);
        }

        public async Task<IHttpActionResult> Get()
        {
            var result = Mapper.Map<List<trespuestaDTO>>(await respuestaService.GetRespuestasALaAAP());

            return Ok(result);
        }

        // POST: api/Respuesta
        public async Task<IHttpActionResult> Post([FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var respuestDTO = JsonConvert.DeserializeObject<trespuestaDTO>(data, dateTimeConverter);

            var respuestaAGuardar = Mapper.Map<trespuesta>(respuestDTO);
            await respuestaService.Save(respuestaAGuardar);
            return Ok("Se ha creado correctamente el miembro");
        }

        // PUT: api/Respuesta/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Respuesta/5
        public void Delete(int id)
        {
        }
    }
}
