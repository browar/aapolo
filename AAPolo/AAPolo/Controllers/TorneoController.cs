﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AAPolo.Controllers
{
    [EnableCors(origins: "http://localhost:56596", headers: "*", methods: "*", SupportsCredentials = true)]
    public class TorneoController : ApiController
    {
        private readonly ITorneoService torneoService;
        private readonly ICategoriaTorneoService categoriaTorneoService;
        private readonly ITorneoHistoricoService torneoHistoricoService;
        private readonly IEquipoService equipoService;
        private readonly ICategoriaHandicapService categoriaHandicapService;
        private readonly SendMail sendMail;
        IMapper Mapper;

        public TorneoController(ITorneoService _torneoService, ICategoriaTorneoService _categoriaTorneoService, ITorneoHistoricoService _torneoHistoricoService,
            IEquipoService _equipoService, ICategoriaHandicapService _categoriaHandicapService)
        {
            torneoService = _torneoService;
            categoriaTorneoService = _categoriaTorneoService;
            torneoHistoricoService = _torneoHistoricoService;
            equipoService = _equipoService;
            categoriaHandicapService = _categoriaHandicapService;
            sendMail = new SendMail();
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        public async Task<IHttpActionResult> Get()
        {
            var torneos = (await torneoService.GetAll());
            var result = new
            {
                torneosDTO = Mapper.Map<List<torneoHome>>(torneos),
                categoriasDTO = Mapper.Map<List<tcategoria_torneoDTO>>(await categoriaTorneoService.GetAll()),
                categoriaHandicapDTO = Mapper.Map<List<tcategoria_handicapDTO>>(await categoriaHandicapService.GetAll()),
                torneoHistoricoDTO = Mapper.Map<List<ttorneo_historicoDTO>>(await torneoHistoricoService.GetAll())
            };

            return Ok(result);
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var torneoDTO = Mapper.Map<ttorneoDTO>(torneoService.GetById(id));
            return Ok(torneoDTO);
        }

        public async Task<IHttpActionResult> GetTorneosHome()
        {
            var torneos = (await torneoService.GetAll());
            var torneosDTO = Mapper.Map<List<torneoHome>>(torneos);

            return Ok(torneosDTO);
        }


        public async Task<IHttpActionResult> Post([FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var torneoDTO = JsonConvert.DeserializeObject<ttorneoDTO>(data, dateTimeConverter);
            var torneoAGuardar = Mapper.Map<ttorneo>(torneoDTO);
            if ((await torneoService.Save(torneoAGuardar)) == 1)
            {
                sendMail.SendMailNotificacion("El usuario: " + torneoDTO.usuario_alta + " ha creado el torneo " +
                        torneoDTO.txt_desc, "Se ha creado un torneo");

                return Ok("Se ha creado correctamente el torneo");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo agregarse el torneo"));
            }
        }

        public async Task<IHttpActionResult> Put([FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var torneoDTO = JsonConvert.DeserializeObject<ttorneoDTO>(data, dateTimeConverter);
            var torneoAModificar = Mapper.Map<ttorneo>(torneoDTO);

            if ((await torneoService.Update(torneoAModificar)) == 1)
            {
                return Ok("Se ha modificado correctamente el torneo");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo modificarse el torneo"));
            }

        }
        public async Task<IHttpActionResult> Delete(int id)
        {
            if ((await torneoService.Delete(id)) == 1)
            {
                return Ok("Se ha dado de baja correctamente el torneo");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo darse de baja el torneo"));
            }
        }
    }
}
