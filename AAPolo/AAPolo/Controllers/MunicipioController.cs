﻿using AAPolo.Entity.DTO;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class MunicipioController : ApiController
    {
        private readonly IMunicipioService municipioService;
        IMapper Mapper;

        public MunicipioController(IMunicipioService _municipioService)
        {
            municipioService = _municipioService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        // GET: api/Municipio
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Municipio/5
        public async Task<IHttpActionResult> Get(int id)
        {
            var result = new
            {
                municipios = Mapper.Map<List<tmunicipioDTO>>(await municipioService.GetByCodProvincia(id))
            };

            return Ok(result);
        }

        // POST: api/Municipio
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Municipio/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Municipio/5
        public void Delete(int id)
        {
        }
    }
}
