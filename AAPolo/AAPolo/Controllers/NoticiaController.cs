﻿using AAPolo.Data.Context.Interface;
using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Linq;

namespace AAPolo.Controllers
{
    [EnableCors(origins: "http://localhost:56596", headers: "*", methods: "*", SupportsCredentials = true)]
    public class NoticiaController : ApiController
    {
        private readonly INoticiaService noticiaService;
        private readonly ITetiquetaService etiquetaService;
        private readonly ITipoNoticiaService tipoNoticiasService;
        private readonly ITnoticia_archivoService noticiaArchivoService;
        private readonly IAAPoloContext iAAPoloContext;

        IMapper Mapper;
        public NoticiaController(INoticiaService _noticiaService, ITetiquetaService _etiquetaService, ITipoNoticiaService _tipoNoticiasService,
            ITnoticia_archivoService _noticiaArchivoService, IAAPoloContext _iAAPoloContext)
        {
            noticiaService = _noticiaService;
            etiquetaService = _etiquetaService;
            tipoNoticiasService = _tipoNoticiasService;
            noticiaArchivoService = _noticiaArchivoService;
            iAAPoloContext = _iAAPoloContext;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }
        // GET: api/Noticia
        public async Task<IHttpActionResult> Get()
        {
            var noticiasDTO = Mapper.Map<List<tnoticiaDTO>>(await noticiaService.GetAll());
            return Ok(noticiasDTO);
        }

        // GET: api/Noticia
        [HttpGet]
        public IHttpActionResult GetNoticiasBySP()
        {
            return Ok(noticiaService.GetAllBySp());
        }

        public IHttpActionResult ObtenerActivas()
        {
            var misNoticias = noticiaService.GetNoticiasActivas();
            return Ok(misNoticias);
        }

        public IHttpActionResult ObtenerDestacadas()
        {
            var misNoticias = noticiaService.GetNoticiasDestacadas();
            return Ok(misNoticias);
        }

        public IHttpActionResult Get(int id)
        {
            var noticiasDTO = Mapper.Map<tnoticiaDTO>(noticiaService.GetById(id));
            return Ok(noticiasDTO);
        }
        public IHttpActionResult Get(string cod_noticia)
        {
            //var noticiasRelacionadas = noticiaService.GetRelatedById(cod_noticia);


            //object[] miParametro = OfType<SqlParameter>();
            var misParametros = new List<System.Data.SqlClient.SqlParameter>();
            System.Data.SqlClient.SqlParameter parameter = new System.Data.SqlClient.SqlParameter();
            parameter.ParameterName = "@cod_noticia";
            parameter.SqlDbType = System.Data.SqlDbType.Int;
            parameter.Direction = System.Data.ParameterDirection.Input;
            parameter.Value = Convert.ToInt32(cod_noticia.ToString().Replace(@"""", @""));
            misParametros.Add(parameter);
            var miObj = iAAPoloContext.ExecuteStoredProcedureBrowar<tnoticia>("sp_get_noticias_relacionadas", misParametros.ToArray());
            List<tnoticiaDTO> noticiasRelacionadas = new List<tnoticiaDTO>();
            tnoticiaDTO miNoti;
            
            //var todasLasNoticias = Mapper.Map<List<tnoticiaDTO>>(noticiaService.GetAll());
            ////noticiasRelacionadas = todasLasNoticias[0].cod_noticia Where(x => );
            //foreach(var miNoticia in todasLasNoticias)
            //{
            //    foreach(var misRelas in miObj)
            //    {
            //        if (miNoticia.cod_noticia == misRelas.cod_noticia)
            //        {
            //            miNoti = Mapper.Map<tnoticiaDTO>(miNoticia);
            //            noticiasRelacionadas.Add(miNoti);
            //        }
            //    }
            //    
            //}
            
            foreach (var miNoticia in miObj)
            {
                miNoti = new tnoticiaDTO();
                miNoti = Mapper.Map<tnoticiaDTO>(noticiaService.GetById(miNoticia.cod_noticia)); //miNoticia);// 
                noticiasRelacionadas.Add(miNoti);
            }
            var noticiasDTO = Mapper.Map<List<tnoticiaDTO>>(noticiasRelacionadas);//noticiaService.GetRelatedById(id));
            return Ok(noticiasRelacionadas);
        }
        
        public async Task<IHttpActionResult> Post([FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var noticia = JsonConvert.DeserializeObject<tnoticiaDTO>(data, dateTimeConverter);
            
            var noticiaAGuardar = Mapper.Map<tnoticia>(noticia);

            if ((await noticiaService.Save(noticiaAGuardar)) == 0)
            {
                return Ok("Se ha creado correctamente a la noticia");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo grabar la noticia"));
            }
        }
        public async Task<IHttpActionResult> Put(int id, [FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var noticia = JsonConvert.DeserializeObject<tnoticiaDTO>(data, dateTimeConverter);
            var noticiaAGuardar = Mapper.Map<tnoticia>(noticia);

            if ((await noticiaService.Update(noticiaAGuardar)) == 0)
            {
                return Ok("Se ha modificado correctamente la noticia");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo modificarse la noticia"));
            }
        }

        public async Task<IHttpActionResult> EditarEstadoNoticia(int id)
        {
            var result = await noticiaService.UpdateEstado(id);
            return Ok(result);
        }
        public async Task<IHttpActionResult> EditarNoticiaDestacada(int id)
        {
            var result = await noticiaService.UpdateEstadoDestacada(id);
            return Ok(result);
        }
        public void Delete(int id)
        {
        }
        public async Task<IHttpActionResult> GetEtiquetas()
        {
            var etiquetasDTO = Mapper.Map<List<tetiquetaDTO>>(await etiquetaService.GetAll());
            return Ok(etiquetasDTO);
        }
        public async Task<IHttpActionResult> GetCategoriasNoticias()
        {
            var noticiasDTO = Mapper.Map<List<ttipo_noticiaDTO>>(await tipoNoticiasService.GetAll());
            return Ok(noticiasDTO);
        }
    }
}
