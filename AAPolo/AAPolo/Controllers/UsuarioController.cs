﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AAPolo.Controllers
{
    public class UsuarioController : ApiController
    {
        private readonly IUsuarioService jugadorService;
        private readonly IHandicapUsuarioService handicapService;
        private readonly ICategoriaHandicapService categoriaHandicapService;
        private readonly IClubService clubService;
        private readonly IPaisService paisService;
        private readonly ITipoJugadorService tipoJugadorService;
        private readonly SendMail sendMail;
        IMapper Mapper;

        public UsuarioController(IUsuarioService _jugadorService, IHandicapUsuarioService _handicapService, ICategoriaHandicapService _categoriaHandicapService,
            IClubService _clubService, IPaisService _paisService, ITipoJugadorService _tipoJugadorService)
        {
            jugadorService = _jugadorService;
            handicapService = _handicapService;
            categoriaHandicapService = _categoriaHandicapService;
            clubService = _clubService;
            paisService = _paisService;
            tipoJugadorService = _tipoJugadorService;
            sendMail = new SendMail();
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        public async Task<IHttpActionResult> Get()
        {
            var users = (await jugadorService.GetBySelect());

            return Ok(Mapper.Map<List<usuarioSelectDTO>>(users));
        }

        public IHttpActionResult Get(int page, string nombre, int cod_tipo_jugador, int cod_club, int nro_hand)
        {
            var usersSearch = (jugadorService.GetManyBySP(page, nombre, cod_tipo_jugador, cod_club, nro_hand, 20));
            if (usersSearch.Count > 0)
            {
                var result = new
                {
                    pages = usersSearch[0].usersCount,
                    usersDTO = usersSearch
                };
                return Ok(result);
            }
            else
            {
                var result = new
                {
                    pages = 0,
                    usersDTO = usersSearch
                };
                return Ok(result);
            }
        }

        public IHttpActionResult GetPublicos(int page, string nombre, int cod_tipo_jugador, int cod_club, int nro_hand)
        {
            var usersSearch = jugadorService.GetManyBySP(page, nombre, cod_tipo_jugador, cod_club, nro_hand, 12);
            var result = new
            {
                pages = usersSearch[0].usersCount,
                usersDTO = usersSearch
            };

            return Ok(result);
        }

        public async Task<IHttpActionResult> Get(int page, string search)
        {
            var users = (await jugadorService.searchUsers(page, search));
            var skipped = users.Take(30);
            var usersMapped = Mapper.Map<List<usuarioSelectDTO>>(users);
            var result = new
            {
                pages = users.Count(),
                usersDTO = usersMapped
            };
            return Ok(result);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetUsersPase(int page, string search)
        {
            var users = (await jugadorService.searchUsersPase(page, search));
            var skipped = users.Take(30);
            var usersMapped = Mapper.Map<List<usuarioSelectDTO>>(users);
            return Ok(usersMapped);
        }

        public async Task<IHttpActionResult> Get(int page)
        {
            var users = (await jugadorService.GetAll());
            var skipped = users.Skip((page - 1) * 20).Take(20);
            var usersMapped = Mapper.Map<List<JugadoresDTO>>(skipped);
            var result = new
            {
                pages = users.Count(),
                usersDTO = usersMapped
            };
            return Ok(result);
        }

        [HttpGet]
        public IHttpActionResult GetBySP(int page)
        {
            var usersMapped = jugadorService.GetAllBySP(page);
            var result = new
            {
                pages = usersMapped[0].usersCount,
                usersDTO = usersMapped
            };
            return Ok(result);
        }

        public async Task<IHttpActionResult> Get(string orden, int? idClub, int? page)
        {
            if (orden == "thandicap" && idClub.Value == 0)
            {
                var usersDTO = jugadorService.GetImportantUsers(page.Value);
                if (usersDTO.Count > 0)
                {
                    var result = new
                    {
                        pages = usersDTO[0].usersCount,
                        usersDTO = usersDTO
                    };
                    return Ok(result);
                }
                else
                {
                    return Ok();
                }
            }
            else if (idClub.HasValue)
            {
                var usersDTO = Mapper.Map<List<JugadoresDTO>>(await jugadorService.GetByClub(idClub.Value));
                return Ok(usersDTO);
            }
            return Ok("No se reconoce el orden solicitado");
        }


        public async Task<IHttpActionResult> Get(int? id)
        {
            var usersEntity = jugadorService.GetById(id.Value);
            usersEntity.thistorial_pagos = null;
            usersEntity.thistorial_pagos = (await jugadorService.GetHistorialPagos(id.Value)).ToList();
            var usersDTO = Mapper.Map<usersDTO>(usersEntity);
            foreach (var item in usersDTO.thandicap_usuario)
            {
                if (item.bool_principal.HasValue)
                    item.thandicap_usuario_historico = Mapper.Map<List<thandicap_usuario_historicoDTO>>(await handicapService.GetHistoricos(item.cod_usuario, item.bool_principal.Value))
                        .OrderByDescending(x => x.cod_handicap_usuario_historico).ToList();
            }
            return Ok(usersDTO);
        }

        public async Task<IHttpActionResult> Post([FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var usuario = JsonConvert.DeserializeObject<usersDTO>(data, dateTimeConverter);
            var usuarioAGuardar = Mapper.Map<users>(usuario);
            var existeDocumento = (await jugadorService.GetByDocument(usuarioAGuardar.nro_doc));
            if (existeDocumento == null)
            {
                if ((await jugadorService.Save(usuarioAGuardar)) == 1)
                {
                    var clubPrincipal = usuarioAGuardar.tuser_club.FirstOrDefault(x => x.bool_principal == true);
                    if (usuario.isAdmin)
                        await clubService.UpdateAdminClub(usuarioAGuardar.cod_usuario, clubPrincipal.cod_club.Value);

                    sendMail.SendMailNotificacion("El usuario: " + usuarioAGuardar.usuario_alta + " ha creado al jugador " +
                        usuarioAGuardar.txt_nombre + " " + usuarioAGuardar.txt_apellido, "Se ha dado de alta un jugador");

                    return Ok("Se ha creado correctamente al jugador");
                }
                else
                {
                    return InternalServerError(new Exception("Ocurrió un error y no pudo agregarse al jugador"));
                }
            }
            else
            {
                return InternalServerError(new Exception("El número de documento está duplicado"));
            }
        }

        public async Task<IHttpActionResult> Put(int id, [FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var usuario = JsonConvert.DeserializeObject<usersDTO>(data, dateTimeConverter);
            var usuarioAModificar = Mapper.Map<users>(usuario);
            var existeDocumento = (await jugadorService.GetByDocument(usuarioAModificar.nro_doc));
            if (existeDocumento != null)
            {
                if (existeDocumento.cod_usuario != usuarioAModificar.cod_usuario)
                    return InternalServerError(new Exception("El número de documento está duplicado"));
            }

            if ((await jugadorService.Update(usuarioAModificar)) == 1)
            {
                var clubPrincipal = usuarioAModificar.tuser_club.FirstOrDefault(x => x.bool_principal == true);
                if (usuario.isAdmin)
                    await clubService.UpdateAdminClub(usuarioAModificar.cod_usuario, clubPrincipal.cod_club.Value);
                else
                    await clubService.UpdateAdminClub(0, clubPrincipal.cod_club.Value);


                return Ok("Se ha modificado correctamente al jugador");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo modificarse al jugador"));
            }


        }

        [HttpGet]
        public async Task<IHttpActionResult> GetUsuarioSinPago(int idJugador)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var handicap = Mapper.Map<thandicap_usuario_historicoDTO>(await jugadorService.GetHistoricoSinPago(idJugador));

            return Ok(handicap);
        }


        public async Task<IHttpActionResult> Delete(int id)
        {
            if ((await jugadorService.Delete(id)) == 1)
            {
                return Ok("Se ha dado de baja correctamente al jugador");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo darse de baja al jugador"));
            }
        }


        public async Task BlanquearContrasena(int cod_usuario)
        {
            await jugadorService.BlanquearContrasena(cod_usuario);
        }

        public void ActualizaArticulo(int cod_usuario)
        {
            jugadorService.ActualizaArticulo(cod_usuario);
        }
    }
}
