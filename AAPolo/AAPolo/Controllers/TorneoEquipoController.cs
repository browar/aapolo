﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class TorneoEquipoController : ApiController
    {
        private readonly IEquipoService equipoService;
        private readonly SendMail sendMail;
        private readonly ITorneoService torneoService;
        private readonly IUsuarioService usuarioService;
        private readonly IHistorialPagoService historialPagoService;
        IMapper Mapper;

        public TorneoEquipoController(IEquipoService _equipoService, ITorneoService _torneoService, IUsuarioService _usuarioService, IHistorialPagoService _historialPagoService)
        {
            this.equipoService = _equipoService;
            sendMail = new SendMail();
            torneoService = _torneoService;
            usuarioService = _usuarioService;
            historialPagoService = _historialPagoService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        // GET: api/PagoHandicap
        public async Task<IHttpActionResult> Get(int cod_usuario)
        {
            var result = Mapper.Map<List<tequipoDTO>>(await equipoService.GetByUser(cod_usuario));
            return Ok(result);
        }


        public async Task<IHttpActionResult> Post([FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var equipoDTO = JsonConvert.DeserializeObject<tequipoDTO>(data, dateTimeConverter);
            var equipoAGuardar = Mapper.Map<tequipo>(equipoDTO);
            if ((await equipoService.Save(equipoAGuardar)) == 1)
            {
                sendMail.SendMailNotificacion("Se creo el equipo: " + equipoDTO.txt_desc, "Se ha creado un equipo");

                return Ok("Se ha creado correctamente el torneo");
            }
            else
            {
                return InternalServerError(new Exception("Ocurrió un error y no pudo agregarse el torneo"));
            }
        }

        public async Task<IHttpActionResult> Put(int id, [FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var equipoDTO = JsonConvert.DeserializeObject<tequipoDTO>(data, dateTimeConverter);
            var equipoAGuardar = Mapper.Map<tequipo>(equipoDTO);
            await equipoService.PagarEquipo(id, equipoAGuardar.cod_torneo, 1);

            return Ok("Equipo Pago");

        }

        public async Task<IHttpActionResult> ValidarEquipo([FromBody]string data)
        {
            var format = "dd/MM/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            var equipoDTO = JsonConvert.DeserializeObject<tequipoDTO>(data, dateTimeConverter);
            var equipo = Mapper.Map<tequipo>(equipoDTO);
            var torneo = torneoService.GetById(equipo.cod_torneo);
            var handicapEquipo = 0;
            var handicapEspecífico = torneo.nro_handicap_especifico.HasValue;
            var errorPago = false;
            var errorValidacion = false;
            var jugadores = new List<string>();
            var listaErrores = new List<string>();
            foreach (var item in equipo.tequipo_user)
            {
                var usuarioABuscar = usuarioService.GetById(item.cod_usuario);
                item.users = usuarioABuscar;
                var historialPago = (await historialPagoService.GetByCodUsuario(item.cod_usuario));
                var jugadorPago = historialPago.Any(x => x.fec_vencimiento > DateTime.Now && x.cod_tipo_pago == 1 && x.nro_resultado_pago == 1);

                if (!jugadorPago)
                {
                    errorPago = true;
                    jugadores.Add("El jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido + " no tiene pago el handicap");

                }
            }


            foreach (var item in equipo.tequipo_user)
            {
                var usuarioABuscar = usuarioService.GetById(item.cod_usuario);
                item.users = usuarioABuscar;
                var boolCumple = usuarioABuscar.thandicap_usuario.Any(p => p.thandicap.cod_categoria_handicap == torneo.cod_categoria_handicap
                || p.thandicap.cod_categoria_handicap == 1);

                if (!boolCumple)
                {
                    errorValidacion = true;
                    listaErrores.Add("El jugador no cumple con la categoría de handicap correspondiente. Jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido);
                }

                if (usuarioABuscar.thandicap_usuario.Count > 0)
                {
                    var handicapPrincipal = usuarioABuscar.thandicap_usuario.Where(p => p.bool_principal.HasValue).FirstOrDefault();

                    if (handicapPrincipal.nro_handicap < torneo.nro_handicap_jugador_minimo)
                    {
                        errorValidacion = true;
                        listaErrores.Add("Uno o más jugadores tienen menos handicap que el mínimo del torneo. Jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido);
                    }

                    if (handicapPrincipal.nro_handicap > torneo.nro_handicap_jugador_maximo)
                    {
                        errorValidacion = true;
                        listaErrores.Add("Uno o más jugadores tienen más alto handicap que el máximo del torneo. Jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido);
                    }

                    if (torneo.tcategoria_torneo.txt_desc == "Juveniles" && usuarioABuscar.ttipo_jugador.txt_desc != "Juvenil")
                    {
                        var boolAcepta = false;
                        if(torneo.bool_menores.HasValue)
                        boolAcepta = torneo.bool_menores.Value;

                        if (boolAcepta && usuarioABuscar.ttipo_jugador.txt_desc != "Menor")
                        {
                            errorValidacion = true;
                            listaErrores.Add("El jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido + " es " + usuarioABuscar.ttipo_jugador.txt_desc +
                                " y no puede anotarse a un torneo de juveniles");
                        }
                        if (!boolAcepta && usuarioABuscar.ttipo_jugador.txt_desc == "Menor")
                        {
                            errorValidacion = true;
                            listaErrores.Add("El jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido + " es " + usuarioABuscar.ttipo_jugador.txt_desc +
                                " y no puede anotarse a un torneo de juveniles");
                        }
                    }

                    if (torneo.tcategoria_torneo.txt_desc == "Menores" && usuarioABuscar.ttipo_jugador.txt_desc != "Menor")
                    {
                        errorValidacion = true;
                        listaErrores.Add("El jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido + " es " + usuarioABuscar.ttipo_jugador.txt_desc +
                            " y no puede anotarse a un torneo de menores");
                    }

                    if (!item.bool_suplente.Value && torneo.tcategoria_torneo.txt_desc != "Femenino")
                    {
                        handicapEquipo = handicapEquipo + handicapPrincipal.nro_handicap.Value;
                    }
                    else if (torneo.tcategoria_torneo.txt_desc == "Femenino")
                    {
                        handicapPrincipal = usuarioABuscar.thandicap_usuario.Where(p => p.bool_principal.Value != true).FirstOrDefault();
                        if (handicapPrincipal != null)
                        {
                            if (handicapPrincipal.nro_handicap != null)
                                handicapEquipo = handicapEquipo + handicapPrincipal.nro_handicap.Value;
                            else
                            {
                                errorValidacion = true;
                                listaErrores.Add("La jugadora no posee handicap femenino. Jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido);
                            }
                        }
                        else
                        {
                            errorValidacion = true;
                            listaErrores.Add("La jugadora no posee handicap femenino. Jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido);
                        }
                    }

                    var inscriptoEnOtroEquipo = (await equipoService.GetByTorneoYCodUsuario(equipo.cod_torneo, item.cod_usuario));
                    if (inscriptoEnOtroEquipo)
                    {
                        errorValidacion = true;
                        listaErrores.Add("Uno o más jugadores están inscriptos en otro equipo. Jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido);
                    }

                    if (torneo.tcategoria_torneo.txt_desc == "Femenino" && usuarioABuscar.tgenero.txt_desc != "Femenino")
                    {
                        errorValidacion = true;
                        listaErrores.Add("El jugador: " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido + " no puede anotarse a un torneo Femenino");
                    }
                }
                else
                {
                    errorValidacion = true;
                    listaErrores.Add("El jugador " + usuarioABuscar.txt_nombre + " " + usuarioABuscar.txt_apellido + " no tiene handicap asignado");
                }
            }

            if (handicapEspecífico)
            {
                var cumpleRegla = equipo.tequipo_user.Any(p => p.users.thandicap_usuario.Any(x => x.nro_handicap == torneo.nro_handicap_especifico.Value));

                if (!cumpleRegla)
                {
                    errorValidacion = true;
                    listaErrores.Add("El equipo no tiene del número de handicap específico solicitado: " + torneo.nro_handicap_especifico.Value);
                }
            }

            if (torneo.nro_handicap_equipo_minimo > handicapEquipo)
            {
                errorValidacion = true;
                listaErrores.Add("El equipo no cumple con el mínimo de handicap del torneo");
            }

            if (torneo.nro_handicap_equipo_maximo < handicapEquipo)
            {
                errorValidacion = true;
                listaErrores.Add("El equipo excede el máximo de handicap del torneo");
            }

            if (errorPago || errorValidacion)
            {
                var result = new
                {
                    error = listaErrores,
                    errorWarning = jugadores
                };
                return Ok(result);
            }

            return Ok("Equipo válido");
        }

    }
}
