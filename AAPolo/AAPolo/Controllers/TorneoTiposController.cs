﻿using AAPolo.Entity.DTO;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AAPolo.Controllers
{
    public class TorneoTiposController : ApiController
    {
        private readonly ICategoriaHandicapService categoriaHandicapService;
        private readonly ICategoriaTorneoService categoriaTorneoService;
        private readonly IUsuarioService usuarioService;
        private readonly IInstanciaService instanciaService;
        private readonly IRefereeService refereeService;
        IMapper Mapper;

        public TorneoTiposController(ICategoriaHandicapService _categoriaHandicapService, ICategoriaTorneoService _categoriaTorneoService,
            IUsuarioService _usuarioService, IInstanciaService _instanciaService, IRefereeService _refereeService)
        {
            categoriaHandicapService = _categoriaHandicapService;
            categoriaTorneoService = _categoriaTorneoService;
            usuarioService = _usuarioService;
            instanciaService = _instanciaService;
            refereeService = _refereeService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        public async Task<IHttpActionResult> Get()
        {
            var result = new
            {
                categoriasHandicap = Mapper.Map<List<tcategoria_handicapDTO>>(await categoriaHandicapService.GetAll()),
                categoriasTorneo = Mapper.Map<List<tcategoria_torneoDTO>>(await categoriaTorneoService.GetAll()),
                veedores = Mapper.Map<List<usuarioSelectDTO>>(await usuarioService.GetVeedores()),
                instancias = Mapper.Map<List<tinstanciaDTO>>(await instanciaService.GetAll()),
                referees = Mapper.Map<List<trefereeDTO>>(await refereeService.GetAll())
            };
            return Ok(result);
        }
    }
}
