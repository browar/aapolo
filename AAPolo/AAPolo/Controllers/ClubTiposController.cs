﻿using AAPolo.Entity.DTO;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AAPolo.Controllers
{
    public class ClubTiposController : ApiController
    {
        private readonly ICategoriaClubService categoriaClubService;
        private readonly IUsuarioService usuarioService;
        private readonly IProvinciaService provinciaService;
        private readonly IPaisService paisService;
        private readonly IMunicipioService municipioService;
        IMapper Mapper;

        public ClubTiposController(ICategoriaClubService _categoriaClubService, IUsuarioService _usuarioService, IProvinciaService _provinciaService,
            IPaisService _paisService, IMunicipioService _municipioService)
        {
            categoriaClubService = _categoriaClubService;
            usuarioService = _usuarioService;
            provinciaService = _provinciaService;
            paisService = _paisService;
            municipioService = _municipioService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        // GET: api/ClubTipos/5
        public async Task<IHttpActionResult> Get()
        {
            var result = new
            {
                categoriaClubes = Mapper.Map<List<tcategoria_clubDTO>>(await categoriaClubService.GetAll()),
                //usuarios = Mapper.Map<List<usuarioSelectDTO>>((await usuarioService.GetAll())),
                paises = Mapper.Map<List<tpaisDTO>>(await paisService.GetAll()),
                provincias = Mapper.Map<List<tprovinciaDTO>>(await provinciaService.GetAll())
            };

            return Ok(result);
        }

        // POST: api/ClubTipos
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ClubTipos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ClubTipos/5
        public void Delete(int id)
        {
        }
    }
}
