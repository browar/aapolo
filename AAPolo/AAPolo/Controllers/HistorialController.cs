﻿using AAPolo.Entity.DTO;
using AAPolo.Entity.Entity;
using AAPolo.Service.Service.Interface;
using AAPolo.Utils;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AAPolo.Controllers
{
    [EnableCors(origins: "http://localhost:56596", headers: "*", methods: "*", SupportsCredentials = true)]
    public class HistorialController : ApiController
    {
        private readonly IClubService clubService;
        private readonly ITorneoEquipoService torneoEquipoService;
        private readonly IReconocimientoUsuarioService reconocimientoUsuarioService;
        IMapper Mapper;

        public HistorialController(IClubService _clubService, ITorneoEquipoService _torneoEquipoService, IReconocimientoUsuarioService _reconocimientoUsuarioService)
        {
            clubService = _clubService;
            torneoEquipoService = _torneoEquipoService;
            reconocimientoUsuarioService = _reconocimientoUsuarioService;
            Mapper = ConfigureAutoMapper.MapperConfiguration.CreateMapper();
        }

        public async Task<IHttpActionResult> Get(int cod_usuario)
        {
            var result = new
            {
                torneosHistorial = Mapper.Map<List<tuser_clubDTO>>(await torneoEquipoService.GetByUser(cod_usuario)),
                galardones = reconocimientoUsuarioService.GetByUser(cod_usuario)
            };
            return Ok(result);
        }
    }
}
