﻿using AAPolo.Entity.Entity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace AAPolo.Data.Context.Interface
{
    public interface IAAPoloContext
    {
        DbSet<tadjunto> tadjunto { get; set; }
        DbSet<tcancha> tcancha { get; set; }
        DbSet<tcategoria_club> tcategoria_club { get; set; }
        DbSet<tcategoria_handicap> tcategoria_handicap { get; set; }
        DbSet<tcategoria_torneo> tcategoria_torneo { get; set; }
        DbSet<tclub> tclub { get; set; }
        DbSet<tclub_historico> tclub_historico { get; set; }
        DbSet<tcomunicado> tcomunicado { get; set; }
        DbSet<tcontacto> tcontacto { get; set; }
        DbSet<tcontacto_usuario> tcontacto_usuario { get; set; }
        DbSet<tdomicilio> tdomicilio { get; set; }
        DbSet<tequipo> tequipo { get; set; }
        DbSet<tequipo_user> tequipo_user { get; set; }
        DbSet<testado> testado { get; set; }
        DbSet<tevento> tevento { get; set; }
        DbSet<tgenero> tgenero { get; set; }
        DbSet<thandicap> thandicap { get; set; }
        DbSet<thandicap_usuario> thandicap_usuario { get; set; }
        DbSet<thandicap_usuario_historico> thandicap_usuario_historico { get; set; }
        DbSet<thistorial_pagos> thistorial_pagos { get; set; }
        DbSet<tinfraccion> tinfraccion { get; set; }
        DbSet<tinfraccion_partido> tinfraccion_partido { get; set; }
        DbSet<tmotivo_baja> tmotivo_baja { get; set; }
        DbSet<tmunicipio> tmunicipio { get; set; }
        DbSet<tnoticia> tnoticia { get; set; }
        DbSet<tnotificacion> tnotificacion { get; set; }
        DbSet<tnotificacion_adjunto> tnotificacion_adjunto { get; set; }
        DbSet<tpais> tpais { get; set; }
        DbSet<tpartido> tpartido { get; set; }
        DbSet<tprovincia> tprovincia { get; set; }
        DbSet<tranking> tranking { get; set; }
        DbSet<treconocimiento> treconocimiento { get; set; }
        DbSet<treconocimiento_usuario> treconocimiento_usuario { get; set; }
        DbSet<treferee> treferee { get; set; }
        DbSet<tsede> tsede { get; set; }
        DbSet<ttipo_contacto> ttipo_contacto { get; set; }
        DbSet<ttipo_doc> ttipo_doc { get; set; }
        DbSet<ttipo_domicilio> ttipo_domicilio { get; set; }
        DbSet<ttipo_evento> ttipo_evento { get; set; }
        DbSet<ttipo_infraccion> ttipo_infraccion { get; set; }
        DbSet<ttipo_jugador> ttipo_jugador { get; set; }
        DbSet<ttipo_noticia> ttipo_noticia { get; set; }
        DbSet<ttipo_notificacion> ttipo_notificacion { get; set; }
        DbSet<ttipo_pago> ttipo_pago { get; set; }
        DbSet<ttipo_reconocimiento> ttipo_reconocimiento { get; set; }
        DbSet<ttipo_referee> ttipo_referee { get; set; }
        DbSet<ttipo_sede> ttipo_sede { get; set; }
        DbSet<ttipo_torneo> ttipo_torneo { get; set; }
        DbSet<ttorneo> ttorneo { get; set; }
        DbSet<ttorneo_historico> ttorneo_historico { get; set; }
        DbSet<ttransicion> ttransicion { get; set; }
        DbSet<tuser_club> tuser_club { get; set; }
        DbSet<tveedor> tveedor { get; set; }
        DbSet<users> users { get; set; }
        DbSet<users_historico> users_historico { get; set; }
        DbSet<tnotificacion_usuario> tnotificacion_usuario { get; set; }
        DbSet<trol> trol { get; set; }

        Database Database { get; }
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        int SaveChanges();
        Task<int> SaveChangesAsync();
        IEnumerable<TEntity> ExecuteStoredProcedure<TEntity>(string storedProcedure, params object[] parameters) where TEntity : class;
        IEnumerable<TEntity> ExecuteStoredProcedureBrowar<TEntity>(string storedProcedure, params object[] parameters) where TEntity : class;
        
        void Dispose();
    }
}
