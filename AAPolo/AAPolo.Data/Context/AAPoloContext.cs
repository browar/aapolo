﻿namespace AAPolo.Data.Context
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using AAPolo.Entity.Entity;
    using AAPolo.Data.Context.Interface;
    using System.Collections.Generic;
    using System.Text;
    using System.Data.SqlClient;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Diagnostics;

    public partial class AAPoloContext : DbContext, IAAPoloContext
    {
        public int _contador = 0;
        public virtual DbSet<tadjunto> tadjunto { get; set; }
        public virtual DbSet<tcancha> tcancha { get; set; }
        public virtual DbSet<tcategoria_club> tcategoria_club { get; set; }
        public virtual DbSet<tcategoria_handicap> tcategoria_handicap { get; set; }
        public virtual DbSet<tcategoria_torneo> tcategoria_torneo { get; set; }
        public virtual DbSet<tclub> tclub { get; set; }
        public virtual DbSet<tclub_historico> tclub_historico { get; set; }
        public virtual DbSet<tcomunicado> tcomunicado { get; set; }
        public virtual DbSet<tcontacto> tcontacto { get; set; }
        public virtual DbSet<tcontacto_club> tcontacto_club { get; set; }
        public virtual DbSet<tcontacto_usuario> tcontacto_usuario { get; set; }
        public virtual DbSet<tdatos_emergencia> tdatos_emergencia { get; set; }
        public virtual DbSet<tdomicilio> tdomicilio { get; set; }
        public virtual DbSet<tequipo> tequipo { get; set; }
        public virtual DbSet<tequipo_user> tequipo_user { get; set; }
        public virtual DbSet<testado> testado { get; set; }
        public virtual DbSet<tetiqueta> tetiqueta { get; set; }
        public virtual DbSet<tevento> tevento { get; set; }
        public virtual DbSet<tgenero> tgenero { get; set; }
        public virtual DbSet<thandicap> thandicap { get; set; }
        public virtual DbSet<thandicap_usuario> thandicap_usuario { get; set; }
        public virtual DbSet<thandicap_usuario_historico> thandicap_usuario_historico { get; set; }
        public virtual DbSet<thistorial_pagos> thistorial_pagos { get; set; }
        public virtual DbSet<tinfraccion> tinfraccion { get; set; }
        public virtual DbSet<tinfraccion_partido> tinfraccion_partido { get; set; }
        public virtual DbSet<tinstancia> tinstancia { get; set; }
        public virtual DbSet<tmiembro> tmiembro { get; set; }
        public virtual DbSet<tmotivo_baja> tmotivo_baja { get; set; }
        public virtual DbSet<tmunicipio> tmunicipio { get; set; }
        public virtual DbSet<tnoticia> tnoticia { get; set; }
        public virtual DbSet<tnoticia_archivo> tnoticia_archivo { get; set; }
        public virtual DbSet<tnoticia_etiqueta> tnoticia_etiqueta { get; set; }
        public virtual DbSet<tnotificacion> tnotificacion { get; set; }
        public virtual DbSet<tnotificacion_adjunto> tnotificacion_adjunto { get; set; }
        public virtual DbSet<tnotificacion_rol> tnotificacion_rol { get; set; }
        public virtual DbSet<tnotificacion_tipo_jugador> tnotificacion_tipo_jugador { get; set; }
        public virtual DbSet<tnotificacion_usuario> tnotificacion_usuario { get; set; }
        public virtual DbSet<tpais> tpais { get; set; }
        public virtual DbSet<tpartido> tpartido { get; set; }
        public virtual DbSet<tpopup> tpopup { get; set; }
        public virtual DbSet<tprovincia> tprovincia { get; set; }
        public virtual DbSet<tranking> tranking { get; set; }
        public virtual DbSet<treconocimiento> treconocimiento { get; set; }
        public virtual DbSet<treconocimiento_usuario> treconocimiento_usuario { get; set; }
        public virtual DbSet<treferee> treferee { get; set; }
        public virtual DbSet<trespuesta> trespuesta { get; set; }
        public virtual DbSet<trespuesta_adjunto> trespuesta_adjunto { get; set; }
        public virtual DbSet<trol> trol { get; set; }
        public virtual DbSet<tsede> tsede { get; set; }
        public virtual DbSet<ttipo_archivo> ttipo_archivo { get; set; }
        public virtual DbSet<ttipo_contacto> ttipo_contacto { get; set; }
        public virtual DbSet<ttipo_doc> ttipo_doc { get; set; }
        public virtual DbSet<ttipo_domicilio> ttipo_domicilio { get; set; }
        public virtual DbSet<ttipo_evento> ttipo_evento { get; set; }
        public virtual DbSet<ttipo_infraccion> ttipo_infraccion { get; set; }
        public virtual DbSet<ttipo_jugador> ttipo_jugador { get; set; }
        public virtual DbSet<ttipo_noticia> ttipo_noticia { get; set; }
        public virtual DbSet<ttipo_notificacion> ttipo_notificacion { get; set; }
        public virtual DbSet<ttipo_pago> ttipo_pago { get; set; }
        public virtual DbSet<ttipo_reconocimiento> ttipo_reconocimiento { get; set; }
        public virtual DbSet<ttipo_referee> ttipo_referee { get; set; }
        public virtual DbSet<ttipo_sede> ttipo_sede { get; set; }
        public virtual DbSet<ttipo_torneo> ttipo_torneo { get; set; }
        public virtual DbSet<ttorneo> ttorneo { get; set; }
        public virtual DbSet<ttorneo_historico> ttorneo_historico { get; set; }
        public virtual DbSet<ttransicion> ttransicion { get; set; }
        public virtual DbSet<ttutor> ttutor { get; set; }
        public virtual DbSet<tuser_club> tuser_club { get; set; }
        public virtual DbSet<tveedor> tveedor { get; set; }
        public virtual DbSet<users> users { get; set; }
        public virtual DbSet<users_admin> users_admin { get; set; }
        public virtual DbSet<users_historico> users_historico { get; set; }


        public IEnumerable<TEntity> ExecuteStoredProcedure<TEntity>(string storedProcedure, params object[] parameters) where TEntity : class
        {
            var sqlQueryBuilder = new StringBuilder();
            sqlQueryBuilder.AppendFormat("EXEC {0}", storedProcedure);

            if (parameters != null && parameters.Any())
            {
                foreach (var parameter in parameters.OfType<SqlParameter>())
                {
                    sqlQueryBuilder.AppendFormat(" {0},", parameter.ParameterName);
                }
            }
            return Database.SqlQuery<TEntity>(sqlQueryBuilder.ToString(), parameters);
        }
        public IEnumerable<TEntity> ExecuteStoredProcedureBrowar<TEntity>(string storedProcedure, params object[] parameters) where TEntity : class
        {
            bool first = true;
            var sqlQueryBuilder = new StringBuilder();
            sqlQueryBuilder.AppendFormat("EXEC {0}", storedProcedure);

            if (parameters != null && parameters.Any())
            {
                foreach (var parameter in parameters.OfType<SqlParameter>())
                {
                    if (first)
                    {
                        sqlQueryBuilder.AppendFormat(" {0} = {1} ", parameter.ParameterName, parameter.Value);
                        first = false;
                    }
                    else
                    {
                        sqlQueryBuilder.AppendFormat(", {0} = {1} ", parameter.ParameterName, parameter.Value);
                    }
                }
            }
            return Database.SqlQuery<TEntity>(sqlQueryBuilder.ToString(), parameters);
        }
        IDbSet<TEntity> IAAPoloContext.Set<TEntity>()
        {
            return base.Set<TEntity>();
        }

        public AAPoloContext() : base("name=AAPoloContext")
        {
            Debug.WriteLine(_contador + "+ + Stack " + Environment.StackTrace.Substring(0, 600));
            _contador++;

            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public AAPoloContext(bool lazyload) : base("name=AAPoloContext")
        {
            Debug.WriteLine(_contador + "o o Stack " + Environment.StackTrace.Substring(0, 600));
            _contador++;

            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
        }

        protected override void Dispose(bool lazyLoad)
        {
            _contador--;
            Debug.WriteLine(_contador + "- - Stack " + Environment.StackTrace.Substring(0, 600));
            Configuration.LazyLoadingEnabled = false;
            base.Dispose(lazyLoad);
        }


        #region OnModelCreating 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tadjunto>()
                .Property(e => e.txt_url)
                .IsUnicode(false);

            modelBuilder.Entity<tadjunto>()
                .HasMany(e => e.tnotificacion_adjunto)
                .WithRequired(e => e.tadjunto)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tcancha>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tcategoria_club>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tcategoria_club>()
                .Property(e => e.txt_desc_corta)
                .IsUnicode(false);

            modelBuilder.Entity<tcategoria_club>()
                .Property(e => e.nro_importe)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tcategoria_handicap>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tcategoria_torneo>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tcategoria_torneo>()
                .HasMany(e => e.ttorneo)
                .WithRequired(e => e.tcategoria_torneo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tcategoria_torneo>()
                .HasMany(e => e.ttorneo_historico)
                .WithRequired(e => e.tcategoria_torneo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.cod_club_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_presidente)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_vicepresidente)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_vocal1)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_vocal2)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_vocal3)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_tesorero)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.usuario_alta)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_historia)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.ClubID)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_facebook)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_twitter)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_instagram)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .Property(e => e.txt_imagen)
                .IsUnicode(false);

            modelBuilder.Entity<tclub>()
                .HasMany(e => e.tsede)
                .WithRequired(e => e.tclub)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tclub>()
                .HasMany(e => e.tcontacto_club)
                .WithRequired(e => e.tclub)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<trespuesta>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tclub_historico>()
                .Property(e => e.cod_club_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tclub_historico>()
                .Property(e => e.txt_presidente)
                .IsUnicode(false);

            modelBuilder.Entity<tclub_historico>()
                .Property(e => e.txt_vicepresidente)
                .IsUnicode(false);

            modelBuilder.Entity<tclub_historico>()
                .Property(e => e.txt_vocal1)
                .IsUnicode(false);

            modelBuilder.Entity<tclub_historico>()
                .Property(e => e.txt_vocal2)
                .IsUnicode(false);

            modelBuilder.Entity<tclub_historico>()
                .Property(e => e.txt_vocal3)
                .IsUnicode(false);

            modelBuilder.Entity<tclub_historico>()
                .Property(e => e.txt_tesorero)
                .IsUnicode(false);

            modelBuilder.Entity<tclub_historico>()
                .Property(e => e.usuario_alta)
                .IsUnicode(false);

            modelBuilder.Entity<tclub_historico>()
                .Property(e => e.usuario_alta_historico)
                .IsUnicode(false);

            modelBuilder.Entity<tcomunicado>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tcomunicado>()
                .Property(e => e.txt_titulo)
                .IsUnicode(false);

            modelBuilder.Entity<tcontacto>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tcontacto>()
                .HasMany(e => e.tcontacto_usuario)
                .WithRequired(e => e.tcontacto)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tcontacto>()
                .HasMany(e => e.tcontacto_club)
                .WithRequired(e => e.tcontacto)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tdatos_emergencia>()
                .Property(e => e.txt_nombre_completo)
                .IsUnicode(false);

            modelBuilder.Entity<tdatos_emergencia>()
                .Property(e => e.txt_telefono_contacto)
                .IsUnicode(false);

            modelBuilder.Entity<tdatos_emergencia>()
                .Property(e => e.txt_obra_social)
                .IsUnicode(false);

            modelBuilder.Entity<tdatos_emergencia>()
                .Property(e => e.txt_sanatorio_emergencia)
                .IsUnicode(false);

            modelBuilder.Entity<tdatos_emergencia>()
                .Property(e => e.txt_tipo_sanguineo)
                .IsUnicode(false);

            modelBuilder.Entity<tdatos_emergencia>()
                .Property(e => e.txt_numero_documento)
                .IsUnicode(false);

            modelBuilder.Entity<tdatos_emergencia>()
                .HasMany(e => e.users1)
                .WithOptional(e => e.tdatos_emergencia1)
                .HasForeignKey(e => e.cod_datos_emergencia);

            modelBuilder.Entity<tdomicilio>()
                .Property(e => e.txt_domicilio)
                .IsUnicode(false);

            modelBuilder.Entity<tdomicilio>()
                .Property(e => e.txt_departamento)
                .IsUnicode(false);

            modelBuilder.Entity<tdomicilio>()
                .Property(e => e.nro_cod_postal)
                .IsUnicode(false);

            modelBuilder.Entity<tdomicilio>()
                .HasMany(e => e.tsede)
                .WithRequired(e => e.tdomicilio)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tequipo>()
                .Property(e => e.txt_alta)
                .IsUnicode(false);

            modelBuilder.Entity<tequipo>()
                .Property(e => e.txt_ultima_modificación)
                .IsUnicode(false);

            modelBuilder.Entity<tequipo>()
                .HasMany(e => e.tequipo_user)
                .WithRequired(e => e.tequipo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tequipo>()
                .HasMany(e => e.tpartido)
                .WithRequired(e => e.tequipo)
                .HasForeignKey(e => e.cod_equipo_visitante)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tequipo>()
                .HasMany(e => e.tpartido1)
                .WithRequired(e => e.tequipo1)
                .HasForeignKey(e => e.cod_equipo_local)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<testado>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<testado>()
                .HasMany(e => e.tequipo_user)
                .WithRequired(e => e.testado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tetiqueta>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tevento>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tevento>()
                .Property(e => e.usuario_alta)
                .IsUnicode(false);

            modelBuilder.Entity<tevento>()
                .Property(e => e.usuario_ultima_modificacion)
                .IsUnicode(false);

            modelBuilder.Entity<tevento>()
                .Property(e => e.txt_desc_larga)
                .IsUnicode(false);

            modelBuilder.Entity<tevento>()
                .Property(e => e.txt_imagen)
                .IsUnicode(false);

            modelBuilder.Entity<tgenero>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tgenero>()
                .HasMany(e => e.tmiembro)
                .WithRequired(e => e.tgenero)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tgenero>()
                .HasMany(e => e.users)
                .WithRequired(e => e.tgenero)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tgenero>()
                .HasMany(e => e.users_historico)
                .WithRequired(e => e.tgenero)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<thandicap>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<thandicap>()
                .Property(e => e.txt_vigencia)
                .IsUnicode(false);

            modelBuilder.Entity<thandicap>()
                .Property(e => e.txt_nacionalidad)
                .IsUnicode(false);

            modelBuilder.Entity<thandicap>()
                .Property(e => e.txt_edad)
                .IsUnicode(false);

            modelBuilder.Entity<thandicap>()
                .Property(e => e.nro_importe)
                .HasPrecision(18, 0);

            modelBuilder.Entity<thandicap>()
                .HasMany(e => e.ttransicion)
                .WithRequired(e => e.thandicap)
                .HasForeignKey(e => e.cod_handicap_desde)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<thandicap>()
                .HasMany(e => e.ttransicion1)
                .WithRequired(e => e.thandicap1)
                .HasForeignKey(e => e.cod_handicap_hasta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<thandicap>()
                .HasMany(e => e.thandicap_usuario)
                .WithRequired(e => e.thandicap)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<thandicap>()
                .HasMany(e => e.thandicap_usuario_historico)
                .WithRequired(e => e.thandicap)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<thistorial_pagos>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<thistorial_pagos>()
                .Property(e => e.txt_url_comprobante)
                .IsUnicode(false);

            modelBuilder.Entity<thistorial_pagos>()
                .Property(e => e.ArticuloID)
                .IsUnicode(false);

            modelBuilder.Entity<thistorial_pagos>()
                .Property(e => e.cod_mercado_pago)
                .IsUnicode(false);

            modelBuilder.Entity<tinfraccion>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tinfraccion>()
                .HasMany(e => e.tinfraccion_partido)
                .WithRequired(e => e.tinfraccion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tinstancia>()
                .Property(e => e.txt_instancia)
                .IsUnicode(false);

            modelBuilder.Entity<tmiembro>()
                .Property(e => e.txt_password)
                .IsUnicode(false);

            modelBuilder.Entity<tmiembro>()
                .Property(e => e.nro_doc)
                .IsUnicode(false);

            modelBuilder.Entity<tmiembro>()
                .Property(e => e.usuario_alta)
                .IsUnicode(false);

            modelBuilder.Entity<tmiembro>()
                .Property(e => e.txt_imagen)
                .IsUnicode(false);

            modelBuilder.Entity<tmiembro>()
                .Property(e => e.txt_mail)
                .IsUnicode(false);

            modelBuilder.Entity<tmiembro>()
                .Property(e => e.txt_celular)
                .IsUnicode(false);

            modelBuilder.Entity<tmotivo_baja>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tmunicipio>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tnoticia>()
                .Property(e => e.txt_titulo)
                .IsUnicode(false);

            modelBuilder.Entity<tnoticia>()
                .Property(e => e.txt_copete)
                .IsUnicode(false);

            modelBuilder.Entity<tnoticia>()
                .Property(e => e.txt_cuerpo)
                .IsUnicode(false);

            modelBuilder.Entity<tnoticia_archivo>()
                .Property(e => e.url_archivo)
                .IsUnicode(false);

            modelBuilder.Entity<tnoticia_archivo>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tnotificacion>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tnotificacion>()
                .Property(e => e.txt_asunto)
                .IsUnicode(false);

            modelBuilder.Entity<tnotificacion>()
                .Property(e => e.txt_alta)
                .IsUnicode(false);

            modelBuilder.Entity<tnotificacion>()
                .Property(e => e.cod_enviado_por)
                .IsUnicode(false);

            modelBuilder.Entity<tnotificacion>()
                .HasMany(e => e.tnotificacion_adjunto)
                .WithRequired(e => e.tnotificacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tnotificacion>()
                .HasMany(e => e.tnotificacion_rol)
                .WithRequired(e => e.tnotificacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tnotificacion>()
                .HasMany(e => e.trespuesta)
                .WithRequired(e => e.tnotificacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tnotificacion>()
                .HasMany(e => e.tnotificacion_usuario)
                .WithRequired(e => e.tnotificacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tnotificacion>()
                .HasMany(e => e.tnotificacion_tipo_jugador)
                .WithRequired(e => e.tnotificacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tpais>()
                .Property(e => e.txt_abreviacion)
                .IsUnicode(false);

            modelBuilder.Entity<tpais>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tpais>()
                .HasMany(e => e.tmiembro)
                .WithOptional(e => e.tpais)
                .HasForeignKey(e => e.cod_nacionalidad);

            modelBuilder.Entity<tpais>()
                .HasMany(e => e.tprovincia)
                .WithRequired(e => e.tpais)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tpais>()
                .HasMany(e => e.users)
                .WithOptional(e => e.tpais)
                .HasForeignKey(e => e.cod_nacionalidad);

            modelBuilder.Entity<tpartido>()
                .HasMany(e => e.tinfraccion_partido)
                .WithRequired(e => e.tpartido)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tpopup>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tpopup>()
                .Property(e => e.txt_imagen)
                .IsUnicode(false);

            modelBuilder.Entity<tpopup>()
                .Property(e => e.txt_video)
                .IsUnicode(false);

            modelBuilder.Entity<tprovincia>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tprovincia>()
                .Property(e => e.nro_codigo)
                .IsUnicode(false);

            modelBuilder.Entity<tprovincia>()
                .Property(e => e.cod_jurisdiccion)
                .IsUnicode(false);

            modelBuilder.Entity<tprovincia>()
                .HasMany(e => e.tmunicipio)
                .WithRequired(e => e.tprovincia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tranking>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<treconocimiento>()
                .HasMany(e => e.treconocimiento_usuario)
                .WithRequired(e => e.treconocimiento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<trespuesta>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<trespuesta>()
                .HasMany(e => e.trespuesta_adjunto)
                .WithRequired(e => e.trespuesta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<trespuesta_adjunto>()
                .Property(e => e.txt_url_image)
                .IsUnicode(false);

            modelBuilder.Entity<trol>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tsede>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<tsede>()
                .HasMany(e => e.tcancha)
                .WithRequired(e => e.tsede)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttipo_archivo>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_contacto>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_doc>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_domicilio>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_evento>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_evento>()
                .HasMany(e => e.tevento)
                .WithRequired(e => e.ttipo_evento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttipo_infraccion>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_infraccion>()
                .HasMany(e => e.tinfraccion)
                .WithRequired(e => e.ttipo_infraccion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttipo_jugador>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_jugador>()
                .HasMany(e => e.users)
                .WithRequired(e => e.ttipo_jugador)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttipo_jugador>()
                .HasMany(e => e.users_historico)
                .WithRequired(e => e.ttipo_jugador)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttipo_noticia>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_notificacion>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_notificacion>()
                .HasMany(e => e.tnotificacion)
                .WithRequired(e => e.ttipo_notificacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttipo_pago>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_pago>()
                .HasMany(e => e.thistorial_pagos)
                .WithRequired(e => e.ttipo_pago)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttipo_reconocimiento>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_reconocimiento>()
                .HasMany(e => e.treconocimiento)
                .WithRequired(e => e.ttipo_reconocimiento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttipo_referee>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_referee>()
                .HasMany(e => e.treferee)
                .WithRequired(e => e.ttipo_referee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttipo_sede>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttipo_torneo>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo>()
                .Property(e => e.txt_sexo)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo>()
                .Property(e => e.txt_edad_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo>()
                .Property(e => e.usuario_alta)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo>()
                .Property(e => e.txt_desc_larga)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo>()
                .Property(e => e.txt_imagen)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo>()
                .HasMany(e => e.tequipo)
                .WithRequired(e => e.ttorneo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttorneo>()
                .HasMany(e => e.tpartido)
                .WithRequired(e => e.ttorneo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttorneo>()
                .HasMany(e => e.treconocimiento)
                .WithRequired(e => e.ttorneo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ttorneo_historico>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo_historico>()
                .Property(e => e.txt_sexo)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo_historico>()
                .Property(e => e.txt_edad_desc)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo_historico>()
                .Property(e => e.usuario_alta)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo_historico>()
                .Property(e => e.txt_desc_larga)
                .IsUnicode(false);

            modelBuilder.Entity<ttorneo_historico>()
                .Property(e => e.usuario_alta_historico)
                .IsUnicode(false);

            modelBuilder.Entity<ttutor>()
                .Property(e => e.txt_nombre_completo)
                .IsUnicode(false);

            modelBuilder.Entity<ttutor>()
                .Property(e => e.txt_documento)
                .IsUnicode(false);

            modelBuilder.Entity<ttutor>()
                .Property(e => e.txt_telefono_contacto)
                .IsUnicode(false);

            modelBuilder.Entity<tveedor>()
                .Property(e => e.txt_desc)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.txt_password)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.nro_doc)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.url_foto)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.usuario_alta)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.txt_facebook)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.txt_twitter)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.txt_instagram)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.JugadorID)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.txt_imagen)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.id_register)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.platform_mobile)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.txt_tarjeta)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.tcontacto_usuario)
                .WithRequired(e => e.users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.tdatos_emergencia)
                .WithOptional(e => e.users)
                .HasForeignKey(e => e.cod_usuario);

            modelBuilder.Entity<users>()
                .HasMany(e => e.tequipo_user)
                .WithRequired(e => e.users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.thandicap_usuario)
                .WithRequired(e => e.users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.thandicap_usuario_historico)
                .WithRequired(e => e.users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.tnoticia)
                .WithOptional(e => e.users)
                .HasForeignKey(e => e.cod_usuario_alta);

            modelBuilder.Entity<users>()
                .HasMany(e => e.tnoticia_archivo)
                .WithOptional(e => e.users)
                .HasForeignKey(e => e.cod_usuario_alta);

            modelBuilder.Entity<users>()
                .HasMany(e => e.tpartido)
                .WithOptional(e => e.users)
                .HasForeignKey(e => e.cod_veedor);

            modelBuilder.Entity<users>()
                .HasMany(e => e.tranking)
                .WithRequired(e => e.users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.treconocimiento_usuario)
                .WithRequired(e => e.users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.treferee)
                .WithRequired(e => e.users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.trespuesta)
                .WithOptional(e => e.users)
                .HasForeignKey(e => e.cod_usuario_envia);

            modelBuilder.Entity<users>()
                .HasMany(e => e.trespuesta1)
                .WithOptional(e => e.users1)
                .HasForeignKey(e => e.cod_usuario_recibe);

            modelBuilder.Entity<users>()
                .HasMany(e => e.ttutor)
                .WithRequired(e => e.users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.tveedor)
                .WithRequired(e => e.users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users_admin>()
                .Property(e => e.txt_usuario)
                .IsUnicode(false);

            modelBuilder.Entity<users_admin>()
                .Property(e => e.txt_password)
                .IsUnicode(false);

            modelBuilder.Entity<users_historico>()
                .Property(e => e.txt_nombre)
                .IsUnicode(false);

            modelBuilder.Entity<users_historico>()
                .Property(e => e.txt_apellido)
                .IsUnicode(false);

            modelBuilder.Entity<users_historico>()
                .Property(e => e.txt_password)
                .IsUnicode(false);

            modelBuilder.Entity<users_historico>()
                .Property(e => e.nro_doc)
                .IsUnicode(false);

            modelBuilder.Entity<users_historico>()
                .Property(e => e.url_foto)
                .IsUnicode(false);

            modelBuilder.Entity<users_historico>()
                .Property(e => e.usuario_alta)
                .IsUnicode(false);

            modelBuilder.Entity<users_historico>()
                .Property(e => e.usuario_alta_historico)
                .IsUnicode(false);

            modelBuilder.Entity<users_historico>()
                .Property(e => e.txt_facebook)
                .IsUnicode(false);

            modelBuilder.Entity<users_historico>()
                .Property(e => e.txt_twitter)
                .IsUnicode(false);

            modelBuilder.Entity<users_historico>()
                .Property(e => e.txt_instagram)
                .IsUnicode(false);
        }

        #endregion
    }
}

