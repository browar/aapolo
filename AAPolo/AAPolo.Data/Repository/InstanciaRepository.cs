﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class InstanciaRepository : BaseRepository<tinstancia, IAAPoloContext>, IInstanciaRepository
    {
        public InstanciaRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
