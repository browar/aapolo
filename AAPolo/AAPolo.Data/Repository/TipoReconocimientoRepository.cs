﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TipoReconocimientoRepository : BaseRepository<ttipo_reconocimiento, IAAPoloContext>, ITipoReconocimientoRepository
    {
        public TipoReconocimientoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}

