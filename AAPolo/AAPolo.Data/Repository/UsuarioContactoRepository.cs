﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class UsuarioContactoRepository : BaseRepository<tcontacto_usuario, IAAPoloContext>, IUsuarioContactoRepository
    {
        public UsuarioContactoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
