﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class HandicapUsuarioRepository : BaseRepository<thandicap_usuario, IAAPoloContext>, IHandicapUsuarioRepository
    {
        public HandicapUsuarioRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
