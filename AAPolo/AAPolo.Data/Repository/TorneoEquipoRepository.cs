﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TorneoEquipoRepository : BaseRepository<tequipo_user, IAAPoloContext>, ITorneoEquipoRepository
    {
        public TorneoEquipoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
