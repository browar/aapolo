﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class MiembroRepository : BaseRepository<tmiembro, IAAPoloContext>, IMiembroRepository
    {
        public MiembroRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
