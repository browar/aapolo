﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class NotificacionRepository : BaseRepository<tnotificacion, IAAPoloContext>, INotificacionRepository
    {
        public NotificacionRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
