﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TorneoHistoricoRepository : BaseRepository<ttorneo_historico, IAAPoloContext>, ITorneoHistoricoRepository
    {
        public TorneoHistoricoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
