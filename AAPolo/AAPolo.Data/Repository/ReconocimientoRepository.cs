﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class ReconocimientoRepository : BaseRepository<treconocimiento, IAAPoloContext>, IReconocimientoRepository
    {
        public ReconocimientoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
