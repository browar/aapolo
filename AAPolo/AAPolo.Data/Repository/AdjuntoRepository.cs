﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class AdjuntoRepository : BaseRepository<tadjunto, IAAPoloContext>, IAdjuntoRepository
    {
        public AdjuntoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
