﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class PartidoRepository : BaseRepository<tpartido, IAAPoloContext>, IPartidoRepository
    {
        public PartidoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}

