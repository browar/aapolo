﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data.Repository
{
    public class CategoriaHandicapRepository : BaseRepository<tcategoria_handicap, IAAPoloContext>, ICategoriaHandicapRepository
    {
        public CategoriaHandicapRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
