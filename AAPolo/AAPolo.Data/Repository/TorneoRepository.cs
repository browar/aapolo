﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TorneoRepository : BaseRepository<ttorneo, IAAPoloContext>, ITorneoRepository
    {
        public TorneoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
