﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data.Repository
{
    public class EstadoRepository : BaseRepository<testado, IAAPoloContext>, IEstadoRepository
    {
        public EstadoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
