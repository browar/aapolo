﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class HistorialPagoRepository : BaseRepository<thistorial_pagos, IAAPoloContext>, IHistorialPagoRepository
    {
        public HistorialPagoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}

