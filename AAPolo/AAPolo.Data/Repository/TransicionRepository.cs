﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TransicionRepository : BaseRepository<ttransicion, IAAPoloContext>, ITransicionRepository
    {
        public TransicionRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}

