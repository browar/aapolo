﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class CanchaRepository : BaseRepository<tcancha, IAAPoloContext>, ICanchaRepository
    {
        public CanchaRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
