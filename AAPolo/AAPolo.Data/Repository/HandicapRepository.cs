﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class HandicapRepository : BaseRepository<thandicap, IAAPoloContext>, IHandicapRepository
    {
        public HandicapRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
