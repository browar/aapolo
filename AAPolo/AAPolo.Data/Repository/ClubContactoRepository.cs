﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class ClubContactoRepository : BaseRepository<tcontacto_club, IAAPoloContext>, IClubContactoRepository
    {
        public ClubContactoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
