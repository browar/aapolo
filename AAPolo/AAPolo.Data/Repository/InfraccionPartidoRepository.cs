﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class InfraccionPartidoRepository : BaseRepository<tinfraccion_partido, IAAPoloContext>, IInfraccionPartidoRepository
    {
        public InfraccionPartidoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
