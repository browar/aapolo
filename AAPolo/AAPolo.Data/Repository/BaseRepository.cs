﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AAPolo.Data.Repository
{
    public abstract class BaseRepository<T, C> : IBaseRepository<T>
        where T : class
        where C : IAAPoloContext
    {
        private C _context;

        public C Context
        {
            get { return _context; }
            set { _context = value; }
        }

        protected readonly IDbSet<T> _dbSet;
        protected readonly IDbSet<T> _dbSetReadOnly;

        protected BaseRepository(C context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public T Add(T entity)
        {
            _dbSet.Add(entity);
            return entity;
        }

        public virtual void Add(List<T> entity)
        {
            foreach (var item in entity)
            {
                _dbSet.Add(item);
            }
        }

        public virtual T Update(T entity)
        {
            _dbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;

            return entity;
        }

        public virtual void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }

        public virtual async Task<IEnumerable<T>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> GetAllReadOnly()
        {
            return await _dbSetReadOnly.ToListAsync();
        }

        public virtual void Delete(Expression<Func<T, bool>> condition)
        {
            IEnumerable<T> list = _dbSet.Where<T>(condition).AsEnumerable();
            foreach (T item in list)
                this.Delete(item);
        }

        public virtual T GetById(long id)
        {
            return _dbSet.Find(id);
        }

        public virtual T GetByIdReadOnly(long id)
        {
            return _dbSetReadOnly.Find(id);
        }

        public virtual T GetById(string id)
        {
            return _dbSet.Find(id);
        }

        public virtual T GetByIdReadOnly(string id)
        {
            return _dbSetReadOnly.Find(id);
        }

        public virtual async Task<IEnumerable<T>> GetMany(Expression<Func<T, bool>> condition)
        {
            return await _dbSet.Where<T>(condition).ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> GetManyReadOnly(Expression<Func<T, bool>> condition)
        {
            return await _dbSetReadOnly.Where<T>(condition).ToListAsync();
        }

        public virtual async Task<T> GetByCondition(Expression<Func<T, bool>> condition)
        {
            return await _dbSet.Where<T>(condition).FirstOrDefaultAsync();
        }

        public virtual async Task<T> GetByConditionReadOnly(Expression<Func<T, bool>> condition)
        {
            return await _dbSetReadOnly.Where<T>(condition).FirstOrDefaultAsync();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}