﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class EquipoUsuarioRepository : BaseRepository<tequipo_user, IAAPoloContext>, IEquipoUsuarioRepository
    {
        public EquipoUsuarioRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
