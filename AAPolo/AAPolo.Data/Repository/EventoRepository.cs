﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class EventoRepository : BaseRepository<tevento, IAAPoloContext>, IEventoRepository
    {
        public EventoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
