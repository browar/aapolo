﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class CategoriaTorneoRepository : BaseRepository<tcategoria_torneo, IAAPoloContext>, ICategoriaTorneoRepository
    {
        public CategoriaTorneoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
