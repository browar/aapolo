﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class ClubRepository : BaseRepository<tclub, IAAPoloContext>, IClubRepository
    {
        public ClubRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
