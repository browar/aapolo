﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data.Repository
{
    public class TipoNoticiaRepository : BaseRepository<ttipo_noticia, IAAPoloContext>, ITipoNoticiaRepository
    {
        public TipoNoticiaRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
