﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class NoticiaRepository : BaseRepository<tnoticia, IAAPoloContext>, INoticiaRepository
    {
        public NoticiaRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
