﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class ReconocimientoUsuarioRepository: BaseRepository<treconocimiento_usuario, IAAPoloContext>, IReconocimientoUsuarioRepository
    {
        public ReconocimientoUsuarioRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
