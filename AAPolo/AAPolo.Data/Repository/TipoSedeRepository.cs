﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TipoSedeRepository : BaseRepository<ttipo_sede, IAAPoloContext>, ITipoSedeRepository
    {
        public TipoSedeRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
