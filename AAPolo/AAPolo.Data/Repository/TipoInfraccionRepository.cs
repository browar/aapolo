﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TipoInfraccionRepository : BaseRepository<ttipo_infraccion, IAAPoloContext>, ITipoInfraccionRepository
    {
        public TipoInfraccionRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
