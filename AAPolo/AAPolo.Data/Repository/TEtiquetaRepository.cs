﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;
namespace AAPolo.Data.Repository
{
    public class TEtiquetaRepository : BaseRepository<tetiqueta, IAAPoloContext>, ITEtiquetaRepository
    {
        public TEtiquetaRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}

