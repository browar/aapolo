﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TipoEventoRepository : BaseRepository<ttipo_evento, IAAPoloContext>, ITipoEventoRepository
    {
        public TipoEventoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
