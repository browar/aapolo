﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class UsuarioHistoricoRepository : BaseRepository<users_historico, IAAPoloContext>, IUsuarioHistoricoRepository
    {
        public UsuarioHistoricoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
