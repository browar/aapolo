﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class InfraccionRepository : BaseRepository<tinfraccion, IAAPoloContext>, IInfraccionRepository
    {
        public InfraccionRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
