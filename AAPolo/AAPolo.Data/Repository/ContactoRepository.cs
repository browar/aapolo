﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class ContactoRepository :BaseRepository<tcontacto, IAAPoloContext>, IContactoRepository
    {
        public ContactoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
