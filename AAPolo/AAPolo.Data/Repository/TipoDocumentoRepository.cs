﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TipoDocumentoRepository : BaseRepository<ttipo_doc, IAAPoloContext>, ITipoDocumentoRepository
    {
        public TipoDocumentoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
