﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class JugadorRepository : BaseRepository<users, IAAPoloContext>, IJugadorRepository
    {
        public JugadorRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}