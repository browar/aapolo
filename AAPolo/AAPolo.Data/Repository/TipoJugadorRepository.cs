﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TipoJugadorRepository : BaseRepository<ttipo_jugador, IAAPoloContext>, ITipoJugadorRepository
    {
        public TipoJugadorRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
