﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class HandicapUsuarioHistoricoRepository : BaseRepository<thandicap_usuario_historico, IAAPoloContext>, IHandicapUsuarioHistoricoRepository
    {
        public HandicapUsuarioHistoricoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
