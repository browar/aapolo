﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TipoTorneoRepository : BaseRepository<ttipo_torneo, IAAPoloContext>, ITipoTorneoRepository
    {
        public TipoTorneoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
