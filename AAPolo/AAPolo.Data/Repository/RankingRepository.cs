﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class RankingRepository : BaseRepository<tranking, IAAPoloContext>, IRankingRepository
    {
        public RankingRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}

