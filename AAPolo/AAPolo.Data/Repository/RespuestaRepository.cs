﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class RespuestaRepository : BaseRepository<trespuesta, IAAPoloContext>, IRespuestaRepository
    {
        public RespuestaRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
