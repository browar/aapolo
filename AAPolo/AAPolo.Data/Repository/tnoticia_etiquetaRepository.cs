﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class tnoticia_etiquetaRepository : BaseRepository<tnoticia_etiqueta, IAAPoloContext>, Itnoticia_etiquetaRepository
    {
        public tnoticia_etiquetaRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}