﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class SedeRepository : BaseRepository<tsede, IAAPoloContext>, ISedeRepository
    {
        public SedeRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
