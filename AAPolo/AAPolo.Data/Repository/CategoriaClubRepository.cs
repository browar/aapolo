﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class CategoriaClubRepository : BaseRepository<tcategoria_club, IAAPoloContext>, ICategoriaClubRepository
    {
        public CategoriaClubRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
