﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AAPolo.Data.Repository.Interface
{
    public interface IBaseRepository<T> where T : class
    {
        T Add(T entity);
        void Add(List<T> entity);
        T Update(T entity);
        void Delete(T entity);
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> GetAllReadOnly();
        void Delete(Expression<Func<T, bool>> condition);
        T GetById(long id);
        T GetById(string id);
        T GetByIdReadOnly(long id);
        T GetByIdReadOnly(string id);
        Task<IEnumerable<T>> GetMany(Expression<Func<T, bool>> condition);
        Task<IEnumerable<T>> GetManyReadOnly(Expression<Func<T, bool>> condition);
        Task<T> GetByCondition(Expression<Func<T, bool>> condition);
        Task<T> GetByConditionReadOnly(Expression<Func<T, bool>> condition);
        void SaveChanges();
        Task<int> SaveChangesAsync();
    }
}