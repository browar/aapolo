﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TipoNotificacionRepository : BaseRepository<ttipo_notificacion, IAAPoloContext>, ITipoNotificacionRepository
    {
        public TipoNotificacionRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
