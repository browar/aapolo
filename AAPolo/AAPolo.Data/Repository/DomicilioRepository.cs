﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class DomicilioRepository : BaseRepository<tdomicilio, IAAPoloContext>, IDomicilioRepository
    {
        public DomicilioRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
