﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class EquipoRepository : BaseRepository<tequipo, IAAPoloContext>, IEquipoRepository
    {
        public EquipoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
