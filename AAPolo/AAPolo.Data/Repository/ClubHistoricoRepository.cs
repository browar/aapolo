﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class ClubHistoricoRepository : BaseRepository<tclub_historico, IAAPoloContext>, IClubHistoricoRepository
    {
        public ClubHistoricoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
