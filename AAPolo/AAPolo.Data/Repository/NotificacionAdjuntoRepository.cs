﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class NotificacionAdjuntoRepository : BaseRepository<tnotificacion_adjunto, IAAPoloContext>, INotificacionAdjuntoRepository
    {
        public NotificacionAdjuntoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
