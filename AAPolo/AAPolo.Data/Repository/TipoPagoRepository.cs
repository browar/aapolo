﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class TipoPagoRepository : BaseRepository<ttipo_pago, IAAPoloContext>, ITipoPagoRepository
    {
        public TipoPagoRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
