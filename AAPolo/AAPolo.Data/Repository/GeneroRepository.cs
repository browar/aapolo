﻿using AAPolo.Data.Context.Interface;
using AAPolo.Data.Repository.Interface;
using AAPolo.Entity.Entity;

namespace AAPolo.Data.Repository
{
    public class GeneroRepository : BaseRepository<tgenero, IAAPoloContext>, IGeneroRepository
    {
        public GeneroRepository(IAAPoloContext context) : base(context)
        {
        }
    }
}
