namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ttransicion")]
    public partial class ttransicion
    {
        [Key]
        public int cod_transicion { get; set; }

        public int cod_handicap_desde { get; set; }

        public int cod_handicap_hasta { get; set; }

        public decimal nro_precio { get; set; }

        public virtual thandicap thandicap { get; set; }

        public virtual thandicap thandicap1 { get; set; }
    }
}
