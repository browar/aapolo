namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tmiembro")]
    public partial class tmiembro
    {
        [Key]
        public int cod_miembro { get; set; }

        [StringLength(50)]
        public string txt_nombre { get; set; }

        [StringLength(50)]
        public string txt_apellido { get; set; }

        public string txt_password { get; set; }

        public int? cod_tipo_doc { get; set; }

        [StringLength(50)]
        public string nro_doc { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_alta { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_ult_modificacion { get; set; }

        public int? cod_domicilio { get; set; }

        [StringLength(200)]
        public string usuario_alta { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fec_nacimiento { get; set; }

        public int cod_genero { get; set; }

        public bool? bool_activo { get; set; }

        public int? cod_nacionalidad { get; set; }

        public string txt_imagen { get; set; }

        public int? cod_usuario { get; set; }

        [StringLength(100)]
        public string txt_tarjeta { get; set; }

        public int? cod_club { get; set; }

        [StringLength(255)]
        public string txt_mail { get; set; }

        [StringLength(25)]
        public string txt_celular { get; set; }

        public virtual tclub tclub { get; set; }

        public virtual tdomicilio tdomicilio { get; set; }

        public virtual tgenero tgenero { get; set; }

        public virtual tpais tpais { get; set; }

        public virtual ttipo_doc ttipo_doc { get; set; }

        public virtual users users { get; set; }
    }
}
