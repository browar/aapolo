namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tcomunicado")]
    public partial class tcomunicado
    {
        [Key]
        public int cod_comunicado { get; set; }

        [Required]
        [StringLength(2000)]
        public string txt_desc { get; set; }

        [Required]
        [StringLength(200)]
        public string txt_titulo { get; set; }

        public DateTime fec_emision { get; set; }

        public int nro_leido { get; set; }
    }
}
