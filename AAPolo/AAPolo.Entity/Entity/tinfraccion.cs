namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tinfraccion")]
    public partial class tinfraccion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tinfraccion()
        {
            tinfraccion_partido = new HashSet<tinfraccion_partido>();
        }

        [Key]
        public int cod_infraccion { get; set; }

        [Required]
        [StringLength(200)]
        public string txt_desc { get; set; }

        [Column(TypeName = "date")]
        public DateTime fec_infraccion { get; set; }

        public int cod_tipo_infraccion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tinfraccion_partido> tinfraccion_partido { get; set; }

        public virtual ttipo_infraccion ttipo_infraccion { get; set; }
    }
}
