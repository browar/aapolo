namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tevento")]
    public partial class tevento
    {
        [Key]
        public int cod_evento { get; set; }

        [Required]
        [StringLength(200)]
        public string txt_desc { get; set; }

        public DateTime fec_comienzo { get; set; }

        public int cod_tipo_evento { get; set; }

        public int? cod_torneo { get; set; }

        [Required]
        [StringLength(200)]
        public string usuario_alta { get; set; }

        public DateTime fec_alta { get; set; }

        public DateTime? fec_ultima_modificacion { get; set; }

        [StringLength(200)]
        public string usuario_ultima_modificacion { get; set; }

        public bool? bool_activo { get; set; }

        [StringLength(2000)]
        public string txt_desc_larga { get; set; }

        [StringLength(2000)]
        public string txt_imagen { get; set; }

        public virtual ttipo_evento ttipo_evento { get; set; }

        public virtual ttorneo ttorneo { get; set; }
    }
}
