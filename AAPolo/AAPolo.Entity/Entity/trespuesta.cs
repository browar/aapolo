namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("trespuesta")]
    public partial class trespuesta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public trespuesta()
        {
            trespuesta_adjunto = new HashSet<trespuesta_adjunto>();
        }

        [Key]
        public int cod_respuesta { get; set; }

        public int? cod_notificacion { get; set; }

        public int? cod_usuario_envia { get; set; }

        public int? cod_usuario_recibe { get; set; }

        [StringLength(2000)]
        public string txt_desc { get; set; }

        public DateTime? fec_enviado { get; set; }

        public virtual tnotificacion tnotificacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trespuesta_adjunto> trespuesta_adjunto { get; set; }

        public virtual users users { get; set; }

        public virtual users users1 { get; set; }
    }
}
