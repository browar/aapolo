namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tmunicipio")]
    public partial class tmunicipio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tmunicipio()
        {
            tdomicilio = new HashSet<tdomicilio>();
        }

        [Key]
        public int cod_municipio { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        public int cod_provincia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tdomicilio> tdomicilio { get; set; }

        public virtual tprovincia tprovincia { get; set; }
    }
}
