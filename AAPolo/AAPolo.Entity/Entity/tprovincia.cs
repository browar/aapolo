namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tprovincia")]
    public partial class tprovincia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tprovincia()
        {
            tdomicilio = new HashSet<tdomicilio>();
            tmunicipio = new HashSet<tmunicipio>();
        }

        [Key]
        public int cod_provincia { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        public int cod_pais { get; set; }

        [StringLength(3)]
        public string nro_codigo { get; set; }

        [StringLength(3)]
        public string cod_jurisdiccion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tdomicilio> tdomicilio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tmunicipio> tmunicipio { get; set; }

        public virtual tpais tpais { get; set; }
    }
}
