namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tequipo")]
    public partial class tequipo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tequipo()
        {
            tequipo_user = new HashSet<tequipo_user>();
            tpartido = new HashSet<tpartido>();
            tpartido1 = new HashSet<tpartido>();
        }

        [Key]
        public int cod_equipo { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        public DateTime fec_alta { get; set; }

        [StringLength(200)]
        public string txt_alta { get; set; }

        public DateTime? fec_ultima_modificacion { get; set; }

        [StringLength(200)]
        public string txt_ultima_modificación { get; set; }

        public int cod_torneo { get; set; }

        public int? cod_club { get; set; }

        public virtual tclub tclub { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tequipo_user> tequipo_user { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tpartido> tpartido { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tpartido> tpartido1 { get; set; }

        public virtual ttorneo ttorneo { get; set; }
    }
}
