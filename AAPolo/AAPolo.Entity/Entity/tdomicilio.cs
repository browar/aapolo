namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tdomicilio")]
    public partial class tdomicilio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tdomicilio()
        {
            tclub = new HashSet<tclub>();
            tclub_historico = new HashSet<tclub_historico>();
            tsede = new HashSet<tsede>();
            tmiembro = new HashSet<tmiembro>();
            users = new HashSet<users>();
            users_historico = new HashSet<users_historico>();
        }

        [Key]
        public int cod_domicilio { get; set; }

        public int? cod_tipo_domicilio { get; set; }

        [StringLength(500)]
        public string txt_domicilio { get; set; }

        public int? cod_pais { get; set; }

        public int? cod_provincia { get; set; }

        public int? cod_municipio { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_ult_modificacion { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_alta { get; set; }

        public int? nro_domicilio { get; set; }

        public int? nro_piso { get; set; }

        [StringLength(3)]
        public string txt_departamento { get; set; }

        [StringLength(10)]
        public string nro_cod_postal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tclub> tclub { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tclub_historico> tclub_historico { get; set; }

        public virtual tmunicipio tmunicipio { get; set; }

        public virtual tpais tpais { get; set; }

        public virtual tprovincia tprovincia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tsede> tsede { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tmiembro> tmiembro { get; set; }

        public virtual ttipo_domicilio ttipo_domicilio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<users> users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<users_historico> users_historico { get; set; }
    }
}
