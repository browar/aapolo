namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ttutor")]
    public partial class ttutor
    {
        [Key]
        public int cod_tutor { get; set; }

        [StringLength(200)]
        public string txt_nombre_completo { get; set; }

        [Required]
        [StringLength(30)]
        public string txt_documento { get; set; }

        public int cod_usuario { get; set; }

        [StringLength(50)]
        public string txt_telefono_contacto { get; set; }

        public virtual users users { get; set; }
    }
}
