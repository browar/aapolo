﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.Entity
{
    public class reporteTorneo
    {
        public ttorneo Torneo { get; set; }
        public tequipo Equipo { get; set; }
        public tclub Club { get; set; }
        public List<EquipoUserReporte> Usuarios { get; set; }
        public int? sn_pago { get; set; }
        public decimal? importe { get; set; }
        public DateTime? fec_ult_pago { get; set; }
        public int? cod_historial_pago { get; set; }
        public string txt_url_comprobante { get; set; }
        public bool InscripcionInvididual { get; set; }
    }
    public class EquipoUserReporte
    {
        public int cod_usuario { get; set; }
        public string txt_nombre { get; set; }
        public string txt_apellido { get; set; }
        public int? bool_Capitan { get; set; }
        public int? bool_suplente { get; set; }
        public string sexo { get; set; }
        public string email { get; set; }
        public string telefono { get; set; }
        public int? handicap_principal { get; set; }
        public int? handicap_secundario { get; set; }
    }

}
