namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("treconocimiento")]
    public partial class treconocimiento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public treconocimiento()
        {
            treconocimiento_usuario = new HashSet<treconocimiento_usuario>();
        }

        [Key]
        public int cod_reconocimiento { get; set; }

        public int cod_torneo { get; set; }

        public int cod_tipo_reconocimiento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<treconocimiento_usuario> treconocimiento_usuario { get; set; }

        public virtual ttipo_reconocimiento ttipo_reconocimiento { get; set; }

        public virtual ttorneo ttorneo { get; set; }
    }
}
