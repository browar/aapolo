namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class users_admin
    {
        [Key]
        public int cod_usuario { get; set; }

        [StringLength(50)]
        public string txt_usuario { get; set; }

        public string txt_password { get; set; }

        public bool? bool_activo { get; set; }
    }
}
