namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tnoticia_archivo
    {
        [Key]
        public int cod_noticia_archivo { get; set; }

        public int? cod_noticia { get; set; }

        public int? cod_tipo_archivo { get; set; }

        [StringLength(300)]
        public string url_archivo { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        public int? cod_usuario_alta { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_alta { get; set; }

        public int? orden { get; set; }

        public int? sn_activo { get; set; }

        public virtual tnoticia tnoticia { get; set; }

        public virtual ttipo_archivo ttipo_archivo { get; set; }

        public virtual users users { get; set; }
    }
}
