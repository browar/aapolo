namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tnotificacion_adjunto
    {
        [Key]
        public int cod_notificacion_adjunto { get; set; }

        public int cod_adjunto { get; set; }

        public int cod_notificacion { get; set; }

        public virtual tadjunto tadjunto { get; set; }

        public virtual tnotificacion tnotificacion { get; set; }
    }
}
