namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("treferee")]
    public partial class treferee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public treferee()
        {
            tpartido = new HashSet<tpartido>();
        }

        [Key]
        public int cod_referee { get; set; }

        public int cod_tipo_referee { get; set; }

        public int cod_usuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tpartido> tpartido { get; set; }

        public virtual users users { get; set; }

        public virtual ttipo_referee ttipo_referee { get; set; }
    }
}
