namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tmotivo_baja
    {
        [Key]
        public int cod_motivo_baja { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }
    }
}
