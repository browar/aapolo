namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public users()
        {
            tcontacto_usuario = new HashSet<tcontacto_usuario>();
            tdatos_emergencia = new HashSet<tdatos_emergencia>();
            tequipo_user = new HashSet<tequipo_user>();
            thandicap_usuario = new HashSet<thandicap_usuario>();
            thandicap_usuario_historico = new HashSet<thandicap_usuario_historico>();
            thistorial_pagos = new HashSet<thistorial_pagos>();
            tinfraccion_partido = new HashSet<tinfraccion_partido>();
            tmiembro = new HashSet<tmiembro>();
            tnoticia = new HashSet<tnoticia>();
            tnoticia_archivo = new HashSet<tnoticia_archivo>();
            tnotificacion_usuario = new HashSet<tnotificacion_usuario>();
            tpartido = new HashSet<tpartido>();
            tranking = new HashSet<tranking>();
            treconocimiento_usuario = new HashSet<treconocimiento_usuario>();
            treferee = new HashSet<treferee>();
            trespuesta = new HashSet<trespuesta>();
            trespuesta1 = new HashSet<trespuesta>();
            ttutor = new HashSet<ttutor>();
            tuser_club = new HashSet<tuser_club>();
            tveedor = new HashSet<tveedor>();
        }

        [Key]
        public int cod_usuario { get; set; }

        [StringLength(50)]
        public string txt_nombre { get; set; }

        [StringLength(50)]
        public string txt_apellido { get; set; }

        public string txt_password { get; set; }

        public int? cod_tipo_doc { get; set; }

        [StringLength(50)]
        public string nro_doc { get; set; }

        public int? cod_estado { get; set; }

        public int? cant_intentos_acceso { get; set; }

        [StringLength(2000)]
        public string url_foto { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_ult_login { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_alta { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_ult_modificacion { get; set; }

        public int? cod_domicilio { get; set; }

        [StringLength(200)]
        public string usuario_alta { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fec_nacimiento { get; set; }

        [StringLength(300)]
        public string txt_facebook { get; set; }

        [StringLength(300)]
        public string txt_twitter { get; set; }

        [StringLength(300)]
        public string txt_instagram { get; set; }

        public int? cod_adjunto { get; set; }

        public int cod_tipo_jugador { get; set; }

        public int cod_genero { get; set; }

        public bool? bool_activo { get; set; }

        public int? cod_nacionalidad { get; set; }

        public bool? bool_veedor { get; set; }

        [StringLength(10)]
        public string JugadorID { get; set; }

        public string txt_imagen { get; set; }

        public int? cod_datos_emergencia { get; set; }

        public bool? bool_acepta_terminos { get; set; }

        [StringLength(300)]
        public string id_register { get; set; }

        [StringLength(150)]
        public string platform_mobile { get; set; }

        [StringLength(100)]
        public string txt_tarjeta { get; set; }

        [NotMapped]
        public int? nro_hand { get; set; }

        public virtual tadjunto tadjunto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tcontacto_usuario> tcontacto_usuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tdatos_emergencia> tdatos_emergencia { get; set; }

        public virtual tdatos_emergencia tdatos_emergencia1 { get; set; }

        public virtual tdomicilio tdomicilio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tequipo_user> tequipo_user { get; set; }

        public virtual tgenero tgenero { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<thandicap_usuario> thandicap_usuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<thandicap_usuario_historico> thandicap_usuario_historico { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<thistorial_pagos> thistorial_pagos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tinfraccion_partido> tinfraccion_partido { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tmiembro> tmiembro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tnoticia> tnoticia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tnoticia_archivo> tnoticia_archivo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tnotificacion_usuario> tnotificacion_usuario { get; set; }

        public virtual tpais tpais { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tpartido> tpartido { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tranking> tranking { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<treconocimiento_usuario> treconocimiento_usuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<treferee> treferee { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trespuesta> trespuesta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trespuesta> trespuesta1 { get; set; }

        public virtual ttipo_doc ttipo_doc { get; set; }

        public virtual ttipo_jugador ttipo_jugador { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ttutor> ttutor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tuser_club> tuser_club { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tveedor> tveedor { get; set; }
    }
}
