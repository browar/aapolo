namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tdatos_emergencia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tdatos_emergencia()
        {
            users1 = new HashSet<users>();
        }

        [Key]
        public int cod_datos_emergencia { get; set; }

        [StringLength(200)]
        public string txt_nombre_completo { get; set; }

        [StringLength(50)]
        public string txt_telefono_contacto { get; set; }

        [Required]
        [StringLength(100)]
        public string txt_obra_social { get; set; }

        [Required]
        [StringLength(100)]
        public string txt_sanatorio_emergencia { get; set; }

        [StringLength(4)]
        public string txt_tipo_sanguineo { get; set; }

        public int? cod_usuario { get; set; }

        [StringLength(30)]
        public string txt_numero_documento { get; set; }

        public virtual users users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<users> users1 { get; set; }
    }
}
