namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tnoticia_etiqueta
    {
        [Key]
        public int cod_noticia_etiqueta { get; set; }

        public int? cod_noticia { get; set; }

        public int? cod_etiqueta { get; set; }

        public virtual tetiqueta tetiqueta { get; set; }

        public virtual tnoticia tnoticia { get; set; }
    }
}
