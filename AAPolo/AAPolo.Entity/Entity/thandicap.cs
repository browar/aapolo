namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("thandicap")]
    public partial class thandicap
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public thandicap()
        {
            ttransicion = new HashSet<ttransicion>();
            ttransicion1 = new HashSet<ttransicion>();
            thandicap_usuario = new HashSet<thandicap_usuario>();
            thandicap_usuario_historico = new HashSet<thandicap_usuario_historico>();
        }

        [Key]
        public int cod_handicap { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        [StringLength(100)]
        public string txt_vigencia { get; set; }

        [StringLength(25)]
        public string txt_nacionalidad { get; set; }

        [StringLength(150)]
        public string txt_edad { get; set; }

        public int? cod_categoria_handicap { get; set; }

        public decimal? nro_importe { get; set; }

        public virtual tcategoria_handicap tcategoria_handicap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ttransicion> ttransicion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ttransicion> ttransicion1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<thandicap_usuario> thandicap_usuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<thandicap_usuario_historico> thandicap_usuario_historico { get; set; }
    }
}
