namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tnoticia")]
    public partial class tnoticia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tnoticia()
        {
            tnoticia_etiqueta = new HashSet<tnoticia_etiqueta>();
            tnoticia_archivo = new HashSet<tnoticia_archivo>();
        }

        [Key]
        public int cod_noticia { get; set; }

        public int? cod_tipo_noticia { get; set; }

        [StringLength(400)]
        public string txt_titulo { get; set; }

        [StringLength(500)]
        public string txt_copete { get; set; }

        public string txt_cuerpo { get; set; }

        public int? cod_usuario_alta { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_alta { get; set; }

        public int? cod_usuario_ult_modificacion { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_ult_modificacion { get; set; }

        public int? bool_activo { get; set; }

        public int? bool_destacada { get; set; }

        public string txt_adjunto_footer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tnoticia_etiqueta> tnoticia_etiqueta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tnoticia_archivo> tnoticia_archivo { get; set; }

        public virtual ttipo_noticia ttipo_noticia { get; set; }

        public virtual users users { get; set; }
    }
}
