namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tcategoria_club
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tcategoria_club()
        {
            tclub = new HashSet<tclub>();
            tclub_historico = new HashSet<tclub_historico>();
        }

        [Key]
        public int cod_categoria_club { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        [StringLength(50)]
        public string txt_desc_corta { get; set; }

        public decimal? nro_importe { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tclub> tclub { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tclub_historico> tclub_historico { get; set; }
    }
}
