namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tetiqueta")]
    public partial class tetiqueta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tetiqueta()
        {
            tnoticia_etiqueta = new HashSet<tnoticia_etiqueta>();
        }

        [Key]
        public int cod_etiqueta { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tnoticia_etiqueta> tnoticia_etiqueta { get; set; }
    }
}
