namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tuser_club
    {
        [Key]
        public int cod_jugador_club { get; set; }

        public int? cod_club { get; set; }

        public int? cod_usuario { get; set; }

        public bool bool_principal { get; set; }

        public bool? bool_rechazado { get; set; }

        public bool? bool_republica { get; set; }

        public DateTime? fec_alta { get; set; }

        public DateTime? fec_vigencia { get; set; }

        public virtual tclub tclub { get; set; }

        public virtual users users { get; set; }
    }
}
