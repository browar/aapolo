namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tcontacto_usuario
    {
        [Key]
        public int cod_contacto_usuario { get; set; }

        public int cod_contacto { get; set; }

        public int cod_usuario { get; set; }

        public virtual tcontacto tcontacto { get; set; }

        public virtual users users { get; set; }
    }
}
