namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tcontacto_club
    {
        [Key]
        public int cod_contacto_club { get; set; }

        public int cod_contacto { get; set; }

        public int cod_club { get; set; }

        public virtual tclub tclub { get; set; }

        public virtual tcontacto tcontacto { get; set; }
    }
}
