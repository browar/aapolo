namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tranking")]
    public partial class tranking
    {
        [Key]
        public int cod_ranking { get; set; }

        [Required]
        [StringLength(200)]
        public string txt_desc { get; set; }

        public int cod_usuario { get; set; }

        public DateTime fec_ranking { get; set; }

        public virtual users users { get; set; }
    }
}
