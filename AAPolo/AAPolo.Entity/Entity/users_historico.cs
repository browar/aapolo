namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class users_historico
    {
        [Key]
        public int cod_usuario_historico { get; set; }

        public int? cod_usuario { get; set; }

        [StringLength(50)]
        public string txt_nombre { get; set; }

        [StringLength(50)]
        public string txt_apellido { get; set; }

        public string txt_password { get; set; }

        public int? cod_tipo_doc { get; set; }

        [StringLength(50)]
        public string nro_doc { get; set; }

        public bool? bool_activo { get; set; }

        public int? cant_intentos_acceso { get; set; }

        [StringLength(2000)]
        public string url_foto { get; set; }

        public int? cod_domicilio { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_alta { get; set; }

        [StringLength(200)]
        public string usuario_alta { get; set; }

        public DateTime? fec_alta_historico { get; set; }

        [StringLength(200)]
        public string usuario_alta_historico { get; set; }

        [StringLength(300)]
        public string txt_facebook { get; set; }

        [StringLength(300)]
        public string txt_twitter { get; set; }

        [StringLength(300)]
        public string txt_instagram { get; set; }

        public int? cod_adjunto { get; set; }

        public int cod_tipo_jugador { get; set; }

        public int cod_genero { get; set; }

        public virtual tdomicilio tdomicilio { get; set; }

        public virtual tgenero tgenero { get; set; }

        public virtual ttipo_doc ttipo_doc { get; set; }

        public virtual ttipo_jugador ttipo_jugador { get; set; }
    }
}
