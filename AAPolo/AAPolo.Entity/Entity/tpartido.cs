namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tpartido")]
    public partial class tpartido
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tpartido()
        {
            tinfraccion_partido = new HashSet<tinfraccion_partido>();
        }

        [Key]
        public int cod_partido { get; set; }

        public int cod_torneo { get; set; }

        public int cod_equipo_local { get; set; }

        public int cod_equipo_visitante { get; set; }

        public int nro_resultado_equipo_uno { get; set; }

        public int nro_resultado_equipo_dos { get; set; }

        public DateTime fec_partido { get; set; }

        public int? cod_instancia { get; set; }

        public bool? bool_activo { get; set; }

        public int? cod_referee { get; set; }

        public int? cod_veedor { get; set; }

        public virtual tequipo tequipo { get; set; }

        public virtual tequipo tequipo1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tinfraccion_partido> tinfraccion_partido { get; set; }

        public virtual tinstancia tinstancia { get; set; }

        public virtual treferee treferee { get; set; }

        public virtual ttorneo ttorneo { get; set; }

        public virtual users users { get; set; }
    }
}
