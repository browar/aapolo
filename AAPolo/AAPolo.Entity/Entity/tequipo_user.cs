namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tequipo_user
    {
        [Key]
        public int cod_equipo_jugadores { get; set; }

        public int cod_equipo { get; set; }

        public int cod_usuario { get; set; }

        public bool? bool_capitan { get; set; }

        public int cod_estado { get; set; }

        public bool? bool_suplente { get; set; }

        public int? cod_torneo { get; set; }

        public virtual tequipo tequipo { get; set; }

        public virtual testado testado { get; set; }

        public virtual users users { get; set; }
    }
}
