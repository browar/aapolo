namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class trespuesta_adjunto
    {
        [Key]
        public int cod_respuesta_adjunto { get; set; }

        public int cod_respuesta { get; set; }

        public string txt_url_image { get; set; }

        public virtual trespuesta trespuesta { get; set; }
    }
}
