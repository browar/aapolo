namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tpopup")]
    public partial class tpopup
    {
        [Key]
        public int cod_popup { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        public DateTime? fec_vigencia { get; set; }

        [StringLength(2000)]
        public string txt_imagen { get; set; }

        [StringLength(2000)]
        public string txt_video { get; set; }

        public bool bool_activo { get; set; }
    }
}
