namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tcontacto")]
    public partial class tcontacto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tcontacto()
        {
            tcontacto_usuario = new HashSet<tcontacto_usuario>();
            tcontacto_club = new HashSet<tcontacto_club>();
        }

        [Key]
        public int cod_contacto { get; set; }

        public int? cod_tipo_contacto { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tcontacto_usuario> tcontacto_usuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tcontacto_club> tcontacto_club { get; set; }

        public virtual ttipo_contacto ttipo_contacto { get; set; }
    }
}
