namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tnotificacion")]
    public partial class tnotificacion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tnotificacion()
        {
            tnotificacion_adjunto = new HashSet<tnotificacion_adjunto>();
            tnotificacion_rol = new HashSet<tnotificacion_rol>();
            trespuesta = new HashSet<trespuesta>();
            tnotificacion_usuario = new HashSet<tnotificacion_usuario>();
            tnotificacion_tipo_jugador = new HashSet<tnotificacion_tipo_jugador>();
        }

        [Key]
        public int cod_notificacion { get; set; }

        public string txt_desc { get; set; }

        [Required]
        [StringLength(200)]
        public string txt_asunto { get; set; }

        public int cod_tipo_notificacion { get; set; }

        public bool? bool_destacado { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fec_enviado { get; set; }

        [StringLength(50)]
        public string txt_alta { get; set; }

        [StringLength(200)]
        public string cod_enviado_por { get; set; }

        public int? nro_handicap_minimo { get; set; }

        public int? nro_handicap_maximo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tnotificacion_adjunto> tnotificacion_adjunto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tnotificacion_rol> tnotificacion_rol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trespuesta> trespuesta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tnotificacion_usuario> tnotificacion_usuario { get; set; }

        public virtual ttipo_notificacion ttipo_notificacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tnotificacion_tipo_jugador> tnotificacion_tipo_jugador { get; set; }
    }
}
