namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class thistorial_pagos
    {
        [Key]
        public int cod_historial_pagos { get; set; }

        [Required]
        [StringLength(200)]
        public string txt_desc { get; set; }

        public decimal? nro_importe { get; set; }

        public DateTime fec_alta { get; set; }

        public int cod_usuario { get; set; }

        [Required]
        [StringLength(500)]
        public string txt_url_comprobante { get; set; }

        public int cod_tipo_pago { get; set; }

        [Column(TypeName = "date")]
        public DateTime fec_vencimiento { get; set; }

        public int? cod_usuario_alta { get; set; }

        public int? nro_resultado_pago { get; set; }

        public int? cod_club_alta { get; set; }

        [StringLength(30)]
        public string ArticuloID { get; set; }

        public int? cod_torneo { get; set; }

        public int? cod_equipo { get; set; }

        [StringLength(50)]
        public string cod_mercado_pago { get; set; }

        public virtual ttipo_pago ttipo_pago { get; set; }
    }
}
