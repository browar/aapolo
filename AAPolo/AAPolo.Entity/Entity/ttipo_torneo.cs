namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ttipo_torneo
    {
        [Key]
        public int cod_tipo_torneo { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }
    }
}
