namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tveedor")]
    public partial class tveedor
    {
        [Key]
        public int cod_veedor { get; set; }

        public int cod_usuario { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        public virtual users users { get; set; }
    }
}
