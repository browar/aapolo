namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ttorneo_historico
    {
        [Key]
        public int cod_torneo { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fec_comienzo { get; set; }

        public int cod_categoria_torneo { get; set; }

        [StringLength(20)]
        public string txt_sexo { get; set; }

        [StringLength(20)]
        public string txt_edad_desc { get; set; }

        public int? nro_handicap_equipo_minimo { get; set; }

        public int? nro_handicap_equipo_maximo { get; set; }

        public int? nro_handicap_jugador_minimo { get; set; }

        public int? nro_handicap_jugador_maximo { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fec_alta { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fec_ult_modificacion { get; set; }

        [StringLength(200)]
        public string usuario_alta { get; set; }

        public bool? bool_inscripcion { get; set; }

        public int? nro_handicap_especifico { get; set; }

        public decimal? nro_precio_inscripcion { get; set; }

        public int? cod_categoria_handicap { get; set; }

        [StringLength(1000)]
        public string txt_desc_larga { get; set; }

        [StringLength(250)]
        public string usuario_alta_historico { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fec_alta_historico { get; set; }

        public virtual tcategoria_handicap tcategoria_handicap { get; set; }

        public virtual tcategoria_torneo tcategoria_torneo { get; set; }
    }
}
