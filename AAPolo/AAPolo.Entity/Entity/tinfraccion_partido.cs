namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tinfraccion_partido
    {
        [Key]
        public int cod_infraccion_partido { get; set; }

        public int cod_infraccion { get; set; }

        public int cod_partido { get; set; }

        public int? cod_usuario { get; set; }

        public virtual tinfraccion tinfraccion { get; set; }

        public virtual tpartido tpartido { get; set; }

        public virtual users users { get; set; }
    }
}
