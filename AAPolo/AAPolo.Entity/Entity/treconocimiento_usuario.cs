namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class treconocimiento_usuario
    {
        [Key]
        public int cod_reconocimiento_usuario { get; set; }

        public int cod_reconocimiento { get; set; }

        public int cod_usuario { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fec_reconocimiento { get; set; }

        public virtual treconocimiento treconocimiento { get; set; }

        public virtual users users { get; set; }
    }
}
