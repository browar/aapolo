namespace AAPolo.Entity.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tclub")]
    public partial class tclub
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tclub()
        {
            tsede = new HashSet<tsede>();
            tuser_club = new HashSet<tuser_club>();
            tequipo = new HashSet<tequipo>();
            tcontacto_club = new HashSet<tcontacto_club>();
            tmiembro = new HashSet<tmiembro>();
        }

        [Key]
        public int cod_club { get; set; }

        public int? cod_categoria_club { get; set; }

        [StringLength(200)]
        public string txt_desc { get; set; }

        [StringLength(200)]
        public string cod_club_desc { get; set; }

        public int? nro_canchas { get; set; }

        [StringLength(400)]
        public string txt_presidente { get; set; }

        [StringLength(400)]
        public string txt_vicepresidente { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_alta { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fec_ult_modificacion { get; set; }

        [StringLength(255)]
        public string txt_vocal1 { get; set; }

        [StringLength(255)]
        public string txt_vocal2 { get; set; }

        [StringLength(255)]
        public string txt_vocal3 { get; set; }

        [StringLength(255)]
        public string txt_tesorero { get; set; }

        public int? cod_domicilio { get; set; }

        [StringLength(200)]
        public string usuario_alta { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? fec_fundacion { get; set; }

        public string txt_historia { get; set; }

        public int? cod_admin_club { get; set; }

        public int? nro_cant_socios { get; set; }

        public bool? bool_activo { get; set; }

        [StringLength(10)]
        public string ClubID { get; set; }

        [StringLength(300)]
        public string txt_facebook { get; set; }

        [StringLength(300)]
        public string txt_twitter { get; set; }

        [StringLength(300)]
        public string txt_instagram { get; set; }

        public string txt_imagen { get; set; }

        public virtual tcategoria_club tcategoria_club { get; set; }

        public virtual tdomicilio tdomicilio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tsede> tsede { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tuser_club> tuser_club { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tequipo> tequipo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tcontacto_club> tcontacto_club { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tmiembro> tmiembro { get; set; }
    }
}
