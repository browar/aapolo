﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tcanchaDTO
    {
        public int cod_cancha { get; set; }
        public string txt_desc { get; set; }
        public int cod_sede { get; set; }
        //public virtual tsede tsede { get; set; }
    }
}
