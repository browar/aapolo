﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tcontactoDTO
    {
        public int cod_contacto { get; set; }
        public int? cod_tipo_contacto { get; set; }
        public string txt_desc { get; set; }
        public ttipo_contactoDTO ttipo_contacto { get; set; }
    }
}
