﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class thandicap_usuario_historicoDTO
    {
        public int cod_handicap_usuario_historico { get; set; }
        public int cod_handicap_usuario { get; set; }
        public int cod_handicap { get; set; }
        public int cod_usuario { get; set; }
        public bool? bool_principal { get; set; }
        public DateTime? fec_ult_modificacion { get; set; }
        public int? nro_handicap { get; set; }
    }
}
