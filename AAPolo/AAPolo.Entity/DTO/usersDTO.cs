﻿using AAPolo.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class usersDTO
    {
        public int cod_usuario { get; set; }
        public string txt_nombre { get; set; }
        public string txt_apellido { get; set; }
        public string txt_password { get; set; }
        public int? cod_tipo_doc { get; set; }
        public int? cod_tipo_jugador { get; set; }
        public string nro_doc { get; set; }
        public int? bool_activo { get; set; }
        public int? bool_veedor { get; set; }
        public int? cant_intentos_acceso { get; set; }
        public string url_foto { get; set; }
        public DateTime? fec_ult_login { get; set; }
        public DateTime? fec_alta { get; set; }
        public DateTime? fec_ult_modificacion { get; set; }
        public int? cod_estado { get; set; }
        public int? cod_domicilio { get; set; }
        public tdomicilioDTO tdomicilio { get; set; }
        public string usuario_alta { get; set; }
        public List<thandicap_usuarioDTO> thandicap_usuario { get; set; }
        public ttipo_docDTO ttipo_doc { get; set; }
        public tgeneroDTO tgenero { get; set; }
        public int? cod_genero { get; set; }
        public int? nro_handicap { get; set; }
        public string txt_facebook { get; set; }
        public string txt_twitter { get; set; }
        public string txt_instagram { get; set; }
        public int? cod_adjunto { get; set; }
        public DateTime? fec_nacimiento { get; set; }
        public string txt_mail { get; set; }
        public List<tcontacto_usuarioDTO> tcontacto_usuario { get; set; }
        public List<tuser_clubDTO> tuser_club { get; set; }
        public ttipo_jugadorDTO ttipo_jugador { get; set; }
        public List<thistorial_pagosDTO> thistorial_pagos { get; set; }
        public int? cod_nacionalidad { get; set; }
        public virtual tpaisDTO tpais { get; set; }
        public string txt_imagen { get; set; }
        public bool? bool_acepta_terminos { get; set; }
        public bool isAdmin { get; set; }
        public string id_register { get; set; }
        public string platform_mobile { get; set; }
        public string txt_tarjeta { get; set; }
    }
}
