﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tsedeDTO
    {
        public int cod_sede { get; set; }        
        public string txt_desc { get; set; }
        public int cod_domicilio { get; set; }
        public int cod_club { get; set; }
        public int? cod_tipo_sede { get; set; }
        public tdomicilioDTO tdomicilio { get; set; }
        public ICollection<tcanchaDTO> tcancha { get; set; }
        public ttipo_sedeDTO ttipo_sede { get; set; }
    }
}
