﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tcomunicadoDTO
    {
        public int cod_comunicado { get; set; }
        public string txt_desc { get; set; }
        public string txt_titulo { get; set; }
        public DateTime fec_emision { get; set; }
        public int nro_leido { get; set; }
    }
}
