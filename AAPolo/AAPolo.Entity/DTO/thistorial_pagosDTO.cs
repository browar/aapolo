namespace AAPolo.Entity.DTO
{
    using System;

    public partial class thistorial_pagosDTO
    {
        public int cod_historial_pagos { get; set; }
        public string txt_desc { get; set; }
        public decimal nro_importe { get; set; }
        public DateTime fec_alta { get; set; }
        public int cod_usuario { get; set; }
        public string txt_url_comprobante { get; set; }
        public int cod_tipo_pago { get; set; }
        public DateTime fec_vencimiento { get; set; }
        public int nro_resultado_pago { get; set; }
        public ttipo_pagoDTO ttipo_pago { get; set; }
        public int cod_usuario_alta { get; set; }
        public int? cod_club_alta { get; set; }
        public int cod_torneo { get; set; }
        public string cod_mercado_pago { get; set; }
    }
}
