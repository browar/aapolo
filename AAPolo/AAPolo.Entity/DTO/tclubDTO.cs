﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tclubDTO
    {
        public int cod_club { get; set; }
        public int? cod_categoria_club { get; set; }
        public string txt_desc { get; set; }
        public string cod_club_desc { get; set; }
        public int? nro_canchas { get; set; }
        public int? cod_estado { get; set; }
        public string txt_presidente { get; set; }
        public string txt_vicepresidente { get; set; }
        public DateTime? fec_alta { get; set; }
        public DateTime? fec_ult_modificacion { get; set; }
        public string txt_vocal1 { get; set; }
        public string txt_vocal2 { get; set; }
        public string txt_vocal3 { get; set; }
        public string txt_tesorero { get; set; }
        public int? cod_domicilio { get; set; }
        public int? cod_contacto { get; set; }
        public tcategoria_clubDTO tcategoria_club { get; set; }
        public tcontactoDTO tcontacto { get; set; }
        public tdomicilioDTO tdomicilio { get; set; }
        public int? nro_cant_socios { get; set; }
        public bool? bool_activo { get; set; }
        public DateTime fec_fundacion { get; set; }
        public string txt_historia { get; set; }
        public int? cod_admin_club { get; set; }
        public List<tcontacto_clubDTO> tcontacto_club { get; set; }
        public string txt_direccion_canchas { get; set; }
        public List<tsedeDTO> tsede { get; set; }
        public string usuario_alta { get; set; }
        public string txt_facebook { get; set; }        
        public string txt_twitter { get; set; }
        public string txt_instagram { get; set; }
        public string txt_imagen { get; set; }
    }
}
