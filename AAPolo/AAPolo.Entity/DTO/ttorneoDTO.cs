﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class ttorneoDTO
    {
        public int cod_torneo { get; set; }
        public string txt_desc { get; set; }
        public DateTime fec_comienzo { get; set; }
        public int cod_categoria_torneo { get; set; }
        public int cod_tipo_torneo { get; set; }
        public ttipo_torneoDTO ttipo_torneo { get; set; }
        public DateTime fec_alta { get; set; }
        public DateTime? fec_ult_modificacion { get; set; }
        public int? nro_handicap_equipo_minimo { get; set; }
        public int? nro_handicap_equipo_maximo { get; set; }
        public int? nro_handicap_jugador_minimo { get; set; }
        public int? nro_handicap_jugador_maximo { get; set; }
        public tcategoria_torneoDTO tcategoria_torneo { get; set; }
        public string txt_sexo { get; set; }
        public string txt_edad_desc { get; set; }
        public string usuario_alta { get; set; }
        public bool? bool_inscripcion { get; set; }
        public int? nro_handicap_especifico { get; set; }
        public decimal? nro_precio_inscripcion { get; set; }
        public string txt_desc_larga { get; set; }
        public int? cod_categoria_handicap { get; set; }
        public tcategoria_handicapDTO tcategoria_handicap { get; set; }
        public string txt_imagen { get; set; }
        public bool? bool_activo { get; set; }
        public bool? bool_menores { get; set; }
        public bool? bool_inscripcion_individual { get; set; }
        public List<tpartidoDTO> tpartido { get; set; }
        public List<tequipoDTO> tequipo { get; set; }
    }
}
