﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tcontacto_clubDTO
    {
        public int cod_contacto_club { get; set; }
        public int cod_contacto { get; set; }
        public int cod_club { get; set; }
        public virtual tcontactoDTO tcontacto { get; set; }
    }
}
