﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tprovinciaDTO
    {
        public int cod_provincia { get; set; }
        public string txt_desc { get; set; }
        public int cod_pais { get; set; }
        public string nro_codigo { get; set; }
        public string cod_jurisdiccion { get; set; }
        public tpaisDTO tpais { get; set; }
    }
}
