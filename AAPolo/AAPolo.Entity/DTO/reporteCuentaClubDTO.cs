﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class reporteCuentaClubDTO
    {
        public int cod_club { get; set; }
        public string Club { get; set; }
        public int cod_categoria_club { get; set; }
        public string CategoriaClub { get; set; }
        public int? UsuarioAdmin { get; set; }
        public string NombreAdmin { get; set; }
        public string ApellidoAdmin { get; set; }
        public int? sn_pago { get; set; }
        public decimal? importe { get; set; }
        public DateTime? fec_ult_pago { get; set; }
    }
}
