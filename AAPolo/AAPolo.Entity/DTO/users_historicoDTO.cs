﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class users_historicoDTO
    {
        public int cod_usuario_historico { get; set; }
        public int cod_usuario { get; set; }
        public string txt_nombre { get; set; }
        public string txt_apellido { get; set; }
        public string txt_password { get; set; }
        public int? cod_tipo_doc { get; set; }
        public string nro_doc { get; set; }
        public int? cod_club { get; set; }
        public int? cod_estado { get; set; }
        public int? sn_activo { get; set; }
        public int? cant_intentos_acceso { get; set; }
        public string url_foto { get; set; }
        public int? cod_contacto { get; set; }
        public int? cod_domicilio { get; set; }
        public tcontactoDTO tcontacto { get; set; }
        public tdomicilioDTO tdomicilio { get; set; }
        public ttipo_docDTO ttipo_doc { get; set; }
        public DateTime? fec_alta { get; set; }
        public string usuario_alta { get; set; }
        public string usuario_alta_historico { get; set; }
    }
}
