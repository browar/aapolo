﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tinfraccion_partidoDTO
    {
        public int cod_infraccion_partido { get; set; }
        public int cod_infraccion { get; set; }
        public int cod_partido { get; set; }
        public int? cod_usuario { get; set; }
        public virtual tinfraccionDTO tinfraccion { get; set; }
        public virtual tpartidoDTO tpartido { get; set; }
        public virtual usersDTO users { get; set; }
    }
}
