﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tnoticiaDTO
    {
        public int cod_noticia { get; set; }
        public int? cod_tipo_noticia { get; set; }
        public string txt_titulo { get; set; }
        public string txt_copete { get; set; }
        public string txt_cuerpo { get; set; }
        public int? cod_usuario_alta { get; set; }
        public DateTime? fec_alta { get; set; }
        public int? cod_usuario_ult_modificacion { get; set; }
        public DateTime? fec_ult_modificacion { get; set; }
        public int? bool_activo{ get; set; }
        public int? bool_destacada { get; set; }
        public string txt_adjunto_footer { get; set; }
        public virtual ICollection<tnoticia_etiquetaDTO> tnoticia_etiqueta { get; set; }
        public virtual ICollection<tnoticia_archivoDTO> tnoticia_archivo { get; set; }
        public ttipo_noticiaDTO ttipo_noticia { get; set; }
    }
}
