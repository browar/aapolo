namespace AAPolo.Entity.DTO
{
    public partial class tnotificacion_adjuntoDTO
    {
        public int cod_notificacion_adjunto { get; set; }
        public int cod_adjunto { get; set; }
        public int cod_notificacion { get; set; }
        public tadjuntoDTO tadjunto { get; set; }
        public tnotificacionDTO tnotificacion { get; set; }
    }
}
