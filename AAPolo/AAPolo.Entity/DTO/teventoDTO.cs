﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class teventoDTO
    {
        public int cod_evento { get; set; }
        public string txt_desc { get; set; }
        public DateTime fec_comienzo { get; set; }
        public DateTime fec_fin { get; set; }
        public int cod_tipo_evento { get; set; }
        public int? cod_torneo { get; set; }
        public string usuario_alta { get; set; }
        public DateTime fec_alta { get; set; }
        public DateTime? fec_ultima_modificacion { get; set; }
        public string usuario_ultima_modificacion { get; set; }
        public bool? bool_activo { get; set; }
        public string txt_desc_larga { get; set; }
        public string txt_imagen { get; set; }
        public ttipo_eventoDTO ttipo_evento { get; set; }
        public torneoHome ttorneo { get; set; }
    }
}
