namespace AAPolo.Entity.Entity
{
    public partial class treconocimiento_usuarioDTO
    {
        public int cod_reconocimiento_usuario { get; set; }
        public int cod_reconocimiento { get; set; }
        public int cod_usuario { get; set; }
        public treconocimiento treconocimiento { get; set; }
        public users users { get; set; }
    }
}
