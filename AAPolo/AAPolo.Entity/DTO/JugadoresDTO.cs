﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class JugadoresDTO
    {
        public int cod_usuario { get; set; }
        public string nro_doc { get; set; }
        public string txt_nombre { get; set; }
        public string txt_apellido { get; set; }
        public string txt_imagen { get; set; }
        public ttipo_jugadorDTO ttipo_jugador { get; set; }
        public List<thandicap_usuarioDTO> thandicap_usuario { get; set; }
        public List<thistorial_pagosDTO> thistorial_pagos { get; set; }
        public List<tuser_clubDTO> tuser_club { get; set; }
        public tdomicilioDTO tdomicilio { get; set; }
        public bool bool_activo { get; set; }
        public int? cod_tipo_jugador { get; set; }
        public int cod_genero { get; set; }
        public tgeneroDTO tgenero { get; set; }
        public DateTime? fec_nacimiento { get; set; }
        public bool? bool_acepta_terminos { get; set; }
        public DateTime fec_alta { get; set; }
        public string txt_tarjeta { get; set; }
    }
}
