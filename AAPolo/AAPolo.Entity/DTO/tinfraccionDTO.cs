namespace AAPolo.Entity.DTO
{
    using System;

    public partial class tinfraccionDTO
    {
        public int cod_infraccion { get; set; }
        public string txt_desc { get; set; }
        public DateTime fec_infraccion { get; set; }
        public int cod_tipo_infraccion { get; set; }
        public ttipo_infraccionDTO ttipo_infraccion { get; set; }
    }
}
