namespace AAPolo.Entity.DTO
{
    using System;
    using System.Collections.Generic;

    public partial class tpartidoDTO
    {
        public int cod_partido { get; set; }
        public int cod_torneo { get; set; }
        public int cod_equipo_local { get; set; }
        public int cod_equipo_visitante { get; set; }
        public int nro_resultado_equipo_uno { get; set; }
        public int nro_resultado_equipo_dos { get; set; }
        public DateTime fec_partido { get; set; }
        public tequipoDTO tequipo { get; set; }
        public tequipoDTO tequipo1 { get; set; }
        public int? cod_instancia { get; set; }
        public bool? bool_activo { get; set; }
        public int? cod_referee { get; set; }
        public int? cod_veedor { get; set; }
        public JugadoresDTO users { get; set; }
        public trefereeDTO treferee { get; set; }
        public tinstanciaDTO tinstancia { get; set; }
        public List<tinfraccion_partidoDTO> tinfraccion_partido { get; set; }
    }
}
