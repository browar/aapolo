﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tmiembroDTO
    {
        public int cod_miembro { get; set; }
        public string txt_nombre { get; set; }
        public string txt_apellido { get; set; }
        public string txt_password { get; set; }
        public int? cod_tipo_doc { get; set; }
        public string nro_doc { get; set; }
        public DateTime? fec_alta { get; set; }
        public DateTime? fec_ult_modificacion { get; set; }
        public int? cod_domicilio { get; set; }
        public string usuario_alta { get; set; }
        public DateTime? fec_nacimiento { get; set; }
        public int cod_genero { get; set; }
        public bool? bool_activo { get; set; }
        public int? cod_nacionalidad { get; set; }
        public string txt_imagen { get; set; }
        public int? cod_usuario { get; set; }
        public string txt_tarjeta { get; set; }        
        public string txt_celular { get; set; }        
        public string txt_mail { get; set; }
        public int? cod_club { get; set; }
        public tdomicilioDTO tdomicilio { get; set; }
        public tgeneroDTO tgenero { get; set; }
        public tpaisDTO tpais { get; set; }
        public ttipo_docDTO ttipo_doc { get; set; }
        public tclubDTO tclub { get; set; }
    }
}

