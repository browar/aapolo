﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tmunicipioDTO
    {
        public int cod_municipio { get; set; }
        public string txt_desc { get; set; }
        public int cod_provincia { get; set; }
        public tprovinciaDTO tprovincia { get; set; }
    }
}
