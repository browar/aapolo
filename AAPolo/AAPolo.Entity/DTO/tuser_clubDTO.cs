﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tuser_clubDTO
    {
        public int cod_jugador_club { get; set; }
        public int? cod_club { get; set; }
        public int? cod_usuario { get; set; }
        public bool bool_principal { get; set; }
        public tclubDTO tclub { get; set; }
        public bool? bool_rechazado { get; set; }
        public DateTime? fec_alta { get; set; }
        public DateTime? fec_vigencia { get; set; }
        public bool? bool_republica { get; set; }
    }
}
