namespace AAPolo.Entity.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tnotificacionDTO
    {
        public int cod_notificacion { get; set; }
        public string txt_desc { get; set; }        
        public string txt_asunto { get; set; }
        public int cod_tipo_notificacion { get; set; }
        public bool? bool_destacado { get; set; }        
        public DateTime? fec_enviado { get; set; }        
        public string txt_alta { get; set; }
        public string cod_enviado_por { get; set; }
        public int? nro_handicap_minimo { get; set; }
        public int? nro_handicap_maximo { get; set; }
        public List<tnotificacion_usuarioDTO> tnotificacion_usuario { get; set; }
        public List<tnotificacion_rolDTO> tnotificacion_rol { get; set; }
        public List<tnotificacion_tipo_jugadorDTO> tnotificacion_tipo_jugador { get; set; }
    }
}
