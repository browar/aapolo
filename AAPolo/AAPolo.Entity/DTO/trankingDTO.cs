﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class trankingDTO
    {
        public int cod_ranking { get; set; }
        public string txt_desc { get; set; }
        public int cod_usuario { get; set; }
        public DateTime fec_ranking { get; set; }
    }
}
