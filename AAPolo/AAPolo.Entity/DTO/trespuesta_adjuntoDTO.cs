﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class trespuesta_adjuntoDTO
    {
        public int cod_respuesta_adjunto { get; set; }
        public int cod_respuesta { get; set; }
        public string txt_url_image { get; set; }
    }
}
