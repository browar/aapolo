﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class ttorneo_historicoDTO
    {
        public int cod_torneo_historico { get; set; }
        public int cod_torneo { get; set; }
        public string txt_desc { get; set; }
        public DateTime fec_comienzo { get; set; }
        public int nro_handicap_equipo_minimo { get; set; }
        public int nro_handicap_equipo_maximo { get; set; }
        public int nro_handicap_jugador_minimo { get; set; }
        public int nro_handicap_jugador_maximo { get; set; }
        public int cod_categoria_torneo { get; set; }
        public DateTime fec_alta { get; set; }
        public string usuario_alta { get; set; }
        public tcategoria_torneoDTO tcategoria_torneo { get; set; }
    }
}
