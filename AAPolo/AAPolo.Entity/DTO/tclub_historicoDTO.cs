namespace AAPolo.Entity.DTO
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class tclub_historicoDTO
    {
        public int cod_club_historico { get; set; }
        public int? cod_club { get; set; }
        public int? cod_categoria_club { get; set; }
        public string txt_desc { get; set; }
        public string cod_club_desc { get; set; }
        public int? nro_canchas { get; set; }
        public int? cod_estado { get; set; }
        public string txt_presidente { get; set; }
        public string txt_vicepresidente { get; set; }
        public string txt_vocal1 { get; set; }
        public string txt_vocal2 { get; set; }
        public string txt_vocal3 { get; set; }
        public string txt_tesorero { get; set; }
        public int? cod_domicilio { get; set; }
        public int? cod_contacto { get; set; }
        public DateTime? fec_alta { get; set; }
        public string usuario_alta { get; set; }
        public DateTime? fec_alta_historico { get; set; }
        public string usuario_alta_historico { get; set; }
        public virtual tcategoria_clubDTO tcategoria_club { get; set; }
        public virtual tcontactoDTO tcontacto { get; set; }
        public virtual tdomicilioDTO tdomicilio { get; set; }
    }
}
