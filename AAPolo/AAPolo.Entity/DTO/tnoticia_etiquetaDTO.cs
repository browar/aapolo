﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tnoticia_etiquetaDTO
    {
        public int cod_noticia_etiqueta { get; set; }
        public int? cod_noticia { get; set; }
        public int? cod_etiqueta { get; set; }
        public tetiquetaDTO tetiqueta { get; set; }
    }
}
