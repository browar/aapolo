namespace AAPolo.Entity.DTO
{
    using System;

    public partial class treconocimientoDTO
    {
        public int cod_reconocimiento { get; set; }
        public int cod_torneo { get; set; }
        public int cod_tipo_reconocimiento { get; set; }
        public DateTime fec_reconocimiento { get; set; }
        public ttipo_reconocimientoDTO ttipo_reconocimiento { get; set; }
        public ttorneoDTO ttorneo { get; set; }
    }
}
