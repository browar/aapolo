﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tcategoria_clubDTO
    {
        public int cod_categoria_club { get; set; }
        public string txt_desc { get; set; }
        public string txt_desc_corta { get; set; }
        public decimal nro_importe { get; set; }
    }
}
