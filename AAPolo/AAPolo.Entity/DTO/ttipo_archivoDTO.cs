﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class ttipo_archivoDTO
    {
        public int cod_tipo_archivo { get; set; }
        public string txt_desc { get; set; }
    }
}
