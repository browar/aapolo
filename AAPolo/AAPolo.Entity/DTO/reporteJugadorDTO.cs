﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class reporteJugadorDTO
    {
        public string JugadorID { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string TipoDoc { get; set; }
        public string Documento { get; set; }
        public string TipoJugador { get; set; }
        public string FecNac { get; set; }
        public string Nacionalidad { get; set; }
        public string Genero { get; set; }
        public string Activo { get; set; }
        public string Mail { get; set; }
        public string Telefono { get; set; }
        public string Club { get; set; }
        public string TipoHandicapPrincipal { get; set; }
        public string CategoriaHandicapPrincipal { get; set; }
        public string HandicapPrincipal { get; set; }
        public string TipoHandicapSecundario { get; set; }
        public string CategoriaHandicapSecundario { get; set; }
        public string HandicapSecundario { get; set; }
        public string Domicilio { get; set; }
        public string TipoDomicilio { get; set; }
        public string Piso { get; set; }
        public string Dpto { get; set; }
        public string CodPostal { get; set; }
        public string Veedor { get; set; }
        public string AdminClub { get; set; }
    }
}
