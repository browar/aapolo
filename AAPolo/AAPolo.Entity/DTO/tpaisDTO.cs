﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tpaisDTO
    {
        public int cod_pais { get; set; }
        public string txt_desc { get; set; }
    }
}
