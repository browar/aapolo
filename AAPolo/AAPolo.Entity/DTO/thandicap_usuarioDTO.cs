﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class thandicap_usuarioDTO
    {
        public int cod_handicap_usuario { get; set; }
        public int cod_handicap { get; set; }
        public int cod_usuario { get; set; }
        public bool? bool_principal { get; set; }
        public int? nro_handicap { get; set; }
        public DateTime? fec_desde { get; set; }
        public DateTime? fec_hasta { get; set; }
        public thandicapDTO thandicap { get; set; }
        public List<thandicap_usuario_historicoDTO> thandicap_usuario_historico { get; set; }

    }
}
