﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class userListDTO
    {
        public int cod_usuario { get; set; }
        public string txt_nombre { get; set; }
        public string txt_apellido { get; set; }
        public int? cod_tipo_doc { get; set; }
        public int? cod_tipo_jugador { get; set; }
        public bool bool_activo { get; set; }
        public int? nro_handicap { get; set; }
        public int? cod_nacionalidad { get; set; }
        public string text_tipo_jugador { get; set; }
        public string text_tipo_doc { get; set; }
        public string text_tipo_handicap { get; set; }
        public string text_categoria_handicap { get; set; }
        public string txt_club { get; set; }
        public int? cod_club { get; set; }
        public string txt_nacionalidad { get; set; }
        public int? usersCount { get; set; }
        public string text_genero { get; set; }
        public DateTime? fec_nacimiento { get; set; }
        public string txt_imagen { get; set; }
    }
}
