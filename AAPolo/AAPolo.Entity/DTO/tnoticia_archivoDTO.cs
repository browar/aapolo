﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tnoticia_archivoDTO
    {
        public int cod_noticia_archivo { get; set; }
        public int? cod_noticia { get; set; }
        public int? cod_tipo_archivo { get; set; }
        public string url_archivo { get; set; }
        public string txt_desc { get; set; }
        public int? cod_usuario_alta { get; set; }
        public DateTime? fec_alta { get; set; }
        public int? orden { get; set; }
        public int? sn_activo { get; set; }
        //public virtual ttipo_archivoDTO ttipo_archivo { get; set; }
        //public virtual usersDTO users { get; set; }
        //public virtual tnoticiaDTO tnoticia { get; set; }
        
    }
}
