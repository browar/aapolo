namespace AAPolo.Entity.DTO
{
    public partial class tmotivo_bajaDTO
    {
        public int cod_motivo_baja { get; set; }
        public string txt_desc { get; set; }
    }
}
