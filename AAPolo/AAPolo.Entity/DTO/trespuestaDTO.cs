﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class trespuestaDTO
    {
        public int cod_respuesta { get; set; }
        public int cod_notificacion { get; set; }
        public int? cod_usuario_envia { get; set; }
        public int? cod_usuario_recibe { get; set; }
        public string txt_desc { get; set; }        
        public DateTime? fec_enviado { get; set; }
        public List<trespuesta_adjuntoDTO> trespuesta_adjunto { get; set; }
    }
}
