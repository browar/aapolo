﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class tcontacto_usuarioDTO
    {
        public int cod_contacto_usuario { get; set; }

        public int cod_contacto { get; set; }

        public int cod_usuario { get; set; }

        public tcontactoDTO tcontacto { get; set; }
    }
}
