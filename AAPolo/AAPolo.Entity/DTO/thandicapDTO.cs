﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entity.DTO
{
    public class thandicapDTO
    {
        public int cod_handicap { get; set; }
        public string txt_desc { get; set; }
        public string txt_vigencia { get; set; }
        public string txt_nacionalidad { get; set; }
        public string txt_edad { get; set; }
        public int? cod_categoria_handicap { get; set; }
        public tcategoria_handicapDTO tcategoria_handicap { get; set; }
        public List<ttransicionDTO> ttransicion { get; set; }
        public List<ttransicionDTO> ttransicion1 { get; set; }
        public decimal nro_importe { get; set; }
    }
}
