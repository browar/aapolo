﻿using System;

namespace AAPolo.Entity.DTO
{
    public class tdomicilioDTO
    {
        public int cod_domicilio { get; set; }
        public int? cod_tipo_domicilio { get; set; }
        public string txt_domicilio { get; set; }
        public int cod_pais { get; set; }
        public int? cod_provincia { get; set; }
        public int? cod_municipio { get; set; }
        public int nro_domicilio { get; set; }
        public int? nro_piso { get; set; }
        public string txt_departamento { get; set; }
        public string nro_cod_postal { get; set; }
        public DateTime? fec_alta { get; set; }
        public DateTime? fec_ult_modificacion { get; set; }
        public string cod_postal { get; set; }
        public ttipo_domicilioDTO ttipo_domicilio { get; set; }
        public tmunicipioDTO tmunicipio { get; set; }
        public tprovinciaDTO tprovincia { get; set; }
        public tpaisDTO tpais { get; set; }
    }
}
