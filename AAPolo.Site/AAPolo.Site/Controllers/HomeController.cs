﻿using AAPolo.Site.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AAPolo.Site.Controllers
{
    public class HomeController : Controller
    {
        string url = ConfigurationManager.AppSettings["UrlApi"].ToString();

        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetUser()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Login?name=" + HttpContext.User.Identity.Name);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var result = new { name = HttpContext.User.Identity.Name };
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetJugadorAutenticado()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/LoginJugador?nro_doc=" + HttpContext.User.Identity.Name);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var result = new { name = HttpContext.User.Identity.Name };
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Login(users_admin loginDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Login/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var result = JsonConvert.SerializeObject(loginDTO);
            HttpResponseMessage response = client.PostAsJsonAsync("", result).Result;
            var content = await response.Content.ReadAsStringAsync();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                FormsAuthentication.SetAuthCookie(loginDTO.txt_usuario, true);
            }

            return Redirect("/PanelControl/#/Jugadores");
        }

        public async Task<JsonResult> LoginJugador(users loginDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/LoginJugador/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var result = JsonConvert.SerializeObject(loginDTO);
            HttpResponseMessage response = client.PostAsJsonAsync("", result).Result;
            var content = await response.Content.ReadAsStringAsync();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                FormsAuthentication.SetAuthCookie(loginDTO.nro_doc.ToString(), true);
            }

            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> LoginMiembro(users loginDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Miembro?nro_doc=" + loginDTO.nro_doc + "&password=" + loginDTO.txt_password);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var result = JsonConvert.SerializeObject(loginDTO);
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                FormsAuthentication.SetAuthCookie(loginDTO.nro_doc.ToString(), true);
            }

            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        public async Task<JsonResult> ActualizarContrasena(users loginDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/LoginJugador/CambioPassword");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var result = JsonConvert.SerializeObject(loginDTO);
            HttpResponseMessage response = client.PostAsJsonAsync("", result).Result;
            var content = await response.Content.ReadAsStringAsync();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                FormsAuthentication.SetAuthCookie(loginDTO.nro_doc.ToString(), true);
            }

            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AceptarTerminos(int cod_usuario, string data)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/LoginJugador/ActualizarTerminos?id=" + cod_usuario);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PutAsJsonAsync("", data).Result;
            var content = await response.Content.ReadAsStringAsync();

            return Json(content, JsonRequestBehavior.AllowGet);
        }
    }
}