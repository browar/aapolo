﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Site.Controllers
{
    public class MiembroController : Controller
    {
        string url = ConfigurationManager.AppSettings["UrlApi"].ToString();

        public ActionResult Index()
        {
            return View();
        }

        // GET: Miembro
        public async Task<ActionResult> GetMiembroLogueado(int cod_usuario)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Miembro?idMiembro=" + cod_usuario);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        // GET: Miembro
        public async Task<ActionResult> GetMiembroByCodUsuario(int cod_usuario)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Miembro/GetByCodUsuario?cod_usuario=" + cod_usuario);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetTarjeta(string tarjeta)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Miembro?tarjeta=" + tarjeta);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetTipos()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Miembro");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GuardarMiembro(string miembroDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Miembro/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(miembroDTO);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = client.PostAsJsonAsync("", miembroDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ModificarMiembro(string id, string miembroDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Miembro?id=" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(miembroDTO);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = client.PutAsJsonAsync("", miembroDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }
    }
}