﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Site.Controllers
{
    public class AsociacionController : Controller
    {
        // GET: Asociacion
        public ActionResult Vision()
        {
            return View();
        }

        public ActionResult Consejo()
        {
            return View();
        }

        public ActionResult Historia()
        {
            return View();
        }

        public ActionResult Sedes()
        {
            return View();
        }

        public ActionResult Subcomision()
        {
            return View();
        }

        public ActionResult Staff()
        {
            return View();
        }

        public ActionResult Estatutos()
        {
            return View();
        }
        public ActionResult RSE()
        {
            return View();
        }
    }
}