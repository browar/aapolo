﻿using AAPolo.Site.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Site.Controllers
{
    public class MercadoPagoController : Controller
    {
        private readonly string apiClientId = ConfigurationManager.AppSettings["MercadoPagoClientId"].ToString();
        private readonly string apiClientSecret = ConfigurationManager.AppSettings["MercadoPagoClientSecret"].ToString();
        private readonly string mercadoPagoMoneda = ConfigurationManager.AppSettings["MercadoPagoMoneda"].ToString();
        private readonly string urlPasarelaDePago = ConfigurationManager.AppSettings["UrlPasarelaDePago"].ToString();
        private readonly string urlAppMP = ConfigurationManager.AppSettings["UrlAppMP"].ToString();
        string url = ConfigurationManager.AppSettings["UrlApi"].ToString();

        // GET: MercadoPagoApi
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> Pagar(DatosMercadoPago datosMercadoPago)
        {
            datosMercadoPago.ID_Credencial = apiClientId;
            datosMercadoPago.Clave_Credencial = apiClientSecret;
            datosMercadoPago.UrlAPP = urlAppMP;
            datosMercadoPago.Moneda = mercadoPagoMoneda;
            datosMercadoPago.Referencia_Externa = DateTime.Now.ToString("ddMMyyyy-HHmm");
            var result = JsonConvert.SerializeObject(datosMercadoPago);
            var Jsonresult = urlPasarelaDePago + HttpUtility.UrlEncode(result);

            foreach (var item in datosMercadoPago.lstItem)
            {
                var pago = new thistorial_pagos();
                pago.fec_alta = DateTime.Now;
                var fec_vencimiento = new DateTime();
                if (item.IsAnual)
                {
                    fec_vencimiento = new DateTime(DateTime.Now.Year, 12, 31);
                }
                else
                {
                    if (DateTime.Now.Month < 12)
                    {
                        fec_vencimiento = new DateTime(DateTime.Now.Year, (DateTime.Now.Month + 1), DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month + 1));
                    }
                    else
                    {
                        fec_vencimiento = new DateTime(DateTime.Now.Year, (DateTime.Now.Month), DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));

                    }
                }

                pago.fec_vencimiento = fec_vencimiento;
                pago.nro_resultado_pago = 2;
                pago.cod_tipo_pago = item.cod_tipo_pago;
                if (datosMercadoPago.cod_usuario_alta.HasValue)
                    pago.cod_usuario_alta = datosMercadoPago.cod_usuario_alta.Value;
                else
                    pago.cod_club_alta = datosMercadoPago.cod_club_alta.Value;

                pago.nro_importe = Convert.ToDecimal(item.unit_price.Replace(".", ","));
                pago.cod_usuario = item.cod_usuario;
                if (item.cod_usuario == 0)
                {
                    var resultado = await GetUser();
                    dynamic data = JObject.Parse(resultado.Data.ToString());

                    // Index the Array and select your CompanyID
                    pago.cod_usuario = data.cod_usuario;

                }
                pago.txt_desc = item.title;
                if (datosMercadoPago.cod_torneo.HasValue)
                    pago.cod_torneo = datosMercadoPago.cod_torneo.Value;

                pago.txt_url_comprobante = "" + item.cod_usuario + "-" + DateTime.Now.ToString("ddMMyyyyHHmm");
                string pagoHandicap = JsonConvert.SerializeObject(pago);
                if (datosMercadoPago.cod_historial_pagos.HasValue)
                    pago.cod_historial_pagos = datosMercadoPago.cod_historial_pagos.Value;

                if (pago.cod_historial_pagos == 0)
                {
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(url + "/Pago/");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var buffer = System.Text.Encoding.UTF8.GetBytes(pagoHandicap);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage response = client.PostAsJsonAsync("", pagoHandicap).Result;
                    var content = await response.Content.ReadAsStringAsync();
                }
            }

            return Json(Jsonresult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> CambiarCategoria(string jugadorId, string jugadoresDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/PromocionHandicap/" + jugadorId);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(jugadoresDTO);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = client.PutAsJsonAsync("", jugadoresDTO).Result;
            var content = await response.Content.ReadAsStringAsync();

            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ResultadoDePago(int? cod_usuario_alta, int resultado, string external_reference, int? cod_club_alta, string collection_id)
        {
            var pago = new thistorial_pagos();
            pago.nro_resultado_pago = resultado;
            pago.cod_tipo_pago = 1;
            pago.txt_url_comprobante = external_reference;
            if (cod_usuario_alta.HasValue)
            {
                pago.cod_usuario_alta = cod_usuario_alta.Value;
            }
            else
            {
                pago.cod_club_alta = cod_club_alta.Value;
            }
            pago.cod_mercado_pago = collection_id;
            string pagoRealizado = JsonConvert.SerializeObject(pago);
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Pago/" + cod_usuario_alta);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(pagoRealizado);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = client.PutAsJsonAsync("", pagoRealizado).Result;
            var content = await response.Content.ReadAsStringAsync();

            return Redirect("/PerfilJugador/#/" + pago.cod_usuario_alta);

        }

        public async Task ReintentoPagoEquipo(int? cod_usuario_alta, int? cod_equipo, int? cod_torneo, int resultado, string external_reference, string collection_id)
        {
            var pago = new thistorial_pagos();
            pago.nro_resultado_pago = resultado;
            pago.cod_tipo_pago = 3;
            pago.txt_url_comprobante = external_reference;

            pago.cod_usuario_alta = cod_usuario_alta.Value;

            if (cod_torneo.HasValue)
                pago.cod_torneo = cod_torneo.Value;

            pago.cod_mercado_pago = collection_id;
            string pagoRealizado = JsonConvert.SerializeObject(pago);
            HttpClient clientPago = new HttpClient();
            clientPago.BaseAddress = new Uri(url + "/Pago/" + cod_usuario_alta);
            clientPago.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(pagoRealizado);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = await clientPago.PutAsJsonAsync("", pagoRealizado);

            if (resultado == 1)
            {
                string empty = JsonConvert.SerializeObject("");
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url + "/TorneoEquipo/" + cod_equipo);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var bufferEquipo = System.Text.Encoding.UTF8.GetBytes(empty);
                var byteContentEquipo = new ByteArrayContent(bufferEquipo);
                byteContentEquipo.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage responseEquipo = await client.PutAsJsonAsync("", empty);

            }

        }

        public async Task<ActionResult> ResultadoDePagoAdmin(int? cod_usuario_alta, int resultado, string external_reference, int? cod_club_alta, string collection_id)
        {
            var pago = new thistorial_pagos();
            pago.nro_resultado_pago = resultado;
            if (cod_club_alta.HasValue)
                pago.cod_tipo_pago = 2;
            else
                pago.cod_tipo_pago = 1;

            pago.txt_url_comprobante = external_reference;
            if (cod_usuario_alta.HasValue)
            {
                pago.cod_usuario_alta = cod_usuario_alta.Value;
            }
            else
            {
                pago.cod_club_alta = cod_club_alta.Value;
            }

            pago.cod_mercado_pago = collection_id;
            string pagoRealizado = JsonConvert.SerializeObject(pago);
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Pago/" + cod_usuario_alta);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(pagoRealizado);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = client.PutAsJsonAsync("", pagoRealizado).Result;
            var content = await response.Content.ReadAsStringAsync();

            if (cod_club_alta.HasValue)
                return Redirect("/PanelControl/#/Clubes/reporte");
            else
            {
                return Redirect("/PanelControl/#/Jugadores/reporte");
            }

        }


        public async Task<JsonResult> ValidarInscripcion(string equipo)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/TorneoEquipo/ValidarEquipo");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync("", equipo).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }


        public async Task<JsonResult> PagarComprobante(string usuario, DatosMercadoPago datosMercadoPago, string pagoComprobante)
        {
            try
            {
                var path = GuardarImagen("" + usuario);

                var pago = new thistorial_pagos();
                pago.nro_resultado_pago = 1;
                if (datosMercadoPago.cod_club_alta.HasValue)
                    pago.cod_tipo_pago = 2;
                else
                    pago.cod_tipo_pago = 1;
                pago.txt_url_comprobante = path;
                pago.nro_importe = Convert.ToDecimal(datosMercadoPago.lstItem[0].unit_price.Replace(".", ","));
                if (datosMercadoPago.cod_historial_pagos.HasValue)
                    pago.cod_historial_pagos = datosMercadoPago.cod_historial_pagos.Value;
                if (datosMercadoPago.cod_usuario_alta.HasValue)
                {
                    pago.cod_usuario_alta = datosMercadoPago.cod_usuario_alta.Value;
                    pago.cod_usuario = datosMercadoPago.cod_usuario_alta.Value;
                }
                else
                {
                    if (pago.cod_usuario == 0)
                    {
                        var resultado = await GetUser();
                        dynamic data = JObject.Parse(resultado.Data.ToString());

                        // Index the Array and select your CompanyID
                        pago.cod_usuario = data.cod_usuario;

                    }
                    pago.cod_club_alta = datosMercadoPago.cod_club_alta.Value;
                }

                pago.txt_desc = datosMercadoPago.lstItem[0].title;
                var fec_vencimiento = new DateTime();
                fec_vencimiento = DateTime.Now.AddYears(1);

                pago.fec_vencimiento = fec_vencimiento;
                string pagoRealizado = JsonConvert.SerializeObject(pago);
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url + "/Pago/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.PostAsJsonAsync("", pagoRealizado).Result;
                var content = await response.Content.ReadAsStringAsync();
                return Json(content, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GuardarImagen(string folder)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                System.IO.Directory.CreateDirectory(HttpContext.Server.MapPath("..\\Uploads\\" + folder).ToString());
                string fname = HttpContext.Server.MapPath("..\\Uploads\\" + folder + "\\"
                    + DateTime.Now.ToString("ddMMyyyy-HHmm") + " " + file.FileName);
                file.SaveAs(fname);
                var route = "..\\Uploads\\" + folder + "\\"
                    + DateTime.Now.ToString("ddMMyyyy-HHmm") + " " + file.FileName;
                fname = route.Replace("\\", "/");
                string quoted = System.Web.Helpers.Json.Encode(fname);
                return quoted.Substring(1, quoted.Length - 2);
            }

            return "No tiene";
        }

        public async Task<JsonResult> GetUser()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Login?name=" + HttpContext.User.Identity.Name);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var result = new { name = HttpContext.User.Identity.Name };
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }
    }

}