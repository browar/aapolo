﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace AAPolo.Site.Controllers
{
    public class LoginController : Controller
    {
        string url = ConfigurationManager.AppSettings["UrlApi"].ToString();

        // GET: Login
        public ActionResult Index()
        {
            bool val1 = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            if (val1)
                return Redirect("/PanelControl/#/Jugadores");
            else
                return View();
        }

        public async Task<ActionResult> OlvideMiContrasena(string dni)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Login/ReestablecerPassword?dni=" + dni);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var result = new { name = HttpContext.User.Identity.Name };
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }


        //        desde el local storage de js
        //id_register = localStorage.getItem("id_register");
        //platform = localStorage.getItem("platform");


        //public void SendPushNotificationAUsuarioEspecifico()
        //{
        //    //var user = (id == null ? 726 : (int)id);
        //    var miNotificacion = new object();
        //    miNotificacion.UserMobileID = @"djNA-8fpJEY:APA91bHUr84BOj9RB_Q4Z6SCl0MWFyA0cNZZKdaBsuRtroXy6PLEH9CARgetZo3FqFia2KhouLGfHz4yhfXs1YB89GwOTctAmPJdCKHH2ndgKcVCjO_UkNKVXSIvn9tpun4HvemIRjtU";
        //    miNotificacion.UserPlatform = @"IOS";
        //    miNotificacion.Mensaje = @"Víctor, Claudia, gracias por participar de esta reunión!";
        //    miNotificacion.Asunto = @"Movistar Elige Aprender";
        //    //var datosNotificacionUsuario = new ObjetivosComercialesManagementService().getComunicadosObjetivosComercialesDelUsuario(3);
        //    //var datosNotificacionUsuario = new ObjetivosComercialesManagementService().getComunicadosObjetivosComercialesDelUsuario(user);
        //    //foreach (var miNotificacion in datosNotificacionUsuario)
        //    //{
        //    try
        //    {
        //        string applicationID = "AAAA8NM1jf8:APA91bGOBA_EH2IRPsGcN5XBH9cw80MgJbxVeaYUyd9_qk8fh9cVp67UOm4qHTata94g_CubM7PqG8qRY9W4ynTUG1aIgMbedrhQ6fbZk_T045bPo-38H9A8tAFx655SW9UOwxTG82dj";
        //        //string senderId = "30............8";
        //        string deviceId = miNotificacion.UserMobileID;//"egZo-YthS8w:APA91bGm1-UONlkn_LQInnYx9A5fySf3BFv5t2gmgekKfkUII5kODJtXJ_93eI_OYGIhXizVGDDyFDvRqNBD7eI9nTAUiqFPENQh-NRXypdvDmB5hJlTivJo3Xtj9dg1sKr6FJRAZ7_6";
        //        WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        //        tRequest.Method = "post";
        //        tRequest.ContentType = "application/json";
        //        object data;
        //        if (miNotificacion.UserPlatform.ToUpper() == "ANDROID")
        //        {
        //            data = new
        //            {
        //                to = deviceId,
        //                data = new
        //                {
        //                    body = miNotificacion.Mensaje,// "Testeo Push",
        //                    title = miNotificacion.Asunto,// "Notificacion de ejemplo enviada desde la pc",
        //                    style = "inbox",
        //                    message = miNotificacion.Mensaje,
        //                    sound = "default",
        //                    badge = 1,

        //                }
        //            };
        //        }
        //        else
        //        {
        //            data = new
        //            {
        //                to = deviceId,
        //                notification = new
        //                {
        //                    body = miNotificacion.Mensaje,// "Testeo Push",
        //                    title = miNotificacion.Asunto,// "Notificacion de ejemplo enviada desde la pc",
        //                    style = "inbox",
        //                    message = miNotificacion.Mensaje,
        //                    sound = "default",
        //                    badge = 1,

        //                }
        //            };
        //        }
        //        var serializer = new JavaScriptSerializer();
        //        var json = serializer.Serialize(data);
        //        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
        //        tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
        //        //tRequest.Headers.Add(string.Format("Content-Type: application/json"));
        //        //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
        //        tRequest.ContentLength = byteArray.Length;

        //        using (Stream dataStream = tRequest.GetRequestStream())
        //        {
        //            dataStream.Write(byteArray, 0, byteArray.Length);
        //            using (WebResponse tResponse = tRequest.GetResponse())
        //            {
        //                using (Stream dataStreamResponse = tResponse.GetResponseStream())
        //                {
        //                    using (StreamReader tReader = new StreamReader(dataStreamResponse))
        //                    {
        //                        String sResponseFromServer = tReader.ReadToEnd();
        //                        string str = sResponseFromServer;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string str = ex.Message;
        //    }
        //    //}
        //}
    }
}