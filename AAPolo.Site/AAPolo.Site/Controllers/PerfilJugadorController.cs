﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace AAPolo.Site.Controllers
{
    public class PerfilJugadorController : Controller
    {
        string url = ConfigurationManager.AppSettings["UrlApi"].ToString();

        // GET: PerfilJugador
        public ActionResult Index()
        {
            bool val1 = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            if (val1)
                return View();
            else
                return Redirect("/Home/Index");
        }

        public async Task<JsonResult> GetTorneos()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Torneo/");
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> BuscarJugadores(int page, string search)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Usuario?page=" + page + "&search=" + search);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetUsersPase(int page, string search)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Usuario/GetUsersPase?page=" + page + "&search=" + search);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> BuscarJugadoresSinPago(int page, string search)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Pago?page=" + page + "&search=" + search);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetHistorialPagos(int idUser)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Pago/" + idUser);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetHandicaps()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Handicap/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> FinalizarPreInscripcion(string equipoDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/TorneoEquipo/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync("", equipoDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetEquipo(int idJugador)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/TorneoEquipo?cod_usuario=" + idJugador);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetUsuarioSinPago(int idJugador)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Usuario/GetUsuarioSinPago?idJugador=" + idJugador);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetRespuestas(int idJugador)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Respuesta?idRespuesta=" + idJugador);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> EnviarRespuesta(string respuesta)
        {
            HttpClient client = new HttpClient();
            var path = GuardarAdjunto();
            respuesta = respuesta.Replace("reemplazarEsteAdjunto", path);
            client.BaseAddress = new Uri(url + "/Respuesta/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync("", respuesta).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public string GuardarAdjunto()
        {
            if (Request.Files.Count > 0)
            {
                return GuardarDocumento();
            }
            return null;
        }

        public string GuardarDocumento()
        {
            var file = Request.Files[0];
            System.IO.Directory.CreateDirectory(HttpContext.Server.MapPath("..\\Uploads\\Notificaciones\\").ToString());
            var route = "..\\Uploads\\Notificaciones\\" + DateTime.Now.ToString("ddMMyyyy-HHmm") + " " + file.FileName;
            string fname = HttpContext.Server.MapPath(route);
            file.SaveAs(fname);

            route = route.Replace("\\", "/");
            string quoted = System.Web.Helpers.Json.Encode(route);
            return quoted.Substring(1, quoted.Length - 2);
        }

    }
}