﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Site.Controllers
{
    public class PerfilClubController : Controller
    {
        string url = ConfigurationManager.AppSettings["UrlApi"].ToString();

        // GET: PerfilClub
        public ActionResult Index()
        {
            bool val1 = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            if (val1)
                return View();
            else
                return Redirect("/Home/Index");
        }

        public async Task<JsonResult> GetSocios(int? idClub)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/UsuarioClub?clubId=" + idClub);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AceptarORechazar(int? idUsuario, bool opcion)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/UsuarioClub/AceptarRechazar?idUsuario=" + idUsuario + "&opcion=" + opcion);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetHistorialPagosClub(int idClub)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Pago?cod_club=" + idClub);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }
    }
}