﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Site.Controllers
{
    public class PerfilPublicoController : Controller
    {
        string url = ConfigurationManager.AppSettings["UrlApi"].ToString();
        // GET: PerfilPublico
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProximosPartidos()
        {
            return View("~/Views/PerfilPublico/Torneo/ProximosPartidos.cshtml");
        }

        public async Task<JsonResult> GetJugadoresDestacados(int? idClub, int? page)
        {
            HttpClient client = new HttpClient();
            if (idClub.HasValue)
                client.BaseAddress = new Uri(url + "/Usuario?orden=thandicap&idClub=" + idClub.Value);
            else
                client.BaseAddress = new Uri(url + "/Usuario?orden=thandicap&idClub=0&page=" + page);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetEventos()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Evento/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }
    }
}