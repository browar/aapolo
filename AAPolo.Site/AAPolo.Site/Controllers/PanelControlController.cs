﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Site.Controllers
{
    public class PanelControlController : Controller
    {
        string url = ConfigurationManager.AppSettings["UrlApi"].ToString();

        // GET: Jugador
        public ActionResult Index()
        {
            bool val1 = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            if (val1)
                return View();
            else
                return Redirect("/Login/#/");
        }

        #region Jugadores

        public async Task<JsonResult> GetJugador(int? idJugador)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Usuario/" + idJugador);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetJugadoresSinPagina()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Usuario/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetJugadores(int page)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Usuario/GetBySP?page=" + page);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetJugadoresFiltrados(int page, string nombre, int cod_tipo_jugador, int cod_club, int nro_hand)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + string.Format("/Usuario?page={0}&nombre={1}&cod_tipo_jugador={2}&cod_club={3}&nro_hand={4}", page, nombre, cod_tipo_jugador, cod_club, nro_hand));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetJugadoresFiltradosPublico(int page, string nombre, int cod_tipo_jugador, int cod_club, int nro_hand)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + string.Format("/Usuario/GetPublicos?page={0}&nombre={1}&cod_tipo_jugador={2}&cod_club={3}&nro_hand={4}", page, nombre, cod_tipo_jugador, cod_club, nro_hand));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;

            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GuardarJugador(string jugadorDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Usuario/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync("", jugadorDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteJugador(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Usuario/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.DeleteAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> EditarJugador(int id, string jugadorDTO)
        {
            if (jugadorDTO != null && id != 0)
            {
                var path = GuardarImagen("Jugador");
                jugadorDTO = jugadorDTO.Replace("reemplazarEste", path);
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url + "/Usuario/" + id);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var buffer = System.Text.Encoding.UTF8.GetBytes(jugadorDTO);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage response = client.PutAsJsonAsync("", jugadorDTO).Result;
                var content = await response.Content.ReadAsStringAsync();
                return Json(content, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Noticias 


        public async Task<JsonResult> GetNoticias()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetNoticiasBySP()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/GetNoticiasBySP");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetNoticia(int? id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetNoticiaRelacionadas(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia?cod_noticia=" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetNoticiasActivas()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/ObtenerActivas");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync(client.BaseAddress, "").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetNoticiasDestacadas()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/ObtenerDestacadas");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync(client.BaseAddress, "").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public async Task<JsonResult> GuardarNoticia(string noticiaDTO)
        {
            //Ver tema validaciones.. cuantos archivos vamos a tener y como los manejamos
            var path = "";
            if (Request.Files.Count > 0)
            {
                path = GuardarArchivo("Noticia", 0, "ImagenPrincipal");
                noticiaDTO = noticiaDTO.Replace("reemplazarEstePrincipal", path);
            }
            if (Request.Files.Count > 1)
            {
                path = GuardarArchivo("Noticia", 1, "Slideshow");
                noticiaDTO = noticiaDTO.Replace("reemplazarEsteSlideshow", path);
            }
            if (Request.Files.Count > 2)
            {
                path = GuardarArchivo("Noticia", 2, "Video");
                noticiaDTO = noticiaDTO.Replace("reemplazarEsteVideo", path);
            }

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync("", noticiaDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public void GuardarFooter()
        {
            if (Request.Files.Count > 0)
            {
               GuardarDocumento();
            }          
        }

        [ValidateInput(false)]
        public async Task<JsonResult> EditarNoticiaUpload(int id, string noticiaDTO, int[] idsEditedFiles)
        {
            var path = ""; string nombreImagen = ""; string nombreReemplazar = "";
            int length = idsEditedFiles == null ? 0 : idsEditedFiles.Length;
            for (int i = 0; i < length; i++)
            {
                if (Request.Files.Count > 0)
                {
                    switch (idsEditedFiles[i])
                    {
                        case 0:
                            nombreImagen = "ImagenPrincipal"; nombreReemplazar = "reemplazarEstePrincipal";
                            break;
                        case 1:
                            nombreImagen = "Slideshow"; nombreReemplazar = "reemplazarEsteSlideshow";
                            break;
                        case 2:
                            nombreImagen = "reemplazarEsteVideo"; nombreReemplazar = "reemplazarEsteVideo";
                            break;
                        default:
                            break;

                    }
                    path = GuardarArchivo("Noticia", i, nombreImagen);
                    noticiaDTO = noticiaDTO.Replace(nombreReemplazar, path);
                }
            }
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(noticiaDTO);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = client.PutAsJsonAsync("", noticiaDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public async Task<JsonResult> EditarNoticia(int id, string noticiaDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(noticiaDTO);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = client.PutAsJsonAsync("", noticiaDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public async Task<JsonResult> EditarEstadoNoticia(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/EditarEstadoNoticia?id=" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync(client.BaseAddress, id).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public async Task<JsonResult> EditarEstadoNoticiaDestacada(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/EditarNoticiaDestacada?id=" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync(client.BaseAddress, id).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetEtiquetasNoticia()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/GetEtiquetas/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetCategoriasNoticia()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Noticia/GetCategoriasNoticias/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Otros

        public async Task<JsonResult> GetJugadorTipos()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/JugadorTipos/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetClubTipos()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/ClubTipos/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetTorneoTipos()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/TorneoTipos/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetMunicipios(int cod_provincia)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Municipio/" + cod_provincia);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }


        public string GuardarImagen(string folder)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                System.IO.Directory.CreateDirectory(HttpContext.Server.MapPath("..\\Uploads\\" + folder).ToString());
                string fname = HttpContext.Server.MapPath("..\\Uploads\\" + folder + "\\"
                    + DateTime.Now.ToString("ddMMyyyy-HHmm") + " " + file.FileName);
                file.SaveAs(fname);
                var route = "..\\Uploads\\" + folder + "\\"
                    + DateTime.Now.ToString("ddMMyyyy-HHmm") + " " + file.FileName;
                fname = route.Replace("\\", "/");
                string quoted = System.Web.Helpers.Json.Encode(fname);
                return quoted.Substring(1, quoted.Length - 2);
            }

            return "No tiene";
        }
        public string GuardarArchivo(string folder, int indice, string extra)
        {
            var file = Request.Files[indice];
            System.IO.Directory.CreateDirectory(HttpContext.Server.MapPath("..\\Uploads\\" + folder).ToString());
            var route = "..\\Uploads\\" + folder + "\\" + DateTime.Now.ToString("ddMMyyyy-HHmm") + " " + extra + file.FileName;
            string fname = HttpContext.Server.MapPath(route);
            file.SaveAs(fname);

            route = route.Replace("\\", "/");
            string quoted = System.Web.Helpers.Json.Encode(route);
            return quoted.Substring(1, quoted.Length - 2);
        }

        public string GuardarDocumento()
        {
            var file = Request.Files[0];
            System.IO.Directory.CreateDirectory(HttpContext.Server.MapPath("..\\doc\\").ToString());
            var route = "..\\doc\\" + file.FileName;
            string fname = HttpContext.Server.MapPath(route);
            file.SaveAs(fname);

            route = route.Replace("\\", "/");
            string quoted = System.Web.Helpers.Json.Encode(route);
            return quoted.Substring(1, quoted.Length - 2);
        }

        #endregion

        #region Club

        public async Task<JsonResult> GuardarClub(string clubDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Club/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(clubDTO);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = client.PostAsJsonAsync("", clubDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> EditarClub(int id, string clubDTO)
        {
            if (clubDTO != null && id != 0)
            {
                var path = GuardarImagen("Club");
                clubDTO = clubDTO.Replace("reemplazarEste", path);
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url + "/Club/" + id);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var buffer = System.Text.Encoding.UTF8.GetBytes(clubDTO);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage response = client.PutAsJsonAsync("", clubDTO).Result;
                var content = await response.Content.ReadAsStringAsync();
                return Json(content, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> GetClub(int? idClub)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Club/" + idClub);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetDestacadosByClub(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Club/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetClubes(int page)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Club?page=" + page);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetAllClubes()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Club");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetClubesPagos()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Club/ClubesPagos");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetClubesFiltrados(int page, string nombre, int cod_categoria_club)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + string.Format("/Club?page={0}&nombre={1}&cod_categoria_club={2}", page, nombre, cod_categoria_club));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteClub(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Club/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.DeleteAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Torneos

        public async Task<JsonResult> GetTorneo(int idTorneo)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Torneo/" + idTorneo);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetTorneos()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Torneo/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetTorneosHome()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Torneo/GetTorneosHome");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GuardarTorneo(string torneoDTO)
        {
            var path = GuardarImagen("Torneo");
            torneoDTO = torneoDTO.Replace("reemplazarEste", path);
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Torneo/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync("", torneoDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> EditarTorneo(int id, string torneoDTO)
        {
            var path = GuardarImagen("Torneo");
            torneoDTO = torneoDTO.Replace("reemplazarEste", path);
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Torneo/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(torneoDTO);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = client.PutAsJsonAsync("", torneoDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteTorneo(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Torneo/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.DeleteAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Eventos
        public async Task<JsonResult> GetEvento(int idEvento)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Evento/" + idEvento);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GuardarEvento(string eventoDTO)
        {
            var path = GuardarImagen("Evento");
            eventoDTO = eventoDTO.Replace("reemplazarEste", path);
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Evento/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync("", eventoDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> EditarEvento(int id, string eventoDTO)
        {
            var path = GuardarImagen("Evento");
            eventoDTO = eventoDTO.Replace("reemplazarEste", path);
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Evento/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var buffer = System.Text.Encoding.UTF8.GetBytes(eventoDTO);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = client.PutAsJsonAsync("", eventoDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteEvento(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Evento/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.DeleteAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Reportes

        public async Task<JsonResult> GetReporteClubes()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetCuentaCorrienteClubes");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public void GetExcelReporteJugadoresCompleto()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetReporteJugadores");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage result = client.GetAsync(client.BaseAddress).Result;

            if (result.IsSuccessStatusCode)
            {
                byte[] bytes = result.Content.ReadAsByteArrayAsync().Result;
                this.Response.Charset = "UTF-8";
                this.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                this.Response.ContentType = "application/octet-stream";

                this.Response.AddHeader("Content-Disposition", "attachment; filename=ReporteJugadores.xlsx");
                this.Response.BinaryWrite(bytes);
                this.Response.Flush();
                this.Response.End();
            }
        }

        public async Task<JsonResult> GetReporteJugadores()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetCuentaCorrienteJugadores");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();

            //var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //serializer.MaxJsonLength = 500000000;

            var json = Json(content, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = 500000000;
            return json;
            //return Json(content, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetReporteTorneos()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetCuentaTorneos");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            var json = Json(content, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = 500000000;
            return json;
        }

        public async Task<JsonResult> GetReporteEquiposInscriptos(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetReporteInscripcionesTorneos?id=" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();

            //var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //serializer.MaxJsonLength = 500000000;

            var json = Json(content, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = 500000000;
            return json;
            //return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetReporteJugadoresIndividualesInscriptos(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetReporteInscripcionesTorneoJugadoresExcel?id=" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();

            //var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //serializer.MaxJsonLength = 500000000;

            var json = Json(content, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = 500000000;
            return json;
            //return Json(content, JsonRequestBehavior.AllowGet);
        }

        public void GetExcelReporteEquiposIndividuales(int id)
        {
            HttpClient client = new HttpClient(); client.BaseAddress = new Uri(url + "/Reporte/GetReporteInscripcionesTorneoJugadoresExcel?id=" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage result = client.GetAsync(client.BaseAddress).Result;

            if (result.IsSuccessStatusCode)
            {
                byte[] bytes = result.Content.ReadAsByteArrayAsync().Result;
                this.Response.Charset = "UTF-8";
                this.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                this.Response.ContentType = "application/octet-stream";

                this.Response.AddHeader("Content-Disposition", "attachment; filename=ReporteCuentaTorneos.xlsx");
                this.Response.BinaryWrite(bytes);
                this.Response.Flush();
                this.Response.End();
            }
        }

        public void GetExcelReporteEquipos()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetReporteInscripcionesEquiposExcel");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage result = client.GetAsync(client.BaseAddress).Result;

            if (result.IsSuccessStatusCode)
            {
                byte[] bytes = result.Content.ReadAsByteArrayAsync().Result;
                this.Response.Charset = "UTF-8";
                this.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                this.Response.ContentType = "application/octet-stream";

                this.Response.AddHeader("Content-Disposition", "attachment; filename=ReporteCuentaTorneos.xlsx");
                this.Response.BinaryWrite(bytes);
                this.Response.Flush();
                this.Response.End();
            }
        }
        public void GetExcelReporteClubes()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetReporteCuentasCorrientesClubes");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage result = client.GetAsync(client.BaseAddress).Result;

            if (result.IsSuccessStatusCode)
            {
                byte[] bytes = result.Content.ReadAsByteArrayAsync().Result;
                this.Response.Charset = "UTF-8";
                this.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                this.Response.ContentType = "application/octet-stream";

                this.Response.AddHeader("Content-Disposition", "attachment; filename=ReporteCuentaCorrienteClubes.xlsx");
                this.Response.BinaryWrite(bytes);
                this.Response.Flush();
                this.Response.End();
            }
        }

        public void GetExcelReporteJugadores()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetReporteCuentasCorrientesJugadores");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage result = client.GetAsync(client.BaseAddress).Result;

            if (result.IsSuccessStatusCode)
            {
                byte[] bytes = result.Content.ReadAsByteArrayAsync().Result;
                this.Response.Charset = "UTF-8";
                this.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                this.Response.ContentType = "application/octet-stream";

                this.Response.AddHeader("Content-Disposition", "attachment; filename=ReporteCuentaCorrienteJugadores.xlsx");
                this.Response.BinaryWrite(bytes);
                this.Response.Flush();
                this.Response.End();
            }
        }

        public void GetExcelReporteAllJugadores()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetReporteAllCuentasCorrientesJugadores");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage result = client.GetAsync(client.BaseAddress).Result;

            if (result.IsSuccessStatusCode)
            {
                byte[] bytes = result.Content.ReadAsByteArrayAsync().Result;
                this.Response.Charset = "UTF-8";
                this.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                this.Response.ContentType = "application/octet-stream";

                this.Response.AddHeader("Content-Disposition", "attachment; filename=ReporteCuentaCorrienteJugadores.xlsx");
                this.Response.BinaryWrite(bytes);
                this.Response.Flush();
                this.Response.End();
            }
        }

        public void GetExcelReporteTorneos(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Reporte/GetReporteInscripcionesTorneosExcel?id=" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage result = client.GetAsync(client.BaseAddress).Result;

            if (result.IsSuccessStatusCode)
            {
                byte[] bytes = result.Content.ReadAsByteArrayAsync().Result;
                this.Response.Charset = "UTF-8";
                this.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                this.Response.ContentType = "application/octet-stream";

                this.Response.AddHeader("Content-Disposition", "attachment; filename=ReporteCuentaInscripcionTorneos.xlsx");
                this.Response.BinaryWrite(bytes);
                this.Response.Flush();
                this.Response.End();
            }
        }
        #endregion

        #region Popup

        public async Task<JsonResult> GetPopups()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Popup");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetPopup(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Popup?id=" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public async Task<JsonResult> GuardarPopup(string popUpDTO, int tipo)
        {
            //Ver tema validaciones.. cuantos archivos vamos a tener y como los manejamos
            var path = "";
            if (tipo == 0)
            {
                path = GuardarArchivo("Popup", 0, "ImagenPrincipal");
                popUpDTO = popUpDTO.Replace("reemplazarEstePrincipal", path);
            }
            if (tipo == 1)
            {
                path = GuardarArchivo("Popup", 0, "Slideshow");
                popUpDTO = popUpDTO.Replace("reemplazarEsteSlideshow", path);
            }

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Popup/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync("", popUpDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public async Task<JsonResult> EditarPopupUpload(int id, string popupDTO, int tipo)
        {
            //Ver tema validaciones.. cuantos archivos vamos a tener y como los manejamos
            var path = "";
            if (tipo == 0)
            {
                path = GuardarArchivo("Popup", 0, "ImagenPrincipal");
                popupDTO = popupDTO.Replace("reemplazarEstePrincipal", path);
            }
            if (tipo == 1)
            {
                path = GuardarArchivo("Popup", 0, "Slideshow");
                popupDTO = popupDTO.Replace("reemplazarEsteSlideshow", path);
            }

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Popup/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PutAsJsonAsync("", popupDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> CambiarActividadPopup(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Popup/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.DeleteAsync("").Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetPopUpActivo()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Popup/GetPopUpActivo");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Notificaciones

        public async Task<JsonResult> GetNotificaciones()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Notificacion");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetNotificacionById(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Notificacion/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetTiposNotificacion()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/TipoNotificacion");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GuardarNotificacion(string notificacionDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Notificacion/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PostAsJsonAsync("", notificacionDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> EditarNotificacion(int id, string notificacionDTO)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Notificacion/" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.PutAsJsonAsync("", notificacionDTO).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetNotificacionesRecibidas()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Notificacion/GetNotificacionesRecibidas");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetRespuestasALaAAP()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Respuesta/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetRespuestaPorID(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url + "/Respuesta/GetById?id=" + id);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            var content = await response.Content.ReadAsStringAsync();
            return Json(content, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}