﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAPolo.Site.Models
{
    public class tequipo_userDTO
    {
        public int cod_equipo_jugadores { get; set; }
        public int cod_equipo { get; set; }
        public int cod_usuario { get; set; }
        public int cod_torneo { get; set; }
        public bool bool_capitan { get; set; }
        public int cod_estado { get; set; }
        public bool? bool_suplente { get; set; }
    }
}