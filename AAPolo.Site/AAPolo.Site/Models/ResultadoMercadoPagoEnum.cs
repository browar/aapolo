﻿namespace AAPolo.Site.Models
{
    public enum ResultadoMercadoPagoEnum
    {
        Success = 1,
        Pendiente = 2,
        Error = 3
    }
}