﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAPolo.Site.Models
{
    public class users
    {
        public int cod_usuario { get; set; }
        public string nro_doc { get; set; }
        public string txt_password { get; set; }
        public string platform_mobile { get; set; }
        public string id_register { get; set; }
    }
}