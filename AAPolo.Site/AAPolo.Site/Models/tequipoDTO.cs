﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAPolo.Site.Models
{
    public class tequipoDTO
    {
        public int cod_equipo { get; set; }
        public string txt_desc { get; set; }
        public DateTime fec_alta { get; set; }
        public string txt_alta { get; set; }
        public DateTime? fec_ultima_modificacion { get; set; }
        public string txt_ultima_modificación { get; set; }
        public int cod_torneo { get; set; }
        public virtual List<tequipo_userDTO> tequipo_user { get; set; }
        public int? cod_club { get; set; }
    }
}