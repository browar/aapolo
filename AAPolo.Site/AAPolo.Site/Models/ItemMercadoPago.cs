﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAPolo.Site.Models
{
    public class ItemMercadoPago
    {
        public string title { get; set; }
        public string currency_id { get; set; }
        public string quantity { get; set; }
        public string unit_price { get; set; }
        public int cod_usuario { get; set; }
        public bool IsAnual { get; set; }
        public int cod_tipo_pago { get; set; }
    }
}