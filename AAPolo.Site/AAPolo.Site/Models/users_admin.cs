﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAPolo.Site.Models
{
    public class users_admin
    {
        public int cod_usuario { get; set; }
        
        public string txt_usuario { get; set; }

        public string txt_password { get; set; }

        public bool? bool_activo { get; set; }
    }
}