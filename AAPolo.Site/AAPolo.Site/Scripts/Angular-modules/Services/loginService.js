﻿angular.module('login.service', [])
    .factory('loginService', [
        '$http',
        function ($http) {
            return {
                login: function (loginDTO) {
                    return $http({
                        method: 'POST',
                        url: '/Home/Login',
                        data: { loginDTO: loginDTO }
                    });
                },
                signOut: function () {
                    return $http({
                        method: 'GET',
                        url: '/Home/SignOut',
                    });
                },
                getUser: function () {
                    return $http({
                        method: 'GET',
                        url: '/Home/GetUser',
                    });
                },
                getJugadorAutenticado: function () {
                    return $http({
                        method: 'GET',
                        url: '/Home/GetJugadorAutenticado',
                    });
                },
                loginJugador: function (loginDTO) {
                    return $http({
                        method: 'POST',
                        url: '/Home/LoginJugador',
                        data: { loginDTO: loginDTO }
                    });
                },
                loginMiembro: function (loginDTO) {
                    return $http({
                        method: 'POST',
                        url: '/Home/LoginMiembro',
                        data: { loginDTO: loginDTO }
                    });
                },
                actualizarContrasena: function (loginDTO) {
                    return $http({
                        method: 'POST',
                        url: '/Home/ActualizarContrasena',
                        data: { loginDTO: loginDTO }
                    });
                },
                aceptarTerminos: function (cod_usuario, data) {
                    return $http({
                        method: 'POST',
                        url: '/Home/AceptarTerminos',
                        data: {
                            cod_usuario: cod_usuario,
                            data: data
                        }
                    });
                },
                olvideMiContrasena: function (dni) {
                    return $http({
                        method: 'POST',
                        url: '/Login/OlvideMiContrasena',
                        data: { dni: dni }
                    });
                },
                getMiembroLogueado: function (cod_usuario) {
                    return $http({
                        method: 'POST',
                        url: '/Miembro/GetMiembroLogueado',
                        data: {
                            cod_usuario: cod_usuario
                        }
                    });
                },
                getPopUpActivo: function () {
                    return $http({
                        method: 'GET',
                        url: '/PanelControl/GetPopUpActivo',
                    });
                },
                getNotificacionesRecibidas: function () {
                    return $http({
                        method: 'GET',
                        url: '/PanelControl/GetNotificacionesRecibidas'
                    });
                },
            };
        }]);
