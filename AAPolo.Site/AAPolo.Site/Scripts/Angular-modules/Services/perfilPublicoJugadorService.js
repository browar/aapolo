﻿angular.module('perfilPublicoJugador.service', [])
    .factory('perfilPublicoJugadorService', [
        '$http',
        function ($http) {
            return {
                getJugadoresDestacados: function (page) {
                    return $http({
                        method: 'GET',
                        url: '/PerfilPublico/GetJugadoresDestacados?page=' + page,
                    });
                },              
            };
        }]);
