﻿angular.module('perfilClub.service', [])
    .factory('perfilClubService', [
        '$http',
        function ($http) {
            return {
                post: function (perfilClubDTO) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilClub/Post',
                        data: { perfilClubDTO: perfilClubDTO }
                    });
                },
                get: function () {
                    return $http({
                        method: 'GET',
                        url: '/PerfilClub/Get',
                    });
                },
                getSocios: function (idClub) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilClub/GetSocios',
                        data: { idClub: idClub }
                    });
                },
                buscarJugadores: function (page, search) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilJugador/BuscarJugadores',
                        data: {
                            page: page,
                            search: search
                        }
                    });
                },
                buscarJugadoresSinPago: function (page, search) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilJugador/BuscarJugadoresSinPago',
                        data: {
                            page: page,
                            search: search
                        }
                    });
                },
                getTorneos: function () {
                    return $http({
                        method: 'GET',
                        url: '/PerfilJugador/GetTorneos',
                    });
                },
                getHistorialPagosClub: function (idClub) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilClub/GetHistorialPagosClub',
                        data: {
                            idClub: idClub
                        }
                    });
                },
                pagar: function (datosMercadoPago) {
                    return $http({
                        method: 'POST',
                        url: '/MercadoPago/Pagar',
                        data: {
                            datosMercadoPago: datosMercadoPago
                        }
                    });
                },
                aceptarORechazar: function (idUsuario, opcion) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilClub/AceptarORechazar',
                        data: {
                            idUsuario: idUsuario,
                            opcion: opcion
                        }
                    });
                },
                cambiarCategoria: function (jugadorId, jugadoresDTO) {
                    return $http({
                        method: 'POST',
                        url: '/MercadoPago/CambiarCategoria',
                        data: {
                            jugadorId: jugadorId,
                            jugadoresDTO: jugadoresDTO
                        }
                    });
                },
                getHandicaps: function () {
                    return $http({
                        method: 'GET',
                        url: '/PerfilJugador/GetHandicaps'
                    });
                },
                getHistorialPagos: function (idUser) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilJugador/GetHistorialPagos',
                        data: {
                            idUser: idUser
                        }
                    });
                },
            };
        }]);
