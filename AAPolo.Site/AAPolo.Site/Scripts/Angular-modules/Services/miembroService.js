﻿angular.module('miembro.service', [])
    .factory('miembroService', [
        '$http',
        function ($http) {
            return {
                getMiembroLogueado: function (idJugador) {
                    return $http({
                        method: 'POST',
                        url: '/PanelControl/GetJugador',
                        data: {
                            idJugador: idJugador
                        }
                    });
                },
                getTipos: function () {
                    return $http({
                        method: 'GET',
                        url: '/Miembro/GetTipos',
                    });
                },
                guardarMiembro: function (miembroDTO) {
                    return $http({
                        method: 'POST',
                        url: '/Miembro/GuardarMiembro',
                        data: { miembroDTO: miembroDTO }
                    });
                }, 
                getMunicipios: function (cod_provincia) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/GetMunicipios',
                        data: { cod_provincia: cod_provincia }
                    });
                },
                getTarjeta: function (tarjeta) {
                    return $http({
                        method: 'GET',
                        url: '/Miembro/GetTarjeta?tarjeta=' + tarjeta,
                    });
                },
                getAllClubes: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/getAllClubes'
                    });
                },
              
            };
        }]);
