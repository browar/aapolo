﻿angular.module('perfilMiembro.service', [])
    .factory('perfilMiembroService', [
        '$http',
        function ($http) {
            return {
                getMiembroLogueado: function (cod_usuario) {
                    return $http({
                        method: 'POST',
                        url: '/Miembro/GetMiembroLogueado',
                        data: {
                            cod_usuario: cod_usuario
                        }
                    });
                },
                updateMiembro: function (id, miembroDTO) {
                    return $http({
                        method: 'POST',
                        url: '/Miembro/ModificarMiembro',
                        data: {
                            id : id,
                            miembroDTO: miembroDTO
                        }
                    });
                },

            };
        }]);
