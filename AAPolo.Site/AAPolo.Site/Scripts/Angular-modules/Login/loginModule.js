﻿angular.module('Login', [
    'login.service',
    'login.ctrl',
    'ngRoute',
    'ngAnimate',
    'mgcrea.ngStrap',
    'ngSanitize',
    'pascalprecht.translate',
]).config([
    '$routeProvider',
    '$locationProvider',
    "$httpProvider",
    '$compileProvider',
    '$translateProvider',
    function ($routeProvider, $locationProvider, $httpProvider, $compileProvider, $translateProvider) {
        $locationProvider.hashPrefix('');
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);

        $routeProvider.when('/', {
            templateUrl: 'loginList',
            controller: 'loginCtrl'
        });

        $translateProvider.translations('en', {
            HOME: 'Home',
            PLAYERS: 'Players',
            CLUBS: 'Clubs',
            TOURNAMENTS: 'Tournaments',
            NEWS: 'News',
            ABOUTAAP: 'About AAP',
            PALERMOOPEN: 'Palermo Open',
            POLOCOMMUNITY: 'Polo Community',
            LOGIN: 'Log In',
            CHOOSELANGUAGEESP: "Spanish",
            CHOOSELANGUAGEENG: "English",
            SELECTIDIOMA: "Language: English",
            DNI: "ID",
            CHANGEPASSWORD: 'Change password',
            MYPROFILE: 'My profile',
            LOGOUT: 'Log out',
            IFYOURFIRST: 'If you are a player and it’s your first income, use your ID as password.',
            SHIPRECEIVED: 'Registration will be completed when the payment is credited.',
            PAYTYPES: 'new payment methods',
            HOWTOPAY: 'How to pay my',
            HANDICAP: 'Handicap',
            PAYTOURNAMENT: 'How to sign up for',
            ACTIVETOURNAMENTS: 'Active Tournaments',
            IFYOURFIRST: 'If you are a player and it’s your first income, use your ID as password.',
            OURS: 'our',
            CHANGES: 'About',
            REQUESTREFUND: 'Request your credit refund',
            HANDICAPAFILIACION: 'New handicaps and memberships 2019',
        });
        $translateProvider.translations('es', {
            HOME: 'Inicio',
            PLAYERS: 'Jugadores',
            CLUBS: 'Clubes',
            TOURNAMENTS: 'Torneos',
            NEWS: 'Noticias',
            ABOUTAAP: 'La Asociación',
            PALERMOOPEN: 'Abierto de Palermo',
            POLOCOMMUNITY: 'Polo Community',
            LOGIN: 'Iniciar Sesión',
            CHOOSELANGUAGEESP: "Español",
            CHOOSELANGUAGEENG: "Ingles",
            SELECTIDIOMA: "Idioma: Español",
            DNI: "DNI",
            CHANGEPASSWORD: 'Cambiar contraseña',
            MYPROFILE: 'Mi perfil',
            LOGOUT: 'Cerrar sesión',
            CHANGES: 'Cambios',
            OURS: 'en nuestros',
            PAYTYPES: 'medios de cobro',
            HOWTOPAY: 'Cómo pagar mi',
            HANDICAP: 'Handicap',
            PAYTOURNAMENT: 'Cómo inscribirme a',
            ACTIVETOURNAMENTS: 'Torneos activos',
            IFYOURFIRST: 'Si sos jugador y es la primera vez que ingresás, tu usuario y contraseña es igual a tu documento.',
            REQUESTREFUND: 'SOLICITÁ LA DEVOLUCIÓN DE TU CRÉDITO',
            HANDICAPAFILIACION: 'Conocé los Handicaps y Afiliaciones 2019',
        });
        $translateProvider.preferredLanguage('es');


        var regexIso8601 = /\/Date\((\d*)\)\//;

        $httpProvider.defaults.transformResponse.push(function (responseData) {
            convertDateStringsToDates(responseData);
            return responseData;
        });

        function convertDateStringsToDates(input) {
            // Ignore things that aren't objects.
            if (typeof input !== "object") return input;


            for (var key in input) {
                if (!input.hasOwnProperty(key)) continue;
                var value = input[key];
                var match;
                // Check for string properties which look like dates.
                // TODO: Improve this regex to better match ISO 8601 date strings.
                if (typeof value === "string" && (match = value.match(regexIso8601))) {
                    // Assume that Date.parse can parse ISO 8601 strings, or has been shimmed in older browsers to do so.   
                    var milliseconds = new Date(parseInt(match[1]));

                    if (!isNaN(milliseconds)) {

                        var d = new Date(milliseconds);
                        var day = d.getUTCDate().toString().length == 1 ? '0' + parseInt(d.getUTCDate()) : d.getUTCDate();
                        var month = d.getUTCMonth().toString().length == 1 ? '0' + parseInt(d.getUTCMonth() + 1) : d.getUTCMonth() + 1;
                        var year = d.getUTCFullYear();
                        var hours = d.getUTCHours();
                        var minutes = d.getUTCMinutes();
                        var result = d;
                        input[key] = result;
                    }
                } else if (typeof value === "object") {
                    // Recurse into object
                    convertDateStringsToDates(value);
                }
            }
        };

    }
]).run(['$rootScope', function ($rootScope) {
    $rootScope.$on('$routeChangeSuccess', function () {
        $('body, html').removeClass('overflow-hidden');
        setTimeout(pageSetUp, 200);
    });
    }]);