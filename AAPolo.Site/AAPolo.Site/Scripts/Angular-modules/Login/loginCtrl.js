﻿var app = angular.module('login.ctrl', [])
    .controller('loginCtrl', [
        '$scope',
        '$filter',
        '$rootScope',
        '$routeParams',
        '$location',
        'loginService',
        '$timeout',
        function ($scope, $filter, $rootScope, $routeParams, $location, loginService, $timeout) {
            $scope.isOpen = false;
            $scope.regexPassword = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z])(?=.*[@#$%^&+*!=]).*/;
            $scope.thereIsError = false;
            $scope.datos = [];
            $scope.aceptaTerminos = false;
            $scope.submitted = false;
            $scope.regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            $scope.perfil = {
                contacto: {}
            };
            $scope.jugador = {
                contacto: {}
            };

            $scope.cambiarIdioma = function (idioma) {
                localStorage.setItem("idioma", idioma);
                $scope.idiomaElegido = idioma;
                $(".dropdown-abierto").click();
            }

            $scope.getNotificaciones = function () {
                loginService.getNotificacionesRecibidas().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.notificaciones = json;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.boolActive = function () {
                $scope.aceptaTerminos = !$scope.aceptaTerminos;
            }

            $rootScope.$watch(function () {
                return $location.path();
            },
                function (a) {
                    $scope.initLogin();
                });

            $scope.initLogin = function () {
                var idioma = localStorage.getItem("idioma");
                $scope.idiomaElegido = idioma;
                $scope.loginId = localStorage.getItem('id');
                $scope.getNotificaciones();
                $scope.getPopUpActivo();
                var url = new URL(location.href);
                var dni = url.hash.substr(2);
                if (dni.includes("token=")) {
                    dni = dni.replace("token=", "");
                    $scope.idSearch = dni;
                }

                if ($scope.idSearch != undefined && $scope.idSearch != '' && $scope.idSearch != null) {
                    $scope.actualizaCredenciales = {};
                    $scope.actualizaCredenciales.nro_doc = $scope.idSearch;
                    $(".nroDocumento").attr("readonly", "readonly");
                    clickModal("cambiarPass");
                } else {
                    $scope.checkMobile();
                    loginService.getUser().then(function (response) {
                        $scope.user = JSON.parse(response.data);
                        if ($scope.user == null || $scope.user == undefined) {
                            if (window.location.pathname.includes("PanelControl")) {
                                window.location.href = "/Home/Index";
                            }
                            loginService.getJugadorAutenticado().then(function (response) {
                                var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
                                $scope.user = JSON.parse(response.data);
                                if ($scope.user != undefined) {
                                    var displayName = localStorage.getItem("displayName");
                                    if (displayName == undefined || displayName == "undefined")
                                        localStorage.setItem("displayName", $scope.user.txt_nombre);

                                    localStorage.setItem('id', $scope.user.cod_usuario);
                                    if ($scope.user.cod_usuario != id && id != undefined && location.pathname.includes("PerfilJugador") && !location.hash.includes("/Torneos/" + id) && !location.hash.includes("/Inbox/" + id))
                                        window.location.href = "/Home/Index";
                                    else if (location.hash.includes("/Torneos/" + id) || (location.hash.includes("/Inbox/" + id))) {
                                        var url = window.location.hash.substring(window.location.hash.indexOf('/') + 1);
                                        id = url.substring(0, $scope.user.cod_usuario.toString().length);
                                        if ($scope.user.cod_usuario != id && id != undefined && location.pathname.includes("PerfilJugador")) {
                                            window.location.href = "/Home/Index";
                                        }
                                    }

                                    if (($scope.user == null || $scope.user == undefined) && window.location.pathname != "/Home/Index" && !window.location.pathname.includes("/PerfilPublico/")) {
                                        window.location.href = "/Home/Index";
                                    } else {
                                        $scope.user.txt_usuario = $scope.user.txt_nombre + " " + $scope.user.txt_apellido;
                                        $scope.jugador = $scope.user;
                                        $scope.jugador.contacto = {};
                                        var findTelefono = $filter('filter')($scope.user.tcontacto_usuario, { "$": "Telefono" }, true)[0];
                                        if (findTelefono != undefined)
                                            $scope.jugador.contacto.txt_desc = findTelefono.tcontacto.txt_desc;

                                        var findMail = $filter('filter')($scope.user.tcontacto_usuario, { "$": "Email" }, true)[0];
                                        if (findMail != undefined)
                                            $scope.jugador.txt_mail = findMail.tcontacto.txt_desc;
                                        if (!$scope.user.bool_acepta_terminos) {
                                            clickModal("condicionesUsuario");
                                        }
                                    }

                                } else {
                                    var id = localStorage.getItem("id");
                                    if (id != "undefined" && id != null && id != undefined) {
                                        loginService.getMiembroLogueado(localStorage.getItem("id")).then(function (response) {
                                            $scope.user = JSON.parse(response.data);
                                            var displayName = localStorage.getItem("displayName");
                                            if (displayName == undefined)
                                                localStorage.setItem("displayName", $scope.user.txt_nombre);
                                        }).catch(function (error) {
                                            console.log(error);
                                        });
                                    }
                                }
                            }).catch(function (error) {
                                console.log(error);
                            });
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.checkMobile = function () {
                var isMobile = false; //initiate as false
                $scope.isMobile = false;
                // device detection
                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
                    $(".no-mobile").remove();
                    $scope.isMobile = true;
                }
            };


            $scope.login = function () {
                loginService.login($scope.user).then(function (response) {
                    localStorage.setItem("displayName", $scope.user.txt_usuario);
                    window.location.href = "/PanelControl/#/Jugadores";
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.loginJugador = function (credenciales, form) {
                credenciales.id_register = localStorage.getItem("id_register");
                credenciales.platform_mobile = localStorage.getItem("platform");
                loginService.loginJugador(credenciales).then(function (response) {
                    $scope.jugador = JSON.parse(response.data);
                    if ($scope.jugador.ExceptionMessage != 'Ocurrió un error y no pudo loguearse') {
                        $scope.jugador.contacto = {};
                        localStorage.setItem('id', $scope.jugador.cod_usuario);
                        var landing = localStorage.getItem("redirectLanding");
                        if (landing != undefined && landing == "true" && landing != null && landing != "undefined" && $scope.jugador.cod_usuario != undefined && $scope.jugador.cod_usuario != 'undefined')
                            location.href = "/Miembro/#/PoloCommunity";
                        if ($scope.jugador.cod_usuario != undefined) {
                            localStorage.setItem("displayName", $scope.jugador.txt_nombre);
                            localStorage.setItem("isMember", false);
                            if (!$scope.jugador.bool_acepta_terminos) {
                                var findTelefono = $filter('filter')($scope.jugador.tcontacto_usuario, { "$": "Telefono" }, true)[0];
                                if (findTelefono != undefined)
                                    $scope.jugador.contacto.txt_desc = findTelefono.tcontacto.txt_desc;

                                var findMail = $filter('filter')($scope.jugador.tcontacto_usuario, { "$": "Email" }, true)[0];
                                if (findMail != undefined)
                                    $scope.jugador.txt_mail = findMail.tcontacto.txt_desc;

                                clickModal("condicionesUsuario");
                                $scope.actualizaCredenciales.nro_doc = credenciales.nro_doc;
                            } else {
                                if (credenciales.nro_doc == credenciales.txt_password) {
                                    $scope.actualizaCredenciales = {};
                                    $scope.actualizaCredenciales.nro_doc = credenciales.nro_doc;
                                    clickModal("cambiarPass");
                                } else if (landing != undefined && landing == "true" && landing != null && landing != "undefined" &&
                                    $scope.jugador.cod_usuario != undefined && $scope.jugador.cod_usuario != 'undefined') {

                                }
                                else
                                    window.location.href = "/PerfilJugador/#/" + $scope.jugador.cod_usuario;
                            }
                        } else {
                            loginService.loginMiembro(credenciales).then(function (response) {
                                var json = JSON.parse(response.data);
                                if (json.cod_miembro != undefined && json.cod_miembro != "undefined") {
                                    $scope.user = json;
                                    localStorage.setItem("id", json.cod_miembro);
                                    localStorage.setItem("isMember", true);
                                    localStorage.setItem("displayName", $scope.user.txt_nombre);
                                    if (credenciales.nro_doc == credenciales.txt_password) {
                                        $scope.actualizaCredenciales = {};
                                        $scope.actualizaCredenciales.nro_doc = credenciales.nro_doc;
                                        clickModal("cambiarPass");
                                    } else
                                        if (!location.href.includes("PoloCommunity")) {
                                            location.href = "/PerfilMiembro/Index#/";
                                        } else {
                                            location.href = "/Miembro/#/PoloCommunity";
                                        }
                                } else {
                                    $scope.thereIsError = true;
                                }
                            }).catch(function (error) {
                                $scope.thereIsError = true;
                                console.log(error);
                            });
                        }
                    } else {
                        $scope.thereIsError = true;
                    }
                }).catch(function (error) {
                    console.log(error);
                });

            }

            $scope.signOut = function () {
                loginService.signOut().then(function (response) {
                    var idioma = localStorage.getItem("idioma");
                    localStorage.clear();
                    if (idioma == undefined) {
                        var userLang = navigator.language || navigator.userLanguage;

                        if (userLang.includes("es-")) {
                            $scope.idiomaElegido = "es";
                        } else {
                            $scope.idiomaElegido = "en";
                        }
                        localStorage.setItem("idioma", idioma);
                    } else {
                        localStorage.setItem("idioma", $scope.idiomaElegido);
                    }
                    location.reload();
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.verifyCurrent = function (path) {
                if (location.href.includes(path)) {
                    return true;
                } else {
                    return false;
                }
            }

            $scope.updateTerminosYCondiciones = function () {
                $scope.perfil.contacto.cod_tipo_contacto = 1;
                $scope.perfil.tcontacto_usuario.push({ tcontacto: $scope.perfil.contacto });
                var mail = {};
                mail.tcontacto = { cod_contacto: 0, cod_tipo_contacto: 3, txt_desc: $scope.jugador.txt_mail }
                $scope.jugador.tcontacto_usuario.push(mail);
            }

            $scope.actualizarContrasena = function (credenciales, form) {
                if (form.$error.required == undefined && form.$error.minlength == undefined && form.$error.pattern == undefined) {
                    if (credenciales.txt_password == credenciales.re_txt_password) {
                        credenciales.cod_usuario = $scope.jugador.cod_usuario;
                        loginService.actualizarContrasena(credenciales).then(function (response) {
                            $scope.loginJugador(credenciales);
                        }).catch(function (error) {
                            console.log(error);
                        });
                    } else {
                        $scope.submitted = true;
                    }
                } else {
                    $scope.submitted = true;
                }
            }

            $scope.aceptarTerminos = function () {
                $scope.jugador.tcontacto_usuario = [];
                if ($scope.jugador.contacto != undefined) {
                    $scope.jugador.contacto.cod_tipo_contacto = 1;
                    $scope.jugador.tcontacto_usuario.push({ tcontacto: $scope.jugador.contacto });
                }
                var mail = {};
                mail.tcontacto = { cod_contacto: 0, cod_tipo_contacto: 3, txt_desc: $scope.jugador.txt_mail }
                $scope.jugador.tcontacto_usuario.push(mail);

                loginService.aceptarTerminos($scope.jugador.cod_usuario, JSON.stringify($scope.jugador)).then(function (response) {
                    closeModal();
                    clickModal("cambiarPass");
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.olvideMiContrasena = function () {
                loginService.olvideMiContrasena($scope.nro_doc).then(function (response) {
                    localStorage.setItem('nro_doc', $scope.nro_doc);
                    location.reload();
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.goToPerfil = function () {
                var isMember = localStorage.getItem("isMember");
                if (isMember == "true")
                    location.href = "/PerfilMiembro/Index#";
                else {
                    var id = localStorage.getItem('id');
                    location.href = "/PerfilJugador/#/" + id;
                }
            }

            $scope.getPopUpActivo = function () {
                loginService.getPopUpActivo().then(function (response) {
                    $scope.comunicado = JSON.parse(response.data);
                    var date = new Date();
                    var viewPopUp = sessionStorage.getItem("viewPopUp");
                    if ($scope.comunicado != undefined && viewPopUp != "true") {
                        if (moment($scope.comunicado.fec_vigencia) >= date) {
                            if ($scope.comunicado.txt_video != undefined && $scope.comunicado.txt_video != "reemplazarEsteSlideshow") {
                                clickModal("comunicadoVideo");
                            }
                            if ($scope.comunicado.txt_imagen != undefined && $scope.comunicado.txt_imagen != "reemplazarEstePrincipal") {
                                clickModal("comunicadoImagen");
                            }
                        }
                    }

                    sessionStorage.setItem('viewPopUp', true);
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }]);