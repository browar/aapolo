﻿var app = angular.module('perfilMiembro.ctrl', [])
    .controller('perfilMiembroListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'perfilMiembroService',
        '$timeout',
        'Upload',
        function ($scope, $filter, $routeParams, $location, perfilMiembroService, $timeout, Upload) {

            $scope.init = function () {
                $scope.id = localStorage.getItem("id");
                if ($scope.id == undefined || $scope.id == "undefined")
                    location.href = "/Home/Index#";
                else
                    $scope.getMiembroLogueado();
            }

            $scope.getMiembroLogueado = function () {
                perfilMiembroService.getMiembroLogueado($scope.id).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.perfil = json;
                });
            }

            $scope.guardarMiembro = function () {
                perfilMiembroService.guardarMiembro($scope.id, $scope.perfil).then(function (response) {
                        
                });
            }

        }]);