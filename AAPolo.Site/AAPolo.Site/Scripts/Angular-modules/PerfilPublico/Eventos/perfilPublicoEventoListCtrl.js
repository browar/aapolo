﻿var app = angular.module('perfilPublicoEvento.ctrl', [])
    .controller('perfilPublicoEventoListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {          
            $scope.eventosCalendar = [];
            $scope.isLoading = false;
            $scope.init = function () {
                $scope.getEventos();

                $('#calendar').fullCalendar({
                    eventLimit: true, // allow "more" link when too many events
                    events: $scope.eventosCalendar,
                    displayEventTime: false,
                    timeFormat: ' '
                });
            }

            $scope.getEventos = function () {
                $scope.isLoading = true;
                panelControlService.getEventos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.eventos = json;
                    angular.forEach($scope.eventos, function (key) {
                        var dateNew = $filter('date')(key.fec_comienzo, "yyyy-MM-dd");
                        if (key.cod_tipo_evento == 1) {
                            $scope.evento = {
                                title: "" + key.txt_desc,
                                start: "" + dateNew,
                                end: "" + dateNew,
                                url: "/PerfilPublico/#/Torneo/" + key.cod_torneo,
                                editable: false,
                                allDay: true,
                                className: "calendar-torneo"
                            };
                        } else {
                            $scope.evento = {
                                title: "" + key.txt_desc,
                                start: "" + dateNew,
                                end: "" + dateNew,
                                url: "javascript:void(0)",
                                editable: false,
                                allDay: true,
                                className: "calendar-evento"
                            };
                        }
                        $scope.eventosCalendar.push($scope.evento);
                        $('#calendar').fullCalendar('renderEvent', $scope.evento, true);
                    });
                    $scope.isLoading = false;
                }).catch(function (error) {
                    console.log(error);
                });
            }

        }]);