﻿angular.module('index.service', [])
    .factory('indexService', [
        '$http',
        function ($http) {
            return {
                // Modulo Noticias
                getNoticias: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetNoticias',
                    });
                },
                getNoticia: function (id) {
                    return $http({
                        method: 'POST',
                        url: '/PanelControl/GetNoticia',
                        data: {
                            id: id
                        }
                    });
                },
                getNoticiasDestacadas: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetNoticiasDestacadas',
                    });
                },
                
                getNoticiasRelacionadas: function (id) {
                    return $http({
                        method: 'POST',
                        url: '/PanelControl/GetNoticiaRelacionadas',
                        data: {
                            id: id
                        }
                    });
                },
                updateEstadoNoticia: function (id) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/EditarEstadoNoticia',
                        data: {
                            id: id
                        }
                    });
                },

                postNoticia: function (noticiaDTO) {
                    return $http({
                        method: 'POST',
                        url: '/PanelControl/GuardarNoticia',
                        data: { noticiaDTO: noticiaDTO }
                    });
                },
                putNoticia: function (id, noticiaDTO) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/EditarNoticia',
                        data: {
                            id: id,
                            noticiaDTO: noticiaDTO
                        }
                    });
                },
                getEtiquetasNoticia: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetEtiquetasNoticia',
                    });
                },
                getCategoriasNoticia: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetCategoriasNoticia',
                    });
                },
                getTorneos: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetTorneos'
                    });
                },
                getEventos: function () {
                    return $http({
                        method: 'Get',
                        url: '/PerfilPublico/GetEventos',
                    });
                },
            };
        }]);
