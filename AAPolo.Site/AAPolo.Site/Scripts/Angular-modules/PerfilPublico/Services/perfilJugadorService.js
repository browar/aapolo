﻿angular.module('perfilJugador.service', [])
    .factory('perfilJugadorService', [
        '$http',
        function ($http) {
            return {
                getTorneos: function () {
                    return $http({
                        method: 'GET',
                        url: '/PerfilJugador/GetTorneos',
                    });
                },              
                getPagos: function (idUser) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilJugador/GetPagos',
                        data: {
                            idUser: idUser
                        }
                    });
                },
                pagar: function (datosMercadoPago) {
                    return $http({
                        method: 'POST',
                        url: '/MercadoPago/Pagar',
                        data: {
                            datosMercadoPago: datosMercadoPago
                        }
                    });
                },
                buscarJugadores: function (page, search) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilJugador/BuscarJugadores',
                        data: {
                            page: page,
                            search: search
                        }
                    });
                },
                cambiarCategoria: function (jugadorId, jugadoresDTO) {
                    return $http({
                        method: 'POST',
                        url: '/MercadoPago/CambiarCategoria',
                        data: {
                            jugadorId: jugadorId,
                            jugadoresDTO: jugadoresDTO
                        }
                    });
                },
                getHandicaps: function () {
                    return $http({
                        method: 'GET',
                        url: '/PerfilJugador/GetHandicaps'
                    });
                },
                getHistorialPagos: function (idUser) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilJugador/GetHistorialPagos',
                        data: {
                            idUser: idUser
                        }
                    });
                },
                finalizarPreInscripcion: function (equipoDTO) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilJugador/FinalizarPreInscripcion',
                        data: {
                            equipoDTO: equipoDTO
                        }
                    });
                },
                getEquipo: function (idJugador) {
                    return $http({
                        method: 'POST',
                        url: '/PerfilJugador/GetEquipo',
                        data: {
                            idJugador: idJugador
                        }
                    });
                },
                validarInscripcion: function (equipo) {
                    return $http({
                        method: 'POST',
                        url: '/MercadoPago/ValidarInscripcion',
                        data: {
                            equipo: equipo
                        }
                    });
                },
            };
        }]);
