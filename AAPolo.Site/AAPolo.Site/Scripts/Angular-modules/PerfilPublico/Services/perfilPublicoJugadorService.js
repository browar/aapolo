﻿angular.module('perfilPublicoJugador.service', [])
    .factory('perfilPublicoJugadorService', [
        '$http',
        function ($http) {
            return {
                getJugadoresDestacados: function () {
                    return $http({
                        method: 'GET',
                        url: '/PerfilPublico/GetJugadoresDestacados',
                    });
                },              
            };
        }]);
