﻿angular.module('login.service', [])
    .factory('loginService', [
        '$http',
        function ($http) {
            return {
                login: function (loginDTO) {
                    return $http({
                        method: 'POST',
                        url: '/Home/Login',
                        data: { loginDTO: loginDTO }
                    });
                },
                signOut: function () {
                    return $http({
                        method: 'GET',
                        url: '/Home/SignOut',
                    });
                },
                getUser: function () {
                    return $http({
                        method: 'GET',
                        url: '/Home/GetUser',
                    });
                },
                getJugadorAutenticado: function () {
                    return $http({
                        method: 'GET',
                        url: '/Home/GetJugadorAutenticado',
                    });
                },
                loginJugador: function (loginDTO) {
                    return $http({
                        method: 'POST',
                        url: '/Home/LoginJugador',
                        data: { loginDTO: loginDTO }
                    });
                },
                actualizarContrasena: function (loginDTO) {
                    return $http({
                        method: 'POST',
                        url: '/Home/ActualizarContrasena',
                        data: { loginDTO: loginDTO }
                    });
                },
                aceptarTerminos: function (cod_usuario, data) {
                    return $http({
                        method: 'POST',
                        url: '/Home/AceptarTerminos',
                        data: {
                            cod_usuario: cod_usuario,
                            data: data
                        }
                    });
                },
            };
        }]);
