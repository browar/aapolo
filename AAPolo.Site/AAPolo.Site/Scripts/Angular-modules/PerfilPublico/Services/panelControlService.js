﻿angular.module('panelControl.service', [])
    .factory('panelControlService', [
        '$http',
        function ($http) {
            return {
                pagar: function (datosMercadoPago) {
                    return $http({
                        method: 'POST',
                        url: '/MercadoPago/Pagar',
                        data: {
                            datosMercadoPago: datosMercadoPago
                        }
                    });
                },
                pagarComprobante: function (usuario, datosMercadoPago, pagoComprobante) {
                    return $http({
                        method: 'POST',
                        url: '/MercadoPago/PagarComprobante',
                        data: {
                            usuario: usuario, 
                            datosMercadoPago: datosMercadoPago,
                            pagoComprobante: pagoComprobante
                        }
                    });
                },
                // Modulo Jugadores
                getUser: function () {
                    return $http({
                        method: 'GET',
                        url: '/Home/GetUser',
                    });
                },
                getJugadorAutenticado: function () {
                    return $http({
                        method: 'GET',
                        url: '/Home/GetJugadorAutenticado',
                    });
                },
                postJugador: function (jugadorDTO) {
                    return $http({
                        method: 'POST',
                        url: '/PanelControl/GuardarJugador',
                        data: { jugadorDTO: jugadorDTO }
                    });
                },
                getJugadores: function (page) {
                    return $http({
                        method: 'GET',
                        url: '/PanelControl/GetJugadores?page=' + page,
                    });
                },
                getJugadoresFiltrados: function (page, nombre, cod_tipo_jugador, cod_club) {
                    return $http({
                        method: 'GET',
                        url: '/PanelControl/GetJugadoresFiltrados?page=' + page + "&nombre=" + nombre + "&cod_tipo_jugador=" + cod_tipo_jugador + "&cod_club=" + cod_club,
                    });
                },
                GetJugadoresFiltradosPublico : function (page, nombre, cod_tipo_jugador, cod_club, sendHandicap) {
                    return $http({
                        method: 'GET',
                        url: '/PanelControl/GetJugadoresFiltradosPublico?page=' + page + "&nombre=" + nombre + "&cod_tipo_jugador=" + cod_tipo_jugador + "&cod_club=" + cod_club + "&nro_hand=" + sendHandicap,
                    });
                },
                getJugadoresSinPagina: function () {
                    return $http({
                        method: 'GET',
                        url: '/PanelControl/GetJugadoresSinPagina',
                    });
                },
                getJugador: function (idJugador) {
                    return $http({
                        method: 'POST',
                        url: '/PanelControl/GetJugador',
                        data: {
                            idJugador: idJugador
                        }
                    });
                },
                putJugador: function (id, jugadorDTO) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/EditarJugador',
                        data: {
                            id: id,
                            jugadorDTO: jugadorDTO
                        }
                    });
                },
                deleteJugador: function (id) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/DeleteJugador',
                        data: {
                            id: id
                        }
                    });
                },
                // Modulo Tipos
                getTiposJugador: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetJugadorTipos',
                    });
                },
                getTiposClub: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetClubTipos',
                    });
                },
                getMunicipios: function (cod_provincia) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/GetMunicipios',
                        data: { cod_provincia: cod_provincia}
                    });
                },
                getTorneoTipos: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetTorneoTipos',
                    });
                },
                // Modulo Eventos
                getEventos: function () {
                    return $http({
                        method: 'Get',
                        url: '/PerfilPublico/GetEventos',
                    });
                },
                getEvento: function (idEvento) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/GetEvento',
                        data: {
                            idEvento: idEvento
                        }
                    });
                },
                postEvento: function (eventoDTO) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/GuardarEvento',
                        data: {
                            eventoDTO: eventoDTO
                        }
                    });
                },
                putEvento: function (eventoDTO) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/EditarEvento',
                        data: {
                            eventoDTO: eventoDTO
                        }
                    });
                },

                deleteEvento: function (id) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/DeleteEvento',
                        data: {
                            id: id
                        }
                    });
                },
                // Modulo Noticias
                getNoticias: function() {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetNoticias',
                    });
                },
                getNoticia: function (id) {
                    return $http({
                        method: 'POST',
                        url: '/PanelControl/GetNoticia',
                        data: {
                            id: id
                        }
                    });
                },
                getNoticiasRelacionadas: function (id) {
                    return $http({
                        method: 'POST',
                        url: '/PanelControl/GetNoticiaRelacionadas',
                        data: {
                            id: id
                        }
                    });
                },
                updateEstadoNoticia: function (id) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/EditarEstadoNoticia',
                        data: {
                            id: id
                        }
                    });
                },
                updateEstadoNoticiaDestacada: function (id) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/EditarEstadoNoticiaDestacada',
                        data: {
                            id: id
                        }
                    });
                },
                GetNoticiasActivas: function () {
                    return $http({
                        method: 'GET',
                        url: '/PanelControl/GetNoticiasActivas',
                    });
                },
                GetNoticiasDestacadas: function () {
                    return $http({
                        method: 'GET',
                        url: '/PanelControl/GetNoticiasDestacadas',
                    });
                },
                postNoticia: function (noticiaDTO) {
                    return $http({
                        method: 'POST',
                        url: '/PanelControl/GuardarNoticia',
                        data: { noticiaDTO: noticiaDTO }
                    });
                },
                putNoticia: function (id, noticiaDTO) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/EditarNoticia',
                        data: {
                            id: id,
                            noticiaDTO: noticiaDTO
                        }
                    });
                },
                getEtiquetasNoticia: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetEtiquetasNoticia',
                    });
                },
                getCategoriasNoticia: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetCategoriasNoticia',
                    });
                },
                // Reportes
                getReporteJugadores: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetReporteJugadores',
                    });
                },
                getReporteClubes: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetReporteClubes',
                    });
                },
                getReporteTorneos: function (id) {
                    return $http({
                        method: 'POST',
                        url: '/PanelControl/GetReporteEquiposInscriptos',
                        data: {
                            id: id,
                        }
                    });
                },
                getExcelReporte: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetExcelReporte',
                    });
                },
                // Modulo Club
                postClub: function (clubDTO) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/GuardarClub',
                        data: { clubDTO: clubDTO }
                    });
                },
                putClub: function (id, clubDTO) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/EditarClub',
                        data: {
                            id: id,
                            clubDTO: clubDTO
                        }
                    });
                },
                getClub: function (clubId) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/GetClub',
                        data: { idClub: clubId }
                    });
                },
                getClubes: function (page) {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetClubes?page='+ page
                    });
                },
                getClubesFiltrados: function (page, nombre, cod_categoria_club) {
                    return $http({
                        method: 'GET',
                        url: '/PanelControl/GetClubesFiltrados?page=' + page + "&nombre=" + nombre + "&cod_categoria_club=" + cod_categoria_club,
                    });
                },
                getDestacadosByClub: function (idClub) {
                    return $http({
                        method: 'GET',
                        url: '/PerfilPublico/GetJugadoresDestacados?idClub=' + idClub,
                    });
                },

                deleteClub: function (id) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/DeleteClub',
                        data: {
                            id: id
                        }
                    });
                },
                // Modulo Torneo
                postTorneo: function (torneoDTO) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/GuardarTorneo',
                        data: {
                            torneoDTO: torneoDTO
                        }
                    });
                },
                putTorneo: function (id, torneoDTO) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/EditarTorneo',
                        data: {
                            id: id,
                            torneoDTO: torneoDTO
                        }
                    });
                },
                getTorneo: function (idTorneo) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/GetTorneo',
                        data: { idTorneo: idTorneo }
                    });
                },
                getTorneos: function () {
                    return $http({
                        method: 'Get',
                        url: '/PanelControl/GetTorneos'
                    });
                },
                deleteTorneo: function (id) {
                    return $http({
                        method: 'Post',
                        url: '/PanelControl/DeleteTorneo',
                        data: {
                            id: id
                        }
                    });
                },
            };
        }]);
