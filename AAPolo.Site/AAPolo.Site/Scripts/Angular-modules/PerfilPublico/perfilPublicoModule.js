﻿angular.module('MasterPerfilPublico', [
    'panelControl.service',
    'ngRoute',
    'ngAnimate',
    'mgcrea.ngStrap',
    'ngSanitize',
    'ngMap',
]).config([
    '$routeProvider',
    '$locationProvider',
    "$httpProvider",
    '$compileProvider',
    function ($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $locationProvider.hashPrefix('');
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);

        $routeProvider.when('/Jugadores', {
            templateUrl: 'perfilPublicoJugadorList',
            controller: 'perfilPublicoJugadorListCtrl'
        });

        $routeProvider.when('/Jugador/:idJugador', {
            templateUrl: 'perfilPublicoJugadorBuscado',
            controller: 'perfilPublicoJugadorBuscadoCtrl'
        });

        $routeProvider.when('/Clubes', {
            templateUrl: 'perfilPublicoClubList',
            controller: 'perfilPublicoClubListCtrl'
        });

        $routeProvider.when('/Club/:idClub', {
            templateUrl: 'perfilPublicoClubBuscado',
            controller: 'perfilPublicoClubBuscadoCtrl'
        });

        $routeProvider.when('/Noticias', {
            templateUrl: 'perfilPublicoNoticiaList',
            controller: 'perfilPublicoNoticiaListCtrl'
        });

        $routeProvider.when('/Eventos', {
            templateUrl: 'perfilPublicoEventoList',
            controller: 'perfilPublicoEventoListCtrl'
        });

        $routeProvider.when('/Noticia/:idNoticia', {
            templateUrl: 'perfilPublicoNoticiaBuscada',
            controller: 'perfilPublicoNoticiaBuscadaCtrl'
        });

        $routeProvider.when('/Torneos', {
            templateUrl: 'perfilPublicoTorneoList',
            controller: 'perfilPublicoTorneoListCtrl'
        });

        $routeProvider.when('/Torneo/:idTorneo', {
            templateUrl: 'perfilPublicoTorneoBuscado',
            controller: 'perfilPublicoTorneoBuscadoCtrl'
        });

        //$routeProvider.otherwise({
        //    redirectTo: '/'
        //});
        
        var regexIso8601 = /\/Date\((\d*)\)\//;

        $httpProvider.defaults.transformResponse.push(function (responseData) {
            convertDateStringsToDates(responseData);
            return responseData;
        });

        function convertDateStringsToDates(input) {
            // Ignore things that aren't objects.
            if (typeof input !== "object") return input;


            for (var key in input) {
                if (!input.hasOwnProperty(key)) continue;
                var value = input[key];
                var match;
                // Check for string properties which look like dates.
                // TODO: Improve this regex to better match ISO 8601 date strings.
                if (typeof value === "string" && (match = value.match(regexIso8601))) {
                    // Assume that Date.parse can parse ISO 8601 strings, or has been shimmed in older browsers to do so.   
                    var milliseconds = new Date(parseInt(match[1]));

                    if (!isNaN(milliseconds)) {

                        var d = new Date(milliseconds);
                        var day = d.getUTCDate().toString().length == 1 ? '0' + parseInt(d.getUTCDate()) : d.getUTCDate();
                        var month = d.getUTCMonth().toString().length == 1 ? '0' + parseInt(d.getUTCMonth() + 1) : d.getUTCMonth() + 1;
                        var year = d.getUTCFullYear();
                        var hours = d.getUTCHours();
                        var minutes = d.getUTCMinutes();
                        var result = d;
                        input[key] = result;
                    }
                } else if (typeof value === "object") {
                    // Recurse into object
                    convertDateStringsToDates(value);
                }
            }
        };

    }
]).run(['$rootScope', function ($rootScope) {
    $rootScope.$on('$routeChangeSuccess', function () {
        setTimeout(pageSetUp, 200);
    });
}]);


angular.module("PerfilPublico", ['MasterPerfilPublico', 'PerfilPublicoClub', 'PerfilPublicoJugador', 'PerfilPublicoTorneo', 'PerfilPublicoNoticia', 'PerfilPublicoEvento']);

var Login = document.getElementById('Login');

angular.element(document).ready(function () {
    angular.bootstrap(Login, ['Login']);
});
