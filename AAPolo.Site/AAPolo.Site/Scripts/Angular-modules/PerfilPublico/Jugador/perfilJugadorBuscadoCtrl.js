﻿var app = angular.module('perfilPublicoJugadorBuscado.ctrl', [])
    .controller('perfilPublicoJugadorBuscadoCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.filter = {
                handicap: {},
                tuser_club: []
            }

            $scope.init = function () {
                $scope.getJugador();
            }

            $scope.getJugador = function () {
                if ($routeParams.idJugador != undefined) {
                    panelControlService.getJugador($routeParams.idJugador).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.jugador = json;
                        console.log(json);
                        $scope.jugador.handicap = $filter('filter')($scope.jugador.thandicap_usuario, { "bool_principal": true }, true)[0];
                        $scope.jugador.club_principal = $filter('filter')($scope.jugador.tuser_club, { "bool_principal": true }, true)[0];
                        $scope.jugadorHabilitado();
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.jugadorHabilitado = function () {
                angular.forEach($scope.jugador.thistorial_pagos, function (key) {
                    if (key.cod_tipo_pago == 1) {
                        var newDate = new Date();
                        var dateVencimiento = new Date(key.fec_vencimiento);
                        if (dateVencimiento > newDate)
                            $scope.jugador.isPago = true;
                    }
                });
            }
        }]);