﻿var app = angular.module('perfilPublicoJugador.ctrl', [])
    .controller('perfilPublicoJugadorListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'perfilPublicoJugadorService',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, perfilPublicoJugadorService, panelControlService, $timeout) {
            $scope.isLoading = false;
            $scope.page = 1;
            $scope.filter = {
                nombre_completo: "",
                cod_club: 0,
                cod_tipo_jugador: 0,
            };
            $scope.filterOriginal = {
                nombre_completo: "",
                cod_club: 0,
                cod_tipo_jugador: 0,
            };

            $scope.init = function () {
                $scope.getJugadores();
                $scope.getTipos();
            }


            $scope.getPagination = function () {
                var count = $scope.pagesJugadores;
                $scope.pages = Math.ceil(count / 12);
            }


            $scope.beforePage = function () {
                $scope.page = $scope.page - 1;
                $timeout(function () {
                    $scope.paginate($scope.page);
                }, 100)
            }

            $scope.nextPage = function () {
                $scope.page = $scope.page + 1;
                $timeout(function () {
                    $scope.paginate($scope.page);
                }, 100)
            }

            $scope.paginate = function (page) {
                if (!$scope.isFiltered)
                    $scope.getJugadores(page);
                else
                    $scope.filtrarJugadores(page);
            }

            $scope.getTipos = function () {
                panelControlService.getTiposJugador().then(function (response) {
                    var json = JSON.parse(response.data);
                    //$scope.tiposJugador = json.tiposJugador;
                    $scope.tiposJugador = $filter("filter")(json.tiposJugador, { "txt_desc": '!' + 'Fallecido' }, true);
                    $scope.clubes = json.clubes;
                    $scope.clubes.push({ cod_club: 0, txt_desc: "Todos" });
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.filtrarJugadores = function (page) {
                $scope.isLoading = true;
                var sendHandicap = 0;
                if (page > $scope.pages) {
                    page = $scope.pages;
                }

                if ($scope.filterOriginal == $scope.filter) {
                    $scope.getJugadores(1);
                } else {
                    var send_handicap;
                    if ($scope.filter.nro_hand == undefined || $scope.filter.nro_hand == null || $scope.filter.nro_hand == '')
                        sendHandicap = -1;
                    else {
                        sendHandicap = angular.copy($scope.filter.nro_hand);
                    }

                    panelControlService.GetJugadoresFiltradosPublico($scope.page, $scope.filter.nombre_completo, $scope.filter.cod_tipo_jugador, $scope.filter.cod_club, sendHandicap).then(function (response) {
                        var json = JSON.parse(response.data);

                        $scope.jugadores = json.usersDTO;
                        $scope.pagesJugadores = json.pages;
                        $scope.getPagination();
                        $scope.isFiltered = true;
                        $scope.page = page;
                        $scope.isLoading = false;
                        angular.forEach($scope.jugadores, function (key, value) {
                            key.handicap = $filter('filter')(key.thandicap_usuario, { "bool_principal": true }, true)[0];
                            key.nombre_completo = key.txt_nombre + " " + key.txt_apellido;
                        });
                    }).catch(function (error) {
                        console.log(error);
                    });
                }

            }

            $scope.getJugadores = function () {
                $scope.isLoading = true;
                perfilPublicoJugadorService.getJugadoresDestacados($scope.page).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.jugadoresOriginal = angular.copy(json.usersDTO);
                    $scope.jugadores = angular.copy(json.usersDTO);

                    $scope.pagesJugadores = 0;
                    //$scope.getPagination();
                    angular.forEach($scope.jugadores, function (key, value) {
                        if (key.thandicap_usuario != undefined) {
                            key.handicap = $filter('filter')(key.thandicap_usuario, { "bool_principal": true }, true)[0];
                            key.nombre_completo = key.txt_nombre + " " + key.txt_apellido;
                            $scope.jugadoresOriginal[value].handicap = key.handicap;
                            $scope.jugadoresOriginal[value].nombre_completo = key.nombre_completo;
                        }
                    });
                    $scope.isLoading = false;
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }]);