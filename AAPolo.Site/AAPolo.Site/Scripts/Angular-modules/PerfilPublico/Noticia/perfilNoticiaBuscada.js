﻿var app = angular.module('perfilPublicoNoticiaBuscada.ctrl', [])
    .controller('perfilPublicoNoticiaBuscadaCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        '$sce',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout, $sce) { 
            $scope.noticia = {
                imgPrincipal : null
            };
            $scope.isLoading = false;
            $scope.init = function () {
                $scope.getCategorias();
                //$scope.getEtiquetas();
                //$scope.getNoticia();
                //$scope.getTorneo();
            }

            $scope.trustAsHtml = function (string) {
                return $sce.trustAsHtml(string);
            };

            $scope.getCategorias = function () {
                $scope.isLoading = true;
                panelControlService.getCategoriasNoticia().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categorias = json;
                    $scope.getEtiquetas();
                });
            }
            $scope.getEtiquetas = function () {
                panelControlService.getEtiquetasNoticia().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.etiquetas = json;
                    $scope.getNoticia();
                });
            }
            $scope.getNoticia = function () {
                if ($routeParams.idNoticia != undefined) {
                    panelControlService.getNoticia($routeParams.idNoticia).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.noticia = json;
                        $scope.noticia.cod_etiqueta = [];
                        $scope.noticia.cod_tipo_noticia = parseInt($scope.noticia.cod_tipo_noticia);

                        angular.forEach($scope.noticia.tnoticia_etiqueta, function (key) {
                            $scope.noticia.cod_etiqueta.push(key.cod_etiqueta);
                        });
                        angular.forEach($scope.noticia.tnoticia_etiqueta, function (key) {
                            key.etiquetaNombre = $filter("filter")(angular.copy($scope.etiquetas), { "cod_etiqueta": key.cod_etiqueta}, true)[0].txt_desc;
                        });
                        $scope.noticia.categoriaNombre = $filter("filter")(angular.copy($scope.categorias), { "cod_tipo_noticia": $scope.noticia.cod_tipo_noticia }, true)[0].txt_desc;
                        
                        //angular.forEach($scope.noticia.tnoticia_archivo, function (key) {
                        $scope.noticia.imgPrincipal = $filter("filter")(angular.copy($scope.noticia.tnoticia_archivo), { "orden": 1 }, true)[0].url_archivo;
                        //$scope.noticia.imgSlideshow = $filter("filter")(angular.copy($scope.noticia.tnoticia_archivo), { "orden": 2 }, true)[0].url_archivo;
                        try {
                            $scope.noticia.imgSlideshow = $filter("filter")(angular.copy($scope.noticia.tnoticia_archivo), { "orden": 2 }, true)[0].url_archivo;
                        } catch (err) {
                            $scope.noticia.imgSlideshow = undefined;
                        }
                        try {
                            $scope.noticia.urlVideo = $filter("filter")(angular.copy($scope.noticia.tnoticia_archivo), { "orden": 3 }, true)[0].url_archivo;
                        } catch (err) {
                            $scope.noticia.urlVideo = undefined;
                        }
                        //});
                            //key.imgSlideshow = $filter("filter")(angular.copy($scope.lstNoticias.tnoticia_archivo), { "orden": 1 }, true)[0].url_archivo;
                        
                        
                        panelControlService.getNoticiasRelacionadas($routeParams.idNoticia).then(function (response) {
                            var json = JSON.parse(response.data);
                            $scope.lstRelacionadas = json;
                            angular.forEach($scope.lstRelacionadas, function (key) {
                                key.categoriaNombre = $filter("filter")(angular.copy($scope.categorias), { "cod_tipo_noticia": key.cod_tipo_noticia }, true)[0].txt_desc;
                            });
                            angular.forEach($scope.lstRelacionadas, function (key) {
                                key.imgPrincipal = $filter("filter")(angular.copy(key.tnoticia_archivo), { "orden": 1 }, true)[0].url_archivo;
                                //key.imgSlideshow = $filter("filter")(angular.copy(key.tnoticia_archivo), { "orden": 2 }, true)[0].url_archivo;
                                try {
                                    key.imgSlideshow = $filter("filter")(angular.copy(key.tnoticia_archivo), { "orden": 2 }, true)[0].url_archivo;
                                } catch (err) {
                                    key.imgSlideshow = undefined;
                                }
                                try {
                                    key.urlVideo = $filter("filter")(angular.copy(key.tnoticia_archivo), { "orden": 3 }, true)[0].url_archivo;
                                } catch (err) {
                                    key.urlVideo = undefined;
                                }
                                //key.imgSlideshow = $filter("filter")(angular.copy($scope.lstNoticias.tnoticia_archivo), { "orden": 1 }, true)[0].url_archivo;
                            });

                            //angular.forEach($scope.lstRelacionadas.tnoticia_etiqueta, function (key) {
                            //    key.etiquetaNombre = $filter("filter")(angular.copy($scope.etiquetas), { "cod_etiqueta": key.cod_etiqueta }, true)[0].txt_desc;
                            //});
                            //$scope.noticia.categoriaNombre = $filter("filter")(angular.copy($scope.categorias), { "cod_tipo_noticia": $scope.noticia.cod_tipo_noticia }, true)[0].txt_desc;
                            $timeout(function () {
                                fixHeight(".noticia");
                                $scope.isLoading = false;
                            }, 500);
                        });

                    }).catch(function (error) {
                        console.log(error);
                    });
                } else {
                    location.href = '/PerfilPublico/#/Noticias';
                }

            }

            $scope.initTooltip = function () {
                $('.has-tooltip').tooltip();
                //fixHeight('.noticia');
                //$(window).resize(function () {
                //    fixHeight('.noticia');
                //});
            }
        }]);