﻿var app = angular.module('perfilPublicoNoticia.ctrl', [])
    .controller('perfilPublicoNoticiaListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.buscado = {};
            $scope.isLoading = false;
            $scope.init = function () {
                $scope.getCategorias();
                $scope.getEtiquetas();
            }
            $scope.getCategorias = function () {
                panelControlService.getCategoriasNoticia().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categorias = json;
                    $scope.categorias.push({ cod_tipo_noticia: undefined, txt_desc: "Todas" });
                    $scope.getNoticias();
                });
            }
            $scope.getEtiquetas = function () {
                panelControlService.getEtiquetasNoticia().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.etiquetas = json;
                    $scope.etiquetas.push({ cod_etiqueta: undefined, txt_desc: "Todas" });
                });
            }
            $scope.getNoticias = function () {
                $scope.isLoading = true;
                panelControlService.GetNoticiasActivas().then(function (response) {
                    var json = JSON.parse(response.data);
                    $timeout(function () {
                        fixHeight(".noticia");
                    }, 500);
                    $scope.lstNoticias = $filter('filter')(angular.copy(json), { 'bool_activo': '!'+ 0 });
                    $scope.lstNoticiasOriginal = angular.copy(json);
                    angular.forEach($scope.lstNoticias, function (key) {
                        key.categoriaNombre = $filter("filter")(angular.copy($scope.categorias), { "cod_tipo_noticia": key.cod_tipo_noticia }, true)[0].txt_desc;
                    });
                    angular.forEach($scope.lstNoticias, function (key) {
                        if (key.url_imagen != null)
                            key.imgPrincipal = key.url_imagen;

                        if (key.url_slideshow != null)
                            key.imgSlideshow = key.url_slideshow;

                        if (key.url_video != null)
                            key.urlVideo = key.url_video;
                    });
                    $scope.isLoading = false;
                });

            }
            $scope.filtrarNoticias = function () {
                var filtered = false;
                if ($scope.filter.cod_tipo_noticia == null) {
                    $scope.filter.cod_tipo_noticia = undefined;
                }
                if ($scope.filter.cod_etiqueta == null) {
                    $scope.filter.cod_etiqueta = undefined;
                }

                $scope.buscado = angular.copy($scope.filter);
            }

            $scope.initTooltip = function () {
                $('.has-tooltip').tooltip();
            //    fixHeight('.noticia');
            }

        }]);