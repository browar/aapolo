﻿var app = angular.module('perfilPublicoTorneo.ctrl', [])
    .controller('perfilPublicoTorneoListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.filter = {
                handicap: {},
                tuser_club: []
            }
            $scope.eventosCalendar = [];
            $scope.torneosActivos = [];
            $scope.torneos = [];
            $scope.filtro = {};
            $scope.isLoading = false;


            $scope.init = function () {
                $scope.getTorneos();
                $scope.getEventos();
                $scope.getCategorias();
                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',                        
                        format: 'd-m-yyyy',
                        max: true,
                        //selectYears: true,
                        selectMonths: true,
                        selectYears:100
                    });
                }, 600);
            }

            $scope.getEventos = function () {
                panelControlService.getEventos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.eventos = $filter('filter')(json, { "bool_activo": true });;
                    angular.forEach($scope.eventos, function (key) {
                        var dateNew = $filter('date')(key.fec_comienzo, "yyyy-MM-dd");
                        if (key.cod_tipo_evento == 1) {
                            $scope.evento = {
                                title: "" + key.txt_desc,
                                start: "" + dateNew,
                                end: "" + dateNew,
                                url: "/PerfilPublico/#/Torneo/" + key.cod_torneo,
                                editable: false,
                                allDay: true,
                                className: "calendar-torneo"
                            };
                        } else {
                            $scope.evento = {
                                title: "" + key.txt_desc,
                                start: "" + dateNew,
                                end: "" + dateNew,
                                url: "javascript:void(0)",
                                editable: false,
                                allDay: true,
                                className: "calendar-evento"
                            };
                        }
                        $scope.eventosCalendar.push($scope.evento);
                        $('#calendar').fullCalendar('renderEvent', $scope.evento, true);
                    });
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.renderCalendar = function () {
                $('#calendar').fullCalendar({
                    eventLimit: true, // allow "more" link when too many events
                    events: $scope.eventosCalendar,
                    displayEventTime: false,
                    timeFormat: ' '
                });
            }

            $scope.getCategorias = function () {
                panelControlService.getTorneoTipos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categoriasHandicap = json.categoriasHandicap;
                    $scope.categoriasTorneo = json.categoriasTorneo;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getTorneos = function () {
                $scope.isLoading = true;
                var dateNow = new Date();
                panelControlService.getTorneos().then(function (response) {
                    var json = JSON.parse(response.data);
                    angular.forEach(json.torneosDTO, function (key) {
                        var dateParse = new Date(key.fec_comienzo);
                        key.txt_imagen = key.txt_imagen.replace("C:/Workspace/AAPolo.Site/AAPolo.Site", "");
                        if (!key.bool_inscripcion) {
                            $scope.torneosActivos.push(key);
                        } else {
                            if(key.bool_activo)
                                $scope.torneos.push(key);
                        }
                    });
                    $scope.isLoading = false;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.buscar = function () {
                $scope.filtro = angular.copy($scope.buscado);
            }

        }]);