﻿var app = angular.module('perfilPublicoTorneoBuscado.ctrl', [])
    .controller('perfilPublicoTorneoBuscadoCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.isLoading = false;
            $scope.init = function () {
                $scope.getTorneo();
                $scope.initLogin();
            }

            $scope.initLogin = function () {
                panelControlService.getUser().then(function (response) {
                    $scope.user = JSON.parse(response.data);
                    if ($scope.user == null || $scope.user == undefined) {
                        panelControlService.getJugadorAutenticado().then(function (response) {
                            $scope.user = JSON.parse(response.data);
                            if (($scope.user == null || $scope.user == undefined) && window.location.pathname != "/Home/Index" && !window.location.pathname.includes("/PerfilPublico/")) {
                                window.location.href = "/Home/Index";
                            } else {
                                $scope.user.txt_usuario = $scope.user.txt_nombre + " " + $scope.user.txt_apellido;
                            }
                        }).catch(function (error) {
                            console.log(error);
                        });
                    }
                    console.log($scope.user);
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getTorneo = function () {
                if ($routeParams.idTorneo != undefined) {
                    $scope.isLoading = true;
                    panelControlService.getTorneo($routeParams.idTorneo).then(function (response) {
                        $scope.isLoading = false;
                        var json = JSON.parse(response.data);
                        $scope.torneo = json;
                        console.log($scope.torneo);
                        $scope.torneo.txt_imagen = $scope.torneo.txt_imagen.replace("C:/Workspace/AAPolo.Site/AAPolo.Site", "");
                    }).catch(function (error) {
                        $scope.isLoading = false;
                        console.log(error);
                    });
                }
            }


            $scope.verifyTorneo = function () {
                if ($scope.user != null && $scope.user != undefined) {
                    $scope.goTorneo();
                } else {
                    $(".btn-login").click();
                }
            }

            $scope.goTorneo = function () {
                window.location.href = "/PerfilJugador/#/" + $scope.user.cod_usuario + "/Torneos/" + $scope. torneo.cod_torneo;
            }

        }]);