﻿var app = angular.module('perfilPublicoClubBuscado.ctrl', [])
    .controller('perfilPublicoClubBuscadoCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.filter = {
                handicap: {},
                tuser_club: []
            }

            $scope.init = function () {
                $scope.getClub();
                $scope.getDestacados();
                $scope.getTipos();
            }

            $scope.getClub = function () {
                if ($routeParams.idClub != undefined) {
                    panelControlService.getClub($routeParams.idClub).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.club = json;
                        $scope.calcularFechaFundacion();
                        var findTelefono = $filter('filter')($scope.club.tcontacto_club, { "$": "Telefono" }, true)[0];
                        if (findTelefono != undefined)
                            $scope.club.nro_telefono = parseInt(findTelefono.tcontacto.txt_desc);
                        var findMail = $filter('filter')($scope.club.tcontacto_club, { "$": "Email" }, true)[0];
                        if (findMail != undefined)
                            $scope.club.txt_mail = findMail.tcontacto.txt_desc;
                     
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.getTipos = function () {
                panelControlService.getTiposClub().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categoriaClubes = json.categoriaClubes;
                    $scope.provincias = json.provincias;
                    angular.forEach($scope.usuarios, function (key, value) {
                        key.handicap = $filter('filter')(key.thandicap_usuario, { "bool_principal": true }, true)[0];
                        key.nombre_completo = key.txt_nombre + " " + key.txt_apellido;                   
                    });
                }).catch(function (error) {
                    console.log(error);
                });
            }


            $scope.getDestacados = function () {
                panelControlService.getDestacadosByClub($routeParams.idClub).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.usuarios = json;
                    angular.forEach($scope.usuarios, function (key) {
                        key.handicap = $filter('filter')(key.thandicap_usuario, { "bool_principal": true }, true)[0];
                    })
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.calcularFechaFundacion = function () {
                var fec_fundacion = $filter('date')($scope.club.fec_fundacion, 'yyyyMMdd');
                moment.locale("es");
                var date = new Date();
                $scope.club.anio_fundacion = moment(date).year() - moment(fec_fundacion).year();
            }
        }]).filter("emptyToEnd", function () {
            return function (array, key) {
                if (!angular.isArray(array)) return;
                var present = array.filter(function (item) {
                    return item[key];
                });
                var empty = array.filter(function (item) {
                    return !item[key]
                });
                return present.concat(empty);
            };
        });;