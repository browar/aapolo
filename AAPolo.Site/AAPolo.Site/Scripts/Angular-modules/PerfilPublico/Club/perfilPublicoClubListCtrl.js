﻿var app = angular.module('perfilPublicoClub.ctrl', [])
    .controller('perfilPublicoClubListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.filterOriginal = {
                txt_desc: "",
                cod_categoria_club: 0
            };

            $scope.filter = {
                txt_desc: "",
                cod_categoria_club: 0
            };

            $scope.isLoading = false;

            $scope.filtro = {};

            $scope.init = function () {
                $scope.getClubes();
                $scope.getTipos();
            }

            $scope.getClubes = function () {
                $scope.isLoading = true;
                panelControlService.getClubes(1).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.clubes = $filter('filter')(json.clubesDTO, { "bool_activo": true });
                    $scope.clubes = $filter('filter')($scope.clubes, { "cod_categoria_club": '!' + 1 });
                    $scope.isLoading = false;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getTipos = function () {
                panelControlService.getTiposClub().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categoriaClubes = json.categoriaClubes;
                    $scope.usuarios = json.usuarios;
                    $scope.provincias = json.provincias;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.filterClubes = function (page) {
                $scope.isLoading = true;
                if ($scope.filterOriginal == $scope.filter) {
                    $scope.getClubes(page);
                } else {
                    panelControlService.getClubesFiltrados(1, $scope.filter.txt_desc, $scope.filter.cod_categoria_club).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.clubes = $filter('filter')(json.clubesDTO, { "bool_activo": true });
                        $scope.clubes = $filter('filter')($scope.clubes, { "cod_categoria_club": '!' + 1 });
                        $scope.isFiltered = true;
                        $scope.isLoading = false;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

        }]);