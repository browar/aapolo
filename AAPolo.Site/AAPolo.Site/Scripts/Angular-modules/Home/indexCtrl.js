﻿var app = angular.module('index.ctrl', [])
    .controller('indexCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'indexService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, indexService, $timeout) {
            $scope.eventosCalendar = [];
            $scope.torneosActivos = [];
            $scope.torneos = [];
            $scope.init = function () {
                $scope.isMobile = false;
                var idioma = localStorage.getItem("idioma");
                $scope.isMobile = false;
            }

            $(".chooseLanguage li a").click(function () {
                $timeout(function () {
                    $scope.idiomaElegido = localStorage.getItem('idioma');
                }, 600);
            });

            $scope.initCalendario = function () {
                var idioma = localStorage.getItem("idioma");
                if (idioma != undefined)
                    $scope.idiomaElegido = idioma;
                else {
                    $scope.idiomaElegido = 'es';
                    location.reload();
                }

                $scope.isMobile = false;
                $scope.getTorneos();
                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',
                        format: 'd-m-yyyy',
                        max: true,
                        //selectYears: true,
                        selectMonths: true,
                        selectYears: 100
                    });
                }, 600);
            }

            $scope.getEventos = function () {
                indexService.getEventos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.eventos = json;
                    $scope.eventos = $filter('filter')(json, { "bool_activo": true });;
                    angular.forEach($scope.eventos, function (key) {
                        var dateNew = $filter('date')(key.fec_comienzo, "yyyy-MM-dd");
                        if (key.cod_tipo_evento == 1) {
                            $scope.evento = {
                                title: "" + key.txt_desc,
                                start: "" + dateNew,
                                end: "" + dateNew,
                                url: "/PerfilPublico/#/Torneo/" + key.cod_torneo,
                                editable: false,
                                allDay: true,
                                className: "calendar-torneo"
                            };
                        } else {
                            $scope.evento = {
                                title: "" + key.txt_desc,
                                start: "" + dateNew,
                                end: "" + dateNew,
                                url: "javascript:void(0)",
                                editable: false,
                                allDay: true,
                                className: "calendar-evento"
                            };
                        }
                        $scope.eventosCalendar.push($scope.evento);
                        $('#calendar').fullCalendar('renderEvent', $scope.evento, true);
                        $('.containterCalendario').removeClass('loading-main');
                    });
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.renderCalendar = function () {
                $('#calendar').fullCalendar({
                    eventLimit: true, // allow "more" link when too many events
                    events: $scope.eventosCalendar,
                    displayEventTime: false,
                    timeFormat: ' '
                });
            }

            $scope.getTorneos = function () {
                var dateNow = new Date();
                indexService.getTorneos().then(function (response) {
                    var json = JSON.parse(response.data);
                    angular.forEach(json, function (key) {
                        var dateParse = new Date(key.fec_comienzo);
                        key.txt_imagen = key.txt_imagen.replace("C:/Workspace/AAPolo.Site/AAPolo.Site", "");
                        if (!key.bool_inscripcion) {
                            $scope.torneosActivos.push(key);
                        } else {
                            if (key.bool_activo)
                                $scope.torneos.push(key);
                        }

                        if (key.txt_imagen == "No tiene") {
                            key.txt_imagen = "../img/img-default.jpg";
                        }

                    });
                    
                    $scope.getCategorias();
                }).catch(function (error) {
                    console.log(error);
                });
            }


            $scope.getCategorias = function () {
                indexService.getCategoriasNoticia().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categorias = json; 
                    $scope.getNoticias();
                });
            }
            $scope.getEtiquetas = function () {
                indexService.getEtiquetasNoticia().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.etiquetas = json;
                    $scope.getEventos();
                });
            }
            $scope.getNoticias = function () {
                indexService.getNoticiasDestacadas().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.lstNoticias = $filter('filter')(angular.copy(json), { 'bool_activo': '!' + 0 }); $timeout(function () {
                        fixHeight('.noticia');
                    }, 1000);
                    if ($scope.lstNoticias.length == 0) {
                        $('#Index').remove();
                    }

                    $scope.getEtiquetas();

                    angular.forEach($scope.lstNoticias, function (key) {
                        key.categoriaNombre = $filter("filter")(angular.copy($scope.categorias), { "cod_tipo_noticia": key.cod_tipo_noticia }, true)[0].txt_desc;
                    });
                    angular.forEach($scope.lstNoticias, function (key) {
                        if (key.url_imagen != null)
                            key.imgPrincipal = key.url_imagen;

                        if (key.url_slideshow != null)
                            key.imgSlideshow = key.url_slideshow;

                        if (key.url_video != null)
                            key.urlVideo = key.url_video;
                    });
                    $('#Index').removeClass('loading-main');
                    $('#Index div').removeClass('loading-main');

                });


            }

            $scope.initTooltip = function () {
                $('.has-tooltip').tooltip();
                //fixHeight('.noticia');
            }

        }]);