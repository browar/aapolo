﻿
var app = angular.module('perfilJugador.ctrl', [])
    .controller('perfilJugadorListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'perfilJugadorService',
        'panelControlService',
        '$timeout',
        'Upload',
        function ($scope, $filter, $routeParams, $location, perfilJugadorService, panelControlService, $timeout, Upload) {
            $scope.accion = "Default";
            $scope.torneos = [];
            $scope.pagos = [];
            $scope.thereIsError = false;
            $scope.thereIsWarning = false;
            $scope.searchFilterUno = "";
            $scope.terminosYCondiciones = true;
            $scope.perfil = {};
            $scope.errorPase = false;
            $scope.montoTotal = 0;
            $scope.selectedUno = [parseInt($routeParams.idJugador)];
            $scope.selectedDos = [parseInt($routeParams.idJugador)];
            $scope.selectedTres = [parseInt($routeParams.idJugador)];
            $scope.selectedCuatro = [parseInt($routeParams.idJugador)];
            $scope.selectedCinco = [parseInt($routeParams.idJugador)];
            $scope.finalizoCargaPagos = false;
            $scope.montoTotalPase = 0;
            $scope.jugadoresAPagar = [];
            $scope.codesFiltered = [];
            $scope.respuesta = {};
            $scope.selected = [30];
            $scope.isLoading = false;
            $scope.tequipo = {
                tequipo_user: []
            }

            function getValue() {
                return window.localStorage.getItem('idioma');
            }

            $scope.$watch(getValue, function (newValue, oldValue) {
                if (oldValue !== newValue)
                    $scope.idiomaElegido = newValue;

                $(".dropdown-abierto").click();
            });


            $(".chooseLanguage li a").click(function () {
                $(".profile-nav .current a").click();
            });

            $scope.jugadoresAPagarPase = [];
            $scope.jugadorIds = [];
            $scope.datosMercadoPago = {
                lstItem: []
            };

            $scope.eventosCalendar = [];
            $scope.initValues = function () {
                var idioma = localStorage.getItem("idioma");
                $scope.idiomaElegido = idioma;
                $scope.verifyMiembroLogueado();
                //var actualizaClub = sessionStorage.getItem("viewPopUpActualizar");
                //if (actualizaClub != "true")
                //    clickModal('avisoActualizarClub');

                sessionStorage.setItem("viewPopUpActualizar", true);
                if ($routeParams.resultadoPago != undefined)
                    $scope.accion = $routeParams.resultadoPago;
                $scope.idJugador = parseInt($routeParams.idJugador);
                $scope.getTorneos();
                //$scope.getJugadores(1);
                $scope.getNotificaciones();
                $scope.getAllClubes();
                $scope.getHandicaps();
                $scope.getJugador();
                $scope.getTipos();
                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',
                        format: 'd-m-yyyy',
                        max: true,
                        //selectYears: true,
                        selectMonths: true,
                        selectYears: 100
                    });
                    $('.select2-basic').select2();
                    $('.select2-tags').select2(
                        {
                            tags: true
                        }
                    );
                    $scope.getEventos();
                }, 1000);
            }

            $scope.verifyMiembroLogueado = function () {
                perfilJugadorService.getMiembroLogueado($routeParams.idJugador).then(function (response) {
                    var json = JSON.parse(response.data);
                    if (json.ExceptionMessage != "Error") {
                        $scope.isMember = true;
                    } else {
                        $scope.isMember = false;
                    }

                }).catch(function (error) {
                    console.log(error);
                });
            }


            $scope.subirImagen = function () {
                $("#fileLoad").click();
            }

            $scope.upload = function (file) {
                $scope.perfil.txt_imagen = "reemplazarEste";
                Upload.upload({
                    url: '/PanelControl/EditarJugador',
                    data: { id: $routeParams.idJugador, jugadorDTO: JSON.stringify($scope.perfil) },
                    file: file, // or list of files ($files) for html5 only
                    //}).progress(function (evt) {                       
                }).success(function (response) {
                    if (response != null) {
                        $timeout(function () {
                            $scope.getJugador();
                        }, 400);
                    }
                }).error(function (err) {
                });
            }

            $scope.getNotificaciones = function () {
                perfilJugadorService.getNotificacionesRecibidas().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.notificacionesDeAAP = json;
                    $scope.notificacionElegida = $scope.notificacionesDeAAP[0];
                    perfilJugadorService.getRespuestas($routeParams.idJugador).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.notificacionesEnviadasPorMi = json.enviados;
                        $scope.recibidas = json.recibidos;
                        $scope.notificacionEnviadaElegida = $scope.notificacionesEnviadasPorMi[0];
                        angular.forEach($scope.notificacionesEnviadasPorMi, function (key, value) {
                            key.tnotificacion = $filter("filter")(angular.copy($scope.notificacionesDeAAP), { "cod_notificacion": key.cod_notificacion })[0];
                        });

                        angular.forEach($scope.recibidas, function (key, value) {
                            var notificacion = $filter("filter")(angular.copy($scope.notificacionesDeAAP), { "cod_notificacion": key.cod_notificacion })[0];
                            key.txt_asunto = notificacion.txt_asunto;
                            key.tnotificacion = notificacion;
                            $scope.notificacionesDeAAP.push(key);
                        });

                    }).catch(function (error) {
                        console.log(error);
                    });

                    if ($routeParams.codTorneo != undefined && $routeParams.resultadoPago == 'inbox') {
                        var notificacion = $filter('filter')(angular.copy($scope.notificacionesDeAAP), { "cod_notificacion": parseInt($routeParams.codTorneo) }, true)[0];
                        $scope.notificacionElegida = notificacion;
                        $scope.inbox = "accion";
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.populateNotificacion = function (notificacion) {
                $scope.notificacionElegida = notificacion;
            }

            $scope.uploadNotificacion = function (file) {
                $scope.fileComprobante = file;
            }

            $scope.enviarRespuesta = function () {
                $scope.respuesta.cod_notificacion = $scope.notificacionElegida.cod_notificacion;
                $scope.respuesta.cod_usuario_envia = parseInt($routeParams.idJugador);
                $scope.respuesta.trespuesta_adjunto = [];
                $scope.respuesta.trespuesta_adjunto.push({ txt_url_image: "reemplazarEsteAdjunto" });
                Upload.upload({
                    url: '/PerfilJugador/EnviarRespuesta',
                    data: { respuesta: JSON.stringify($scope.respuesta) },
                    file: $scope.fileComprobante, // or list of files ($files) for html5 only
                    //}).progress(function (evt) {                       
                }).success(function (response) {
                    $scope.respuesta = {};
                    $scope.getNotificaciones();
                }).error(function (err) {
                });
            }

            $scope.getUsuario = function (key, flagPago, isPase) {
                $scope.isLoading = true;
                var index = angular.copy(key.index);
                var select = angular.copy(key.select);
                if (!key.cached) {
                    panelControlService.getJugador(key.cod_usuario).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.isLoading = false;
                        key = json;
                        key.pagoSeleccionado = false;
                        if (key.thandicap_usuario != undefined) {
                            key.handicap = $filter('filter')(angular.copy(key.thandicap_usuario), { "bool_principal": true }, true)[0];
                            if (key.handicap != undefined)
                                key.categoria_handicap = key.handicap.cod_categoria_handicap;

                            key.handicapFemenino = $filter('filter')(angular.copy(key.thandicap_usuario), { "bool_principal": false }, true)[0];
                        }

                        if (select == 'Uno') {
                            $scope.perfilSeleccionadoUno = key;
                            var jugador = $filter("filter")($scope.jugadoresUno, { "cod_usuario": key.cod_usuario }, true)[0];
                            jugador.handicap = key.handicap;
                            jugador.cached = true;
                        }

                        if (select == 'Dos') {
                            $scope.perfilSeleccionadoDos = key;
                            var jugador = $filter("filter")($scope.jugadoresDos, { "cod_usuario": key.cod_usuario }, true)[0];
                            jugador.handicap = key.handicap;
                            jugador.cached = true;
                        }

                        if (select == 'Tres') {
                            $scope.perfilSeleccionadoTres = key;
                            var jugador = $filter("filter")($scope.jugadoresTres, { "cod_usuario": key.cod_usuario }, true)[0];
                            jugador.handicap = key.handicap;
                            jugador.cached = true;
                        }

                        if (select == 'Cuatro') {
                            $scope.perfilSeleccionadoCuatro = key;
                            var jugador = $filter("filter")($scope.jugadoresCuatro, { "cod_usuario": key.cod_usuario }, true)[0];
                            jugador.handicap = key.handicap;
                            jugador.cached = true;
                        }

                        if (select == 'Cinco') {
                            $scope.perfilSeleccionadoCinco = key;
                            var jugador = $filter("filter")($scope.jugadoresCinco, { "cod_usuario": key.cod_usuario }, true)[0];
                            jugador.handicap = key.handicap;
                            jugador.cached = true;
                        }


                        if (flagPago && !isPase) {
                            var jugador = $filter('filter')(angular.copy($scope.jugadoresHandicap), { "cod_usuario": $scope.jugadorAPagarSelect }, true)[0];
                            jugador = json;
                            if ($filter('filter')(angular.copy($scope.jugadoresAPagar), { "cod_usuario": $scope.jugadorAPagarSelect }, true).length == 0) {
                                jugador.copyHandicap = angular.copy(jugador.handicap);
                                if (jugador.copyHandicap != undefined)
                                    $scope.jugadoresAPagar.push(jugador);

                                if (jugador.handicap == undefined) {
                                    jugador.handicap = $filter('filter')(angular.copy(jugador.thandicap_usuario), { "bool_principal": true }, true)[0];
                                }
                            }

                        } else if (isPase) {
                            var jugador = $filter('filter')(angular.copy($scope.jugadoresHandicapPase), { "cod_usuario": $scope.jugadorAPagarPaseSelect }, true)[0];
                            jugador = json;
                            if (jugador.thandicap_usuario[0] != undefined) {
                                $scope.errorPaseHandicap = false;
                                if (jugador.handicap == undefined) {
                                    jugador.handicap = $filter('filter')(angular.copy(jugador.thandicap_usuario), { "bool_principal": true }, true)[0];
                                }
                                if (jugador.handicap.thandicap.cod_categoria_handicap != 1) {
                                    if ($filter('filter')(angular.copy($scope.jugadoresAPagarPase), { "cod_usuario": $scope.jugadorAPagarPaseSelect }, true).length == 0) {
                                        $scope.jugadoresAPagarPase.push(jugador);
                                    }
                                    $scope.errorPase = false;
                                } else {
                                    $scope.verifyPasePago(jugador);
                                }
                            } else {
                                $scope.errorPaseHandicap = true;
                            }
                        }

                        var club = $filter("filter")(key.tuser_club, { "bool_principal": true })[0];

                        if (club != undefined) {
                            key.cod_club_principal = club.cod_club;
                            key.clubEditable = false;
                            var newDate = new Date();
                            var dateVigencia = new Date();
                            var pagos = key.thistorial_pagos.filter(function (x) {
                                return moment(x.fec_vencimiento) > newDate && x.cod_tipo_pago == 1 && x.nro_resultado_pago == 1;
                            });
                            if (pagos.length > 0) {
                                if (club.fec_vigencia != null) {
                                    if (dateVigencia < newDate) {
                                        key.clubEditable = true;
                                        key.cod_club_principal = null;
                                    }
                                } else {
                                    key.clubEditable = true;
                                    key.cod_club_principal = null;
                                }
                            } else {
                                key.clubEditable = true;
                                key.cod_club_principal = null;
                            }
                        } else {
                            key.clubEditable = true;
                            key.cod_club_principal = null;
                        }

                        $timeout(function () {
                            $('.select2-basic').select2();
                            $('.select2-tags').select2(
                                {
                                    tags: true
                                }
                            );

                        }, 400);

                        $timeout(function () {
                            $('.select2-basic').select2();
                            $('.select2-tags').select2(
                                {
                                    tags: true
                                }
                            );
                            $('.select2-basic').select2().trigger('change');
                        }, 600);

                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.verifyPasePago = function (jugador) {
                perfilJugadorService.getUsuarioSinPago(jugador.cod_usuario).then(function (response) {
                    var json = JSON.parse(response.data);

                    if (json == null)
                        $scope.errorPase = true;
                    else {
                        var handicap = $filter('filter')($scope.handicaps, { "cod_handicap": json.cod_handicap }, true)[0];
                        if (handicap.cod_categoria_handicap != 1) {
                            jugador.handicap = json;
                            jugador.handicap.thandicap = handicap;
                            $scope.jugadoresAPagarPase.push(jugador);
                            $scope.errorPase = false;
                        } else {
                            $scope.errorPase = true;
                        }
                    }


                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getTipos = function () {
                $scope.isLoading = true;
                panelControlService.getTiposJugador().then(function (response) {
                    $scope.isLoading = false;
                    var json = JSON.parse(response.data);
                    $scope.clubes = json.clubes;

                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getEventos = function () {
                panelControlService.getEventos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.eventos = json;
                    angular.forEach($scope.eventos, function (key) {
                        var dateNew = $filter('date')(key.fec_comienzo, "yyyy-MM-dd");
                        if (key.cod_tipo_evento == 1) {
                            $scope.evento = {
                                title: "" + key.txt_desc,
                                start: "" + dateNew,
                                end: "" + dateNew,
                                url: "/PerfilPublico/#/Torneo/" + key.cod_torneo,
                                editable: false,
                                allDay: true,
                                className: "calendar-torneo"
                            };
                        } else {
                            $scope.evento = {
                                title: "" + key.txt_desc,
                                start: "" + dateNew,
                                end: "" + dateNew,
                                url: "javascript:void(0)",
                                editable: false,
                                allDay: true,
                                className: "calendar-evento"
                            };
                        }

                        $scope.eventosCalendar.push($scope.evento);
                        $('#calendar').fullCalendar('renderEvent', $scope.evento, true);
                    });
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.limpiarModalPago = function (modal) {
                $scope.jugadoresAPagar = [];
                $scope.jugadoresAPagarPase = [];
                $scope.searchFilterHandicap = "";
                $scope.jugadoresHandicap = [];
                $scope.jugadoresHandicapPase = [];
                $scope.errorPago = false;
                $scope.errorPase = false;
                $timeout(function () {
                    $scope.verifyFecNac(modal);
                }, 200);
            }

            $scope.getHistorialPago = function () {
                if ($scope.perfil.thistorial_pagos != undefined) {
                    if ($scope.perfil.thistorial_pagos.length > 0) {
                        //angular.forEach($scope.perfil.thistorial_pagos, function (key) {
                        //    var jugador = $filter('filter')(angular.copy($scope.jugadores), { "cod_usuario": key.cod_usuario }, true)[0];
                        //    if (jugador != undefined)
                        //        key.txt_nombre_alta = jugador.txt_nombre + " " + jugador.txt_apellido
                        //});
                        var historialFiltrado = $filter('filter')(angular.copy($scope.perfil.thistorial_pagos), { "cod_usuario": $scope.idJugador }, true)[0];
                        if (historialFiltrado != undefined) {
                            $scope.fec_vencimiento = historialFiltrado.fec_vencimiento;
                            if (historialFiltrado.nro_resultado_pago == 1) {
                                $scope.isAlDia = true;
                            } else {
                                $scope.isAlDia = false;
                            }
                            $scope.finalizoCargaPagos = true;
                        }
                    }
                }
            }

            $scope.getTorneos = function () {
                perfilJugadorService.getTorneos().then(function (response) {
                    var json = JSON.parse(response.data);
                    var newDate = new Date();
                    $scope.torneosOriginal = json.torneosDTO;
                    var torneos = json.torneosDTO;
                    $scope.torneos = torneos.filter(function (x) {
                        return moment(x.fec_comienzo) > newDate && !x.bool_inscripcion_individual;
                    });

                    $scope.torneosIndividual = torneos.filter(function (x) {
                        return moment(x.fec_comienzo) > newDate && x.bool_inscripcion_individual;
                    });
                    $scope.categoriasHandicap = json.categoriaHandicapDTO;
                    if ($routeParams.codTorneo != undefined) {
                        var codTorneo = parseInt($routeParams.codTorneo);
                        $scope.torneoCod = $filter('filter')($scope.torneos, { "cod_torneo": codTorneo }, true)[0];
                        $scope.perfil.torneo = $scope.torneoCod;
                        $timeout(function () {
                            $('.select2-basic').select2().trigger('change');
                        }, 1200);
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getEquipo = function () {
                perfilJugadorService.getEquipo($routeParams.idJugador).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.equipos = json;
                    //$timeout(function () {
                    //    angular.forEach($scope.equipos, function (key) {
                    //        key.ttorneo = $filter("filter")(angular.copy($scope.torneos), { "cod_torneo": key.cod_torneo }, true)[0];
                    //        angular.forEach(key.tequipo_user, function (keyUser) {
                    //            keyUser.jugador = $filter('filter')(angular.copy($scope.jugadores), { "cod_usuario": keyUser.cod_usuario }, true)[0];
                    //        });
                    //    });
                    //}, 500);
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getHandicaps = function () {
                perfilJugadorService.getHandicaps().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.handicaps = json;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getTransicion = function (idHandicap, jugadorAPagar) {
                var handicapEncontrado = $filter('filter')(angular.copy($scope.handicaps), { "cod_handicap": idHandicap }, true)[0];
                if (!handicapEncontrado.txt_desc.includes("Temporario")) {
                    if (handicapEncontrado.txt_desc == jugadorAPagar.ttipo_jugador.txt_desc || jugadorAPagar.cod_tipo_jugador == 2)
                        return "" + handicapEncontrado.txt_desc + " " + handicapEncontrado.tcategoria_handicap.txt_desc;
                }
            }

            $scope.getAllClubes = function () {
                panelControlService.getClubesPagos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.clubesAll = json;
                    $timeout(function () {
                        $('.select2-basic').select2().trigger('change');
                    }, 700);
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.verifyDateClubPay = function () {
                angular.forEach($scope.jugadoresAPagar, function (key, value) {
                    if (key.cod_club_principal != undefined && key.cod_club_principal != null) {
                        key.noSelected = false;
                        var club = $filter('filter')($scope.clubesAll, { 'cod_club': key.cod_club_principal }, true)[0];
                        var dateToday = moment();

                        if (club.fec_ult_pago != null) {
                            key.pago = true;
                        }
                        if (club.fec_ult_pago == null && dateToday > moment("2019-05-31 00:00:00")) {
                            key.pago = false;
                        }
                    }

                    if (key.cod_club_principal == undefined) {
                        key.noSelected = true;
                        $scope.disabledPagoPase = false;
                        $scope.disabledPago = true;
                    }
                });
            }

            $scope.verifySelectedClub = function () {
                var isSelected = $filter('')($scope.jugadoresAPagar, { "cod_club_principal": false }, true);
                return isSelected;
            }

            $scope.verifyFecNac = function (modal) {
                if ($scope.perfil.fec_nacimiento == null) {
                    clickModal("perfilIncompleto");
                } else {
                    clickModal("" + pagarHandicap.id);
                }
            }

            $scope.getJugador = function () {
                panelControlService.getJugador($routeParams.idJugador).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.perfil = json;
                    $scope.getHistorialPago();
                    if ($scope.perfil.cod_usuario == undefined)
                        window.location.href = "/Home/Index";

                    $scope.perfil.handicap = $filter('filter')(angular.copy($scope.perfil.thandicap_usuario), { "bool_principal": true }, true)[0];
                    $scope.perfil.handicapFemenino = $filter('filter')(angular.copy($scope.perfil.thandicap_usuario), { "bool_principal": false }, true)[0];
                    $scope.perfil.clubPrincipal = $filter('filter')(angular.copy($scope.perfil.tuser_club), { "bool_principal": true }, true)[0];
                    $scope.perfil.clubRepublica = $filter('filter')(angular.copy($scope.perfil.tuser_club), { "bool_republica": true }, true)[0];

                    var newDate = new Date();
                    var dateVigencia = new Date();
                    var dateVigenciaRepublica = new Date();
                    if ($scope.perfil.clubPrincipal != undefined)
                        dateVigencia = new Date($scope.perfil.clubPrincipal.fec_vigencia);

                    if ($scope.perfil.clubRepublica != undefined)
                        dateVigenciaRepublica = new Date($scope.perfil.clubRepublica.fec_vigencia);

                    if ($scope.perfil.clubPrincipal != undefined) {
                        if ($scope.perfil.clubPrincipal.fec_vigencia != null) {
                            if (dateVigencia < newDate) {
                                $scope.perfil.clubPrincipal.tclub.isEditable = true;
                            }
                        } else {
                            $scope.perfil.clubPrincipal.tclub.isEditable = true;
                        }
                    }
                    if ($scope.perfil.clubRepublica != undefined) {
                        if ($scope.perfil.clubRepublica.fec_vigencia != null) {
                            if (dateVigenciaRepublica < newDate) {
                                $scope.perfil.clubRepublica.tclub.isEditable = true;
                            }
                        } else {
                            $scope.perfil.clubRepublica.tclub.isEditable = true;
                        }
                    } else {
                        $scope.perfil.clubRepublica = {
                            tclub: {}
                        };
                        $scope.perfil.clubRepublica.tclub.isEditable = true;
                    }

                    var contacto = $filter("filter")(angular.copy($scope.perfil.tcontacto_usuario), { "$": 1 }, true)[0];
                    if (contacto != undefined)
                        $scope.perfil.nro_telefono = contacto.tcontacto.txt_desc;
                    $scope.perfil.club_secundarios = [];
                    angular.forEach($scope.perfil.tuser_club, function (key, value) {
                        if (key.bool_principal != true) {
                            $scope.perfil.club_secundarios.push(key.cod_club);
                        } else {
                            $scope.perfil.cod_club_principal = key.cod_club;
                        }
                    });
                    var mail = $filter("filter")(angular.copy($scope.perfil.tcontacto_usuario), { "$": 3 }, true)[0];
                    if (mail != undefined)
                        $scope.perfil.txt_mail = mail.tcontacto.txt_desc;

                    $scope.getEquipo();
                }).catch(function (error) {
                    console.log(error);
                });
            }


            //$scope.getJugadores = function (page) {
            //    panelControlService.getJugadoresSinPagina().then(function (response) {
            //        var json = JSON.parse(response.data);
            //        $scope.jugadores = json;
            //        angular.forEach($scope.jugadores, function (key, value) {
            //            if ($scope.perfil.cod_usuario == key.cod_usuario)
            //                key.hiddenUs = true;

            //            key.nombre_completo = key.txt_nombre + " " + key.txt_apellido + " (" + key.nro_doc + ")";

            //            key.index = value;
            //        });
            //        $scope.jugadoresOriginal = angular.copy($scope.jugadores);
            //        //$scope.jugadoresUno = angular.copy($scope.jugadoresOriginal);
            //        //$scope.jugadoresDos = angular.copy($scope.jugadoresOriginal);
            //        //$scope.jugadoresTres = angular.copy($scope.jugadoresOriginal);
            //        //$scope.jugadoresCuatro = angular.copy($scope.jugadoresOriginal);
            //        //$scope.jugadoresCinco = angular.copy($scope.jugadoresOriginal);
            //    }).catch(function (error) {
            //        console.log(error);
            //    });
            //}

            $scope.elegirMenu = function (accion) {
                $scope.accion = accion;
                $timeout(function () {
                    $('.select2-basic').select2();
                    $('.select2-tags').select2(
                        {
                            tags: true
                        }
                    );
                }, 300);
            }

            $scope.renderCalendar = function () {
                $('#calendar').fullCalendar({
                    eventLimit: true, // allow "more" link when too many events
                    events: $scope.eventosCalendar,
                    displayEventTime: false,
                    timeFormat: ' '
                });
            }

            $scope.renderSelect = function (element) {

                if (element != undefined)
                    $(element.target).parent().parent().parent().find('.torneo-jugador').fadeIn(500).addClass('jugador-agregado');
            }

            $scope.guardarJugadores = function (jugadores) {
                angular.forEach(jugadores, function (key, value) {
                    $scope.populateJugadores(key);
                    panelControlService.putJugador(key.cod_usuario, JSON.stringify(key)).then(function (response) {
                        var json = JSON.parse(response.data);
                        //console.log(json);
                    }).catch(function (error) {
                        console.log(error);
                    });
                })

            }

            $scope.guardarJugador = function () {
                $scope.populateJugador();
                panelControlService.putJugador($routeParams.idJugador, JSON.stringify($scope.perfil)).then(function (response) {
                    var json = JSON.parse(response.data);
                    //console.log(json);
                    $scope.getJugador();
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.actualizarCategoria = function (key) {
                $timeout(function () {
                    $('.select2-basic').select2().trigger('change');

                }, 100);
            }

            $scope.populateJugadores = function (jugador) {
                jugador.tcontacto_usuario = [];
                if (jugador.tdomicilio != undefined)
                    jugador.tdomicilio.cod_tipo_domicilio = 3;
                jugador.tcontacto = {};
                jugador.tcontacto.cod_tipo_contacto = 1;
                jugador.tcontacto.txt_desc = jugador.nro_telefono;
                jugador.tcontacto_usuario.push({ tcontacto: jugador.tcontacto });
                var mail = {};
                jugador.tuser_club = [];
                mail.tcontacto = { cod_contacto: 0, cod_tipo_contacto: 3, txt_desc: jugador.txt_mail }
                jugador.tcontacto_usuario.push(mail);
                var club = { cod_club: jugador.cod_club_principal, bool_principal: true, bool_republica: false };
                jugador.tuser_club.push(club);
                angular.forEach(jugador.club_secundarios, function (key, value) {
                    var clubSecundario = { cod_club: key, bool_principal: false, bool_republica: false };
                    jugador.tuser_club.push(clubSecundario);
                });
                if (jugador.cod_club_republica != undefined)
                    jugador.tuser_club.push({ cod_club: jugador.cod_club_republica, bool_principal: false, bool_republica: true });
            }

            $scope.populateJugador = function () {
                $scope.perfil.tcontacto_usuario = [];
                $scope.perfil.tdomicilio.cod_tipo_domicilio = 3;
                $scope.perfil.tcontacto = {};
                $scope.perfil.tcontacto.cod_tipo_contacto = 1;
                $scope.perfil.tcontacto.txt_desc = $scope.perfil.nro_telefono;
                $scope.perfil.tcontacto_usuario.push({ tcontacto: $scope.perfil.tcontacto });
                var mail = {};
                $scope.perfil.tuser_club = [];
                mail.tcontacto = { cod_contacto: 0, cod_tipo_contacto: 3, txt_desc: $scope.perfil.txt_mail }
                $scope.perfil.tcontacto_usuario.push(mail);
                var club = { cod_club: $scope.perfil.cod_club_principal, bool_principal: true, bool_republica: false };
                $scope.perfil.tuser_club.push(club);
                angular.forEach($scope.perfil.club_secundarios, function (key, value) {
                    var clubSecundario = { cod_club: key, bool_principal: false, bool_republica: false };
                    $scope.perfil.tuser_club.push(clubSecundario);
                });
                if ($scope.perfil.cod_club_republica != undefined)
                    $scope.perfil.tuser_club.push({ cod_club: $scope.perfil.cod_club_republica, bool_principal: false, bool_republica: true });
            }

            $scope.marcar = function (key, codHandicap, index) {
                console.log(key);
                var marcado = $(".tipo-" + index).prop("checked");

                if (marcado) {
                    $(".tipo-" + index).prop("checked", false);
                }

                key.handicapElegido = '' + codHandicap;
                if (codHandicap == 61) {
                    key.handicapCategoria = '' + 52;
                    key.temporalidad = 'anual';
                }
                else if (!key.handicap.thandicap.txt_desc.includes('Temporario')) {
                    key.handicapCategoria = '' + 1;
                    key.temporalidad = 'anual';
                } else {
                    key.handicapCategoria = '' + 2;
                    key.temporalidad = 'temporal';
                }

                $scope.calcularPrecio();
            }

            $scope.pagarHandicap = function () {
                $scope.jugadoresAPagarPase = angular.copy($scope.jugadoresAPagar);
                angular.forEach($scope.jugadoresAPagarPase, function (key, value) {
                    if (key.handicap != undefined) {
                        key.categoriaHandicapId = key.handicap.thandicap.cod_handicap;
                        //key.precioCambio = key.handicap.thandicap.nro_importe;
                        key.handicap.thandicap = $filter('filter')(angular.copy($scope.handicaps), { "cod_handicap": key.categoriaHandicapId }, true)[0];
                    }
                    if (key.handicapElegido == '61') {
                        key.categoriaHandicapId = 61;
                        key.handicap.thandicap = $filter('filter')(angular.copy($scope.handicaps), { "cod_handicap": key.categoriaHandicapId }, true)[0];
                        //key.precioCambio = 3000;
                    }
                });
                $timeout(function () {
                    $scope.pagarHandicapPase();
                }, 400);

            }

            $scope.pagarHandicapPase = function () {
                var countPase = $filter('filter')($scope.jugadoresAPagarPase, { "pagoSeleccionado": false });
                if (countPase == 0) {
                    $scope.errorPago = false;
                    $scope.verifyDateClubPay();
                    $timeout(function () {
                        var isPago = $filter('filter')($scope.jugadoresAPagar, { "pago": false }).length;
                        var noSeleccionados = $filter('filter')($scope.jugadoresAPagar, { "noSelected": true }).length;
                        if ((isPago == 0 || isPago == undefined) && (noSeleccionados == 0 || noSeleccionados == undefined)) {
                            var filterDate = new Date();
                            $scope.datosMercadoPago = {
                                lstItem: []
                            };
                            var orderDate = $filter("date")(filterDate, "ddMMyyyyHHmm");
                            $scope.datosMercadoPago.UrlSuccess = location.origin + "/MercadoPago/ResultadoDePago?cod_usuario_alta=" + $routeParams.idJugador + "&resultado=1" +
                                "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                            $scope.datosMercadoPago.UrlPending = location.origin + "/MercadoPago/ResultadoDePago?cod_usuario_alta=" + $routeParams.idJugador + "&resultado=2" +
                                "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                            $scope.datosMercadoPago.UrlFailure = location.origin + "/MercadoPago/ResultadoDePago?cod_usuario_alta=" + $routeParams.idJugador + "&resultado=3" +
                                "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                            $scope.datosMercadoPago.cod_usuario_alta = $routeParams.idJugador;
                            $scope.datosMercadoPago.payer = {};
                            $scope.datosMercadoPago.payer.name = $scope.perfil.txt_nombre;
                            $scope.datosMercadoPago.payer.surname = $scope.perfil.txt_apellido;
                            $scope.datosMercadoPago.payer.email = $scope.perfil.txt_mail;
                            $scope.datosMercadoPago.payer.date_created = moment().format('YYYY-MM-DD HH:mm:ss');
                            $scope.datosMercadoPago.payer.phone = {};
                            $scope.datosMercadoPago.payer.phone.area_code = "11";
                            $scope.datosMercadoPago.payer.phone.number = $scope.perfil.nro_telefono;
                            $scope.datosMercadoPago.payer.identification = {};
                            $scope.datosMercadoPago.payer.identification.type = "DNI";
                            $scope.datosMercadoPago.payer.identification.number = $scope.perfil.nro_doc;;

                            if ($scope.jugadoresAPagarPase[0].cod_usuario != $routeParams.idJugador) {
                                angular.forEach($scope.jugadoresAPagarPase, function (key, value) {
                                    key.thandicap_usuario = [];
                                    key.thandicap_usuario.push({
                                        cod_handicap: key.categoriaHandicapId,
                                        nro_handicap: $scope.jugadoresAPagarPase[0].handicap.nro_handicap,
                                        bool_principal: true
                                    });
                                    $scope.item = {};
                                    if (key.precioCambio == undefined)
                                        $scope.item.unit_price = key.handicap.thandicap.nro_importe;
                                    else {
                                        $scope.item.unit_price = key.precioCambio;
                                    }

                                    //$scope.item.unit_price = 5;
                                    $scope.item.title = "Pago de handicap del usuario: (" + key.txt_nombre + " " + key.txt_apellido + ")";
                                    $scope.item.currency_id = "ARS";
                                    $scope.item.quantity = 1;
                                    $scope.item.cod_tipo_pago = 1;
                                    $scope.item.cod_usuario = key.cod_usuario;
                                    $scope.item.IsAnual = false;

                                    var containsAnual = $filter('filter')(angular.copy($scope.handicaps), { "cod_handicap": key.categoriaHandicapId }, true)[0].txt_vigencia;
                                    if (containsAnual.includes("Anual")) {
                                        $scope.item.IsAnual = true;
                                    }

                                    $scope.datosMercadoPago.lstItem.push($scope.item);

                                });
                            } else if ($scope.jugadoresAPagarPase[0].cod_usuario == $routeParams.idJugador) {
                                $scope.item = {};
                                if ($scope.jugadoresAPagarPase[0].precioCambio == undefined)
                                    $scope.item.unit_price = $scope.jugadoresAPagarPase[0].handicap.thandicap.nro_importe;
                                else {
                                    $scope.item.unit_price = $scope.jugadoresAPagarPase[0].precioCambio;
                                }

                                //$scope.item.unit_price = 5;
                                $scope.item.title = "Pago de handicap de " + $scope.perfil.txt_nombre + " " + $scope.perfil.txt_apellido;
                                $scope.item.currency_id = "ARS";
                                $scope.item.quantity = 1;
                                $scope.item.cod_tipo_pago = 1;
                                $scope.item.cod_usuario = $routeParams.idJugador;
                                $scope.datosMercadoPago.lstItem.push($scope.item);
                                $scope.jugadoresAPagarPase[0].thandicap_usuario = [];
                                $scope.jugadoresAPagarPase[0].thandicap_usuario.push({
                                    cod_handicap: $scope.jugadoresAPagarPase[0].categoriaHandicapId,
                                    nro_handicap: $scope.jugadoresAPagarPase[0].handicap.nro_handicap,
                                    bool_principal: true
                                });
                                $scope.item.IsAnual = false;
                                var containsAnual = $filter('filter')(angular.copy($scope.handicaps), { "cod_handicap": $scope.jugadoresAPagarPase[0].categoriaHandicapId }, true)[0].txt_vigencia;
                                if (containsAnual.includes("Anual")) {
                                    $scope.item.IsAnual = true;
                                }
                            }

                            $timeout(function () {
                                panelControlService.pagar($scope.datosMercadoPago).then(function (response) {
                                    perfilJugadorService.cambiarCategoria($routeParams.idJugador, JSON.stringify($scope.jugadoresAPagarPase)).then(function (responseData) {
                                        $scope.guardarJugadores($scope.jugadoresAPagarPase);
                                        $timeout(function () {
                                            window.location.href = response.data;
                                        }, 1500);
                                    }).catch(function (error) {
                                        console.log(error);
                                    });
                                });
                            }, 600);
                        } else {
                            if (isPago > 0)
                                $scope.clubesImpagos = true;

                            $scope.disabledPagoPase = false;
                            $scope.disabledPago = false;
                        }
                    }, 1000);
                } else {
                    $scope.errorPago = true;
                    $timeout(function () {
                        $scope.disabledPagoPase = false;
                        $scope.disabledPago = false;
                    }, 500);

                }
            }

            $scope.clickCategoria = function (jugadorAPagar, nro_precio) {
                jugadorAPagar.pagoSeleccionado = true;
                var dateToday = moment();
                var dateVencimientoHandicap = moment("" + dateToday._d.getFullYear() + "-12-31 23:59:00")
                var diferencia = dateVencimientoHandicap.diff(dateToday, 'days');
                console.log(diferencia + " días");

                nro_precio = (nro_precio * diferencia) / 365;
                jugadorAPagar.precioCambio = Math.round(nro_precio);
                $scope.calcularPrecioPase();
            }

            $scope.filterHandicapPorEdad = function (jugador) {
                jugador.handicap.thandicap = $scope.handicaps.filter(function (x) {
                    return x.txt_desc.includes(jugador.ttipo_jugador.txt_desc) && (x.tcategoria_handicap.txt_desc == "Total" ||
                        x.tcategoria_handicap.txt_desc == "Membresía")
                })[0];
            }

            $scope.filterPrecio = function (jugadorAPagar) {
                $scope.loadingSelect = true;
                $timeout(function () {
                    var resultado = angular.copy($scope.handicaps);
                    jugadorAPagar.pagoSeleccionado = true;
                    if (jugadorAPagar.handicapCategoria == "1" && jugadorAPagar.temporalidad == "anual") {
                        jugadorAPagar.handicap.thandicap.txt_desc = jugadorAPagar.handicap.thandicap.txt_desc.replace("Temporario ", "");
                        jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                            return jugadorAPagar.ttipo_jugador.txt_desc === x.txt_desc && x.tcategoria_handicap.txt_desc == "Total"
                        })[0];
                    }
                    if (jugadorAPagar.handicapCategoria == "2" && jugadorAPagar.temporalidad == "anual") {
                        jugadorAPagar.handicap.thandicap.txt_desc = jugadorAPagar.handicap.thandicap.txt_desc.replace("Temporario ", "");
                        jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                            return jugadorAPagar.ttipo_jugador.txt_desc == x.txt_desc && x.tcategoria_handicap.txt_desc == "Federado" && x.txt_vigencia == 'Anual'
                        })[0];
                    }

                    if (jugadorAPagar.handicapCategoria == "52" && jugadorAPagar.temporalidad == "anual") {
                        jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                            return ("" + x.tcategoria_handicap.cod_categoria_handicap) == jugadorAPagar.handicapCategoria && x.txt_vigencia == 'Anual'
                        })[0];
                    }

                    if (jugadorAPagar.temporalidad == "temporal") {
                        jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                            return x.txt_desc.includes("Temporario " + jugadorAPagar.ttipo_jugador.txt_desc) && ("" + x.tcategoria_handicap.cod_categoria_handicap) == jugadorAPagar.handicapCategoria
                        })[0];
                    }

                    if (jugadorAPagar.handicap.thandicap == undefined) {
                        jugadorAPagar.handicap.thandicap = $filter("filter")($scope.handicaps, { "txt_desc": jugadorAPagar.ttipo_jugador.txt_desc }, true)[0];
                    } 1
                    jugadorAPagar.handicapElegido = "" + jugadorAPagar.handicap.thandicap.cod_handicap;

                    $scope.calcularPrecio();
                    $timeout(function () { $scope.loadingSelect = false }, 700);

                }, 500);
            }

            $scope.calcularPrecio = function () {
                $scope.montoTotal = 0;
                angular.forEach($scope.jugadoresAPagar, function (key, value) {
                    if (key.handicapElegido != '61' && key.pagoSeleccionado)
                        $scope.montoTotal = key.handicap.thandicap.nro_importe + $scope.montoTotal;
                    else if (key.pagoSeleccionado)
                        $scope.montoTotal = 3500 + $scope.montoTotal;
                });
            }

            $scope.agregarJugador = function (cod_usuario) {
                var key = {};
                $scope.jugadorAPagarSelect = cod_usuario;
                key.cod_usuario = $scope.jugadorAPagarSelect;
                $scope.getUsuario(key, true, false);
            }

            $scope.calcularPrecioPase = function () {
                $scope.montoTotalPase = 0;
                angular.forEach($scope.jugadoresAPagarPase, function (key, value) {
                    if (key.pagoSeleccionado)
                        $scope.montoTotalPase = key.precioCambio + $scope.montoTotalPase;
                });
            }


            $scope.agregarJugadorPase = function (cod_usuario) {
                var key = {};
                $scope.jugadorAPagarPaseSelect = cod_usuario;
                key.cod_usuario = $scope.jugadorAPagarPaseSelect;
                $scope.getUsuario(key, true, true);
            }

            $scope.removerJugador = function (jugador) {
                $scope.errorPago = false;
                if ($scope.montoTotal > 0) {
                    if (jugador.handicap.thandicap.nro_importe <= $scope.montoTotal)
                        $scope.montoTotal = $scope.montoTotal - jugador.handicap.thandicap.nro_importe;
                    else
                        $scope.montoTotal = $scope.montoTotal - jugador.handicap.thandicap.nro_importe;
                }
                if ($scope.montoTotalPase > 0) {
                    if (jugador.precioCambio <= $scope.montoTotal)
                        $scope.montoTotal = $scope.montoTotal - jugador.handicap.thandicap.nro_importe;
                    else
                        $scope.montoTotalPase = $scope.montoTotalPase - jugador.precioCambio;
                }
                $scope.jugadoresAPagar.splice($scope.jugadoresAPagar.indexOf(jugador), 1);
                $scope.jugadoresAPagarPase.splice($scope.jugadoresAPagarPase.indexOf(jugador), 1);
            }

            $scope.filtrarJugadoresInscripcion = function (id) {
                $scope.codesFiltered.push(id);
            }

            $scope.finalizarPreInscripcion = function (perfilSeleccionadoUno, perfilSeleccionadoDos, perfilSeleccionadoTres, perfilSeleccionadoCuatro, perfilSeleccionadoCinco, torneo) {
                if ($scope.tequipo.cod_club != undefined) {
                    if (perfilSeleccionadoUno != undefined && perfilSeleccionadoDos != undefined && perfilSeleccionadoTres != undefined) {
                        $scope.isLoading = true;
                        if ($scope.terminosYCondiciones) {
                            $scope.tequipo.tequipo_user = [];
                            $scope.tequipo.txt_alta = $scope.perfil.txt_nombre + " " + $scope.perfil.txt_apellido;
                            $scope.tequipo.tequipo_user.push({ cod_usuario: $scope.perfil.cod_usuario, bool_capitan: true, cod_estado: 3, bool_suplente: false });
                            $scope.tequipo.tequipo_user.push({ cod_usuario: perfilSeleccionadoUno.cod_usuario, bool_capitan: false, cod_estado: 3, bool_suplente: false });
                            $scope.tequipo.tequipo_user.push({ cod_usuario: perfilSeleccionadoDos.cod_usuario, bool_capitan: false, cod_estado: 3, bool_suplente: false });
                            $scope.tequipo.tequipo_user.push({ cod_usuario: perfilSeleccionadoTres.cod_usuario, bool_capitan: false, cod_estado: 3, bool_suplente: false });
                            if (perfilSeleccionadoCuatro != undefined) {
                                $scope.tequipo.tequipo_user.push({ cod_usuario: perfilSeleccionadoCuatro.cod_usuario, bool_capitan: false, cod_estado: 3, bool_suplente: true });
                            }
                            if (perfilSeleccionadoCinco != undefined) {
                                $scope.tequipo.tequipo_user.push({ cod_usuario: perfilSeleccionadoCinco.cod_usuario, bool_capitan: false, cod_estado: 3, bool_suplente: true });
                            }
                            $scope.tequipo.cod_torneo = torneo.cod_torneo;
                            perfilJugadorService.validarInscripcion(JSON.stringify($scope.tequipo)).then(function (responseValida) {
                                var json = JSON.parse(responseValida.data);
                                if (json == "Equipo válido") {
                                    $scope.datosMercadoPago = {
                                        lstItem: []
                                    };
                                    $scope.datosMercadoPago.cod_torneo = torneo.cod_torneo;
                                    var filterDate = new Date();
                                    var orderDate = $filter("date")(filterDate, "ddMMyyyyHHmm");
                                    $scope.datosMercadoPago.UrlSuccess = location.origin + "/MercadoPago/ResultadoDePago?cod_usuario_alta=" + $routeParams.idJugador + "&resultado=1" +
                                        "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                                    $scope.datosMercadoPago.UrlPending = location.origin + "/MercadoPago/ResultadoDePago?cod_usuario_alta=" + $routeParams.idJugador + "&resultado=2" +
                                        "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                                    $scope.datosMercadoPago.UrlFailure = location.origin + "/MercadoPago/ResultadoDePago?cod_usuario_alta=" + $routeParams.idJugador + "&resultado=3" +
                                        "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                                    $scope.datosMercadoPago.cod_usuario_alta = $routeParams.idJugador;
                                    $scope.datosMercadoPago.payer = {};
                                    $scope.datosMercadoPago.payer.name = $scope.perfil.txt_nombre;
                                    $scope.datosMercadoPago.payer.surname = $scope.perfil.txt_apellido;
                                    $scope.datosMercadoPago.payer.email = $scope.perfil.txt_mail;
                                    $scope.datosMercadoPago.payer.date_created = moment().format('YYYY-MM-DD HH:mm:ss');
                                    $scope.datosMercadoPago.payer.phone = {};
                                    $scope.datosMercadoPago.payer.phone.area_code = "11";
                                    $scope.datosMercadoPago.payer.phone.number = $scope.perfil.nro_telefono;
                                    $scope.datosMercadoPago.payer.identification = {};
                                    $scope.datosMercadoPago.payer.identification.type = "DNI";
                                    $scope.datosMercadoPago.payer.identification.number = $scope.perfil.nro_doc;

                                    $scope.item = {};
                                    //$scope.item.unit_price = 1;
                                    //$scope.item.unit_price = 5;
                                    $scope.item.unit_price = torneo.nro_precio_inscripcion;
                                    $scope.item.title = "Inscripción a torneo del equipo: " + $scope.tequipo.txt_desc;
                                    $scope.item.currency_id = "ARS";
                                    $scope.item.quantity = 1;
                                    $scope.item.cod_tipo_pago = 3;
                                    $scope.item.cod_usuario = $routeParams.idJugador;
                                    $scope.datosMercadoPago.lstItem.push($scope.item);

                                    $timeout(function () {
                                        perfilJugadorService.pagar($scope.datosMercadoPago).then(function (responsePago) {
                                            perfilJugadorService.finalizarPreInscripcion(JSON.stringify($scope.tequipo)).then(function (response) {
                                                var json = JSON.parse(response.data);
                                                //console.log(json);
                                            }).catch(function (error) {
                                                console.log(error);
                                            });
                                            window.location.href = responsePago.data;
                                        }).catch(function (error) {
                                            console.log(error);
                                        });
                                    }, 600);
                                } else {
                                    if (json.errorWarning != undefined) {
                                        $scope.thereIsWarning = true;
                                        $scope.messageWarning = json.errorWarning;
                                        $scope.isLoading = false;
                                    }
                                    if (json.error) {
                                        $scope.thereIsError = true;
                                        $scope.messageError = json.error;
                                        $scope.isLoading = false;
                                    }

                                    $('html,body').animate({
                                        scrollTop: $("#errorAlertas").offset().top
                                    }, 'slow');

                                    closeModal();
                                    window.scrollTo(0, 0);
                                }
                            }).catch(function (error) {
                                console.log(error);
                                $scope.isLoading = false;
                            });
                        }
                    }
                }
            }


            $scope.finalizarInscripcionPropia = function (torneo) {
                $scope.isLoading = true;
                $scope.tequipo.tequipo_user = [];
                $scope.tequipo.txt_alta = $scope.perfil.txt_nombre + " " + $scope.perfil.txt_apellido;
                $scope.tequipo.tequipo_user.push({ cod_usuario: $scope.perfil.cod_usuario, bool_capitan: false, cod_estado: 3, bool_suplente: false });
                $scope.tequipo.txt_desc = "" + $scope.tequipo.txt_alta;
                $scope.tequipo.cod_torneo = torneo.cod_torneo;
                perfilJugadorService.validarInscripcion(JSON.stringify($scope.tequipo)).then(function (responseValida) {
                    var json = JSON.parse(responseValida.data);
                    if (json == "Equipo válido") {
                        $scope.datosMercadoPago = {
                            lstItem: []
                        };
                        $scope.datosMercadoPago.cod_torneo = torneo.cod_torneo;
                        var filterDate = new Date();
                        var orderDate = $filter("date")(filterDate, "ddMMyyyyHHmm");
                        $scope.datosMercadoPago.UrlSuccess = location.origin + "/MercadoPago/ResultadoDePago?cod_usuario_alta=" + $routeParams.idJugador + "&resultado=1" +
                            "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                        $scope.datosMercadoPago.UrlPending = location.origin + "/MercadoPago/ResultadoDePago?cod_usuario_alta=" + $routeParams.idJugador + "&resultado=2" +
                            "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                        $scope.datosMercadoPago.UrlFailure = location.origin + "/MercadoPago/ResultadoDePago?cod_usuario_alta=" + $routeParams.idJugador + "&resultado=3" +
                            "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                        $scope.datosMercadoPago.cod_usuario_alta = $routeParams.idJugador;
                        $scope.datosMercadoPago.payer = {};
                        $scope.datosMercadoPago.payer.name = $scope.perfil.txt_nombre;
                        $scope.datosMercadoPago.payer.surname = $scope.perfil.txt_apellido;
                        $scope.datosMercadoPago.payer.email = $scope.perfil.txt_mail;
                        $scope.datosMercadoPago.payer.date_created = moment().format('YYYY-MM-DD HH:mm:ss');
                        $scope.datosMercadoPago.payer.phone = {};
                        $scope.datosMercadoPago.payer.phone.area_code = "11";
                        $scope.datosMercadoPago.payer.phone.number = $scope.perfil.nro_telefono;
                        $scope.datosMercadoPago.payer.identification = {};
                        $scope.datosMercadoPago.payer.identification.type = "DNI";
                        $scope.datosMercadoPago.payer.identification.number = $scope.perfil.nro_doc;

                        $scope.item = {};
                        //$scope.item.unit_price = 1;
                        $scope.item.unit_price = torneo.nro_precio_inscripcion;
                        $scope.item.title = "Inscripción del jugador: " + $scope.perfil.txt_nombre + " " + $scope.perfil.txt_apellido + " al torneo " + torneo.txt_desc;
                        $scope.item.currency_id = "ARS";
                        $scope.item.quantity = 1;
                        $scope.item.cod_tipo_pago = 3;
                        $scope.item.cod_usuario = $routeParams.idJugador;
                        $scope.datosMercadoPago.lstItem.push($scope.item);

                        $timeout(function () {
                            perfilJugadorService.pagar($scope.datosMercadoPago).then(function (responsePago) {
                                perfilJugadorService.finalizarPreInscripcion(JSON.stringify($scope.tequipo)).then(function (response) {
                                    var json = JSON.parse(response.data);
                                    //console.log(json);
                                }).catch(function (error) {
                                    console.log(error);
                                });
                                window.location.href = responsePago.data;
                            }).catch(function (error) {
                                console.log(error);
                            });
                        }, 600);
                    } else {
                        if (json.errorWarning != undefined) {
                            $scope.thereIsWarningMe = true;
                            $scope.messageWarningMe = json.errorWarning;
                            $scope.isLoading = false;
                        }
                        if (json.error) {
                            $scope.thereIsErrorMe = true;
                            $scope.messageErrorMe = json.error;
                            $scope.isLoading = false;
                        }

                        $('html,body').animate({
                            scrollTop: $("#errorAlertas").offset().top
                        }, 'slow');

                        closeModal();
                        window.scrollTo(0, 0);
                    }
                }).catch(function (error) {
                    console.log(error);
                });


            }


            $scope.reintentarPreInscripcion = function (equipo) {
                $scope.isLoading = true;

                $scope.datosMercadoPago = {
                    lstItem: []
                };
                $scope.datosMercadoPago.cod_torneo = equipo.cod_torneo;
                var filterDate = new Date();
                var orderDate = $filter("date")(filterDate, "ddMMyyyyHHmm");
                $scope.datosMercadoPago.UrlSuccess = location.origin + "/MercadoPago/ReintentoPagoEquipo?cod_usuario_alta=" + $routeParams.idJugador + "&cod_equipo=" + equipo.cod_equipo +
                    "&cod_torneo=" + equipo.cod_torneo + "&resultado=1&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                $scope.datosMercadoPago.UrlPending = location.origin + "/MercadoPago/ReintentoPagoEquipo?cod_usuario_alta=" + $routeParams.idJugador + "&cod_equipo=" + equipo.cod_equipo +
                    "&cod_torneo=" + equipo.cod_torneo + "&resultado=2&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                $scope.datosMercadoPago.UrlFailure = location.origin + "/MercadoPago/ReintentoPagoEquipo?cod_usuario_alta=" + $routeParams.idJugador + "&cod_equipo=" + equipo.cod_equipo +
                    "&cod_torneo=" + equipo.cod_torneo + "&resultado=3&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                $scope.datosMercadoPago.cod_usuario_alta = $routeParams.idJugador;
                $scope.datosMercadoPago.payer = {};
                $scope.datosMercadoPago.payer.name = $scope.perfil.txt_nombre;
                $scope.datosMercadoPago.payer.surname = $scope.perfil.txt_apellido;
                $scope.datosMercadoPago.payer.email = $scope.perfil.txt_mail;
                $scope.datosMercadoPago.payer.date_created = moment().format('YYYY-MM-DD HH:mm:ss');
                $scope.datosMercadoPago.payer.phone = {};
                $scope.datosMercadoPago.payer.phone.area_code = "11";
                $scope.datosMercadoPago.payer.phone.number = $scope.perfil.nro_telefono;
                $scope.datosMercadoPago.payer.identification = {};
                $scope.datosMercadoPago.payer.identification.type = "DNI";
                $scope.datosMercadoPago.payer.identification.number = $scope.perfil.nro_doc;

                $scope.item = {};
                //$scope.item.unit_price = 1;
                $scope.item.unit_price = $filter('filter')($scope.torneosOriginal, { "cod_torneo": equipo.cod_torneo }, true)[0].nro_precio_inscripcion;
                $scope.item.title = "Inscripción a torneo del equipo: " + equipo.txt_desc;
                $scope.item.currency_id = "ARS";
                $scope.item.quantity = 1;
                $scope.item.cod_tipo_pago = 3;
                $scope.item.cod_usuario = $routeParams.idJugador;
                $scope.datosMercadoPago.lstItem.push($scope.item);

                $timeout(function () {
                    perfilJugadorService.pagar($scope.datosMercadoPago).then(function (responsePago) {
                        window.location.href = responsePago.data;
                    }).catch(function (error) {
                        $scope.isLoading = false;
                        console.log(error);
                    });
                }, 600);
            }


            $scope.goPerfilClub = function () {
                if ($scope.perfil.clubPrincipal.tclub.cod_admin_club == $scope.perfil.cod_usuario) {
                    window.location.href = "/PerfilClub/#/" + $scope.perfil.clubPrincipal.cod_club;
                }
            }

            $scope.checkTerminosYCondiciones = function () {
                $scope.terminosYCondiciones = !$scope.terminosYCondiciones;
            }

            $scope.clickModal = function (perfilSeleccionadoUno, perfilSeleccionadoDos, perfilSeleccionadoTres) {
                $scope.submitted = true;
                var modal = "finInscripcion";
                $('.modal#' + modal).fadeIn(300).addClass('modal-abierto');
                if ($('.modal#' + modal).attr('data-duracion')) {
                    var duracion = $('.modal#' + modal).attr('data-duracion');
                    setTimeout(function () {
                        $('.modal#' + modal).fadeOut(300).removeClass('modal-abierto')
                    }, duracion);
                }
            }

            $scope.myFilterByUno = function (e) {
                return ($scope.selectedUno.indexOf(e.cod_usuario) === -1);
            }

            $scope.myFilterByDos = function (e) {
                return ($scope.selectedDos.indexOf(e.cod_usuario) === -1);
            }

            $scope.myFilterByTres = function (e) {
                return ($scope.selectedTres.indexOf(e.cod_usuario) === -1);
            }

            $scope.myFilterByCuatro = function (e) {
                return ($scope.selectedCuatro.indexOf(e.cod_usuario) === -1);
            }

            $scope.myFilterByCinco = function (e) {
                return ($scope.selectedCinco.indexOf(e.cod_usuario) === -1);
            }

            $scope.selectJugadoresUno = function (newValue) {
                $scope.messageWarning = [];
                $scope.messageError = [];
                if (newValue != null) {
                    $scope.perfilSeleccionadoUno = newValue;
                    $scope.selectedDos.push(newValue.cod_usuario);
                    $scope.selectedTres.push(newValue.cod_usuario);
                    $scope.selectedCuatro.push(newValue.cod_usuario);
                    $scope.selectedCinco.push(newValue.cod_usuario);
                    newValue.select = 'Uno';
                    if (!newValue.cached)
                        $scope.getUsuario(newValue);
                }
            }

            $scope.selectJugadoresDos = function (newValue) {
                if (newValue != null) {
                    $scope.perfilSeleccionadoDos = newValue;
                    $scope.selectedUno.push(newValue.cod_usuario);
                    $scope.selectedTres.push(newValue.cod_usuario);
                    $scope.selectedCuatro.push(newValue.cod_usuario);
                    $scope.selectedCinco.push(newValue.cod_usuario);
                    newValue.select = 'Dos';
                    $scope.getUsuario(newValue);
                }
            }

            $scope.selectJugadoresTres = function (newValue) {
                $scope.messageWarning = [];
                $scope.messageError = [];
                if (newValue != null) {
                    $scope.perfilSeleccionadoTres = newValue;
                    $scope.selectedUno.push(newValue.cod_usuario);
                    $scope.selectedDos.push(newValue.cod_usuario);
                    $scope.selectedCuatro.push(newValue.cod_usuario);
                    $scope.selectedCinco.push(newValue.cod_usuario);
                    newValue.select = 'Tres';
                    $scope.getUsuario(newValue);
                }
            }

            $scope.selectJugadoresCuatro = function (newValue) {
                $scope.messageWarning = [];
                $scope.messageError = [];
                if (newValue != null) {
                    $scope.perfilSeleccionadoCuatro = newValue;
                    $scope.selectedUno.push(newValue.cod_usuario);
                    $scope.selectedDos.push(newValue.cod_usuario);
                    $scope.selectedTres.push(newValue.cod_usuario);
                    $scope.selectedCinco.push(newValue.cod_usuario);
                    newValue.select = 'Cuatro';
                    $scope.getUsuario(newValue);
                }
            }

            $scope.selectJugadoresCinco = function (newValue) {
                $scope.messageWarning = [];
                $scope.messageError = [];
                if (newValue != undefined) {
                    $scope.perfilSeleccionadoCinco = newValue;
                    $scope.selectedUno.push(newValue.cod_usuario);
                    $scope.selectedDos.push(newValue.cod_usuario);
                    $scope.selectedCuatro.push(newValue.cod_usuario);
                    $scope.selectedTres.push(newValue.cod_usuario);
                    newValue.select = 'Cinco';
                    $scope.getUsuario(newValue);
                }
            }

            $scope.deleteSelected = function (oldvalue) {
                var indexUno = $scope.selectedUno.indexOf(oldvalue.cod_usuario);
                if (indexUno != -1)
                    $scope.selectedUno.splice(indexUno, 1);

                var indexDos = $scope.selectedDos.indexOf(oldvalue.cod_usuario);
                if (indexDos != -1)
                    $scope.selectedDos.splice(indexUno, 1);

                var indexTres = $scope.selectedTres.indexOf(oldvalue.cod_usuario);
                if (indexTres != -1)
                    $scope.selectedTres.splice(indexTres, 1);

                var indexCuatro = $scope.selectedCuatro.indexOf(oldvalue.cod_usuario);
                if (indexCuatro != -1)
                    $scope.selectedCuatro.splice(indexCuatro, 1);

                var indexCinco = $scope.selectedCinco.indexOf(oldvalue.cod_usuario);
                if (indexCinco != -1)
                    $scope.selectedCinco.splice($scope.selectedCinco.indexOf(oldvalue.cod_usuario), 1);
            }


            $scope.filterJugadoresUno = function (searchFilterUno) {
                var arr = searchFilterUno;
                if (arr.length > 3) {
                    $scope.isLoading = true;
                    perfilJugadorService.buscarJugadores(1, searchFilterUno).then(function (response) {
                        $scope.isLoading = false;
                        var json = JSON.parse(response.data);
                        $scope.jugadoresUno = json.usersDTO;
                        $scope.pages = json.pages;
                        if (json.usersDTO.length == 1) {
                            $scope.selectJugadoresUno(json.usersDTO[0]);
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.filterJugadoresDos = function (searchFilterDos) {
                var arr = searchFilterDos;
                if (arr.length > 3) {
                    $scope.isLoading = true;
                    perfilJugadorService.buscarJugadores(1, searchFilterDos).then(function (response) {
                        $scope.isLoading = false;
                        var json = JSON.parse(response.data);
                        $scope.jugadoresDos = json.usersDTO;
                        $scope.pages = json.pages;
                        if (json.usersDTO.length == 1) {
                            $scope.selectJugadoresDos(json.usersDTO[0]);
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.filterJugadoresTres = function (searchFilterTres) {
                var arr = searchFilterTres;
                if (arr.length > 3) {
                    $scope.isLoading = true;
                    perfilJugadorService.buscarJugadores(1, searchFilterTres).then(function (response) {
                        $scope.isLoading = false;
                        var json = JSON.parse(response.data);
                        $scope.jugadoresTres = json.usersDTO;
                        $scope.pages = json.pages;
                        if (json.usersDTO.length == 1) {
                            $scope.selectJugadoresTres(json.usersDTO[0]);
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.filterJugadoresCuatro = function (searchFilterCuatro) {
                var arr = searchFilterCuatro;
                if (arr.length > 3) {
                    $scope.isLoading = true;
                    perfilJugadorService.buscarJugadores(1, searchFilterCuatro).then(function (response) {
                        $scope.isLoading = false;
                        var json = JSON.parse(response.data);
                        $scope.jugadoresCuatro = json.usersDTO;
                        $scope.pages = json.pages;
                        if (json.usersDTO.length == 1) {
                            $scope.selectJugadoresCuatro(json.usersDTO[0]);
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.filterJugadoresCinco = function (searchFilterCinco) {
                var arr = searchFilterCinco;
                if (arr.length > 3) {
                    $scope.isLoading = true;
                    perfilJugadorService.buscarJugadores(1, searchFilterCinco).then(function (response) {
                        $scope.isLoading = false;
                        var json = JSON.parse(response.data);
                        $scope.jugadoresCinco = json.usersDTO;
                        $scope.pages = json.pages;
                        if (json.usersDTO.length == 1) {
                            $scope.selectJugadoresCinco(json.usersDTO[0]);
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.filterJugadoresHandicapPase = function (searchFilterHandicap) {
                var arr = searchFilterHandicap;
                if (arr.length > 3) {
                    $scope.isLoading = true;
                    perfilJugadorService.getUsersPase(1, searchFilterHandicap).then(function (response) {
                        $scope.isLoading = false;
                        var json = JSON.parse(response.data);
                        $scope.jugadoresHandicapPase = json;
                        $scope.pages = json.pages;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.getNombreTorneo = function (cod_torneo) {
                var torneo = $filter('filter')($scope.torneosOriginal, { "cod_torneo": cod_torneo }, true)[0];

                if (torneo != undefined)
                    return torneo.txt_desc;
            }

            $scope.getFechaTorneo = function (cod_torneo) {
                var torneo = $filter('filter')($scope.torneosOriginal, { "cod_torneo": cod_torneo }, true)[0];
                if (torneo != undefined)
                    return torneo.fec_comienzo;

            }

            $scope.filterJugadoresHandicap = function (searchFilterHandicap) {
                var arr = searchFilterHandicap;
                if (arr.length > 3) {
                    $scope.isLoading = true;
                    perfilJugadorService.buscarJugadoresSinPago(1, searchFilterHandicap).then(function (response) {
                        $scope.isLoading = false;
                        var json = JSON.parse(response.data);
                        $scope.jugadoresHandicap = json.usersDTO;
                        $scope.pages = json.pages;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

        }]);
