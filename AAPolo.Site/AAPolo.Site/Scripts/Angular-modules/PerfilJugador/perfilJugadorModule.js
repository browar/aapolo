﻿angular.module('PerfilJugador', [
    'perfilJugador.service',
    'perfilJugador.ctrl',
    'panelControl.service',
    'ngRoute',
    'ngAnimate',
    'mgcrea.ngStrap',
    'ngSanitize',
    'ngMessages',
    'ngFileUpload',
    'angular.filter',
    'pascalprecht.translate',
]).config([
    '$routeProvider',
    '$locationProvider',
    "$httpProvider",
    '$compileProvider',
    '$translateProvider',
    function ($routeProvider, $locationProvider, $httpProvider, $compileProvider, $translateProvider) {
        $locationProvider.hashPrefix('');
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);

        $routeProvider.when('/:idJugador/:resultadoPago?/:codTorneo?', {
            templateUrl: 'perfilJugadorList',
            controller: 'perfilJugadorListCtrl'
        });

        $routeProvider.otherwise({
            redirectTo: '/'
        });

        $translateProvider.translations('en', {
            MYPROFILE: 'My profile',
            MYRECORD: 'My record',
            MYPAYMENTS: 'My payments',
            MYTOURNAMENTS: 'My tournaments',
            TOURNAMENTS: 'Tournaments',
            INBOX: 'Inbox',
            SCHEDULE: 'Schedule',
            MANAGMENTCLUB: 'Management club',
            PAYMENTRECORD: 'Payment record',
            PENDINGPAYMENTS: 'Outstanding payments',
            PAYHANDICAP: 'Pay Handicap',
            UPGRADEHANDICAP: 'Upgrade Handicap',
            HANDICAPPAYMENT: 'Handicap payment',
            SEARCHPAYMENTTEXT: 'Search by ID or name. You can pay your own and others',
            SEARCH: 'Search',
            PLAYERSSELECTED: 'Players',
            FINALAMOUNT: 'Final amount:',
            MERCADOPAGO: 'Pay',
            INSTANTPAYMENT: 'Immediate confirmation by paying online with a credit card.',
            OFFLINEPAYMENT: 'Offline payment methods requires uploading the RECEIPT',
            SELECTTOURNAMENT: 'Select your tournament and team name',
            SELECTTEAMMATES: 'Select teammates',
            SUBSTITUTES: 'Substitutes (optional)',
            ENDREGISTRATION: 'End registration',
            SENDREGISTRATION: 'Send registration',
            NAME: 'First Name',
            SURNAME: 'Last name',
            GENRE: 'Gender',
            CLUBS: 'Clubs',
            COUNTRY: 'Country   ',
            HANDICAPCATEGORY: 'Handicap category',
            BIRTHDATE: 'Birthdate',
            ID: 'Id',
            CLUBPREFERENCE: 'Main club',
            CHOOSECLUB: 'Choose club',
            REPUBLICCLUB: 'Republic cup club',
            MEMBERSHIPCLUBS: 'Membership clubs',
            CHOOSECLUBMULTI: 'Choose one or more clubs',
            MAIL: 'Mail',
            PHONE: 'Phone',
            FLOOR: 'Floor',
            ADDRESS: 'Address',
            ADDRESSNUMBER: 'Address number',
            MEDICALCERTIFICATION: 'Medical certification',
            POSTALCODE: 'Zip code',
            DEPARTMENT: 'Department',
            HISTORYOFHANDICAP: 'History of Handicap',
            HANDICAP: 'Handicap',
            DATE: 'Date',
            PAYMENTTYPE: 'Payment type',
            DETAIL: 'Detail',
            AMOUNT: 'Amount',
            HANDICAPFEE: 'Membership fee payment',
            FEETEXT: 'Your handicap payment is due soon.',
            RECOMMENDPART1: 'We advise you to pay it',
            RECOMMENDPART2: 'before',
            RECOMMENDPART3: 'to keep enjoying the AAP benefits.',
            PAYNOW: 'Pay now',
            YOURFEE: "Your club's AAP membership fee is up to date. You can pay other player's fees from this page.",
            SIGNUPTEAM: 'Sign up team',
            SIGNUP: 'Sign up',
            SELECTPAYTYPE: 'Select the payment method to continue:',
            SELECTONETOURNAMENT: 'Select a tournament',
            CATEGORY: 'Category',
            SELECTCLUB: 'Select a club',
            SHIPRECEIVED: 'Registration will be completed when the payment is credited.',
            PAYTYPES: 'new payment methods',
            HOWTOPAY: 'How to pay my',
            HANDICAP: 'Handicap',
            PAYTOURNAMENT: 'How to sign up for',
            ACTIVETOURNAMENTS: 'Active Tournaments',
            IFYOURFIRST: 'If you are a player and it’s your first income, use your ID as password.',
            OURS: 'our',
            CHANGES: 'About',
            REQUESTREFUND: 'Request your credit refund',
            HANDICAPAFILIACION: 'New handicaps and memberships 2019',
            TYPE: 'Type: ',
            HANDICAPTYPE: 'Handicap type: ',
            TERM: 'Term: ',
            COMINGSOON: 'Category upgrades will be available from March 16',
            INORDER: "Some of your profile's required data is incomplete.In order to update your information so that you can pay your handicap, submit the following information to",
            MAILVERONICA: 'veronica.magnasco@aapolo.com',
            IDPASSPORT: 'Passport',
            COUNTRYORIGIN: 'Nationality',
            DATEOFBIRTH: 'Date of birth',
            UPGRADECONFIRM: "Once updated, we'll notify you via email so that you can make the payment.",
            THANKS: 'Thanks!',
            WARNING:'Warning | Handicap payment'
        });
        $translateProvider.translations('es', {
            MYPROFILE: 'Mi perfil',
            MYRECORD: 'Mi historial',
            MYPAYMENTS: 'Mis pagos',
            TOURNAMENTS: 'Torneos',
            MYTOURNAMENTS: 'Mis torneos',
            INBOX: 'Inbox',
            SCHEDULE: 'Calendario',
            MANAGMENTCLUB: 'Administrar club',
            PAYMENTRECORD: 'Historial de pagos',
            PENDINGPAYMENTS: 'Pagos pendientes',
            PAYHANDICAP: 'Pagar Handicap',
            UPGRADEHANDICAP: 'Pase de Categoría',
            HANDICAPPAYMENT: 'Pagar handicap',
            SEARCHPAYMENTTEXT: 'Buscá por nombre o DNI el jugador cuya categoría deseás modificar.',
            SEARCH: 'Buscar',
            PLAYERSSELECTED: 'Jugadores seleccionados',
            FINALAMOUNT: 'Monto total a abonar:',
            MERCADOPAGO: 'Pagar',
            INSTANTPAYMENT: 'Confirmación inmediata pagando online con tarjeta de crédito.',
            OFFLINEPAYMENT: 'Deberás subir el comprobante y esperar confirmación de la AAP.',
            SELECTTOURNAMENT: 'Seleccioná el torneo en el que vas a participar',
            SELECTTEAMMATES: 'Formá tu equipo',
            SUBSTITUTES: 'Suplentes (opcional)',
            ENDREGISTRATION: 'Finalizar inscripción',
            SENDREGISTRATION: 'Enviar preinscripción',
            NAME: 'Nombre',
            SURNAME: 'Apellido',
            GENRE: 'Género',
            COUNTRY: 'Nacionalidad',
            HANDICAPCATEGORY: 'Categoría handicap',
            BIRTHDATE: 'Fecha de nacimiento',
            ID: 'Documento',
            CLUBPREFERENCE: 'Club principal',
            CHOOSECLUB: 'Seleccioná un club',
            REPUBLICCLUB: 'Club republica',
            MEMBERSHIPCLUBS: 'Clubes pertenencia',
            CHOOSECLUBMULTI: 'Seleccioná uno o más clubes',
            CLUBS: 'Clubes',
            MAIL: 'Email',
            PHONE: 'Teléfono',
            FLOOR: 'Piso',
            ADDRESS: 'Domicilio',
            ADDRESSNUMBER: 'Número',
            MEDICALCERTIFICATION: 'Certificado médico',
            POSTALCODE: 'Código postal',
            DEPARTMENT: 'Departamento',
            HISTORYOFHANDICAP: 'Handicap histórico',
            HANDICAP: 'Handicap',
            DATE: 'Fecha',
            PAYMENTTYPE: 'Tipo de pago',
            DETAIL: 'Detalle',
            AMOUNT: 'Importe',
            HANDICAPFEE: 'Pago de cuota de asociación',
            FEETEXT: 'Tu handicap está próximo a vencer.',
            RECOMMENDPART1: 'Te recomendamos abonarlo',
            RECOMMENDPART2: 'antes del día',
            RECOMMENDPART3: 'para poder seguir disfrutando los beneficios de la asocicación.',
            PAYNOW: 'Pagar ahora',
            YOURFEE: 'Tu cuota de asociación del club a la AAP está al día. Puedes pagar handicap de otros jugadores desde esta sección.',
            SIGNUPTEAM: 'Inscripción por equipo',
            SIGNUP: 'Inscripción individual',
            SELECTPAYTYPE: 'Seleccioná una de las formas de pago disponibles.',
            SELECTONETOURNAMENT: 'Seleccioná un torneo',
            CATEGORY: 'Categoría',
            SELECTCLUB: 'Seleccioná un club',
            SHIPRECEIVED: 'La inscripción se concretará cuando el pago de la tarifa de participación sea recibido.',
            PAYTYPES: 'medios de cobro',
            HOWTOPAY: 'Cómo pagar mi',
            HANDICAP: 'Handicap',
            PAYTOURNAMENT: 'Cómo inscribirme a',
            ACTIVETOURNAMENTS: 'Torneos activos',
            IFYOURFIRST: 'Si sos jugador y es la primera vez que ingresás, tu usuario y contraseña es igual a tu documento.',
            CHANGES: 'Cambios',
            OURS: 'en nuestros',
            REQUESTREFUND: 'SOLICITÁ LA DEVOLUCIÓN DE TU CRÉDITO',
            HANDICAPAFILIACION: 'Conocé los Handicaps y Afiliaciones 2019',
            TYPE: 'Tipo: ',
            HANDICAPTYPE: 'Categoría de handicap: ',
            TERM: 'Duración de handicap: ',
            COMINGSOON: 'Estamos haciendo mejoras en los medios de pago y se encontrará disponible a partir de 16 de marzo.',
            INORDER: 'Algunos datos obligatorios de tu perfil están incompletos. Con el fin de poder actualizar tu información y que puedas pagar tu handicap, enviá la siguiente información a ',
            MAILVERONICA: 'veronica.magnasco@aapolo.com',
            IDPASSPORT: 'DNI/Pasaporte',
            COUNTRYORIGIN: 'País de origen',
            DATEOFBIRTH: 'Fecha de Nacimiento',
            UPGRADECONFIRM: 'Una vez actualizado te confirmaremos por el mismo medio para que puedas efectuar el pago.',
            THANKS: '¡Muchas gracias!',
            WARNING: 'Advertencia | Pago de Handicap'
        });
        $translateProvider.preferredLanguage('es');

        var regexIso8601 = /\/Date\((\d*)\)\//;

        $httpProvider.defaults.transformResponse.push(function (responseData) {
            convertDateStringsToDates(responseData);
            return responseData;
        });

        function convertDateStringsToDates(input) {
            // Ignore things that aren't objects.
            if (typeof input !== "object") return input;


            for (var key in input) {
                if (!input.hasOwnProperty(key)) continue;
                var value = input[key];
                var match;
                // Check for string properties which look like dates.
                // TODO: Improve this regex to better match ISO 8601 date strings.
                if (typeof value === "string" && (match = value.match(regexIso8601))) {
                    // Assume that Date.parse can parse ISO 8601 strings, or has been shimmed in older browsers to do so.   
                    var milliseconds = new Date(parseInt(match[1]));

                    if (!isNaN(milliseconds)) {

                        var d = new Date(milliseconds);
                        var day = d.getUTCDate().toString().length == 1 ? '0' + parseInt(d.getUTCDate()) : d.getUTCDate();
                        var month = d.getUTCMonth().toString().length == 1 ? '0' + parseInt(d.getUTCMonth() + 1) : d.getUTCMonth() + 1;
                        var year = d.getUTCFullYear();
                        var hours = d.getUTCHours();
                        var minutes = d.getUTCMinutes();
                        var result = d;
                        input[key] = result;
                    }
                } else if (typeof value === "object") {
                    // Recurse into object
                    convertDateStringsToDates(value);
                }
            }
        };

    }
]).run(['$rootScope', function ($rootScope) {
    $rootScope.$on('$routeChangeSuccess', function () {
        setTimeout(pageSetUp, 200);
    });
}]);

var Login = document.getElementById('Login');

angular.element(document).ready(function () {
    angular.bootstrap(Login, ['Login']);
});