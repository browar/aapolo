﻿var app = angular.module('miembro.ctrl', [])
    .controller('miembroCtrl', [
        '$scope',
        '$filter',
        '$rootScope',
        '$routeParams',
        '$location',
        'miembroService',
        '$timeout',
        function ($scope, $filter, $rootScope, $routeParams, $location, miembroService, $timeout) {
            $scope.regexInt = /^[0-9]{1,19}$/;
            $scope.regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            $scope.miembro = {
                cod_tipo_documento: 0
            };
            $scope.ExceptionMessage = "";
            $scope.isDuplicated = false;
            $scope.isLoading = false;
            $scope.isLogged = false;
            $scope.tarjetaLength = false;
            $scope.isMayor = true;

            $scope.init = function () {
                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',
                        format: 'dd/mm/yyyy',
                        disabled: [1, 7],
                        max: true,
                        //selectYears: true,
                        selectMonths: true,
                        selectYears: 100
                    });
                }, 400);

                $scope.getTipos();

            }

            $scope.getAllClubes = function () {
                miembroService.getAllClubes().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.clubes = json;

                    $timeout(function () {
                        $('.select2-basic').select2();
                        $('.select2-tags').select2({ tags: true });

                        $('.select2-basic').select2().trigger('change');
                    }, 600);

                    $scope.id = localStorage.getItem('id');
                    if ($scope.id != undefined) {
                        $scope.getMiembroLogueado();
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getTipos = function () {
                miembroService.getTipos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.paises = json.paises;
                    $scope.provincias = json.provincias;
                    $scope.tiposDocumento = json.tiposDocumento;
                    $scope.generos = json.generos;
                    $scope.getAllClubes();
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getMiembroLogueado = function () {  
                $scope.isLoading = true;
                if ($scope.id != undefined) {
                    miembroService.getMiembroLogueado($scope.id).then(function (response) {
                        $scope.miembro = JSON.parse(response.data);
                        if ($scope.miembro.cod_usuario != "undefined" && $scope.miembro.cod_usuario != undefined) {
                            var clubPrincipal = $filter('filter')($scope.miembro.tuser_club, { "bool_principal": true })[0];
                            $scope.miembro.cod_club = $filter('filter')($scope.clubes, { "cod_club": clubPrincipal.cod_club })[0].cod_club;
                            var findTelefono = $filter('filter')($scope.miembro.tcontacto_usuario, { "$": "Telefono" }, true)[0];
                            if (findTelefono != undefined)
                                $scope.miembro.txt_celular = parseInt(findTelefono.tcontacto.txt_desc);
                            var findMail = $filter('filter')($scope.miembro.tcontacto_usuario, { "$": "Email" }, true)[0];
                            if (findMail != undefined)
                                $scope.miembro.txt_mail = findMail.tcontacto.txt_desc;

                            $scope.isLogged = true;
                        }
                        $scope.miembro.fec_nacimiento = $filter('date')($scope.miembro.fec_nacimiento, 'dd/MM/yyyy');

                        $timeout(function () {
                            $('.select2-basic').select2().trigger('change');
                        }, 1000);
                        $scope.isLoading = false;
                    }).catch(function (error) {
                        $scope.isLoading = false;
                        console.log(error);
                    });
                }
            }

            $scope.saveMember = function (form) {
                $scope.submitted = true;
                
                var nacimiento = moment($scope.miembro.fec_nacimiento, 'DD/MM/YYYY');
                var hoy = moment();
                var edad = hoy.diff(nacimiento, "years");

                if (edad < 18) {
                    $scope.isMayor = false;
                } else {
                    $scope.isMayor = true;
                } 
                if ($scope.isMayor || $scope.isLogged) {
                    if (form.$error.required == undefined && form.$error.pattern == undefined && form.$error.minlength == undefined) {
                        if ($scope.miembro.txt_mail == $scope.miembro.repeatMail) {
                            $scope.miembro.tdomicilio.cod_tipo_domicilio = 1;
                            var input = document.getElementById('tarjeta');
                            if (input.value.length < 15)
                                $scope.tarjetaLength = true;
                            else
                                $scope.tarjetaLength = false;
                            if (!$scope.tarjetaLength) {
                                miembroService.getTarjeta($scope.miembro.txt_tarjeta).then(function (responsedata) {
                                    var jsonCard = JSON.parse(responsedata.data);
                                    if (jsonCard == "No hay Tarjeta Asociada") {
                                        miembroService.guardarMiembro(JSON.stringify($scope.miembro)).then(function (response) {
                                            console.log(response.data);
                                            var json = JSON.parse(response.data);
                                            if (json.Message != "An error has occurred.") {
                                                clickModal('confirmarExitoSalir');
                                            } else {
                                                $scope.ExceptionMessage = "¡El documento ya existe!";
                                                $scope.isDuplicated = true;
                                            }
                                        }).catch(function (error) {
                                            console.log(error);
                                        });
                                    } else {
                                        $scope.repeatCard = true;
                                    }

                                }).catch(function (error) {
                                    console.log(error);
                                });
                            }
                        } else {
                            $scope.mainNotCoincidence = true;
                        }
                    }
                }
            }

            $scope.volverAlHome = function () {
                location.href = "/Home/Index#";
            }

            $scope.filterMunicipios = function () {
                if ($scope.miembro.tdomicilio != undefined) {
                    if ($scope.miembro.tdomicilio.cod_provincia != null) {
                        miembroService.getMunicipios($scope.miembro.tdomicilio.cod_provincia).then(function (response) {
                            var json = JSON.parse(response.data);
                            $scope.municipios = json.municipios;
                        }).catch(function (error) {
                            console.log(error);
                        });
                    }
                }
            }

        }]);