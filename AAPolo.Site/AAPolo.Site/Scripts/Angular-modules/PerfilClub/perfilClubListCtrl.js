﻿var app = angular.module('perfilClub.ctrl', [])
    .controller('perfilClubListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'perfilClubService',
        'panelControlService',
        '$timeout',
        'Upload',
        function ($scope, $filter, $routeParams, $location, perfilClubService, panelControlService, $timeout, Upload) {
            $scope.accion = "Default";
            //$scope.torneos = [];
            $scope.canchas = [];
            $scope.torneos = [];
            $scope.dateToday = new Date();
            $scope.pagos = [];
            $scope.perfil = {};
            $scope.montoTotal = 0;
            $scope.isAlDia = false;
            $scope.montoTotalPase = 0;
            $scope.jugadoresAPagarCl = [];
            $scope.codesFiltered = [];
            $scope.tequipo = {
                tequipo_user: []
            }
            $scope.jugadoresAPagarClPase = [];
            $scope.jugadorIds = [];
            $scope.datosMercadoPago = {
                lstItem: []
            };
            $scope.eventosCalendar = [];

            function getValue() {
                return window.localStorage.getItem('idioma');
            }

            $scope.$watch(getValue, function (newValue, oldValue) {
                if (oldValue !== newValue)
                    $scope.idiomaElegido = newValue;

                $(".dropdown-abierto").click();
            });


            $(".chooseLanguage li a").click(function () {
                $(".profile-nav .current a").click();
            });

            $scope.initValues = function () {
                $scope.isLoading = true;
                var idioma = localStorage.getItem("idioma");
                $scope.idiomaElegido = idioma;
                $scope.getClub();
                $scope.getSocios();
                $scope.getHandicaps();
                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',
                        format: 'dd/mm/yyyy',
                        max: true,
                        //selectYears: true,
                        selectMonths: true,
                        selectYears: 100
                    });
                }, 600);
            }

            $scope.getTorneos = function () {
                perfilClubService.getTorneos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.torneos = json.torneosDTO;
                    $scope.categoriasHandicap = json.categoriaHandicapDTO;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getEventos = function () {
                panelControlService.getEventos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.eventos = json;
                    angular.forEach($scope.eventos, function (key) {
                        var dateNew = $filter('date')(key.fec_comienzo, "yyyy-MM-dd");
                        if (key.cod_tipo_evento == 1) {
                            $scope.evento = {
                                title: "" + key.txt_desc,
                                start: "" + dateNew,
                                end: "" + dateNew,
                                url: "/PerfilPublico/#/Torneo/" + key.cod_torneo,
                                editable: false,
                                allDay: true,
                                className: "calendar-torneo"
                            };
                        } else {
                            $scope.evento = {
                                title: "" + key.txt_desc,
                                start: "" + dateNew,
                                end: "" + dateNew,
                                url: "javascript:void(0)",
                                editable: false,
                                allDay: true,
                                className: "calendar-evento"
                            };
                        }
                        $scope.eventosCalendar.push($scope.evento);
                        $('#calendar').fullCalendar('renderEvent', $scope.evento, true);
                    });
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.subirImagen = function () {
                $("#fileLoad").click();
            }

            $scope.upload = function (file) {
                $scope.perfil.txt_imagen = "reemplazarEste";
                Upload.upload({
                    url: '/PanelControl/EditarClub',
                    data: { id: $routeParams.idClub, clubDTO: JSON.stringify($scope.perfil) },
                    file: file, // or list of files ($files) for html5 only
                    //}).progress(function (evt) {                       
                }).success(function (response) {
                    if (response != null) {
                        $timeout(function () {
                            $scope.getClub();
                        }, 400);
                    }
                }).error(function (err) {
                });
            }

            $scope.getSocios = function () {
                perfilClubService.getSocios($routeParams.idClub).then(function (response) {
                    $scope.socios = JSON.parse(response.data);

                    $scope.isLoading = false;
                    $scope.getEventos();
                    $scope.getTorneos();
                    var dateToday = new Date();
                    angular.forEach($scope.socios, function (key) {
                        key.nombre_completo = key.txt_nombre + " " + key.txt_apellido + " (" + key.nro_doc + ")";
                        if (key.thandicap_usuario != undefined)
                            key.handicap = $filter('filter')(angular.copy(key.thandicap_usuario), { "bool_principal": true }, true)[0];

                        var clubBuscado = $filter('filter')(key.tuser_club, { "cod_club": $scope.perfil.cod_club }, true)[0];
                        if (clubBuscado != undefined) {
                            key.bool_rechazado = clubBuscado.bool_rechazado;
                            key.idUserClub = clubBuscado.cod_jugador_club;
                        }

                        var userPagos = $filter('filter')(key.thistorial_pagos, { 'cod_tipo_pago': 1 });

                        var isPagos = userPagos.filter(function (x) {
                            var fec_vencimiento = new Date(x.fec_vencimiento);
                            return (fec_vencimiento > dateToday && x.nro_resultado_pago == 1);
                        });

                        if (isPagos.length > 0) {
                            key.isAlDia = true;
                        } else {
                            key.isAlDia = false;
                        }
                    });

                    $scope.nuevosSocios = [];
                    $scope.nuevosSocios = $scope.socios.filter(function (x) {
                        var fechauno = new Date();
                        fechauno = fechauno.setMonth(fechauno.getMonth() - 1);
                        var fechados = new Date(x.fec_alta);
                        return (fechados >= fechauno);
                    });
                    $scope.getHistorialPago();
                }).catch(function (error) {
                    console.log(error);
                });
            }


            $scope.getClub = function () {
                panelControlService.getClub($routeParams.idClub).then(function (response) {
                    $scope.perfil = JSON.parse(response.data);
                    $scope.calcularFechaFundacion();
                    var direccion_maps = $scope.perfil.tdomicilio.txt_domicilio;
                    $scope.perfil.domicilio_maps = direccion_maps.replace(" ", "+");
                    if ($filter("filter")(angular.copy($scope.perfil.tcontacto_club), { "$": 1 }, true)[0] != undefined)
                        $scope.perfil.nro_telefono = $filter("filter")(angular.copy($scope.perfil.tcontacto_club), { "$": 1 }, true)[0].tcontacto.txt_desc;
                    if ($filter("filter")(angular.copy($scope.perfil.tcontacto_club), { "$": 3 }, true)[0] != undefined)
                        $scope.perfil.txt_mail = $filter("filter")(angular.copy($scope.perfil.tcontacto_club), { "$": 3 }, true)[0].tcontacto.txt_desc;
                    angular.forEach($scope.perfil.tsede, function (key, value) {
                        angular.forEach(key.tcancha, function (keyCancha, valueCancha) {
                            $scope.canchas.push(keyCancha);
                        });
                    });
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getHandicaps = function () {
                perfilClubService.getHandicaps().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.handicaps = json;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.calcularFechaFundacion = function () {
                var fec_fundacion = $filter('date')($scope.perfil.fec_fundacion, 'yyyyMMdd');
                moment.locale("es");
                var date = new Date();
                $scope.perfil.anio_fundacion = moment(date).year() - moment(fec_fundacion).year();
            }

            $scope.pagarHandicap = function () {
                $scope.jugadoresAPagarClPase = $scope.jugadoresAPagarCl;
                angular.forEach($scope.jugadoresAPagarClPase, function (key, value) {
                    key.categoriaHandicapId = key.handicap.thandicap.cod_handicap;
                });
                $timeout(function () {
                    $scope.pagarHandicapPase();
                }, 400);
            }

            $scope.getHistorialPago = function () {
                perfilClubService.getHistorialPagosClub($routeParams.idClub).then(function (response) {
                    $scope.perfil.thistorial_pagos = [];
                    var dateToday = new Date();
                    $scope.perfil.thistorial_pagos = JSON.parse(response.data);
                    if ($scope.perfil.thistorial_pagos.length > 0) {
                        angular.forEach($scope.perfil.thistorial_pagos, function (key) {
                            var jugador = $filter('filter')(angular.copy($scope.socios), { "cod_usuario": key.cod_usuario }, true)[0];
                            //key.txt_nombre_alta = jugador.txt_nombre + " " + jugador.txt_apellido;
                        });
                        $scope.fec_vencimiento = $scope.perfil.thistorial_pagos[0].fec_vencimiento;

                        var fec_vencimiento = new Date($scope.fec_vencimiento);
                        if ($scope.perfil.thistorial_pagos[0].nro_resultado_pago == 1 && fec_vencimiento > dateToday) {
                            $scope.isAlDia = true;
                        } else {
                            $scope.isAlDia = false;
                        }
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }


            $scope.elegirMenu = function (accion) {
                $scope.accion = accion;
            }


            $scope.renderCalendar = function () {
                $('#calendar').fullCalendar({
                    eventLimit: true, // allow "more" link when too many events
                    events: $scope.eventosCalendar,
                    displayEventTime: false,
                    timeFormat: ' '
                });
            }

            $scope.renderSelect = function (element) {

                //if (element != undefined)
                //    $(element.target).parent().parent().parent().find('.torneo-jugador').fadeIn(500).addClass('jugador-agregado');
            }

            $scope.modificarClub = function () {
                $scope.perfil.tdomicilio.cod_tipo_domicilio = 3;
                $scope.perfil.tcontacto_club = [];
                $scope.perfil.tcontacto = {};
                $scope.perfil.tcontacto.txt_desc = $scope.perfil.nro_telefono;
                $scope.perfil.tcontacto.cod_tipo_contacto = 1;
                $scope.perfil.tcontacto_club.push({ tcontacto: $scope.perfil.tcontacto });
                var mail = {};
                mail.tcontacto = { cod_contacto: 0, cod_tipo_contacto: 3, txt_desc: $scope.perfil.txt_mail }
                $scope.perfil.tcontacto_club.push(mail);
                $timeout(function () {
                    panelControlService.putClub($routeParams.idClub, JSON.stringify($scope.perfil)).then(function (response) {
                        var json = JSON.parse(response.data);
                    }).catch(function (error) {
                        console.log(error);
                    });
                }, 500);
            }


            $scope.filterPrecio = function (jugadorAPagar) {
                $timeout(function () {
                    var resultado = angular.copy($scope.handicaps);
                    if (jugadorAPagar.handicapElegido != "61") {
                        if (jugadorAPagar.handicapCategoria == "1" && jugadorAPagar.temporalidad == "anual") {
                            jugadorAPagar.handicap.thandicap.txt_desc = jugadorAPagar.handicap.thandicap.txt_desc.replace("Temporario ", "");
                            jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                                return jugadorAPagar.handicap.thandicap.txt_desc === x.txt_desc && x.tcategoria_handicap.txt_desc == "Total"
                            })[0];
                        }
                        if (jugadorAPagar.handicapCategoria == "2" && jugadorAPagar.temporalidad == "anual") {
                            jugadorAPagar.handicap.thandicap.txt_desc = jugadorAPagar.handicap.thandicap.txt_desc.replace("Temporario ", "");
                            jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                                return jugadorAPagar.handicap.thandicap.txt_desc == x.txt_desc && x.tcategoria_handicap.txt_desc == "Federado" && x.txt_vigencia == 'Anual'
                            })[0];
                        }
                        if (jugadorAPagar.temporalidad == "temporal") {
                            jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                                return x.txt_desc.includes("Temporario " + jugadorAPagar.handicap.thandicap.txt_desc) && ("" + x.tcategoria_handicap.cod_categoria_handicap) == jugadorAPagar.handicapCategoria
                            })[0];
                        }

                        if (jugadorAPagar.handicap.thandicap == undefined) {
                            jugadorAPagar.handicap = angular.copy(jugadorAPagar.copyHandicap);
                            $scope.filterPrecio(jugadorAPagar);
                        }
                        jugadorAPagar.handicapElegido = "" + jugadorAPagar.handicap.thandicap.cod_handicap;
                    }
                    $scope.calcularPrecio();

                }, 600);
            }

            $scope.getUsuario = function (key, flagPago, isPase) {
                $scope.isLoading = true;
                var index = angular.copy(key.index);
                var select = angular.copy(key.select);
                if (!key.cached) {
                    panelControlService.getJugador(key.cod_usuario).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.isLoading = false;
                        key = json;
                        key.pagoSeleccionado = false;
                        if (key.thandicap_usuario != undefined) {
                            key.handicap = $filter('filter')(angular.copy(key.thandicap_usuario), { "bool_principal": true }, true)[0];
                            if (key.handicap != undefined)
                                key.categoria_handicap = key.handicap.cod_categoria_handicap;

                            key.handicapFemenino = $filter('filter')(angular.copy(key.thandicap_usuario), { "bool_principal": false }, true)[0];
                        }

                        if (flagPago && !isPase) {
                            var jugador = $filter('filter')(angular.copy($scope.jugadoresHandicap), { "cod_usuario": $scope.jugadorAPagarSelect }, true)[0];
                            jugador = json;
                            if ($filter('filter')(angular.copy($scope.jugadoresAPagarCl), { "cod_usuario": $scope.jugadorAPagarSelect }, true).length == 0) {
                                jugador.copyHandicap = angular.copy(jugador.handicap);
                                if (jugador.copyHandicap != undefined)
                                    $scope.jugadoresAPagarCl.push(jugador);

                                if (jugador.handicap == undefined) {
                                    jugador.handicap = $filter('filter')(angular.copy(jugador.thandicap_usuario), { "bool_principal": true }, true)[0];
                                }
                            }

                        }

                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.filterPrecio = function (jugadorAPagar) {
                $scope.loadingSelect = true;
                $timeout(function () {
                    var resultado = angular.copy($scope.handicaps);
                    jugadorAPagar.pagoSeleccionado = true;
                    if (jugadorAPagar.handicapCategoria == "1" && jugadorAPagar.temporalidad == "anual") {
                        jugadorAPagar.handicap.thandicap.txt_desc = jugadorAPagar.handicap.thandicap.txt_desc.replace("Temporario ", "");
                        jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                            return jugadorAPagar.ttipo_jugador.txt_desc === x.txt_desc && x.tcategoria_handicap.txt_desc == "Total"
                        })[0];
                    }
                    if (jugadorAPagar.handicapCategoria == "2" && jugadorAPagar.temporalidad == "anual") {
                        jugadorAPagar.handicap.thandicap.txt_desc = jugadorAPagar.handicap.thandicap.txt_desc.replace("Temporario ", "");
                        jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                            return jugadorAPagar.ttipo_jugador.txt_desc == x.txt_desc && x.tcategoria_handicap.txt_desc == "Federado" && x.txt_vigencia == 'Anual'
                        })[0];
                    }

                    if (jugadorAPagar.handicapCategoria == "52" && jugadorAPagar.temporalidad == "anual") {
                        jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                            return ("" + x.tcategoria_handicap.cod_categoria_handicap) == jugadorAPagar.handicapCategoria && x.txt_vigencia == 'Anual'
                        })[0];
                    }

                    if (jugadorAPagar.temporalidad == "temporal") {
                        jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                            return x.txt_desc.includes("Temporario " + jugadorAPagar.ttipo_jugador.txt_desc) && ("" + x.tcategoria_handicap.cod_categoria_handicap) == jugadorAPagar.handicapCategoria
                        })[0];
                    }

                    if (jugadorAPagar.handicap.thandicap == undefined) {
                        jugadorAPagar.handicap.thandicap = $filter("filter")($scope.handicaps, { "txt_desc": jugadorAPagar.ttipo_jugador.txt_desc }, true)[0];
                    } 1
                    jugadorAPagar.handicapElegido = "" + jugadorAPagar.handicap.thandicap.cod_handicap;

                    $scope.calcularPrecio();
                    $timeout(function () { $scope.loadingSelect = false }, 700);

                }, 500);
            }

            $scope.calcularPrecio = function () {
                $scope.montoTotal = 0;
                angular.forEach($scope.jugadoresAPagar, function (key, value) {
                    if (key.handicapElegido != '61' && key.pagoSeleccionado)
                        $scope.montoTotal = key.handicap.thandicap.nro_importe + $scope.montoTotal;
                    else if (key.pagoSeleccionado)
                        $scope.montoTotal = 3500 + $scope.montoTotal;
                });
            }

            $scope.agregarJugador = function (cod_usuario) {
                var key = {};
                $scope.jugadorAPagarSelect = cod_usuario;
                key.cod_usuario = $scope.jugadorAPagarSelect;
                $scope.getUsuario(key, true, false);
            }

            $scope.removerJugador = function (jugador) {
                $scope.errorPago = false;
                if ($scope.montoTotal > 0) {
                    if (jugador.handicap.thandicap.nro_importe <= $scope.montoTotal)
                        $scope.montoTotal = $scope.montoTotal - jugador.handicap.thandicap.nro_importe;
                    else
                        $scope.montoTotal = $scope.montoTotal - jugador.handicap.thandicap.nro_importe;
                }

                $scope.jugadoresAPagar.splice($scope.jugadoresAPagar.indexOf(jugador), 1);
            }

            $scope.pagarHandicap = function () {
                $scope.jugadoresAPagarClPase = $scope.jugadoresAPagarCl;
                angular.forEach($scope.jugadoresAPagarClPase, function (key, value) {
                    key.categoriaHandicapId = key.handicap.thandicap.cod_handicap;
                });
                $timeout(function () {
                    $scope.pagarHandicapPase();
                }, 400);

            }

            $scope.filterJugadoresHandicap = function (searchFilterHandicap) {
                var arr = searchFilterHandicap;
                if (arr.length > 3) {
                    $scope.isLoading = true;
                    perfilClubService.buscarJugadoresSinPago(1, searchFilterHandicap).then(function (response) {
                        $scope.isLoading = false;
                        var json = JSON.parse(response.data);
                        $scope.jugadoresHandicap = json.usersDTO;
                        $scope.pages = json.pages;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.pagarHandicapPase = function () {
                $scope.verifyDateClubPay();
                $timeout(function () {
                    var isPago = $filter('filter')($scope.jugadoresAPagarCl, { "pago": false }).length;
                    var noSeleccionados = $filter('filter')($scope.jugadoresAPagarCl, { "noSelected": true }).length;
                    if ((isPago == 0 || isPago == undefined) && (noSeleccionados == 0 || noSeleccionados == undefined)) {
                        var filterDate = new Date();
                        $scope.datosMercadoPago = {
                            lstItem: []
                        };
                        var filterDate = new Date();
                        var orderDate = $filter("date")(filterDate, "ddMMyyyyHHmm");
                        $scope.datosMercadoPago.UrlSuccess = location.origin + "/MercadoPago/ResultadoDePago?cod_club_alta=" + $routeParams.idClub + "&resultado=1" +
                            "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                        $scope.datosMercadoPago.UrlPending = location.origin + "/MercadoPago/ResultadoDePago?cod_club_alta=" + $routeParams.idClub + "&resultado=2" +
                            "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                        $scope.datosMercadoPago.UrlFailure = location.origin + "/MercadoPago/ResultadoDePago?cod_club_alta=" + $routeParams.idClub + "&resultado=3" +
                            "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                        $scope.datosMercadoPago.UrlNotificacion = location.href;
                        $scope.datosMercadoPago.cod_club_alta = $routeParams.idClub;
                        $scope.datosMercadoPago.payer = {};
                        $scope.datosMercadoPago.payer.name = $scope.perfil.txt_nombre;
                        $scope.datosMercadoPago.payer.surname = $scope.perfil.txt_apellido;
                        $scope.datosMercadoPago.payer.email = $scope.perfil.txt_mail;
                        $scope.datosMercadoPago.payer.date_created = moment().format('YYYY-MM-DD HH:mm:ss');
                        $scope.datosMercadoPago.payer.phone = {};
                        $scope.datosMercadoPago.payer.phone.area_code = "11";
                        $scope.datosMercadoPago.payer.phone.number = $scope.perfil.nro_telefono;
                        $scope.datosMercadoPago.payer.identification = {};
                        $scope.datosMercadoPago.payer.identification.type = "DNI";
                        $scope.datosMercadoPago.payer.identification.number = $scope.perfil.nro_doc;;

                        if ($scope.jugadoresAPagarClPase[0].cod_usuario != $routeParams.idJugador) {
                            angular.forEach($scope.jugadoresAPagarClPase, function (key, value) {
                                key.thandicap_usuario = [];
                                key.thandicap_usuario.push({
                                    cod_handicap: key.categoriaHandicapId,
                                    nro_handicap: $scope.jugadoresAPagarClPase[0].handicap.nro_handicap,
                                    bool_principal: true
                                });
                                $scope.item = {};
                                if (key.precioCambio == undefined)
                                    $scope.item.unit_price = key.handicap.thandicap.nro_importe;
                                else {
                                    $scope.item.unit_price = key.precioCambio;
                                }

                                $scope.item.unit_price = 5;
                                $scope.item.title = "Pago de handicap del usuario: (" + key.txt_nombre + " " + key.txt_apellido + ")";
                                $scope.item.currency_id = "ARS";
                                $scope.item.quantity = 1;
                                $scope.item.cod_tipo_pago = 1;
                                $scope.item.cod_usuario = key.cod_usuario;
                                $scope.item.IsAnual = true;

                                var containsAnual = $filter('filter')(angular.copy($scope.handicaps), { "cod_handicap": key.categoriaHandicapId }, true)[0].txt_vigencia;
                                if (containsAnual.includes("Anual")) {
                                    $scope.item.IsAnual = true;
                                }

                                $scope.datosMercadoPago.lstItem.push($scope.item);

                            });
                        } else if ($scope.jugadoresAPagarClPase[0].cod_usuario == $routeParams.idJugador) {
                            $scope.item = {};
                            //if ($scope.jugadoresAPagarClPase[0].precioCambio == undefined)
                            //    $scope.item.unit_price = $scope.jugadoresAPagarClPase[0].handicap.thandicap.nro_importe;
                            //else {
                            //    $scope.item.unit_price = $scope.jugadoresAPagarClPase[0].precioCambio;
                            //}

                            $scope.item.unit_price = 5;
                            $scope.item.title = "Pago de handicap de " + $scope.perfil.txt_nombre + " " + $scope.perfil.txt_apellido;
                            $scope.item.currency_id = "ARS";
                            $scope.item.quantity = 1;
                            $scope.item.cod_tipo_pago = 1;
                            $scope.item.cod_usuario = $routeParams.idJugador;
                            $scope.datosMercadoPago.lstItem.push($scope.item);
                            $scope.jugadoresAPagarClPase[0].thandicap_usuario = [];
                            $scope.jugadoresAPagarClPase[0].thandicap_usuario.push({
                                cod_handicap: $scope.jugadoresAPagarClPase[0].categoriaHandicapId,
                                nro_handicap: $scope.jugadoresAPagarClPase[0].handicap.nro_handicap,
                                bool_principal: true
                            });
                            $scope.item.IsAnual = true;
                            var containsAnual = $filter('filter')(angular.copy($scope.handicaps), { "cod_handicap": $scope.jugadoresAPagarClPase[0].categoriaHandicapId }, true)[0].txt_vigencia;
                            if (containsAnual.includes("Anual")) {
                                $scope.item.IsAnual = true;
                            }
                        }

                        $timeout(function () {
                            panelControlService.pagar($scope.datosMercadoPago).then(function (response) {
                                perfilClubService.cambiarCategoria($routeParams.idJugador, JSON.stringify($scope.jugadoresAPagarClPase)).then(function (responseData) {
                                    $timeout(function () {
                                        window.location.href = response.data;
                                    }, 1500);
                                }).catch(function (error) {
                                    console.log(error);
                                });
                            });
                        }, 600);
                    } else {
                        if (isPago > 0)
                            $scope.clubesImpagos = true;

                        $scope.disabledPagoPase = false;
                        $scope.disabledPago = false;
                    }
                }, 1000);
            }

            $scope.verifyDateClubPay = function () {
                angular.forEach($scope.jugadoresAPagarCl, function (key, value) {
                    if (key.cod_club_principal != undefined && key.cod_club_principal != null) {
                        key.noSelected = false;
                        var club = $filter('filter')($scope.clubesAll, { 'cod_club': key.cod_club_principal }, true)[0];
                        var dateToday = moment();

                        if (club.fec_ult_pago != null) {
                            key.pago = true;
                        }
                        if (club.fec_ult_pago == null && dateToday > moment("2019-05-31 00:00:00")) {
                            key.pago = false;
                        }
                    }

                    if (key.cod_club_principal == undefined) {
                        key.noSelected = true;
                        $scope.disabledPagoPase = false;
                        $scope.disabledPago = true;
                    }
                });
            }

            $scope.verifySelectedClub = function () {
                var isSelected = $filter('')($scope.jugadoresAPagarCl, { "cod_club_principal": false }, true);
                return isSelected;
            }

            $scope.pagarCuota = function () {
                var filterDate = new Date();
                var orderDate = $filter("date")(filterDate, "ddMMyyyyHHmm");
                $scope.datosMercadoPago.UrlSuccess = location.origin + "/MercadoPago/ResultadoDePago?cod_club_alta=" + $routeParams.idClub + "&resultado=1" +
                    "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                $scope.datosMercadoPago.UrlPending = location.origin + "/MercadoPago/ResultadoDePago?cod_club_alta=" + $routeParams.idClub + "&resultado=2" +
                    "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                $scope.datosMercadoPago.UrlFailure = location.origin + "/MercadoPago/ResultadoDePago?cod_club_alta=" + $routeParams.idClub + "&resultado=3" +
                    "&external_reference=" + $scope.perfil.cod_usuario + "" + orderDate;
                $scope.datosMercadoPago.UrlNotificacion = location.href;
                $scope.datosMercadoPago.cod_club_alta = $routeParams.idClub;
                $scope.item = {};
                //$scope.item.unit_price = key.handicap.thandicap.nro_importe;
                $scope.item.unit_price = $scope.perfil.tcategoria_club.nro_importe;
                $scope.item.title = "Pago de cuota del Club: " + $scope.perfil.txt_desc;
                $scope.item.currency_id = "ARS";
                $scope.item.quantity = 1;
                $scope.item.cod_tipo_pago = 2;
                $scope.item.cod_usuario = $scope.perfil.cod_usuario;
                if ($scope.perfil.cod_admin_club == null) {
                    $scope.item.cod_usuario = 0;
                } else {
                    $scope.item.cod_usuario = $scope.perfil.cod_admin_club;
                }
                $scope.item.IsAnual = false;
                $scope.datosMercadoPago.lstItem.push($scope.item);
                $timeout(function () {
                    perfilClubService.pagar($scope.datosMercadoPago).then(function (response) {
                        window.location.href = response.data;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }, 600);
            }

            $scope.aceptarORechazar = function (idUsuario, opcion) {
                perfilClubService.aceptarORechazar(idUsuario, opcion).then(function (response) {
                    $filter('filter')($scope.socios, { "cod_usuario": idUsuario }, true).bool_rechazado = opcion;
                }).catch(function (error) {
                    console.log(error);
                });
            }

        }]);