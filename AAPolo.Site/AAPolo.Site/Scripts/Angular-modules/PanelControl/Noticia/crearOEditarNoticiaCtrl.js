﻿var app = angular.module('crearOEditarNoticia.ctrl', [])
    .controller('crearOEditarNoticiaCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        'Upload',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout, Upload) {
            $scope.noticia = {}
            $scope.isValid = true;
            $scope.msg = "";
            $scope.isLoading = false;
            $scope.optionsSummer = {
                airMode: false,
                height: 500,
                minHeight: null,
                maxHeight: null,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ],
                disableDragAndDrop: true
            };
            $scope.archivosEditados = [];
            $scope.init = function () {
                $scope.getCategorias();
                $scope.getEtiquetas();
                $scope.date = new Date();
                $scope.getNoticia();

                $timeout(function () {
                    $('.select2-basic').select2();
                    $('.select2-tags').select2(
                        {
                            tags: true
                        }
                    );
                }, 600);
                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',
                        format: 'dd/mm/yyyy',
                        selectMonths: true,
                        selectYears: 100
                    });
                }, 1200);

            }
            $scope.getCategorias = function () {
                panelControlService.getCategoriasNoticia().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categorias = json;
                });
            }
            $scope.getEtiquetas = function () {
                panelControlService.getEtiquetasNoticia().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.etiquetas = json;
                });
            }
            $scope.upload = function (file, id) {
                console.log($scope.noticia);
                switch (id) {
                    case 1:
                        $scope.fileImagen = file;
                        break;
                    case 2:
                        $scope.fileSlideshow = file;
                        break;
                    default:
                        $scope.fileVideo = file;
                        break;
                }
            }

            $scope.uploadFooter = function (file) {
                $scope.fileFooter = file;
                $scope.noticia.txt_adjunto_footer = file.name;
            }

            $scope.getNoticia = function () {
                if ($routeParams.id != undefined) {
                    $scope.isLoading = true;
                    panelControlService.getNoticia($routeParams.id).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.isLoading = false;
                        $scope.noticia = json;
                        $scope.noticia.cod_etiqueta = [];
                        $('.tipoNoticia .select2-selection__rendered').html($scope.noticia.ttipo_noticia.txt_desc);
                        angular.forEach($scope.noticia.tnoticia_etiqueta, function (key) {
                            $scope.noticia.cod_etiqueta.push(key.cod_etiqueta);

                        });

                        $scope.archivosOriginal = angular.copy($scope.noticia.tnoticia_archivo);
                        $timeout(function () {
                            $('.select2-basic').select2().trigger('change');

                        }, 14);

                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }
            $scope.guardarNoticia = function () {
                $scope.populateNoticia();
                $scope.validarNoticia();
                console.log($scope.noticia);
                if ($scope.isValid) {
                    $scope.isLoading = true;
                    if ($scope.noticia.cod_noticia == undefined || $scope.noticia.cod_noticia == null) {
                        //    panelControlService.postNoticia(JSON.stringify($scope.noticia)).then(function (response) {
                        //        var json = JSON.parse(response.data);
                        //    }).catch(function (error) {
                        //        console.log(error);
                        //    });
                        Upload.upload({
                            url: '/PanelControl/GuardarNoticia',
                            data: { noticiaDTO: JSON.stringify($scope.noticia) },
                            file: $scope.files
                        }).success(function (response) {
                            $scope.isLoading = false;
                            if ($scope.fileFooter != undefined) {
                                Upload.upload({
                                    url: '/PanelControl/GuardarFooter',
                                    file: $scope.fileFooter
                                }).success(function (response) {

                                }).error(function (err) {
                                    console.log(err);
                                });
                            }
                            clickModal('confirmarExitoSalir');
                            $scope.saved = true;
                        }).error(function (err) {
                            $scope.isLoading = false;
                            console.log(err);
                        });
                    } else {
                        Upload.upload({
                            url: '/PanelControl/EditarNoticiaUpload',
                            data: { id: $routeParams.id, noticiaDTO: JSON.stringify($scope.noticia), idsEditedFiles: $scope.archivosEditados },
                            file: $scope.files
                        }).success(function (response) {
                            $scope.isLoading = false;
                            if ($scope.fileFooter != undefined) {
                                Upload.upload({
                                    url: '/PanelControl/GuardarFooter',
                                    file: $scope.fileFooter
                                }).success(function (response) {

                                }).error(function (err) {
                                    console.log(err);
                                });
                            }
                            clickModal('confirmarExitoSalir');
                            $scope.saved = true;
                        }).error(function (err) {
                            $scope.isLoading = false;
                            console.log(err);
                        });
                    }
                } else {
                    alert($scope.msg);
                }

            }
            $scope.populateNoticia = function () {
                var misEtiquetas = [];
                var archivos = [];
                var sn_mod = !($routeParams.id == undefined || $scope.noticia.cod_noticia == null);
                $scope.files = [];
                $scope.archivosEditados = [];
                angular.forEach($scope.noticia.cod_etiqueta, function (key, value) {
                    var miEtiqueta = { cod_etiqueta: key };
                    misEtiquetas.push(miEtiqueta);
                });
                if ($scope.fileImagen != undefined) {
                    archivos.push({ cod_tipo_archivo: 1, url_archivo: 'reemplazarEstePrincipal', txt_desc: $scope.fileImagen.name, orden: 1 });
                    $scope.files.push($scope.fileImagen);
                    $scope.archivosEditados.push(0);
                } else {
                    if (sn_mod) {
                        archivos.push($scope.archivosOriginal[0]);
                    }
                }
                if ($scope.fileSlideshow != undefined) {
                    archivos.push({ cod_tipo_archivo: 1, url_archivo: 'reemplazarEsteSlideshow', txt_desc: $scope.fileSlideshow.name, orden: 2 });
                    $scope.files.push($scope.fileSlideshow);
                    $scope.archivosEditados.push(1);
                } else {
                    if (sn_mod) {
                        archivos.push($scope.archivosOriginal[1]);
                    }
                }
                if ($scope.fileVideo != undefined) {
                    archivos.push({ cod_tipo_archivo: 3, url_archivo: 'reemplazarEsteVideo', txt_desc: $scope.fileVideo.name, orden: 3 });
                    $scope.files.push($scope.fileVideo);
                    $scope.archivosEditados.push(2);
                } else {
                    if (sn_mod) {
                        if ($scope.archivosOriginal.length > 2) {
                            archivos.push($scope.archivosOriginal[2]);
                        }
                    }

                }
                $scope.noticia.tnoticia_etiqueta = misEtiquetas;
                $scope.noticia.tnoticia_archivo = archivos;
            }

            $scope.volverAlListado = function () {
                window.location.href = '/PanelControl/#/Noticias';
            }

            $scope.validarNoticia = function () {
                $scope.msg = ''; $scope.isValid = true;
                if ($scope.noticia.fec_alta == undefined || $scope.noticia.fec_alta == "") {
                    $scope.isValid = false;
                    $scope.msg = 'Error, No seleccionó el fecha de la noticia.';
                    return;
                }
                if ($scope.noticia.cod_tipo_noticia == undefined || $scope.noticia.cod_tipo_noticia == "") {
                    $scope.isValid = false;
                    $scope.msg = 'Error, No seleccionó la categoría de la noticia.';
                    return;
                }
                if ($scope.noticia.txt_titulo == undefined || $scope.noticia.txt_titulo == "") {
                    $scope.isValid = false;
                    $scope.msg = 'Error, No seleccionó el título de la noticia.';
                    return;
                }
                if ($scope.noticia.txt_copete == undefined || $scope.noticia.txt_copete == "") {
                    $scope.isValid = false;
                    $scope.msg = 'Error, No seleccionó el copete de la noticia.';
                    return;
                }
                if ($scope.noticia.txt_cuerpo == undefined || $scope.noticia.txt_cuerpo == "") {
                    $scope.isValid = false;
                    $scope.msg = 'Error, No seleccionó el cuerpo de la noticia.';
                    return;
                }
                if ($routeParams.id == undefined) {
                    if ($scope.fileImagen == undefined || $scope.fileImagen == null) {
                        $scope.isValid = false;
                        $scope.msg = 'Error, No seleccionó la imagen principal de la noticia.';
                        return;
                    }
                    //if ($scope.fileSlideshow == undefined || $scope.fileSlideshow == null) {
                    //    $scope.isValid = false;
                    //    $scope.msg = 'Error, No seleccionó la imagen de la miniatura de la noticia.';
                    //    return;
                    //}

                }


            }
        }]);
