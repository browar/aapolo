﻿var app = angular.module('noticia.ctrl', [])
    .controller('noticiaCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            
            $scope.isLoading = false;
            $scope.init = function () {
                $scope.getCategorias();
                
            }

            $scope.getNoticias = function () {
                $scope.isLoading = true;
                panelControlService.getNoticiasBySP().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.isLoading = false;
                    $scope.lstNoticias = json;
                    angular.forEach($scope.lstNoticias, function (key) {
                        key.categoriaNombre = $filter("filter")(angular.copy($scope.categorias), { "cod_tipo_noticia": key.cod_tipo_noticia }, true)[0].txt_desc;
                    });
                });
            }
            $scope.getCategorias = function () {
                panelControlService.getCategoriasNoticia().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categorias = json;
                    $scope.getNoticias();
                });
            }
            $scope.updateEstadoNoticia = function (idNoticia) {
                panelControlService.updateEstadoNoticia(idNoticia).then(function (response) {
                    alert('Noticia modificada con éxito');
                }).catch(function (error) {
                    console.log(error);
                });
            }
            $scope.updateEstadoNoticiaDestacada = function (idNoticia) {
                panelControlService.updateEstadoNoticiaDestacada(idNoticia).then(function (response) {
                    alert('Noticia modificada con éxito');
                }).catch(function (error) {
                    console.log(error);
                });
            }
            
            $scope.toggleInit = function () {
                $('.toggle').click(function () {
                    $(this).toggleClass('toggle-active');
                });
            }
        }]);
