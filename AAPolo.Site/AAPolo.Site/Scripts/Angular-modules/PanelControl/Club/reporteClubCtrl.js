﻿var app = angular.module('reporteClub.ctrl', [])
    .controller('reporteClubCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        'Upload',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout, Upload) {

            $scope.init = function ()
            {
                $scope.getClubes();
            }
            $scope.getClubes = function () {
                $scope.isLoading = true;
                panelControlService.getReporteClubes().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.lstClubes = json.lstClubes;
                    $scope.cant_clubes      = json.cant_clubes;
                    $scope.cant_abonaron     = json.cant_abonaron;
                    $scope.cant_no_abonaron = json.cant_no_abonaron;
                    $scope.isLoading = false;
                });
            }
            $scope.generarReporte = function () {
                $scope.isLoading = true;
                panelControlService.getExcelReporte().then(function (response) {
                    //var json = JSON.parse(response.data);
                    //$scope.lstClubes = json.lstClubes;
                    //$scope.cant_clubes = json.cant_clubes;
                    //$scope.cant_abonaron = json.cant_abonaron;
                    //$scope.cant_no_abonaron = json.cant_no_abonaron;
                    $scope.isLoading = false;
                });
            }
            
            $scope.pagarMercadoPago = function () {
                var filterDate = new Date();
                $scope.datosMercadoPago = {};
                var orderDate = $filter("date")(filterDate, "ddMMyyyyHHmm");
                $scope.datosMercadoPago.UrlSuccess = location.origin + "/MercadoPago/ResultadoDePagoAdmin?cod_usuario_alta=" + $scope.clubElegido.UsuarioAdmin +"&cod_club_alta=" + $scope.clubElegido.idClub + "&resultado=1" +
                    "&external_reference=" + $scope.clubElegido.idClub + "" + orderDate;
                $scope.datosMercadoPago.UrlPending = location.origin + "/MercadoPago/ResultadoDePagoAdmin?cod_usuario_alta=" + $scope.clubElegido.UsuarioAdmin +"&cod_club_alta=" + $scope.clubElegido.idClub + "&resultado=2" +
                    "&external_reference=" + $scope.clubElegido.idClub + "" + orderDate;
                $scope.datosMercadoPago.UrlFailure = location.origin + "/MercadoPago/ResultadoDePagoAdmin?cod_usuario_alta=" + $scope.clubElegido.UsuarioAdmin +"&cod_club_alta=" + $scope.clubElegido.idClub + "&resultado=3" +
                    "&external_reference=" + $scope.clubElegido.idClub + "" + orderDate;
                $scope.datosMercadoPago.UrlNotificacion = location.href;
                $scope.datosMercadoPago.cod_club_alta = $scope.clubElegido.cod_club;
                $scope.datosMercadoPago.lstItem = [];
                $scope.item = {};
                $scope.item.unit_price =$scope.jugadorElegido.PrecioHandicap;
                //$scope.item.unit_price = 5;
                $scope.item.title = "Pago de cuota";
                $scope.item.currency_id = "ARS";
                $scope.item.quantity = 1;
                $scope.item.cod_tipo_pago = 2;
                $scope.item.IsAnual = true;
                $scope.datosMercadoPago.cod_historial_pagos = $scope.clubElegido.cod_historial_pagos;

                $scope.datosMercadoPago.lstItem.push($scope.item);

                $timeout(function () {
                    panelControlService.pagar($scope.datosMercadoPago).then(function (response) {
                        window.location.href = response.data;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }, 600);
            }

            $scope.popularClub = function (row) {
                $scope.clubElegido = row;
            }

            $scope.pagoEfectivoTransferencia = function () {
                if ($scope.file != undefined) {
                    $scope.datosMercadoPago = {};
                    $scope.datosMercadoPago.UrlNotificacion = location.href;
                    $scope.datosMercadoPago.cod_club_alta = $scope.clubElegido.cod_club;
                    $scope.datosMercadoPago.lstItem = [];
                    $scope.item = {};
                    if ($scope.clubElegido.importe != null) {
                        $scope.item.unit_price = $scope.clubElegido.ImporteCat;
                    }
                    $scope.item.title = "Pago de cuota";
                    $scope.item.currency_id = "ARS";
                    $scope.item.quantity = 1;
                    $scope.item.cod_tipo_pago = 2;
                    $scope.item.IsAnual = true;
                    $scope.datosMercadoPago.lstItem.push($scope.item);
                    $scope.datosMercadoPago.cod_historial_pagos = $scope.clubElegido.cod_historial_pagos;
                    var nombreCompleto = $scope.clubElegido.cod_club + " - " + $scope.clubElegido.Club;
                    Upload.upload({
                        url: '/MercadoPago/PagarComprobante',
                        data: {
                            usuario: nombreCompleto,
                            datosMercadoPago: $scope.datosMercadoPago,
                            pagoComprobante: $scope.clubElegido.txt_url_comprobante
                        },
                        file: $scope.file, // or list of files ($files) for html5 only
                        //}).progress(function (evt) {                       
                    }).success(function (response) {
                        closeModal();
                        $scope.errorArchivo = false;
                        $scope.init();
                    }).error(function (err) {
                        closeModal();
                        $scope.init();
                    });
                } else {
                    $scope.errorArchivo = true;
                }
            }

            $scope.upload = function (file) {
                $scope.file = file;
                $scope.clubElegido.txt_url_comprobante = file.name;
            }

        }]);
