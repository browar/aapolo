﻿var app = angular.module('crearOEditarClub.ctrl', [])
    .controller('crearOEditarClubCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.regexInt = /^[0-9]{1,19}$/;
            $scope.regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            $scope.isLoading = false;
            $scope.init = function () {
                console.log($routeParams.idClub);
                if ($routeParams.idClub != undefined) {
                    $scope.getClub();
                }
                $scope.getTipos();

                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',                        
                        format: 'dd/mm/yyyy',
                        max: true,
                        selectYears: 100,
                        selectMonths: true
                    });
                    $('.select2-basic').select2();
                    $('.select2-tags').select2(
                        {
                            tags: true
                        }
                    );
                }, 1000);
            }


            $scope.volverAlListado = function () {
                window.location.href = '/PanelControl/#/Clubes';
            }

            $scope.getClub = function () {
                $scope.isLoading = true;
                panelControlService.getClub($routeParams.idClub).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.isLoading = false;
                    console.log(json);
                    $scope.club = json;
                    $scope.club.contacto = {};
                    $scope.club.fec_fundacion = $filter('date')($scope.club.fec_fundacion, "dd/MM/yyyy");
                    $scope.club.contacto.txt_desc = parseInt($filter('filter')($scope.club.tcontacto_club, { "$": 1 }, true)[0].tcontacto.txt_desc);
                    $scope.club.txt_mail = $filter('filter')($scope.club.tcontacto_club, { "$": 3 }, true)[0].tcontacto.txt_desc;
                    $('.paisContainer .select2-selection__rendered').html($scope.club.tdomicilio.tpais.txt_desc);
                    $scope.filterProvincias();
                    $scope.filterMunicipios();
                    $timeout(function () {
                        var userAdmin = $filter('filter')(angular.copy($scope.usuarios), { "cod_usuario": $scope.club.cod_admin_club }, true)[0];
                        $('.provinciaContainer .select2-selection__rendered').html($scope.club.tdomicilio.tprovincia.txt_desc);
                        $('.adminClubContainer .select2-selection__rendered').html(userAdmin.txt_nombre + " " + userAdmin.txt_apellido);
                    }, 200)
                    $('.tipoContainer .select2-selection__rendered').html($scope.club.tcategoria_club.txt_desc);
                    
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getTipos = function () {
                panelControlService.getTiposClub().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categoriaClubes = json.categoriaClubes;
                    $scope.usuarios = json.usuarios;
                    $scope.paises = json.paises;
                    $scope.originalProvincias = json.provincias;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.filterProvincias = function () {
                $scope.club.cod_provincia = undefined;
                $scope.club.cod_municipio = undefined;
                $scope.provincias = $filter('filter')(angular.copy($scope.originalProvincias), { "cod_pais": $scope.club.tdomicilio.cod_pais }, true);
            }

            $scope.filterMunicipios = function () {
                panelControlService.getMunicipios($scope.club.tdomicilio.cod_provincia).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.municipios = json.municipios;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.guardarClub = function (form) {
                $scope.submitted = true;
                if (form.$valid) {
                    $scope.isLoading = true;
                    $scope.club.tdomicilio.cod_tipo_domicilio = 3;
                    $scope.club.contacto.cod_tipo_contacto = 1;
                    $scope.club.tcontacto_club = [];
                    $scope.club.tcontacto_club.push({ tcontacto: $scope.club.contacto });
                    var mail = {};
                    mail.tcontacto = { cod_contacto: 0, cod_tipo_contacto: 3, txt_desc: $scope.club.txt_mail }
                    $scope.club.tcontacto_club.push(mail);
                    if ($scope.club.cod_club == undefined) {
                        $scope.club.usuario_alta = $(".adminLogin").text();
                        panelControlService.postClub(JSON.stringify($scope.club)).then(function (response) {
                            var json = JSON.parse(response.data);
                            $scope.isLoading = false;
                            //location.href = '/PanelControl/#/Clubes'
                            clickModal('confirmarExitoSalir');
                            $scope.saved = true;
                        }).catch(function (error) {
                            console.log(error);
                            $scope.isLoading = false;
                        });
                    } else {
                        panelControlService.putClub($routeParams.idClub, JSON.stringify($scope.club)).then(function (response) {
                            var json = JSON.parse(response.data);
                            $scope.isLoading = false;
                            //location.href = '/PanelControl/#/Clubes'
                            clickModal('confirmarExitoSalir');
                            $scope.saved = true;
                        }).catch(function (error) {
                            console.log(error);
                            $scope.isLoading = false;
                        });
                    }
                } else {
                    alert("Form Inválido");
                }
            }

        }]);
