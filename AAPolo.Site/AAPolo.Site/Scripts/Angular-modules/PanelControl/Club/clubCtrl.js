﻿var app = angular.module('club.ctrl', [])
    .controller('clubCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.filterOriginal = {
                txt_desc: "",
                cod_categoria_club: 0
            };

            $scope.getTitulo = function (activo) {
                if (activo)
                    return "Activo";
                else
                    return "Inactivo";
            }

            $scope.filter = {
                txt_desc: "",
                cod_categoria_club: 0
            };

            $scope.page = 1;
            $scope.isLoading = false;

            $scope.isFiltered = false;

            $scope.init = function () {
                $scope.getClubes(1);
                $scope.getJugadores();
            }

            $scope.getClubes = function (page) {
                $scope.isLoading = true;
                panelControlService.getClubes(page).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.clubes = json.clubesDTO;
                    $scope.pagesClubes = json.pages;
                    $scope.categoriaClubes = json.categoriaClubes;
                    $scope.getPagination();
                    $scope.isLoading = false;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getJugadores = function () {
                panelControlService.getJugadoresSinPagina().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.jugadores = json;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.deleteClub = function (idClub) {
                panelControlService.deleteClub(idClub).then(function (response) {
                    $scope.init();
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.buscarJugadorPrincipal = function (club) {
                if (club.cod_admin_club != undefined) {
                    panelControlService.getJugador(club.cod_admin_club).then(function (response) {
                        var json = JSON.parse(response.data);
                        club.jugador_principal = json;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.getPagination = function () {
                var count = $scope.pagesClubes;
                $scope.pages = Math.ceil(count / 20);
            }

            $scope.paginate = function (page) {
                if (!$scope.isFiltered)
                    $scope.getClubes(page);
                else
                    $scope.filterClubes(page);
            }

            $scope.filterClubes = function (page) {
                $scope.isLoading = true;
                if (page > $scope.pages) {
                    page = $scope.pages;
                }
                if ($scope.filterOriginal == $scope.filter) {
                    $scope.getClubes(page);
                } else {
                    panelControlService.getClubesFiltrados($scope.page, $scope.filter.txt_desc, $scope.filter.cod_categoria_club).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.clubes = json.clubesDTO;
                        $scope.pagesClubes = json.pages;
                        $scope.getPagination();
                        $scope.isFiltered = true;
                        $scope.page = page;
                        $scope.isLoading = false;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.beforePage = function () {
                $scope.page = $scope.page - 1;
                $timeout(function () {
                    $scope.paginate($scope.page);
                }, 100)
            }

            $scope.nextPage = function () {
                $scope.page = $scope.page + 1;
                $timeout(function () {
                    $scope.paginate($scope.page);
                }, 100)
            }
        }]);
