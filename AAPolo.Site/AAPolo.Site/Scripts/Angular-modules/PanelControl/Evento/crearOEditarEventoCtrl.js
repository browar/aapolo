﻿var app = angular.module('crearOEditarEvento.ctrl', [])
    .controller('crearOEditarEventoCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        'Upload',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout, Upload) {
            $scope.isLoading = false;
            $scope.init = function () {
                if ($routeParams.idEvento != undefined)
                    $scope.getEvento($routeParams.idEvento);
                else {
                    $scope.getEvento(0);
                }

                $scope.getTorneos();

                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',                        
                        format: 'dd/mm/yyyy',
                        //selectYears: true,
                        min: new Date(),
                        selectMonths: true,
                        selectYears: 100
                    });
                }, 400);
            }


            $scope.volverAlListado = function () {
                window.location.href = '/PanelControl/#/Eventos';
            }

            $scope.getTorneos = function () {
                panelControlService.getTorneos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.torneos = json.torneosDTO;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getEvento = function (id) {
                $scope.isLoading = true;
                panelControlService.getEvento(id).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.isLoading = false;
                    $scope.evento = json.evento;
                    $scope.tipos = json.tipos;
                    if (id == 0)
                        $scope.evento.fec_comienzo = new Date();
                    $scope.evento.horario = new Date(angular.copy($scope.evento.fec_comienzo));
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.upload = function (file) {
                $scope.file = file;
            }


            $scope.crearEvento = function (form) {

                var formattedDate = moment($scope.evento.fec_comienzo, 'DD/MM/YYYY').format('MM/DD/YYYY');
                formattedDate = formattedDate + " " + $scope.evento.horario.getHours() + ":" + $scope.evento.horario.getMinutes() + ":00";
                var newDate = new Date(formattedDate);
                $scope.evento.fec_comienzo = newDate;
                if (form.$valid) {
                    $scope.isLoading = true;
                    if ($scope.evento.cod_evento == undefined || $scope.evento.cod_evento == 0) {
                        $scope.evento.txt_imagen = "reemplazarEste";
                        $scope.evento.usuario_alta = $(".adminLogin").text();
                        Upload.upload({
                            url: '/PanelControl/GuardarEvento',
                            data: { eventoDTO: JSON.stringify($scope.evento) },
                            file: $scope.file,                    
                        }).success(function (response) {
                            $scope.isLoading = false;
                            //window.location.href = '/PanelControl/#/Eventos'
                            clickModal('confirmarExitoSalir');
                            $scope.saved = true;
                            }).error(function (err) {
                                $scope.isLoading = false;
                        });
                    } else {
                        Upload.upload({
                            url: '/PanelControl/EditarEvento',
                            data: {
                                id: $routeParams.idEvento,
                                eventoDTO: JSON.stringify($scope.evento)
                            },
                            file: $scope.file,                       
                        }).success(function (response) {
                            $scope.isLoading = false;
                            //window.location.href = '/PanelControl/#/Eventos'
                            clickModal('confirmarExitoSalir');
                            $scope.saved = true;
                            }).error(function (err) {
                                $scope.isLoading = false;
                        });
                    }
                } else {
                    alert("Form Inválido");
                }
            }

        }]);
