﻿var app = angular.module('eventoList.ctrl', [])
    .controller('eventoListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {

            $scope.isLoading = false;
            $scope.init = function () {
                $scope.getEventos();

                
            }

            $scope.getEventos = function () {
                $scope.isLoading = true;
                panelControlService.getEventos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.eventos = json;
                    $scope.isLoading = false;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.deleteEvento = function (cod_evento) {
                panelControlService.deleteEvento(cod_evento).then(function (response) {
                  
                }).catch(function (error) {
                    console.log(error);
                });
            }

        }]);
