﻿var app = angular.module('reporteJugador.ctrl', [])
    .controller('reporteJugadorCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        'perfilJugadorService',
        '$timeout',
        'Upload',
        function ($scope, $filter, $routeParams, $location, panelControlService, perfilJugadorService, $timeout, Upload) {
            $scope.pagina = 1;
            $scope.limitePagina = 20;
            $scope.isLoading = false;
            $scope.init = function () {
                $scope.getJugadores();
                $scope.getHandicaps();
            }

            $scope.filtroNuevo = {};

            $scope.getJugadores = function () {
                $scope.isLoading = true;
                panelControlService.getReporteJugadores().then(function (response) {
                    var json = JSON.parse(response.data);
                    angular.forEach(json.lstJugadores, function (key) {
                        key.nombre_completo = key.Nombre + " " + key.Apellido;
                    });
                    $scope.jugadoresOriginal = angular.copy(json.lstJugadores);
                    $scope.lstJugadores = json.lstJugadores;
                    $scope.cant_Jugadores = json.cant_jugadores;
                    $scope.cant_abonaron = json.cant_abonaron;
                    $scope.cant_no_abonaron = json.cant_no_abonaron;
                    $scope.isLoading = false;
                    $scope.getPagination();
                });
            }

            $scope.getHandicaps = function () {
                perfilJugadorService.getHandicaps().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.handicaps = json;
                }).catch(function (error) {
                    console.log(error);
                });
            }


            $scope.getTransicion = function (idHandicap) {
                var handicapEncontrado = $filter('filter')(angular.copy($scope.handicaps), { "cod_handicap": idHandicap }, true)[0];
                if (!handicapEncontrado.txt_desc.includes("Temporario")) {
                    return "" + handicapEncontrado.txt_desc + " " + handicapEncontrado.tcategoria_handicap.txt_desc;
                }
            }

            $scope.filterPrecio = function (jugadorAPagar) {
                if (jugadorAPagar.temporalidad != undefined && jugadorAPagar.handicapCategoria != undefined) {
                    $timeout(function () {
                        var resultado = angular.copy($scope.handicaps);
                        if (jugadorAPagar.handicapCategoria == "1" && jugadorAPagar.temporalidad == "anual") {
                            jugadorAPagar.handicap.thandicap = resultado.filter(function (x) {
                                return jugadorAPagar.handicap.txt_desc === x.txt_desc && x.tcategoria_handicap.txt_desc == "A"
                            })[0];
                        }
                        if (jugadorAPagar.handicapCategoria == "2" && jugadorAPagar.temporalidad == "anual") {
                            jugadorAPagar.handicap = resultado.filter(function (x) {
                                return jugadorAPagar.handicap.txt_desc == x.txt_desc && x.tcategoria_handicap.txt_desc == "B" && x.txt_vigencia == 'Anual'
                            })[0];
                        }
                        if (jugadorAPagar.handicapCategoria == "2" && jugadorAPagar.temporalidad == "temporal") {
                            jugadorAPagar.handicap = resultado.filter(function (x) {
                                if (jugadorAPagar.handicap.txt_desc == "Menor" || jugadorAPagar.handicap.txt_desc == "Extranjero") {
                                    return x.txt_desc.includes("Temporario " + jugadorAPagar.handicap.txt_desc) && x.tcategoria_handicap.txt_desc == "B"
                                } else {
                                    return x.txt_desc.includes("Temporario Residente") && x.tcategoria_handicap.txt_desc == "B"
                                }
                            })[0];
                        }

                        if (jugadorAPagar.handicap == undefined) {
                            jugadorAPagar.handicap = angular.copy(jugadorAPagar.copyHandicap);
                            $scope.filterPrecio(jugadorAPagar);
                        }
                        $scope.montoTotal = jugadorAPagar.handicap.nro_importe;
                    }, 560);
                }
            }


            $scope.generarReporte = function () {
                $scope.isLoading = true;
                panelControlService.getExcelReporte().then(function (response) {
                    $scope.isLoading = false;
                });
            }
            $scope.nextPage = function () {
                $scope.pagina++;
            }
            $scope.beforePage = function () {
                $scope.pagina--;
            }

            $scope.filtrarJugadores = function () {
                $scope.pagina = 1;
                if ($scope.filtroJugadores != undefined) {
                    $scope.filtroNuevo = angular.copy($scope.filtroJugadores);
                    $timeout(function () {
                        $scope.getPagination($scope.filteredRows.length);
                    }, 400);
                }
            }

            $scope.getPagination = function (length) {
                if (length == undefined) {
                    var count = $scope.cant_Jugadores;
                    $scope.pages = Math.ceil(count / $scope.limitePagina);
                } else {
                    var count = length;
                    $scope.pages = Math.ceil(count / $scope.limitePagina);
                }

            }
            $scope.pagarMercadoPago = function () {
                var filterDate = new Date();
                $scope.datosMercadoPago = {};
                var orderDate = $filter("date")(filterDate, "ddMMyyyyHHmm");
                $scope.datosMercadoPago.UrlSuccess = location.origin + "/MercadoPago/ResultadoDePagoAdmin?cod_usuario_alta=" + $scope.jugadorElegido.cod_usuario + "&resultado=1" +
                    "&external_reference=" + $scope.jugadorElegido.cod_usuario + "" + orderDate;
                $scope.datosMercadoPago.UrlPending = location.origin + "/MercadoPago/ResultadoDePagoAdmin?cod_usuario_alta=" + $scope.jugadorElegido.cod_usuario + "&resultado=2" +
                    "&external_reference=" + $scope.jugadorElegido.cod_usuario + "" + orderDate;
                $scope.datosMercadoPago.UrlFailure = location.origin + "/MercadoPago/ResultadoDePagoAdmin?cod_usuario_alta=" + $scope.jugadorElegido.cod_usuario + "&resultado=3" +
                    "&external_reference=" + $scope.jugadorElegido.cod_usuario + "" + orderDate;
                $scope.datosMercadoPago.UrlNotificacion = location.href;
                $scope.datosMercadoPago.cod_usuario_alta = $scope.jugadorElegido.cod_usuario;
                $scope.datosMercadoPago.lstItem = [];
                $scope.item = {};
                //$scope.item.unit_price =$scope.jugadorElegido.PrecioHandicap;
                $scope.item.unit_price = 5;
                $scope.item.title = "Pago de handicap del usuario: (" + $scope.jugadorElegido.Nombre + " " + $scope.jugadorElegido.Apellido + ")";
                $scope.item.currency_id = "ARS";
                $scope.item.quantity = 1;
                $scope.item.cod_tipo_pago = 1;
                $scope.item.cod_usuario = $scope.jugadorElegido.cod_usuario;
                $scope.item.IsAnual = true;
                $scope.datosMercadoPago.cod_historial_pagos = $scope.jugadorElegido.cod_historial_pagos;

                $scope.datosMercadoPago.lstItem.push($scope.item);
                $scope.jugadorElegido.thandicap_usuario = [];
                $scope.jugadorElegido.thandicap_usuario.push({
                    cod_handicap: $scope.jugadorElegido.handicap.cod_handicap,
                    cod_categoria_handicap: $scope.jugadorElegido.handicapCategoria,
                    nro_handicap: $scope.jugadorElegido.NroHandicap,
                    bool_principal: true
                });
                $timeout(function () {
                    panelControlService.pagar($scope.datosMercadoPago).then(function (responseMercadoLibre) {
                        perfilJugadorService.cambiarCategoria($scope.jugadorElegido.cod_usuario, JSON.stringify([$scope.jugadorElegido])).then(function (response) {
                            window.location.href = responseMercadoLibre.data;
                        }).catch(function (error) {
                            console.log(error);
                        });
                    }).catch(function (error) {
                        console.log(error);
                    });
                }, 600);
            }

            $scope.popularJugador = function (row) {
                $scope.jugadorElegido = row;
                $scope.jugadorElegido.handicap = $scope.handicaps.filter(function (x) {
                    return x => x.txt_desc == $scope.jugadorElegido.Handicap && x.tcategoria_handicap.txt_desc == $scope.jugadorElegido.CategoriaHandicap
                })[0];
                
            }

            $scope.pagoEfectivoTransferencia = function () {
                if ($scope.file != undefined) {
                    $scope.datosMercadoPago = {};
                    $scope.datosMercadoPago.UrlNotificacion = location.href;
                    $scope.datosMercadoPago.cod_usuario_alta = $scope.jugadorElegido.cod_usuario;
                    $scope.datosMercadoPago.lstItem = [];
                    $scope.item = {};

                    var handicap = $filter('filter')($scope.handicaps, { "cod_handicap": $scope.jugadorElegido.handicapAPagar }, true)[0];

                    $scope.item.unit_price = handicap.nro_importe;
                    $scope.item.title = "Pago de handicap del usuario: (" + $scope.jugadorElegido.Nombre + " " + $scope.jugadorElegido.Apellido + ")";
                    $scope.item.currency_id = "ARS";
                    $scope.item.quantity = 1;
                    $scope.item.cod_tipo_pago = 1;
                    $scope.item.cod_usuario = $scope.jugadorElegido.cod_usuario;
                    $scope.item.IsAnual = true;
                    $scope.datosMercadoPago.lstItem.push($scope.item);
                    $scope.datosMercadoPago.cod_historial_pagos = $scope.jugadorElegido.cod_historial_pagos;
                    $scope.jugadorElegido.thandicap_usuario = [];
                    $scope.jugadorElegido.thandicap_usuario.push({
                        cod_handicap: handicap.cod_handicap,
                        cod_categoria_handicap: handicap.cod_categoria_handicap,
                        nro_handicap: $scope.jugadorElegido.NroHandicap,
                        bool_principal: true
                    });

                    var nombreCompleto = $scope.jugadorElegido.cod_usuario + " - " + $scope.jugadorElegido.Nombre + " " + $scope.jugadorElegido.Apellido;
                    Upload.upload({
                        url: '/MercadoPago/PagarComprobante',
                        data: {
                            usuario: nombreCompleto,
                            datosMercadoPago: $scope.datosMercadoPago,
                            pagoComprobante: $scope.jugadorElegido.txt_url_comprobante
                        },
                        file: $scope.file, // or list of files ($files) for html5 only
                        //}).progress(function (evt) {                       
                    }).success(function (response) {
                        closeModal();
                        $scope.errorArchivo = false;
                        perfilJugadorService.cambiarCategoria($scope.jugadorElegido.cod_usuario, JSON.stringify([$scope.jugadorElegido])).then(function (response) {
                            $timeout(function () {
                                $scope.init();
                            }, 500); 
                        }).catch(function (error) {
                            console.log(error);
                        });
                    }).error(function (err) {
                    });
                } else {
                    $scope.errorArchivo = true;
                }
            }

            $scope.upload = function (file) {
                $scope.file = file;
                $scope.jugadorElegido.txt_url_comprobante = file.name;
            }

        }]);
