﻿var app = angular.module('crearOEditarJugador.ctrl', [])
    .controller('crearOEditarJugadorCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.jugador = {
                tuser_club: [],
                tcontacto_usuario: [],
                isAdmin: false
            };
            $scope.isDuplicated = false;
            $scope.isLoading = false;

            $scope.page = 0;

            $scope.regexInt = /^[0-9]{1,19}$/;
            $scope.regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


            $scope.init = function () {
                $scope.getTipos();
                $scope.getJugador();

                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',
                        format: 'dd/mm/yyyy',
                        disabled: [1, 7],
                        max: true,
                        //selectYears: true,
                        selectMonths: true,
                        selectYears: 100
                    });
                }, 1000);
            }

            $scope.getJugador = function () {
                if ($routeParams.idJugador != undefined) {
                    panelControlService.getJugador($routeParams.idJugador).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.jugador = json;
                        if ($scope.jugador.cod_tipo_doc == 3) {
                            $scope.jugador.nro_docString = "" + $scope.jugador.nro_doc;
                        }
                        else {
                            $scope.jugador.nro_doc = parseInt($scope.jugador.nro_doc);
                        }
                        $scope.jugador.fec_nacimiento = $filter('date')($scope.jugador.fec_nacimiento, "dd/MM/yyyy");
                        $scope.jugador.contacto = {}

                        var findTelefono = $filter('filter')($scope.jugador.tcontacto_usuario, { "$": "Telefono" }, true)[0];
                        if (findTelefono != undefined)
                            $scope.jugador.contacto.txt_desc = parseInt(findTelefono.tcontacto.txt_desc);
                        var findMail = $filter('filter')($scope.jugador.tcontacto_usuario, { "$": "Email" }, true)[0];
                        if (findMail != undefined)
                            $scope.jugador.txt_mail = findMail.tcontacto.txt_desc;

                        $scope.jugador.club_secundarios = [];
                        angular.forEach($scope.jugador.tuser_club, function (key, value) {
                            if (key.bool_principal != true && key.bool_republica != true) {
                                $scope.jugador.club_secundarios.push(key.cod_club);
                            } else if (key.bool_republica == true) {
                                $scope.jugador.cod_club_republica = key.cod_club;
                            } else {
                                $scope.jugador.cod_club_principal = key.cod_club;
                            }
                        });

                        var handicap = $filter('filter')($scope.jugador.thandicap_usuario, { "bool_principal": true })[0];
                        $scope.jugador.nro_handicap = handicap.nro_handicap;
                        $scope.jugador.cod_categoria_handicap = handicap.thandicap.tcategoria_handicap.cod_categoria_handicap;
                        $scope.jugador.cod_handicap = handicap.thandicap.cod_handicap;
                        if ($scope.jugador.bool_veedor) {
                            $('#veedor').prop('checked', true);
                        }

                        var handicapFemenino = $filter('filter')($scope.jugador.thandicap_usuario, { "bool_principal": false })[0];
                        if (handicapFemenino != undefined)
                            $scope.jugador.nro_handicap_femenino = handicapFemenino.nro_handicap;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.getTipos = function () {
                $scope.isLoading = true;
                panelControlService.getTiposJugador().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.tiposJugador = $filter('filter')(json.tiposJugador, { "txt_desc": '!' + 'Extranjero' });
                    $scope.handicaps = json.handicaps;
                    $scope.handicaps = $filter('filter')($scope.handicaps, { "txt_desc": '!' + 'Temporario' });
                    $scope.handicaps = $filter('filter')($scope.handicaps, { "txt_desc": '!' + 'Extranjero' });
                    $scope.originalHandicaps = angular.copy($scope.handicaps );
                    $scope.categoriaHandicaps = json.categoriaHandicaps;
                    $scope.originalCategoriaHandicaps = json.categoriaHandicaps;
                    $scope.paises = json.paises;
                    $scope.tiposDocumentos = json.tiposDocumentos;
                    $scope.originalProvincias = json.provincias;
                    $scope.provincias = json.provincias;
                    $scope.generos = json.generos;
                    $scope.getClubes();
                    $scope.filterMunicipios();
                    $timeout(function () {
                        $('.select2-basic').select2();
                        $('.select2-tags').select2(
                            {
                                tags: true
                            }
                        );

                        $('.select2-basic').select2().trigger('change');

                        $scope.filterHand($scope.jugador.cod_tipo_jugador);
                    }, 3300);
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getClubes = function () {
                panelControlService.getAllClubes().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.clubes = json;
                    $scope.verifyAdmin();
                    $scope.isLoading = false;
                    $timeout(function () {
                        $('.select2-basic').select2();
                        $('.select2-tags').select2(
                            {
                                tags: true
                            }
                        );

                        $('.select2-basic').select2().trigger('change');
                    }, 3300);
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.volverAlListado = function () {
                window.location.href = '/PanelControl/#/Jugadores';
            }

            $scope.filterProvincias = function () {
                if ($scope.jugador.tdomicilio.cod_pais == 33) {
                    $scope.jugador.cod_provincia = undefined;
                    $scope.jugador.cod_municipio = undefined;
                    $scope.provincias = $filter('filter')(angular.copy($scope.originalProvincias), { "cod_pais": $scope.jugador.tdomicilio.cod_pais }, true);
                }
            }

            $scope.filterMunicipios = function () {
                if ($scope.jugador.tdomicilio != undefined) {
                    if ($scope.jugador.tdomicilio.cod_provincia != null) {
                        if ($scope.jugador.tdomicilio != undefined) {
                            panelControlService.getMunicipios($scope.jugador.tdomicilio.cod_provincia).then(function (response) {
                                var json = JSON.parse(response.data);
                                $scope.municipios = json.municipios;
                            }).catch(function (error) {
                                console.log(error);
                            });
                        }
                    }
                }
            }

            $scope.filterHandicap = function () {
                if ($scope.originalHandicaps) {
                    var handicaps = $filter('filter')(angular.copy($scope.originalHandicaps), { "cod_handicap": $scope.jugador.cod_handicap }, true)[0];
                    var categorias = $filter('filter')(angular.copy($scope.originalCategoriaHandicaps), { "cod_categoria_handicap": handicaps.cod_categoria_handicap }, true);
                    $scope.jugador.cod_categoria_handicap = categorias[0].cod_categoria_handicap;
                    $timeout(function () {
                        $('.select2-basic').select2().trigger('change');
                    }, 300);
                }
            }

            $scope.filterHandicapPais = function () {
                if ($scope.jugador.cod_nacionalidad != undefined) {
                    var codPais = $scope.jugador.cod_nacionalidad;
                    var pais = $filter('filter')(angular.copy($scope.paises), { 'cod_pais': codPais }, true)[0];
                    if (pais.txt_desc != "Argentina") {
                        var handicaps = angular.copy($scope.originalHandicaps);
                        var handicap = handicaps.filter(function (x) {
                            return x.txt_desc == "Extranjero" && x.tcategoria_handicap.txt_desc == "A";
                        })
                        $scope.jugador.cod_handicap = handicap[0].cod_handicap;
                        $scope.jugador.cod_tipo_jugador = 3;
                        $scope.jugador.cod_categoria_handicap = handicap[0].cod_categoria_handicap;
                    } else {
                        var handicaps = angular.copy($scope.originalHandicaps);
                        var handicap = handicaps.filter(function (x) {
                            return x.txt_desc == "Activo" && x.tcategoria_handicap.txt_desc == "A";
                        })
                        $scope.jugador.cod_categoria_handicap = handicap[0].cod_categoria_handicap;
                        $scope.jugador.cod_handicap = handicap[0].cod_handicap;

                    }

                    $scope.filterHand($scope.jugador.cod_tipo_jugador);
                    $timeout(function () {
                        $('.select2-basic').select2().trigger('change');
                    }, 700);
                }

            }

            $scope.filterCategoria = function () {
                if ($scope.jugador.cod_categoria_handicap == 3) {
                    $scope.jugador.cod_handicap = 11;
                    $('.thandicapContainer .select2-selection__rendered').html("Seleccioná un tipo de handicap");
                    $timeout(function () {
                        $('.select2-basic').select2().trigger('change');
                    }, 300);
                }
            }

            $scope.guardarJugador = function (form) {
                if (form.$valid) {
                    $scope.isLoading = true;
                    $scope.populateJugador();
                    if ($scope.jugador.cod_usuario == undefined || $scope.jugador.cod_usuario == null) {
                        $scope.jugador.usuario_alta = $(".adminLogin").text();
                        panelControlService.postJugador(JSON.stringify($scope.jugador)).then(function (response) {
                            var json = JSON.parse(response.data);
                            $scope.isLoading = false;
                            if (json.ExceptionMessage == "El número de documento está duplicado") {
                                $scope.isDuplicated = true;
                                $scope.thereIsError = true;
                                $scope.errorForm = json.ExceptionMessage;
                            } else {
                                //location.href = '/PanelControl/#/Jugadores';
                                clickModal('confirmarExitoSalir');
                                $scope.saved = true;
                            }
                        }).catch(function (error) {
                            console.log(error);
                            $scope.isLoading = false;
                        });
                    } else {
                        panelControlService.putJugador($routeParams.idJugador, JSON.stringify($scope.jugador)).then(function (response) {
                            var json = JSON.parse(response.data);
                            $scope.isLoading = false;
                            if (json.ExceptionMessage == "El número de documento está duplicado") {
                                $scope.isDuplicated = true;
                                $scope.thereIsError = true;
                                $scope.errorForm = json.ExceptionMessage;
                            } else {
                                //location.href = '/PanelControl/#/Jugadores';
                                clickModal('confirmarExitoSalir');
                                $scope.saved = true;
                            }
                        }).catch(function (error) {
                            console.log(error);
                            $scope.isLoading = false;
                        });
                    }
                } else {
                    $scope.submitted = true;
                }
            }

            $scope.verifyAdmin = function () {
                if ($scope.clubes != undefined) {
                    if ($scope.jugador.cod_usuario != undefined) {
                        var admin = $filter('filter')($scope.clubes, { "cod_admin_club": $scope.jugador.cod_usuario }, true)[0];

                        if (admin != undefined)
                            $scope.jugador.isAdmin = true;
                    }
                }
            }

            $scope.filterHand = function (codTipoJugador) {
                $scope.handicaps = angular.copy($scope.originalHandicaps);
                if ($scope.tiposJugador != undefined) {
                    var tipoJugador = $filter('filter')(angular.copy($scope.tiposJugador), { "cod_tipo_jugador": codTipoJugador }, true)[0];
                    if (tipoJugador.txt_desc != 'Menor') {
                        $scope.filterText = tipoJugador.txt_desc;
                    } else {
                        if ($scope.originalHandicaps != undefined) {
                            $scope.filterText = '';
                            $scope.handicaps = [];
                            var data = $scope.originalHandicaps;
                            $scope.handicaps = data.filter(function (x) {
                                return ((x.txt_desc.includes("Menor") || x.txt_desc.includes("Activo")) && !x.txt_desc.includes("Temporario"));
                            });
                        }
                    }
                    $timeout(function () {
                        $('.select2-basic').select2().trigger('change');
                    }, 700);
                }
            }

            $scope.populateJugador = function () {
                if ($scope.jugador.cod_tipo_doc == 3) {
                    $scope.jugador.nro_doc = $scope.jugador.nro_docString;
                }
                else {
                    $scope.jugador.nro_doc = parseInt($scope.jugador.nro_doc);
                }
                if ($scope.jugador.isAdmin) {
                    $scope.jugador.cod_club_admin = $scope.jugador.cod_usuario;
                }
                if ($("#veedor").prop("checked"))
                    $scope.jugador.bool_veedor = 1;
                else
                    $scope.jugador.bool_veedor = 0;

                $scope.jugador.thandicap_usuario = [];
                $scope.jugador.tcontacto_usuario = [];
                $scope.jugador.tuser_club = [];
                $scope.jugador.tdomicilio.cod_tipo_domicilio = 3;
                if ($scope.jugador.contacto != undefined) {
                    $scope.jugador.contacto.cod_tipo_contacto = 1;
                    $scope.jugador.tcontacto_usuario.push({ tcontacto: $scope.jugador.contacto });
                }
                var club = { cod_club: $scope.jugador.cod_club_principal, bool_principal: true, bool_republica: false };
                $scope.jugador.tuser_club.push(club);
                var mail = {};
                mail.tcontacto = { cod_contacto: 0, cod_tipo_contacto: 3, txt_desc: $scope.jugador.txt_mail }
                $scope.jugador.tcontacto_usuario.push(mail);
                $scope.jugador.thandicap_usuario.push({ cod_handicap: $scope.jugador.cod_handicap, bool_principal: true, nro_handicap: $scope.jugador.nro_handicap });
                if ($scope.jugador.cod_genero == 2) {
                    $scope.jugador.thandicap_usuario.push({ cod_handicap: $scope.jugador.cod_handicap, bool_principal: false, nro_handicap: $scope.jugador.nro_handicap_femenino });
                }
                angular.forEach($scope.jugador.club_secundarios, function (key, value) {
                    var clubSecundario = { cod_club: key, bool_principal: false, bool_republica: false };
                    $scope.jugador.tuser_club.push(clubSecundario);
                });
                if ($scope.jugador.cod_club_republica != undefined)
                    $scope.jugador.tuser_club.push({ cod_club: $scope.jugador.cod_club_republica, bool_principal: false, bool_republica: true });
            }

            $scope.selectJugadorPorNacimiento = function () {
                var date = new Date();
                var nac = moment($scope.jugador.fec_nacimiento, "DD/MM/YYYY");
                var years = moment().diff(nac, 'years');
                var monthBirthDate = nac.format('M');
                var dayBirthDate = nac.format('D');
                var currentMonth = date.getMonth();
                var currentDay = date.getDay();

                if (years < 15) {
                    if (currentMonth < monthBirthDate && dayBirthDate >= currentDay && years >= 14) {
                        $scope.jugador.cod_tipo_jugador = 37;
                        $scope.selectDisabledEdad = true;
                    } 
                    else {
                        $scope.jugador.cod_tipo_jugador = 4;
                        $scope.selectDisabledEdad = true;
                    }
                } else if (years <= 17) {
                        $scope.selectDisabledEdad = true;
                        $scope.jugador.cod_tipo_jugador = 37;
                } else {
                    $scope.jugador.cod_tipo_jugador = 1;
                    $scope.selectDisabledEdad = false;
                }
                $scope.filterHand($scope.jugador.cod_tipo_jugador);
                $timeout(function () {
                    $('.select2-basic').select2().trigger('change');
                }, 400);
            }

        }]);
