﻿var app = angular.module('jugador.ctrl', [])
    .controller('jugadorCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        'loginService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, loginService, $timeout) {
            $scope.filter = {
                cod_club: 0,
                cod_tipo_jugador: 0,
                nombre_completo : ""
            };
            $scope.notEqual = false;
            $scope.filterOriginal = {
                nombre_completo: "",
                cod_club: 0,
                cod_tipo_jugador: 0,
            };
            $scope.page = 1;
            $scope.isLoading = false;

            $scope.isFiltered = false;
            $scope.menu = location.href.substr(location.href.lastIndexOf('/') + 1);
            $scope.init = function () {
                $scope.getJugadores(1);
                $scope.getTipos();
            }


            $scope.getPagination = function () {
                var count = $scope.pagesJugadores;
                $scope.pages = Math.ceil(count / 20);
              
            }

            $scope.deleteJugador = function (idJugador) {
                panelControlService.deleteJugador(idJugador).then(function (response) {
                    $scope.init();
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getClubPrincipal = function (row) {
                row.clubPrincipal = $filter("filter")(row.tuser_club, { "bool_principal": true }, true)[0];
            }
            $scope.getEstadoPago = function (row) {
                row.estadoPago = undefined;
                row.estadoPago = $filter("filter")(row.thistorial_pagos, { "nro_resultado_pago": 1 }, true)[0];
            }
            $scope.getJugadores = function (page) {
                $scope.isLoading = true;
                panelControlService.getJugadores(page).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.jugadores = json.usersDTO;
                    $scope.pagesJugadores = json.pages;

                    //json.usersDTO[1].thistorial_pagos.forEach(function (x) { if (x.nro_resultado_pago == 1) { if (x.fec_vencimiento > Date) { alert('si'); } else { alert('no'); } } })
                    $scope.getPagination();
                    $scope.isLoading = false;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getTipos = function () {
                panelControlService.getTiposJugador().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.tiposJugador = json.tiposJugador;
                    
                    $scope.clubes = json.clubes;                  
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.paginate = function (page) {
                if (!$scope.isFiltered)
                    $scope.getJugadores(page);
                else
                    $scope.filtrarJugadores(page);
            }

            $scope.filtrarJugadores = function (page) {
                $scope.isLoading = true;
                var sendHandicap = 0;
                if (page > $scope.pages) {
                    page = $scope.pages;
                }
                if ($scope.filterOriginal == $scope.filter) {
                    $scope.getJugadores(page);
                } else { 
                    $scope.notEqual = true;
                    if ($scope.filter.nro_hand == undefined || $scope.filter.nro_hand == null || $scope.filter.nro_hand == '')
                        sendHandicap = -1;
                    else {
                        sendHandicap = angular.copy($scope.filter.nro_hand);
                    }

                    panelControlService.getJugadoresFiltrados($scope.page, $scope.filter.nombre_completo, $scope.filter.cod_tipo_jugador, $scope.filter.cod_club, sendHandicap).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.jugadores = json.usersDTO;
                        $scope.pagesJugadores = json.pages;
                        $scope.getPagination();
                        $scope.isFiltered = true;
                        $scope.page = page;
                        $scope.isLoading = false;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

            $scope.beforePage = function () {
                $scope.page = $scope.page - 1;
                $timeout(function () {
                    $scope.paginate($scope.page);
                }, 100)
            }

            $scope.nextPage = function () {
                $scope.page = $scope.page + 1; 
                $timeout(function () { 
                    $scope.paginate($scope.page);
                }, 100)
            }


            $scope.initLogin = function () {
                $scope.checkMobile();
                loginService.getUser().then(function (response) {
                    $scope.user = JSON.parse(response.data);
                    if ($scope.user == null || $scope.user == undefined) {
                        loginService.getJugadorAutenticado().then(function (response) {
                            $scope.user = JSON.parse(response.data);
                            if (($scope.user == null || $scope.user == undefined) && window.location.pathname != "/Home/Index" && !window.location.pathname.includes("/PerfilPublico/")) {
                                window.location.href = "/Home/Index";
                            } else {
                                $scope.user.txt_usuario = $scope.user.txt_nombre + " " + $scope.user.txt_apellido;
                            }
                        }).catch(function (error) {
                            console.log(error);
                        });
                    }
                    console.log($scope.user);
                }).catch(function (error) {
                    console.log(error);
                });

            }

            $scope.checkMobile = function () {
                var isMobile = false; //initiate as false
                $scope.isMobile = false;
                // device detection
                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
                    $(".no-mobile").remove();
                    $scope.isMobile = true;
                }
            };

            $scope.signOut = function () {
                loginService.signOut().then(function (response) {
                    location.reload();
                }).catch(function (error) {
                    console.log(error);
                });
            }

        }]);
