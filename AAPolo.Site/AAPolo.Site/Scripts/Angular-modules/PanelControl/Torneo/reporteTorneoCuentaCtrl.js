﻿var app = angular.module('reporteTorneoCuenta.ctrl', [])
    .controller('reporteTorneoCuentaCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {

            $scope.init = function () {
                $scope.getEquipos();
            }
            $scope.getEquipos = function () {
                $scope.isLoading = true;
                panelControlService.getReporteEquipos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.lstEquipos = json.lstEquipos;
                    $scope.cant_torneos = json.cant_torneos;
                    $scope.cant_equipos = json.cant_equipos;
                    $scope.cant_abonaron = json.cant_abonaron;
                    $scope.cant_no_abonaron = json.cant_no_abonaron;
                    $scope.isLoading = false;
                });
            }
            $scope.generarReporte = function () {
                $scope.isLoading = true;
                panelControlService.getExcelReporteTorneos().then(function (response) {
                    $scope.isLoading = false;
                });
            }
        }]);
