﻿var app = angular.module('crearOEditarTorneo.ctrl', [])
    .controller('crearOEditarTorneoCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        'Upload',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout, Upload) {
            $scope.torneo = {
                tpartido: []
            };
            $scope.saved = false;

            $scope.isLoading = false;

            $scope.init = function () {
                if ($routeParams.idTorneo != undefined) {
                    $scope.getTorneo();
                } else {

                    $timeout(function () {
                        $('.select2-basic').select2();
                        $('.datepicker').pickadate({
                            firstDay: 0,
                            container: 'body',
                            format: 'dd/mm/yyyy',
                            min: new Date(),
                            //selectYears: true,
                            selectMonths: true,
                            selectYears: 100
                        });
                    }, 1000);
                }

                $scope.getTiposTorneo();
                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',
                        format: 'dd/mm/yyyy',
                        min: new Date(),
                        //selectYears: true,
                        selectMonths: true,
                        selectYears: 100
                    });
                    $('.select2-basic').select2();
                    $('.select2-tags').select2(
                        {
                            tags: true
                        }
                    );
                }, 1000);
            }


            $scope.volverAlListado = function () {
                window.location.href = '/PanelControl/#/Torneos';
            }

            $scope.getTorneo = function () {
                $scope.isLoading = true;
                panelControlService.getTorneo($routeParams.idTorneo).then(function (response) {
                    $scope.isLoading = false;
                    var json = JSON.parse(response.data);
                    $scope.torneo = angular.copy(json);
                    $scope.torneo.bool_inscripcion_individual = "" + $scope.torneo.bool_inscripcion_individual;
                    $scope.torneo.bool_menores = "" + $scope.torneo.bool_menores;
                    $scope.torneo.bool_inscripcion = "" + $scope.torneo.bool_inscripcion; 
                    console.log($scope.torneo);
                    $scope.torneo.fec_comienzo = $filter('date')($scope.torneo.fec_comienzo, 'dd/MM/yyyy');
                    $scope.equipos = angular.copy($scope.torneo.tequipo);

                    angular.forEach($scope.torneo.tpartido, function (key) {
                        var formattedDate = new Date(key.fec_partido);
                        key.horario = formattedDate;
                    });


                    $timeout(function () {
                        $('.select2-basic').select2().trigger('change');

                        var formattedDate = moment($scope.torneo.fec_comienzo, 'DD/MM/YYYY').format('MM/DD/YYYY');
                        if (formattedDate == 'Invalid date')
                            formattedDate = moment($scope.torneo.fec_comienzo, 'YYYY-MM-DDTHH:mm:ss').format('MM/DD/YYYY');

                        var newDate = new Date(formattedDate);
                        $('.datepickerPartido').pickadate({
                            firstDay: 0,
                            container: 'body',
                            format: 'dd/mm/yyyy',
                            min: newDate,
                            //selectYears: true,
                            selectMonths: true,
                            selectYears: 100
                        });
                    }, 1000);
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getTiposTorneo = function () {
                panelControlService.getTorneoTipos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categoriasHandicap = json.categoriasHandicap;
                    $scope.categoriasTorneo = json.categoriasTorneo;
                    $scope.veedores = json.veedores;
                    $scope.referees = json.referees;
                    $scope.instancias = json.instancias;

                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.upload = function (file) {
                $scope.file = file;
            }

            $scope.getEquipos = function () {
                return angular.copy($scope.equipos);
            }

            $scope.guardarTorneo = function (form) {
                $scope.saved = true;
                $scope.submitted = true;
                if (form.$valid) {
                    angular.forEach($scope.torneo.tpartido, function (key) {
                        var formattedDate = moment(key.fec_partido, 'DD/MM/YYYY').format('MM/DD/YYYY');
                        if (formattedDate == 'Invalid date')
                            formattedDate = moment(key.fec_partido, 'YYYY-MM-DDTHH:mm:ss').format('MM/DD/YYYY');

                        formattedDate = formattedDate + " " + key.horario.getHours() + ":" + key.horario.getMinutes() + ":00";
                        var newDate = new Date(formattedDate);
                        key.fec_partido = newDate;
                    })
                    if ($routeParams.idTorneo == undefined) {
                        $scope.torneo.txt_imagen = "reemplazarEste";
                        $scope.torneo.usuario_alta = $(".adminLogin").text();
                        Upload.upload({
                            url: '/PanelControl/GuardarTorneo',
                            data: { torneoDTO: JSON.stringify($scope.torneo) },
                            file: $scope.file, // or list of files ($files) for html5 only
                            //}).progress(function (evt) {                       
                        }).success(function (response) {
                            //window.location.href = '/PanelControl/#/Torneos'
                            $scope.saved = true;
                            clickModal('confirmarExitoSalir');
                        }).error(function (err) {
                            $scope.saved = false;
                        });
                    } else {
                        if ($scope.file != undefined)
                            $scope.torneo.txt_imagen = "reemplazarEste";

                        Upload.upload({
                            url: '/PanelControl/EditarTorneo',
                            data: {
                                id: $routeParams.idTorneo,
                                torneoDTO: JSON.stringify($scope.torneo)
                            },
                            file: $scope.file, // or list of files ($files) for html5 only
                            //}).progress(function (evt) {                       
                        }).success(function (response) {
                            //window.location.href = '/PanelControl/#/Torneos'
                            $scope.saved = true;
                            clickModal('confirmarExitoSalir');
                            }).error(function (err) {
                                $scope.saved = false;
                        });
                    }
                } else {
                    $scope.saved = false;
                }
            }

            $scope.agregarPartido = function () {
                $scope.torneo.tpartido.push({ bool_activo: true });
                console.log(new Date($scope.torneo.fec_comienzo));
                $timeout(function () {
                    $('.select2-basic').select2();
                    var formattedDate = moment($scope.torneo.fec_comienzo, 'DD/MM/YYYY').format('MM/DD/YYYY');
                    if (formattedDate == 'Invalid date')
                        formattedDate = moment($scope.torneo.fec_comienzo, 'YYYY-MM-DDTHH:mm:ss').format('MM/DD/YYYY');
                    
                    var newDate = new Date(formattedDate);
                    $('.datepickerPartido').pickadate({
                        firstDay: 0,
                        container: 'body',
                        format: 'dd/mm/yyyy',
                        min: newDate,
                        //selectYears: true,
                        selectMonths: true,
                        selectYears: 100
                    });
                }, 500);
            }

            $scope.actualizarCalendar = function () {
                 $timeout(function () {
                    $('.select2-basic').select2();
                    var formattedDate = moment($scope.torneo.fec_comienzo, 'DD/MM/YYYY').format('MM/DD/YYYY');
                    if (formattedDate == 'Invalid date')
                        formattedDate = moment($scope.torneo.fec_comienzo, 'YYYY-MM-DDTHH:mm:ss').format('MM/DD/YYYY');
                    
                    var newDate = new Date(formattedDate);
                     $('.datepickerPartido').pickadate('picker').set('min', newDate);
                }, 500);
            }

            $scope.borrarPartido = function (index) {
                $scope.torneo.tpartido.splice(index, 1);
            }

        }]);
