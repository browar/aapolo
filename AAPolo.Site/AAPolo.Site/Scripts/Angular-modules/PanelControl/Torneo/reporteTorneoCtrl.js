﻿var app = angular.module('reporteTorneo.ctrl', [])
    .controller('reporteTorneoCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.torneo = {
                bool_inscripcion_individual : false
            };
            $scope.init = function () {
                $scope.getTorneo();
            }

            $scope.getTorneo = function () {
                $scope.isLoading = true;
                panelControlService.getTorneo($routeParams.idTorneo).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.torneo = json;

                    $scope.getEquipos();
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getEquipos = function () {
                $scope.isLoading = true;
                panelControlService.getReporteTorneos($routeParams.idTorneo).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.lstEquipos = json.lstEquipos;
                    $scope.cant_clubes = json.cant_clubes;
                    $scope.cant_equipos = json.cant_equipos;
                    $scope.cant_abonaron = json.cant_abonaron;
                    $scope.cant_no_abonaron = json.cant_no_abonaron;
                    $scope.DatosTorneo = json.Torneo;
                    $scope.isLoading = false;
                });
            }

            $scope.getReporteTorneosIndividual = function () {
                $scope.isLoading = true;
                panelControlService.getReporteTorneosIndividual($routeParams.idTorneo).then(function (response) {
                    $scope.isLoading = false;
                });
            }

            $scope.generarReporte = function () {
                $scope.isLoading = true;
                panelControlService.getExcelReporteTorneos().then(function (response) {
                    $scope.isLoading = false;
                });
            }
        }]);
