﻿var app = angular.module('torneo.ctrl', [])
    .controller('torneoCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.isLoading = false;
            $scope.init = function () {
                $scope.getTorneos();
            }

            $scope.getTorneos = function () {
                $scope.isLoading = true;
                panelControlService.getTorneos().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.torneos = json.torneosDTO;
                    $scope.isLoading = false;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.deleteTorneo = function (idTorneo) {
                panelControlService.deleteTorneo(idTorneo).then(function (response) {
                    $scope.init();
                }).catch(function (error) {
                    console.log(error);
                });
            }
         
            $scope.getPagination = function () {
                var count = $scope.torneos.length + 1;
                $scope.pages = new Array(Math.ceil((count / 10)));
            }

            $scope.paginate = function (page) {

            }
        }]);
