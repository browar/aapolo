﻿angular.module('PanelAdministrador', [
    'panelControl.service',
    'login.service',
    'ngRoute',
    'ngAnimate',
    'mgcrea.ngStrap',
    'ngSanitize',
    'ngMessages',
]).config([
    '$routeProvider',
    '$locationProvider',
    "$httpProvider",
    '$compileProvider',
    function ($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $locationProvider.hashPrefix('');
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);

        $routeProvider.when('/Jugador/:idJugador', {
            templateUrl: 'createJugadorList',
            controller: 'crearOEditarJugadorCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Jugador', {
            templateUrl: 'createJugadorList',
            controller: 'crearOEditarJugadorCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Jugadores', {
            templateUrl: 'jugadorList',
            controller: 'jugadorCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Jugadores/Reporte', {
            templateUrl: 'jugadorReport',
            controller: 'reporteJugadorCtrl', caseInsensitiveMatch: true 
        });

        $routeProvider.when('/Noticia/:id', {
            templateUrl: 'createNoticiaList',
            controller: 'crearOEditarNoticiaCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Noticia', {
            templateUrl: 'createNoticiaList',
            controller: 'crearOEditarNoticiaCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Noticias', {
            templateUrl: 'noticiaList',
            controller: 'noticiaCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Club/:idClub', {
            templateUrl: 'createClubList',
            controller: 'crearOEditarClubCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Club', {
            templateUrl: 'createClubList',
            controller: 'crearOEditarClubCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Clubes', {
            templateUrl: 'clubList',
            controller: 'clubCtrl', caseInsensitiveMatch: true
        });
        $routeProvider.when('/Clubes/Reporte', {
            templateUrl: 'clubReport',
            controller: 'reporteClubCtrl', caseInsensitiveMatch: true 
        });

        $routeProvider.when('/Torneo/:idTorneo', {
            templateUrl: 'createTorneoList',
            controller: 'crearOEditarTorneoCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Torneo', {
            templateUrl: 'createTorneoList',
            controller: 'crearOEditarTorneoCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Torneos', {
            templateUrl: 'torneosList',
            controller: 'torneoCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Torneos/Reporte/:idTorneo', {
            templateUrl: 'torneosReport',
            controller: 'reporteTorneoCtrl', caseInsensitiveMatch: true
        });
        $routeProvider.when('/Torneos/ReporteCuenta', {
            templateUrl: 'torneosReportCuenta',
            controller: 'reporteTorneoCuentaCtrl', caseInsensitiveMatch: true
        });
        $routeProvider.when('/Evento/:idEvento', {
            templateUrl: 'createEventoList',
            controller: 'crearOEditarEventoCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Evento', {
            templateUrl: 'createEventoList',
            controller: 'crearOEditarEventoCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Eventos', {
            templateUrl: 'eventoList',
            controller: 'eventoListCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Popup/:idPopup', {
            templateUrl: 'popUpCrearList',
            controller: 'crearOEditarPopupCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Popup', {
            templateUrl: 'popUpCrearList',
            controller: 'crearOEditarPopupCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Popups', {
            templateUrl: 'popUpList',
            controller: 'popupListCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Notificacion/:idNotificacion', {
            templateUrl: 'notificacionCrearList',
            controller: 'crearOEditarNotificacionCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Notificacion', {
            templateUrl: 'notificacionCrearList',
            controller: 'crearOEditarNotificacionCtrl', caseInsensitiveMatch: true
        });

        $routeProvider.when('/Notificaciones', {
            templateUrl: 'notificacionList',
            controller: 'notificacionListCtrl', caseInsensitiveMatch: true
        });


        $routeProvider.when('/Respuesta/:idRespuesta', {
            templateUrl: 'respuestaCrearList',
            controller: 'respuestaListCtrl', caseInsensitiveMatch: true
        });

        //$routeProvider.otherwise({
        //    redirectTo: '/'
        //});

        var regexIso8601 = /\/Date\((\d*)\)\//;

        $httpProvider.defaults.transformResponse.push(function (responseData) {
            convertDateStringsToDates(responseData);
            return responseData;
        });

        function convertDateStringsToDates(input) {
            // Ignore things that aren't objects.
            if (typeof input !== "object") return input;


            for (var key in input) {
                if (!input.hasOwnProperty(key)) continue;
                var value = input[key];
                var match;
                // Check for string properties which look like dates.
                // TODO: Improve this regex to better match ISO 8601 date strings.
                if (typeof value === "string" && (match = value.match(regexIso8601))) {
                    // Assume that Date.parse can parse ISO 8601 strings, or has been shimmed in older browsers to do so.   
                    var milliseconds = new Date(parseInt(match[1]));

                    if (!isNaN(milliseconds)) {

                        var d = new Date(milliseconds);
                        var day = d.getUTCDate().toString().length == 1 ? '0' + parseInt(d.getUTCDate()) : d.getUTCDate();
                        var month = d.getUTCMonth().toString().length == 1 ? '0' + parseInt(d.getUTCMonth() + 1) : d.getUTCMonth() + 1;
                        var year = d.getUTCFullYear();
                        var hours = d.getUTCHours();
                        var minutes = d.getUTCMinutes();
                        var result = d;
                        input[key] = result;
                    }
                } else if (typeof value === "object") {
                    // Recurse into object
                    convertDateStringsToDates(value);
                }
            }
        };

    }
]).run(['$rootScope', function ($rootScope) {
    $rootScope.$on('$routeChangeSuccess', function () {
        $('body, html').removeClass('overflow-hidden');
        setTimeout(pageSetUp, 200);
    });
}]);


angular.module("PanelControl", ['Jugador', 'PanelAdministrador', 'Club', "Torneo", "Noticia", "Evento", "Popup", "Notificacion"]);

var Login = document.getElementById('Login');

angular.element(document).ready(function () {
    angular.bootstrap(Login, ['Login']);
});