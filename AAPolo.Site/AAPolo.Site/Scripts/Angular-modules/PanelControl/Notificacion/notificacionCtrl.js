﻿var app = angular.module('notificacionList.ctrl', [])
    .controller('notificacionListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.notificacionesPendientes = [];
            $scope.notificacionesEmitidas = [];
            $scope.init = function () {
                $scope.getTipos();
            }

            $scope.getTipos = function () {
                $scope.isLoading = true;
                panelControlService.getTiposNotificacion().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categorias = json.categorias;
                    $scope.tipoJugadores = $filter('filter')(json.tiposJugadores, { "txt_desc": "!" + "Fallecido" }, true);
                    $scope.roles = json.roles;
                    console.log($scope.tipoJugadores);
                    $scope.getNotificaciones();
                }).catch(function (error) {
                    console.log(error);
                });
            }


            $scope.getRespuestas = function () {
                panelControlService.getRespuestasALaAAP().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.respuestas = json;

                    angular.forEach($scope.respuestas, function (key) {
                        panelControlService.getJugador(key.cod_usuario_envia).then(function (response) {
                            var json = JSON.parse(response.data);
                            key.usuarioEnvio = json.txt_nombre + " " + json.txt_apellido ;
                        }).catch(function (error) {
                            console.log(error);
                            });
                        key.txt_asunto = $filter('filter')($scope.notificaciones, { "cod_notificacion": key.cod_notificacion }, true)[0].txt_asunto;
                    });
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getNotificaciones = function () {
                panelControlService.getNotificaciones().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.notificaciones = json;
                    var newDate = new Date();
                    angular.forEach(json, function (key) {
                        key.txt_tipos_jugadores = "";
                        key.txt_roles = "";
                        var dateParse = new Date(key.fec_enviado);
                        if (dateParse >= newDate) {
                            $scope.notificacionesPendientes.push(key);
                        } else {
                            $scope.notificacionesEmitidas.push(key);
                        }

                        if (key.tnotificacion_rol.length == $scope.roles.length) {
                            key.txt_roles = "Todos los roles. ";
                        } else {
                            angular.forEach(key.tnotificacion_rol, function (keyRol, valueRol) {
                                var rolText = $filter("filter")($scope.roles, { "cod_rol": keyRol.cod_rol }, true)[0].txt_desc;
                                if (!key.txt_roles.includes(rolText) && rolText != undefined) {
                                    if ((valueRol + 1) == key.tnotificacion_rol.length)
                                        key.txt_roles = "" + key.txt_roles + rolText + " ";
                                    else
                                        key.txt_roles = "" + key.txt_roles + rolText + " - ";
                                }
                            });
                        }
                        if (key.tnotificacion_tipo_jugador.length >= 5) {
                            if ((key.tnotificacion_tipo_jugador.length / 2) == $scope.tipoJugadores.length)
                                key.txt_tipos_jugadores = "Todos los tipos de jugadores. "
                            else {
                                angular.forEach(key.tnotificacion_tipo_jugador, function (keyTipo, valueTipo) {
                                    var tipoJugadorText = $filter("filter")($scope.tipoJugadores, { "cod_tipo_jugador": keyTipo.cod_tipo_jugador }, true)[0].txt_desc;
                                    if (!key.txt_tipos_jugadores.includes(tipoJugadorText) && tipoJugadorText != undefined) {
                                        if ((valueTipo + 1) == key.tnotificacion_tipo_jugador.length)
                                            key.txt_tipos_jugadores = "" + key.txt_tipos_jugadores + tipoJugadorText + " ";
                                        else
                                            key.txt_tipos_jugadores = "" + key.txt_tipos_jugadores + tipoJugadorText + " - ";
                                    }
                                });
                            }
                        } else {
                            if ((key.tnotificacion_tipo_jugador.length) == $scope.tipoJugadores.length)
                                key.txt_tipos_jugadores = "Todos los tipos de jugadores. "
                            else {
                                angular.forEach(key.tnotificacion_tipo_jugador, function (keyTipo, valueTipo) {
                                    var tipoJugadorText = $filter("filter")($scope.tipoJugadores, { "cod_tipo_jugador": keyTipo.cod_tipo_jugador }, true)[0].txt_desc;
                                    if (!key.txt_tipos_jugadores.includes(tipoJugadorText) && tipoJugadorText != undefined) {
                                        if ((valueTipo + 1) == key.tnotificacion_tipo_jugador.length)
                                            key.txt_tipos_jugadores = "" + key.txt_tipos_jugadores + tipoJugadorText + " ";
                                        else
                                            key.txt_tipos_jugadores = "" + key.txt_tipos_jugadores + tipoJugadorText + " - ";
                                    }
                                });
                            }
                        }

                        $scope.getRespuestas();

                    });

                    $scope.isLoading = false;
                }).catch(function (error) {
                    console.log(error);
                });
            }


        }]);
