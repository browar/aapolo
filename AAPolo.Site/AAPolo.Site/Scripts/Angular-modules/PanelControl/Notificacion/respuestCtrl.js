﻿var app = angular.module('respuestaList.ctrl', [])
    .controller('respuestaListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        'Upload',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout, Upload) {

            $scope.init = function () {
                $scope.getRespuesta();
            }

            $scope.getRespuesta = function () {
                $scope.isLoading = true;
                panelControlService.getRespuestaPorId($routeParams.idRespuesta).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.respuesta = json;
                    console.log($scope.respuesta);
                    panelControlService.getJugador($scope.respuesta.cod_usuario_envia).then(function (responseUser) {
                        var jsonUser = JSON.parse(responseUser.data);
                        $scope.respuesta.usuarioEnvio = jsonUser.txt_nombre + " " + jsonUser.txt_apellido;
                        $scope.isLoading = false;
                    }).catch(function (error) {
                        console.log(error);
                    });
                    panelControlService.getNotificacionById($scope.respuesta.cod_notificacion).then(function (responseNotif) {
                        var jsonNotif = JSON.parse(responseNotif.data);
                        $scope.respuesta.txt_asunto = jsonNotif.txt_asunto;
                    }).catch(function (error) {
                        console.log(error);
                    });
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.sendRespuesta = function () {
                $scope.nuevaRespuesta.cod_notificacion = $scope.respuesta.cod_notificacion;
                $scope.nuevaRespuesta.cod_usuario_recibe = $scope.respuesta.cod_usuario_envia;
                $scope.nuevaRespuesta.cod_usuario_envia = null;
                $scope.nuevaRespuesta.trespuesta_adjunto = [];
                $scope.nuevaRespuesta.trespuesta_adjunto.push({ txt_url_image: "reemplazarEsteAdjunto" });
                Upload.upload({
                    url: '/PerfilJugador/EnviarRespuesta',
                    data: { respuesta: JSON.stringify($scope.nuevaRespuesta) },
                    file: $scope.fileComprobante, // or list of files ($files) for html5 only
                    //}).progress(function (evt) {                       
                }).success(function (response) {
                    $scope.respuesta = {};
                    clickModal("respuesta-exito");
                }).error(function (err) {
                });
            }

            $scope.uploadNotificacion = function (file) {
                $scope.fileComprobante = file;
            }


        }]);
