﻿var app = angular.module('crearOEditarNotificacion.ctrl', [])
    .controller('crearOEditarNotificacionCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {
            $scope.isLoading = false;
            $scope.notificacion = {
                tipos: [],
                roles: [],
                categorias: [],
                tnotificacion_rol: [],
                tnotificacion_tipo_jugador: []
            };

            $scope.init = function () {
                $('.datepicker').pickadate({
                    firstDay: 0,
                    container: 'body',
                    format: 'dd/mm/yyyy',
                    //selectYears: true,
                    min: new Date(),
                    selectMonths: true,
                    selectYears: 100
                });
                $scope.getTipos();
                if ($routeParams.idNotificacion != undefined) {
                    $scope.getNotification();
                }
            }

            $scope.getNotification = function () {
                $scope.isLoading = true;
                panelControlService.getNotificacionById($routeParams.idNotificacion).then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.notificacion = json;
                    $scope.notificacion.tipos = [];
                    $scope.notificacion.roles = [];
                    $scope.notificacion.categorias = [];

                    angular.forEach($scope.notificacion.tnotificacion_rol, function (keyRol, valueRol) {
                        $scope.notificacion.roles.push(keyRol.cod_rol);
                    });
                    angular.forEach($scope.notificacion.tnotificacion_tipo_jugador, function (keyTipo, valueTipo) {
                        $scope.notificacion.tipos.push(keyTipo.cod_tipo_jugador);
                        var existeCategoria = $filter('filter')($scope.notificacion.categorias, { "cod_categoria_handicap": keyTipo.cod_categoria_handicap })[0];
                        if (existeCategoria == undefined) {
                            $scope.notificacion.categorias.push(keyTipo.cod_categoria_handicap);
                        }
                    });

                    $timeout(function () {
                        $('.select2-basic').select2();
                        $('.select2-tags').select2(
                            {
                                tags: true
                            }
                        );
                        $scope.isLoading = false;
                    }, 1000);
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.getTipos = function () {
                panelControlService.getTiposNotificacion().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.categorias = json.categorias;
                    $scope.tipoJugadores = json.tiposJugadores;
                    $scope.roles = json.roles;
                    $timeout(function () {
                        $('.select2-basic').select2();
                        $('.select2-tags').select2(
                            {
                                tags: true
                            }
                        );
                    }, 1000);
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.populateNotificaciones = function () {
                if ($scope.notificacion.tipos.length == 0) {
                    angular.forEach($scope.tipoJugadores, function (key, value) {
                        if (key.cod_tipo_jugador != 6)
                            $scope.notificacion.tipos.push(key.cod_tipo_jugador);
                    });
                }
                angular.forEach($scope.notificacion.tipos, function (key, value) {
                    if ($scope.notificacion.categorias.length > 1 || $scope.notificacion.categorias.length == 0) {
                        $scope.notificacion.tnotificacion_tipo_jugador.push({
                            cod_tipo_jugador: key,
                            cod_categoria_handicap: $scope.categorias[0].cod_categoria_handicap
                        });
                        $scope.notificacion.tnotificacion_tipo_jugador.push({
                            cod_tipo_jugador: key,
                            cod_categoria_handicap: $scope.categorias[1].cod_categoria_handicap
                        });
                    } else {
                        $scope.notificacion.tnotificacion_tipo_jugador.push({
                            cod_tipo_jugador: key,
                            cod_categoria_handicap: $scope.categorias[0]
                        });
                    }
                });

                if ($scope.notificacion.roles.length == 0) {
                    angular.forEach($scope.roles, function (key, value) {
                        $scope.notificacion.roles.push(key.cod_rol);
                    });
                }

                angular.forEach($scope.notificacion.roles, function (key, value) {
                    $scope.notificacion.tnotificacion_rol.push({
                        cod_rol: key
                    });
                });
                $scope.notificacion.cod_tipo_notificacion = 1;
                $scope.notificacion.txt_alta = localStorage.getItem("displayName");
            }

            $scope.saveNotificacion = function () {
                $scope.populateNotificaciones();
                $timeout(function () {
                    if ($scope.notificacion.cod_tipo_notificacion == undefined) {
                        panelControlService.saveNotificacion(JSON.stringify($scope.notificacion)).then(function (response) {
                            clickModal("exito");
                        }).catch(function (error) {
                            console.log(error);
                        });
                    } else {
                        panelControlService.editNotificacion($routeParams.idNotificacion, JSON.stringify($scope.notificacion)).then(function (response) {
                            clickModal("exito");
                        }).catch(function (error) {
                            console.log(error);
                        });
                    }

                }, 1000);
            }

        }]);
