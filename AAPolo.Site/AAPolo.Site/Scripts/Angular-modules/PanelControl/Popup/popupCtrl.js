﻿var app = angular.module('popupList.ctrl', [])
    .controller('popupListCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout) {

            $scope.isLoading = false;
            $scope.init = function () {
                $scope.getPopups();                
            }

            $scope.getPopups = function () {
                $scope.isLoading = true;
                panelControlService.getPopups().then(function (response) {
                    var json = JSON.parse(response.data);
                    $scope.comunicados = json;
                    $scope.isLoading = false;
                }).catch(function (error) {
                    console.log(error);
                });
            }

            $scope.activarDesactivar = function (cod_popup) {
                panelControlService.cambiarActividadPopup(cod_popup).then(function (response) {
                    $scope.init();
                }).catch(function (error) {
                    console.log(error);
                });
            }

        }]);
