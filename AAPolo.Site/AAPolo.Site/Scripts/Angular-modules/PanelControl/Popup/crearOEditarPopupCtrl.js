﻿var app = angular.module('crearOEditarPopup.ctrl', [])
    .controller('crearOEditarPopupCtrl', [
        '$scope',
        '$filter',
        '$routeParams',
        '$location',
        'panelControlService',
        '$timeout',
        'Upload',
        function ($scope, $filter, $routeParams, $location, panelControlService, $timeout, Upload) {
            $scope.isLoading = false;
            $scope.comunicado = {};
            $scope.init = function () {
                if ($routeParams.idPopup != "undefined")
                    $scope.getPopup($routeParams.idPopup);

                $('.datepicker').pickadate({
                    firstDay: 0,
                    container: 'body',
                    format: 'dd/mm/yyyy',
                    //selectYears: true,
                    min: new Date(),
                    selectMonths: true,
                    selectYears: 100
                });

                $timeout(function () {
                    $('.datepicker').pickadate({
                        firstDay: 0,
                        container: 'body',
                        format: 'dd/mm/yyyy',
                        //selectYears: true,
                        min: false,
                        selectMonths: true,
                        selectYears: 100
                    });
                }, 700);
            }


            $scope.volverAlListado = function () {
                window.location.href = '/PanelControl/#/Popup';
            }

            $scope.upload = function (file, id) {
                switch (id) {
                    case 1:
                        $scope.fileImagen = file;
                        break;
                    case 2:
                        $scope.fileSlideshow = file;
                        break;
                    default:
                        $scope.fileVideo = file;
                        break;
                }
            }

            $scope.populatePopup = function () {
                var misEtiquetas = [];
                var archivos = [];
                $scope.files = [];
                $scope.archivosEditados = [];
                if ($scope.fileImagen != undefined) {
                    $scope.comunicado.txt_imagen = "reemplazarEstePrincipal";
                    $scope.comunicado.txt_video = "reemplazarEsteSlideshow";
                    $scope.files.push($scope.fileImagen);
                    $scope.tipo = 0;
                } else {
                    $scope.files.push({});
                }
                if ($scope.fileVideo != undefined) {
                    $scope.comunicado.txt_imagen = "reemplazarEstePrincipal";
                    $scope.comunicado.txt_video = "reemplazarEsteSlideshow";
                    $scope.tipo = 1;
                    $scope.files.push($scope.fileVideo);
                } else {
                    $scope.files.push([]);
                }
            }

            $scope.crearPopup = function (form) {
                $scope.populatePopup();
                $scope.isLoading = true;
                if ($scope.tipo == undefined)
                    $scope.tipo = 4;
                if ($scope.comunicado.cod_popup == undefined || $scope.comunicado.cod_popup == 0) {
                    $scope.comunicado.txt_imagen = "reemplazarEstePrincipal";
                    $scope.comunicado.txt_video = "reemplazarEsteSlideshow";
                    Upload.upload({
                        url: '/PanelControl/GuardarPopUp',
                        data: {
                            popupDTO: JSON.stringify($scope.comunicado),
                            tipo: $scope.tipo
                        },
                        file: $scope.files,
                    }).success(function (response) {
                        $scope.isLoading = false;
                        //window.location.href = '/PanelControl/#/Eventos'
                        clickModal('exito');
                        $scope.saved = true;
                    }).error(function (err) {
                        $scope.isLoading = false;
                    });
                } else {
                    Upload.upload({
                        url: '/PanelControl/EditarPopupUpload',
                        data: {
                            id: $routeParams.idPopup,
                            popupDTO: JSON.stringify($scope.comunicado),
                            tipo: $scope.tipo
                        },
                        file: $scope.files,
                    }).success(function (response) {
                        clickModal('exito');
                        $scope.isLoading = false;
                    }).error(function (err) {
                        $scope.isLoading = false;
                    });
                }
            }

            $scope.getPopup = function () {
                if ($routeParams.idPopup != undefined) {
                    $scope.isLoading = true;
                    panelControlService.getPopup(parseInt($routeParams.idPopup)).then(function (response) {
                        var json = JSON.parse(response.data);
                        $scope.isLoading = false;
                        $scope.comunicado = json;
                        if ($scope.comunicado.txt_video != 'reemplazarEsteSlideshow') {
                            $scope.comunicado.video = $scope.comunicado.txt_video;
                            $scope.select = "Video";
                        }
                        if ($scope.comunicado.txt_imagen != 'reemplazarEstePrincipal') {
                            $scope.comunicado.imagen = $scope.comunicado.txt_imagen;
                            $scope.select = "Imagen";
                        }
                        $scope.comunicado.fec_vigencia = $filter('date')($scope.comunicado.fec_vigencia, "dd/MM/yyyy");

                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }

        }]);
